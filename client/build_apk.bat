rem set LOCAL_PATH=%cd%
rem set API_TEMP=%LOCAL_PATH%\lib\configs\api.dart.temp
rem 
rem if exist %API_TEMP% (
rem 	del %API_TEMP%
rem )
rem 
rem @echo off
rem setlocal EnableDelayedExpansion
rem set "API_Path=%LOCAL_PATH%\lib\configs\api.dart"
rem (for /f "tokens=*" %%i in (%API_Path%) do (
rem 	set s=%%i
rem 	set s=!s:true=false!
rem 	echo !s!
rem ))>"%LOCAL_PATH%\lib\configs\api.dart.temp"
rem 
rem del %API_Path%
rem move %API_TEMP% %API_Path%

fvm flutter build apk --target-platform android-arm --no-sound-null-safety