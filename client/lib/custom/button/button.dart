// ignore_for_file: must_be_immutable

import 'package:fim_app/package/util/screen_util.dart';
import 'package:flutter/material.dart';

class ButtonEx extends StatelessWidget {
  ///按钮颜色
  Color? color;

  ///禁用状态颜色
  Color? disableColor;

  ///圆角
  double? borderCircular;
  BorderRadius? borderRadius;

  ///轮廓
  ShapeBorder? shape;

  ///内边距
  EdgeInsetsGeometry? padding;

  ///编辑
  EdgeInsetsGeometry? margin;

  ///子部件
  Widget? child;

  ///子部件对齐
  Alignment childAlign;

  ///是否启用
  bool isEnable;

  ///点击
  VoidCallback? onPressed;

  ///双击
  VoidCallback? onDoubleTap;

  ///长按
  VoidCallback? onLongPress;

  ///抬起
  void Function(TapDownDetails)? onTapDown;

  ///取消
  VoidCallback? onTapCancel;

  ///焦点
  void Function(bool)? onFocusChange;

  ButtonEx({
    Key? key,
    this.child,
    this.childAlign = Alignment.center,
    this.margin,
    this.padding,
    this.color,
    this.disableColor,
    this.borderCircular,
    this.borderRadius,
    this.shape,
    this.isEnable = true,
    this.onPressed,
    this.onDoubleTap,
    this.onLongPress,
    this.onTapDown,
    this.onTapCancel,
    this.onFocusChange,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BorderRadius _borderRadius = borderRadius ?? BorderRadius.all(Radius.circular(borderCircular ?? 10.sp));
    Color? backColor = isEnable ? color : disableColor ?? color;
    Widget widgetChild = padding != null
        ? Padding(
            padding: padding!,
            child: Align(
              alignment: childAlign,
              widthFactor: 1.0,
              heightFactor: 1.0,
              child: child,
            ),
          )
        : Align(
            alignment: childAlign,
            widthFactor: 1.0,
            heightFactor: 1.0,
            child: child,
          );
    return Padding(
      padding: margin ?? const EdgeInsets.all(0),
      child: Material(
        color: backColor,
        borderRadius: shape == null ? _borderRadius : null,
        shape: shape,
        type: backColor == null ? MaterialType.transparency : MaterialType.button,
        clipBehavior: Clip.antiAlias,
        child: InkWell(
          borderRadius: _borderRadius,
          child: widgetChild,
          onTap: isEnable ? onPressed : null,
          onLongPress: isEnable ? onLongPress : null,
          onTapDown: isEnable ? onTapDown : null,
          onTapCancel: isEnable ? onTapCancel : null,
          onFocusChange: isEnable ? onFocusChange : null,
          onDoubleTap: isEnable ? onDoubleTap : null,
        ),
      ),
    );
  }
}

class Button extends ButtonEx {
  Button({
    Key? key,

    ///按钮颜色
    Color? color,

    ///禁用状态颜色
    Color? disableColor,

    ///圆角
    double? borderCircular,
    BorderRadius? borderRadius,

    ///轮廓
    ShapeBorder? shape,

    ///内边距
    EdgeInsetsGeometry? padding,

    ///编辑
    EdgeInsetsGeometry? margin,

    ///子部件
    Widget? child,

    ///子部件对齐
    Alignment childAlign = Alignment.center,

    ///是否启用
    bool isEnable = true,

    ///点击
    VoidCallback? onPressed,

    ///双击
    VoidCallback? onDoubleTap,

    ///长按
    VoidCallback? onLongPress,

    ///抬起
    void Function(TapDownDetails)? onTapDown,

    ///取消
    VoidCallback? onTapCancel,

    ///焦点
    void Function(bool)? onFocusChange,
  }) : super(
          key: key,
          child: child,
          childAlign: childAlign,
          margin: margin,
          padding: padding,
          color: color,
          disableColor: disableColor,
          borderCircular: borderCircular,
          borderRadius: borderRadius,
          shape: shape,
          isEnable: isEnable,
          onPressed: onPressed,
          onDoubleTap: onDoubleTap,
          onLongPress: onLongPress,
          onTapDown: onTapDown,
          onTapCancel: onTapCancel,
          onFocusChange: onFocusChange,
        );

  @override
  Widget build(BuildContext context) {
    padding = padding ?? EdgeInsets.symmetric(horizontal: 30.sp, vertical: 15.sp);
    return super.build(context);
  }
}
