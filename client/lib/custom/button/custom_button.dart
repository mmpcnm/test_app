import 'package:fim_app/custom/button/button.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class CustomButtonConttroller extends Model {
  CustomButtonConttroller({bool isEnable = false}) {
    _isEnable = isEnable;
  }
  bool _isEnable = false;
  void set isEnable(bool value) {
    _isEnable = value;
    notifyListeners();
  }

  bool get isEnable => _isEnable;
}

class CustomButton extends StatefulWidget {
  ///按钮颜色
  Color? color;

  ///禁用状态颜色
  Color? disableColor;

  ///圆角
  double? borderCircular;

  ///内边距
  EdgeInsetsGeometry? padding;

  ///编辑
  EdgeInsetsGeometry? margin;

  ///子部件
  Widget? child;

  ///子部件对齐
  Alignment childAlign;

  ///是否启用
  bool isEnable;

  ///点击
  VoidCallback? onPressed;

  ///双击
  VoidCallback? onDoubleTap;

  ///长按
  VoidCallback? onLongPress;

  ///抬起
  void Function(TapDownDetails)? onTapDown;

  ///取消
  VoidCallback? onTapCancel;

  ///焦点
  void Function(bool)? onFocusChange;

  late CustomButtonConttroller mConttroller;

  CustomButton({
    Key? key,
    this.child,
    this.childAlign = Alignment.center,
    this.margin,
    this.padding,
    this.color,
    this.disableColor,
    this.borderCircular,
    this.isEnable = true,
    this.onPressed,
    this.onDoubleTap,
    this.onLongPress,
    this.onTapDown,
    this.onTapCancel,
    this.onFocusChange,
    CustomButtonConttroller? conttroller,
  }) : super(key: key) {
    if (conttroller == null) {
      mConttroller = CustomButtonConttroller(isEnable: isEnable);
    } else {
      mConttroller = conttroller;
      mConttroller._isEnable = isEnable;
    }
  }
  @override
  State<StatefulWidget> createState() => StateCustomButton();
}

class StateCustomButton extends State<CustomButton> {
  @override
  Widget build(BuildContext context) {
    return ScopedModel<CustomButtonConttroller>(
      model: widget.mConttroller,
      child: ScopedModelDescendant<CustomButtonConttroller>(
        builder: (context, child, model) {
          return Button(
            child: widget.child,
            childAlign: widget.childAlign,
            margin: widget.margin,
            padding: widget.padding,
            color: widget.color,
            disableColor: widget.disableColor,
            borderCircular: widget.borderCircular,
            isEnable: widget.mConttroller.isEnable,
            onPressed: widget.onPressed,
            onDoubleTap: widget.onDoubleTap,
            onLongPress: widget.onLongPress,
            onTapDown: widget.onTapDown,
            onTapCancel: widget.onTapCancel,
            onFocusChange: widget.onFocusChange,
          );
        },
      ),
    );
  }
}
