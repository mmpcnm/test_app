// Copyright 2019 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// ignore_for_file: avoid_print

import 'dart:math';

import 'package:flutter/foundation.dart';

import 'item_positions_listener.dart';

/// Internal implementation of [ItemPositionsListener].
class ItemPositionsNotifier implements ItemPositionsListener {
  @override
  final ValueNotifier<Iterable<ItemPosition>> itemPositions = ValueNotifier([]);

  @override
  double getAlignment(int index) {
    var positions = itemPositions.value.toList();
    int miniIndex = -1;
    int maxIndex = -1;
    double oneAlignment = 0.8;
    // double oneAlignment = 1;
    // for (var i = 0; i < positions.length; i++) {
    //   var position = positions[i];
    //   print('---------------------index:$i itemLeadingEdge:${position.itemLeadingEdge}, itemTrailingEdge:${position.itemTrailingEdge}');
    // }
    for (var i = 0; i < positions.length; i++) {
      var position = positions[i];
      if (position.itemLeadingEdge >= 0 && position.itemTrailingEdge >= 0) {
        miniIndex = miniIndex == -1 ? i : min(i, miniIndex);
        maxIndex = maxIndex == -1 ? i : max(i, maxIndex);
      }

      if (position.index == index) {
        if (position.itemLeadingEdge < 0) {
          print(
              '---------------------1--- index:$index Alignment:0, itemLeadingEdge:${position.itemLeadingEdge}, itemTrailingEdge:${position.itemTrailingEdge}');
          return 0;
        }
        if (position.itemTrailingEdge < 0) {
          print(
              '---------------------2--- index:$index Alignment:1, itemLeadingEdge:${position.itemLeadingEdge}, itemTrailingEdge:${position.itemTrailingEdge}');
          return oneAlignment;
        }
        print(
            '---------------------3--- index:$index Alignment:-1, itemLeadingEdge:${position.itemLeadingEdge}, itemTrailingEdge:${position.itemTrailingEdge}');
        return -1;
      }
    }

    if (index <= miniIndex) {
      print('---------------------4--- index:$index Alignment:0, miniIndex:$miniIndex');
      return 0;
    }

    if (index > maxIndex) {
      print('---------------------5--- index:$index Alignment:1 maxIndex:$maxIndex');
      return oneAlignment;
    }
    print('---------------------6--- index:$index Alignment:-1');
    return -1;
  }
}
