import 'dart:async';

import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:flutter/material.dart';

class ClockText extends StatefulWidget {
  ///毫秒
  int millisecond;
  bool isReverse;
  bool isRunting;
  bool isEndReset;
  TextStyle? style;
  ClockTextControll? controll;
  VoidCallback? onAnimationEnd;

  ClockText({
    required this.millisecond,
    this.isReverse = false,
    this.isRunting = false,
    this.isEndReset = true,
    this.style,
    this.controll,
    this.onAnimationEnd,
  });

  @override
  State<StatefulWidget> createState() {
    StateClockText stateClockText = StateClockText();
    if (controll != null) {
      controll!._stateClockText = stateClockText;
    }
    return stateClockText;
  }
}

class StateClockText extends State<ClockText> {
  ClockTextState _mState = ClockTextState.UNKNOWN;
  int _currtenMillisecond = 0;
  int _startMillisecond = 0;
  Timer? _mTimer;

  @override
  void initState() {
    super.initState();
    if (widget.isRunting) {
      _mState = ClockTextState.RUNTING;
      _startMillisecond = DateTime.now().millisecondsSinceEpoch;
      _currtenMillisecond = 0;
      _createTimer();
    }
  }

  @override
  void dispose() {
    _disposeTimer();
    super.dispose();
  }

  bool get isRunting => _mState == ClockTextState.RUNTING;

  void start() {
    if (_mState == ClockTextState.RUNTING) return;

    _mState = ClockTextState.RUNTING;
    _startMillisecond = DateTime.now().millisecondsSinceEpoch;
    _currtenMillisecond = 0;
    _createTimer();
    _reSetState();
  }

  void restart() {
    _mState = ClockTextState.UNKNOWN;
    start();
  }

  void stop({bool isReset = false}) {
    _disposeTimer();
    _mState = ClockTextState.UNKNOWN;

    if (isReset) _currtenMillisecond = 0;
    _reSetState();
  }

  void paused() {
    Future.delayed(Duration(milliseconds: 1), () {});
    _mState = ClockTextState.PAUSED;
    _disposeTimer();
    _reSetState();
  }

  void recover() {
    if (_mState == ClockTextState.PAUSED) {
      _mState = ClockTextState.RUNTING;
      _startMillisecond = DateTime.now().millisecondsSinceEpoch;
      _createTimer();
    }
  }

  void _createTimer() {
    if (_mTimer == null) {
      _mTimer = Timer.periodic(Duration(milliseconds: 100), (timer) {
        updateTime();
      });
    }
  }

  void _disposeTimer() {
    if (_mTimer != null) {
      _mTimer!.cancel();
      _mTimer = null;
    }
  }

  void _reSetState() {
    Future.delayed(Duration(milliseconds: 1), () {
      setState(() {});
    });
  }

  void updateTime() {
    if (_mState != ClockTextState.RUNTING) return;
    setState(() {
      int endMillisecond = DateTime.now().millisecondsSinceEpoch;
      int dex = endMillisecond - _startMillisecond;
      _startMillisecond = endMillisecond;
      _currtenMillisecond += dex;
      if (_currtenMillisecond > widget.millisecond) {
        _currtenMillisecond = widget.millisecond;
        _mState = ClockTextState.UNKNOWN;
        _disposeTimer();
        if (widget.isEndReset) _currtenMillisecond = 0;
        if (widget.onAnimationEnd != null) widget.onAnimationEnd!.call();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (widget.controll != null) widget.controll!._stateClockText = this;
    int millisecond = _currtenMillisecond;
    if (widget.isReverse) {
      millisecond = widget.millisecond - millisecond;
    }

    if (millisecond <= 0) {
      //秒
      return Text('0${STR(10172)}', style: widget.style);
    } else {
      int second = (millisecond / 1000 % 60).toInt();
      int minute = (millisecond / 1000 / 60 % 60).toInt();
      int hour = (millisecond / 1000 / 60 / 60 % 60).toInt();

      String timeStr = '';
      //小时
      if (hour > 0) timeStr = '$timeStr$hour${STR(10174)}';
      //分钟
      if (minute > 0) timeStr = '$timeStr$minute${STR(10173)}';
      if (hour == 0 && minute == 0 && second == 0)
        timeStr = '$timeStr$second${STR(10172)}';
      else if (second > 0) timeStr = '$timeStr$second${STR(10172)}';

      // print('ClockText: $timeStr');
      return Text(timeStr, style: widget.style);
    }
  }
}

enum ClockTextState {
  UNKNOWN,
  RUNTING,
  PAUSED,
}

class ClockTextControll {
  // GlobalKey<StateClockText> _key = GlobalKey<StateClockText>();
  StateClockText? _stateClockText;

  bool get isRunting => _stateClockText != null ? _stateClockText!.isRunting : false;

  void start() {
    if (_stateClockText != null) {
      _stateClockText!.start();
    }
  }

  void stop({bool isReset = false}) {
    if (_stateClockText != null) {
      _stateClockText!.stop(isReset: isReset);
    }
  }

  void paused() {
    if (_stateClockText != null) {
      _stateClockText!.paused();
    }
  }

  void recover() {
    if (_stateClockText != null) {
      _stateClockText!.recover();
    }
  }
}
