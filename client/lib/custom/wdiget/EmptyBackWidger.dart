import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:flutter/material.dart';

class EmptyBackWidget extends StatelessWidget {
  String image;
  String text;
  double? imageWidth;
  double? imageHeight;
  EmptyBackWidget({required this.image, required this.text, this.imageWidth, this.imageHeight});
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          ConstrainedBox(
            constraints: BoxConstraints(
              maxWidth: imageWidth ?? 660.sp * 0.5,
              maxHeight: imageHeight ?? 356.sp * 0.5,
            ),
            child: Image(
              image: AssetImage(image),
              fit: BoxFit.contain,
            ),
          ),
          SizedBox(height: 15.sp),
          Text(text, style: TextStyle(fontSize: defFontSize, color: grayColor)),
        ],
      ),
    );
  }
}
