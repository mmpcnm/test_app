// ignore_for_file: must_be_immutable

import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/custom/image/CustomImage.dart' as custom;
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/material.dart';

class UserIconWidget extends StatefulWidget {
  final double? size;
  final double? circular;
  final bool frame;
  final Color? frameColor;
  final void Function(Int64 userId)? onTap;
  final void Function(Int64 userId)? onLongPress;
  final String imageUrl;
  final bool isNetworkIamge;
  final Int64 userId;
  const UserIconWidget(
    this.imageUrl, {
    Key? key,
    this.userId = Int64.ZERO,
    this.size,
    this.circular,
    this.frame = false,
    this.frameColor,
    this.isNetworkIamge = true,
    this.onTap,
    this.onLongPress,
  }) : super(key: key);
  @override
  State<StatefulWidget> createState() => _UserIconWidget();

  static Widget createUserIconImage(String imageUrl, {double? size, bool isNetworkIamge = true}) {
    double iconSize = size ?? defAvatarSize;
    if (imageUrl.isEmpty) {
      return custom.CustomImage(
        image: AssetImage(ResImgs("default_head_icon")),
        width: iconSize,
        height: iconSize,
      );
    } else if (isNetworkIamge) {
      return custom.CustomImage.network(
        imageUrl,
        width: iconSize,
        height: iconSize,
        loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent? loadingProgress) {
          if (loadingProgress == null) {
            return child;
          }
          return Image.asset(
            ResImgs("default_head_icon"),
            width: iconSize,
            height: iconSize,
          );
        },
        errorBuilder: (BuildContext context, Object exception, StackTrace? stackTrace) {
          return Image.asset(
            ResImgs("default_head_icon"),
            width: iconSize,
            height: iconSize,
          );
        },
      );
    } else {
      return custom.CustomImage(
        image: AssetImage(imageUrl),
        width: iconSize,
        height: iconSize,
      );
    }
  }
}

class _UserIconWidget extends State<UserIconWidget> {
  @override
  Widget build(BuildContext context) {
    double iconSize = widget.size ?? defAvatarSize;
    Widget icon = UserIconWidget.createUserIconImage(widget.imageUrl,
        size: iconSize, isNetworkIamge: widget.isNetworkIamge);
    ShapeBorder? shapeBorder;
    if (widget.frame) {
      shapeBorder = RoundedRectangleBorder(
        //无背景 圆角边框
        side: BorderSide(
            color: widget.frameColor ?? Colors.grey.withOpacity(0.3),
            width: 1,
            style: BorderStyle.solid),
        borderRadius: BorderRadius.all(Radius.circular(widget.circular ?? defAvatarCircular)),
      );
    }
    return Button(
      padding: EdgeInsets.zero,
      borderRadius: BorderRadius.all(Radius.circular(widget.circular ?? defAvatarCircular)),
      shape: shapeBorder,
      child: SizedBox(
        width: iconSize,
        height: iconSize,
        child: icon,
      ),
      onPressed: widget.onTap != null ? () => widget.onTap?.call(widget.userId) : null,
      onLongPress:
          widget.onLongPress != null ? () => widget.onLongPress?.call(widget.userId) : null,
    );
    // InkWell(
    //   child: SizedBox(
    //     width: iconSize,
    //     height: iconSize,
    //     child: ClipRRect(
    //       borderRadius: BorderRadius.all(
    //         Radius.circular(widget.circular ?? defAvatarCircular),
    //       ),
    //       child: icon,
    //     ),
    //   ),
    //   borderRadius: BorderRadius.all(Radius.circular(widget.circular ?? defAvatarCircular)),
    //   onTap: widget.onTap != null
    //       ? () {
    //           if (widget.onTap != null) widget.onTap!.call(widget.userId);
    //         }
    //       : null,
    //   onLongPress: widget.onLongPress != null
    //       ? () {
    //           if (widget.onLongPress != null) widget.onLongPress!.call(widget.userId);
    //         }
    //       : null,
    // );
  }
}
