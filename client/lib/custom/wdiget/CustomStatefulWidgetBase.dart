import 'package:flutter/material.dart';

abstract class CustomStatefulWidgetBase extends StatefulWidget {
  @protected
  String get pageName;

  const CustomStatefulWidgetBase({ Key? key }) : super(key: key);
}