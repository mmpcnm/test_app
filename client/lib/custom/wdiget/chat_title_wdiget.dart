import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/model/friend/friend_info.dart';
import 'package:fim_app/model/friend/friend_scoped.dart';
import 'package:fim_app/model/group/room_members_model.dart';
import 'package:fim_app/model/group/group_member_scoped.dart';
import 'package:fim_app/model/group/group_model.dart';
import 'package:fim_app/model/group/group_scoped.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class ChatTitle extends StatefulWidget {
  final Int64 receiverId;
  final pb.ReceiverType receiverType;
  const ChatTitle({Key? key, required this.receiverId, required this.receiverType})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _ChatTitle();
  }

  static String getTitleText(Int64 receiverId, pb.ReceiverType receiverType,
      {bool isNumber = true}) {
    //未知
    String ret = STR(10195);
    if (receiverType == pb.ReceiverType.RT_USER) {
      FriendInfo? friendModel = FriendScoped.instance.findFriend(receiverId);
      if (friendModel != null) {
        ret = friendModel.userNickname;
      } else {
        ret = '好友'; // UserModel.nickName;
      }
    } else if (receiverType == pb.ReceiverType.RT_GROUP) {
      GroupModel? groupModel = GroupScoped.instance.getGroup(receiverId);
      int count = GroupMemberScoped.instance.getMemberCount(receiverId);
      if (groupModel != null) {
        //群聊
        ret = groupModel.name.isEmpty ? '${STR(10029)}($count)' : '${groupModel.name}($count)';
      } else {
        //群聊
        ret = '${STR(10029)}($count)';
      }
    } else if (receiverType == pb.ReceiverType.RT_UNKNOWN) {
      //公告
      ret = STR(10194);
    } else if (receiverType == pb.ReceiverType.RT_ROOM) {
      RoomData roomData = GroupScoped.instance.getRoomData(receiverId);
      var list = RoomMembersModel().getMembers(receiverId);
      return roomData.name + (list.isEmpty ? '' : '(${list.length})');
      // ret = DeviceInfo.appName;
    }
    return ret;
  }
}

class _ChatTitle extends State<ChatTitle> {
  @override
  void initState() {
    super.initState();
    GroupScoped.instance.addListener(onGroupListener);
  }

  @override
  void dispose() {
    super.dispose();
    GroupScoped.instance.removeListener(onGroupListener);
  }

  void onGroupListener() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    if (widget.receiverType == pb.ReceiverType.RT_USER) {
      ScopedModel<FriendScoped>(
        model: FriendScoped.instance,
        child: ScopedModelDescendant<FriendScoped>(
          builder: (BuildContext context, Widget? child, FriendScoped model) {
            return _buildTitleText();
          },
        ),
      );
    } else if (widget.receiverType == pb.ReceiverType.RT_GROUP) {
      return ScopedModel<GroupScoped>(
        model: GroupScoped.instance,
        child: ScopedModelDescendant<GroupScoped>(
          builder: (BuildContext context, Widget? child, GroupScoped model) {
            return _buildTitleText();
          },
        ),
      );
    } else if (widget.receiverType == pb.ReceiverType.RT_UNKNOWN) {
      return _buildTitleText();
    } else if (widget.receiverType == pb.ReceiverType.RT_ROOM) {
      return _buildTitleText();
    }
    return _buildTitleText();
  }

  Widget _buildTitleText() {
    return Text(ChatTitle.getTitleText(widget.receiverId, widget.receiverType),
        style: defTitleTextStyle);
  }
}
