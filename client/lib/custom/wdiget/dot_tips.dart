import 'package:fim_app/package/util/screen_util.dart';
import 'package:flutter/material.dart';

class DotTips extends StatelessWidget {
  double? size;
  Widget? child;
  Color color;

  DotTips({this.size, this.color = Colors.red, this.child});
  @override
  Widget build(BuildContext context) {
    List<Widget> widgets = [
      Center(
        child: Icon(
          Icons.lens,
          size: size ?? 24.w,
          color: color,
        ),
      )
    ];

    if (child != null) {
      widgets.add(
        Center(
          child: child,
        ),
      );
    }

    return Stack(
      fit: StackFit.loose,
      alignment: Alignment.center,
      children: widgets,
    );
  }
}
