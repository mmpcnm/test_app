import 'package:fim_app/package/util/screen_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomSwitch extends StatefulWidget {
  bool value;
  double scale;
  void Function(bool) onChanged;
  CustomSwitch({required this.value, this.scale = 1.3, required this.onChanged});
  @override
  State<StatefulWidget> createState() => _CustomSwitch(isSwitch: value);
}

class _CustomSwitch extends State<CustomSwitch> {
  bool isSwitch;
  _CustomSwitch({required this.isSwitch});
  @override
  Widget build(BuildContext context) {
    return Transform.scale(
      scale: widget.scale.scl,
      child: CupertinoSwitch(
          value: isSwitch,
          onChanged: (value) {
            setState(() {
              isSwitch = value;
              widget.onChanged.call(value);
            });
          }),
    );
  }
}
