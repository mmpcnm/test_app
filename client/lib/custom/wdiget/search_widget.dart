import 'package:flutter/material.dart';

class SearchWidget extends StatefulWidget {
  double height;
  SearchWidget({
    this.height = 30,
  });

  @override
  State<StatefulWidget> createState() {
    return _SearchWidget();
  }
}

class _SearchWidget extends State<SearchWidget> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: widget.height,
        child: TextFormField(
            decoration: InputDecoration(
          fillColor: Colors.grey,
          prefixIcon: const Icon(
            Icons.search,
            color: Colors.black,
          ),
          contentPadding: const EdgeInsets.symmetric(horizontal: 10),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(45),
            borderSide: BorderSide(color: Colors.grey.shade300, style: BorderStyle.none),
          ),
          counterText: '',

          // suffixIcon: _buildRightIcons(),
        )));
  }
}
