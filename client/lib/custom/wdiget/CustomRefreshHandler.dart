import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:flutter/material.dart';
import 'package:m_loading/m_loading.dart';

class CustomRefreshHanler {
  int _mRefreshStatus = 0;
  Future Function()? _onRefreshHandlerStart;
  Future Function()? _onRefreshHandlerEnd;
  CustomRefreshHanler({
    required Future Function() onRefreshHandlerStart,
    required Future Function() onRefreshHandlerEnd,
  }) {
    _onRefreshHandlerStart = onRefreshHandlerStart;
    _onRefreshHandlerEnd = onRefreshHandlerEnd;
  }

  void init({
    required Future Function() onRefreshHandlerStart,
    required Future Function() onRefreshHandlerEnd,
  }) {
    _onRefreshHandlerStart = onRefreshHandlerStart;
    _onRefreshHandlerEnd = onRefreshHandlerEnd;
  }

  void dispose() {
    _onRefreshHandlerStart = null;
    _onRefreshHandlerEnd = null;
  }

  /// status 1.刷新中， 0.完成
  int get mRefreshStatus => _mRefreshStatus;
  Future refreshHandler({bool isSetStatus = true, Duration? duration}) async {
    if (isSetStatus) _mRefreshStatus = 1;
    if (duration != null) {
      await Future.delayed(duration);
    }
    try {
      if (_onRefreshHandlerStart != null) await _onRefreshHandlerStart?.call();

      _mRefreshStatus = 0;
      await Future.delayed(Duration(milliseconds: 10));
      if (_onRefreshHandlerEnd != null) await _onRefreshHandlerEnd?.call();
    } catch (e) {
      Debug.e('CustomRefreshHanler :$e');
    }
  }

  Widget buildWidget(BuildContext context, {double? loadingSize, double? ballSize, required Widget Function() builder}) {
    if (_mRefreshStatus == 1) {
      return Center(
        child: SizedBox(
          width: loadingSize ?? 100.sp,
          height: loadingSize ?? 100.sp,
          child: BallCircleOpacityLoading(
            // duration: Duration(minutes: 1),
            ballStyle: BallStyle(
              size: ballSize ?? 10.sp,
              color: Colors.blue,
              ballType: BallType.solid,
            ),
          ),
        ),
      );
    } else {
      return builder();
    }
  }
}
