import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/custom/wdiget/custom_switch.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:flutter/material.dart';

class CustomListTable {
  static Widget buildTable({
    Widget? leading,
    Widget? leftWidget,
    String? leftText,
    CrossAxisAlignment leftAlignment = CrossAxisAlignment.center,
    Widget? middleWidget,
    String? middleText,
    Widget? rightWidget,
    String? rightText,
    Widget? trailing,
    EdgeInsets? padding,
    Color? backColor,
    bool isLine = false,
    double lineIndent = 0,
    double lineEndIndent = 0,
    double borderCircular = 0,
    MainAxisSize mainAxisSize = MainAxisSize.min,
    VoidCallback? onPressed,
  }) {
    leftWidget = leftWidget ?? (leftText != null ? Text(leftText, style: defTextStyle, overflow: TextOverflow.ellipsis) : SizedBox());
    rightWidget = rightWidget ??
        (rightText != null ? Text(rightText, style: TextStyle(fontSize: defFontSize, color: grayColor), overflow: TextOverflow.ellipsis) : null);

    middleWidget = middleWidget ?? (middleText != null ? Text(middleText, style: defTextStyle, overflow: TextOverflow.ellipsis) : null);

    if (middleWidget != null) {
      middleWidget = Align(alignment: Alignment.center, child: middleWidget);
    }

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Button(
          childAlign: Alignment.centerLeft,
          color: backColor,
          borderCircular: borderCircular,
          onPressed: onPressed,
          padding: padding ?? EdgeInsets.symmetric(horizontal: defPaddingSize, vertical: defPaddingSize),
          child: Stack(
            alignment: Alignment.center,
            children: [
              Row(
                crossAxisAlignment: leftAlignment,
                children: [
                  if (leading != null) leading,
                  if (leading != null) SizedBox(width: 15.sp),
                  Expanded(child: leftWidget),
                  if (rightWidget != null) rightWidget,
                  SizedBox(width: 3.sp),
                  trailing ?? Icon(Icons.arrow_forward_ios, size: 28.sp, color: blackColor),
                ],
              ),
              if (middleWidget != null) middleWidget,
            ],
          ),
        ),
        if (isLine) Divider(height: 1, indent: lineIndent, endIndent: lineEndIndent, color: lineColor)
      ],
    );
  }
}

class CustomListTile<T> extends StatelessWidget {
  final Widget? leading;
  final Widget? title;
  final Widget? subtitle;
  final Widget? middletitle;
  final Widget? trailing;
  final Color? color;
  final BorderRadius borderRadius;
  final ShapeBorder? shape;
  final EdgeInsets? padding;
  final EdgeInsets margin;
  final CrossAxisAlignment alignment;
  final bool isArrowIcon;
  final Divider? bottomLine;
  final VoidCallback? onPressed;
  final Function(T? value)? onValuePressed;
  final T? sender;

  const CustomListTile({
    Key? key,
    this.leading,
    this.title,
    this.subtitle,
    this.middletitle,
    this.trailing,
    this.color,
    this.borderRadius = BorderRadius.zero,
    this.shape,
    this.padding,
    this.margin = EdgeInsets.zero,
    this.alignment = CrossAxisAlignment.center,
    this.isArrowIcon = false,
    this.bottomLine,
    this.onPressed,
    this.onValuePressed,
    this.sender,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.topCenter,
      children: [
        Button(
          childAlign: Alignment.centerLeft,
          color: color,
          borderCircular: 0,
          borderRadius: borderRadius,
          shape: shape,
          onPressed: onPressed ??
              (onValuePressed != null
                  ? () {
                      onValuePressed?.call(sender);
                    }
                  : null),
          margin: margin,
          padding: padding ?? EdgeInsets.symmetric(horizontal: defPaddingSizeH, vertical: defPaddingSizeV),
          child: Stack(
            alignment: Alignment.centerLeft,
            children: [
              Row(
                crossAxisAlignment: alignment,
                children: [
                  if (leading != null) leading!,
                  if (leading != null) SizedBox(width: 15.sp),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        if (title != null) title!,
                        if (subtitle != null) subtitle!,
                      ],
                    ),
                  ),
                  if (trailing != null) SizedBox(width: 3.sp),
                  if (trailing != null) trailing!,
                  if (isArrowIcon) Icon(Icons.arrow_forward_ios, size: 28.sp, color: grayColor),
                ],
              ),
              if (middletitle != null) Center(child: middletitle),
            ],
          ),
        ),
        if (bottomLine != null) Positioned(left: 0, right: 0, bottom: 0, child: bottomLine!),
      ],
    );
  }
}

class CustomListTileSwith extends CustomListTile {
  final void Function(bool) onChanged;
  final double switchScale;
  final bool value;

  CustomListTileSwith({
    Key? key,
    Widget? leading,
    Widget? title,
    Widget? subtitle,
    Widget? middletitle,
    Widget? trailing,
    Color? color,
    BorderRadius borderRadius = BorderRadius.zero,
    ShapeBorder? shape,
    EdgeInsets? padding,
    EdgeInsets margin = EdgeInsets.zero,
    CrossAxisAlignment alignment = CrossAxisAlignment.center,
    Divider? bottomLine,
    this.switchScale = 1.3,
    required this.value,
    required this.onChanged,
  }) : super(
          key: key,
          leading: leading,
          title: title,
          subtitle: subtitle,
          middletitle: middletitle,
          trailing: trailing != null
              ? Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    trailing,
                    CustomSwitch(scale: switchScale, value: value, onChanged: onChanged),
                  ],
                )
              : CustomSwitch(scale: switchScale, value: value, onChanged: onChanged),
          color: color,
          borderRadius: borderRadius,
          shape: shape,
          padding: padding,
          margin: margin,
          alignment: alignment,
          isArrowIcon: false,
          bottomLine: bottomLine,
          onPressed: null,
        );
}
