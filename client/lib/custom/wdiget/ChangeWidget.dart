import 'package:flutter/cupertino.dart';

class ChangeWidgetContorller extends ChangeNotifier {
  Widget? _mChild;

  void changeWidget(Widget child) {
    _mChild = child;
    notifyListeners();
  }
}

class ChangeWidget extends StatefulWidget {
  Widget? child;
  ChangeWidgetContorller? contorller;
  ChangeWidget({this.child, this.contorller}) {
    if (contorller != null) {
      contorller?._mChild = child;
    } else {
      contorller = ChangeWidgetContorller();
      contorller?._mChild = child;
    }
  }

  Widget? getChild() {
    if (contorller != null) return contorller?._mChild;
    return null;
  }

  @override
  State<StatefulWidget> createState() => _ChangeWidget();
}

class _ChangeWidget extends State<ChangeWidget> {
  @override
  void initState() {
    super.initState();
    if (widget.contorller != null) {
      widget.contorller?.addListener(onChange);
    }
  }

  @override
  void dispose() {
    if (widget.contorller != null) {
      widget.contorller?.removeListener(onChange);
    }
    super.dispose();
  }

  void onChange() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    Widget? child = widget.getChild();
    if (child != null)
      print('change: true');
    else
      print('change: null');

    if (child != null) {
      return child;
    } else {
      return Center();
    }
  }
}
