import 'dart:math';

import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomSliverPersistentHeaderDelegate extends SliverPersistentHeaderDelegate {
  Widget child;
  Color? color;
  double? mMinExtent;
  double? mMaxExtent;
  CustomSliverPersistentHeaderDelegate({required this.child, this.color, this.mMinExtent, this.mMaxExtent});

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    if (maxExtent + shrinkOffset <= 0) {
      return Center();
    }
    return SizedBox(
      height: max(maxExtent + shrinkOffset, minExtent),
      child: Container(
        color: color,
        child: child,
      ),
    );
  }

  @override
  double get maxExtent => max(mMaxExtent ?? defTabelHeight, minExtent);

  @override
  double get minExtent => max(mMinExtent ?? defTabelHeight, 0);

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}
