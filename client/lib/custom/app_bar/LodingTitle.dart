// ignore_for_file: import_of_legacy_library_into_null_safe, use_key_in_widget_constructors

import 'package:fim_app/package/net/socket/connect_socket.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:flutter/material.dart';
import 'package:m_loading/m_loading.dart';
import 'package:scoped_model/scoped_model.dart';

class LodingTitle extends StatelessWidget {
  final Widget noneChild;
  final Widget waitingChild;
  const LodingTitle({required this.noneChild, required this.waitingChild});
  @override
  Widget build(BuildContext context) {
    return ScopedModel<ConnectSocket>(
      model: ConnectSocket(),
      child: ScopedModelDescendant<ConnectSocket>(
        builder: (context, child, model) {
          if (model.isConnect) {
            return noneChild;
          } else {
            if (model.connectionState == ConnectionState.waiting) {
              return Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    width: 26.sp,
                    height: 26.sp,
                    child: Ring2InsideLoading(
                      color: Colors.blue,
                      strokeWidth: 5.sp,
                      // duration: Duration(seconds: 30),
                      // radius: 14.sp,
                      // ballStyle: BallStyle(
                      //   size: SUT_W(5),
                      //   color: Colors.blue,
                      //   ballType: BallType.solid,
                      // ),
                    ),
                  ),
                  SizedBox(width: 10.sp),
                  waitingChild,
                ],
              );
            } else {
              return waitingChild;
            }
          }
        },
      ),
    );
  }
}
