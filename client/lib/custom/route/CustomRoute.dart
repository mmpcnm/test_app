// ignore_for_file: constant_identifier_names, file_names

import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/model/red_packet/red_packet_model.dart';
import 'package:fim_app/pages/loading/init_page.dart';
import 'package:fim_app/model/ModelBase.dart';
import 'package:fim_app/model/chat/MessageModel.dart';
import 'package:fim_app/model/friend/friend_scoped.dart';
import 'package:fim_app/model/friend/new_friend_model.dart';
import 'package:fim_app/model/group/room_members_model.dart';
import 'package:fim_app/model/group/group_scoped.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:fim_app/package/net/socket/connect_socket.dart';
import 'package:fim_app/package/sqflite/sqlite_manager.dart';
import 'package:fim_app/pages/home/home_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:one_context/one_context.dart';

export 'package:one_context/one_context.dart';

// final GlobalKey<NavigatorState> navGK = new GlobalKey();
// GlobalKey<ScaffoldState> scaffoldGK = GlobalKey(debugLabel: 'home');

enum HomeViewIndex {
  MESSAGE,
  FRIEND,
  FIND,
  WEB,
  MY,
}

class CustomRoute extends ModelBase<CustomRoute> with NavigatorObserver {
  static final CustomRoute _instance = CustomRoute._create();
  CustomRoute._create();
  factory CustomRoute() => _instance;

  static String getTypeName(Object obj) {
    // Debug.w('''pageRounte:{
    //   runtimeType: ${obj.runtimeType},
    //   runtimeTypeString: ${obj.runtimeType.toString()},
    // }''');
    return '${obj.runtimeType}';
  }

  RouteSettings? _currentSettings;
  RouteSettings? get settings => _currentSettings;

  String get currentRouteName {
    return _currentSettings?.name ?? "";
  }

  T? getCurrentWidget<T extends Widget>() {
    print('${_currentSettings?.arguments} ${_currentSettings?.arguments is T}');
    if (_currentSettings != null &&
        _currentSettings?.arguments != null &&
        _currentSettings?.arguments is T) {
      return _currentSettings?.arguments as T;
    }
    return null;
  }

  bool isCurrentWidget<T extends Widget>() => getCurrentWidget<T>() != null;

  BuildContext? get context => OneContext().context;

  Future pushPage(Widget widget) async {
    return OneContext().push(
      PageRouteBuilder(
        settings: RouteSettings(name: CustomRoute.getTypeName(widget), arguments: widget),
        transitionDuration: const Duration(milliseconds: 300),
        pageBuilder: (context, animation, secondaryAnimation) {
          return widget;
        },
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          const begin = Offset(1.0, 0.0);
          const end = Offset.zero;
          const curve = Curves.fastOutSlowIn;

          var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

          return SlideTransition(
            position: animation.drive(tween),
            child: child,
          );
        },
      ),
    );
  }

  Future pushFade(Widget widget, {int milliseconds = 500}) async {
    return OneContext().push(
      PageRouteBuilder(
        settings: RouteSettings(name: CustomRoute.getTypeName(widget), arguments: widget),
        transitionDuration: Duration(milliseconds: milliseconds),
        pageBuilder: (BuildContext context, animation, secondaryAnimation) {
          return widget;
        },
        transitionsBuilder: (BuildContext context, animation, secondaryAnimation, Widget child) {
          return FadeTransition(
            opacity: Tween(begin: 0.0, end: 1.0)
                .animate(CurvedAnimation(parent: animation, curve: Curves.fastOutSlowIn)),
            child: child,
          );
        },
      ),
    );
  }

  Future pushScale(Widget widget, {int milliseconds = 500}) async {
    return OneContext().push(
      PageRouteBuilder(
        settings: RouteSettings(name: CustomRoute.getTypeName(widget), arguments: widget),
        transitionDuration: Duration(milliseconds: milliseconds),
        pageBuilder: (BuildContext context, animation, secondaryAnimation) {
          return widget;
        },
        transitionsBuilder: (BuildContext context, animation, secondaryAnimation, Widget child) {
          return ScaleTransition(
              scale: Tween(begin: 0.0, end: 1.0)
                  .animate(CurvedAnimation(parent: animation, curve: Curves.fastOutSlowIn)),
              child: child);
        },
      ),
    );
  }

  Future pushReplacement(Widget widget, {int milliseconds = 500}) async {
    return OneContext().pushReplacement(
      PageRouteBuilder(
        settings: RouteSettings(name: CustomRoute.getTypeName(widget), arguments: widget),
        transitionDuration: Duration(milliseconds: milliseconds),
        pageBuilder: (BuildContext context, animation, secondaryAnimation) {
          return widget;
        },
        transitionsBuilder: (BuildContext context, animation, secondaryAnimation, Widget child) {
          return FadeTransition(
            opacity: Tween(begin: 0.0, end: 1.0)
                .animate(CurvedAnimation(parent: animation, curve: Curves.fastOutSlowIn)),
            child: child,
          );
        },
      ),
    );
  }

  Future popPushPage(Widget widget) async {
    pop();
    return pushPage(widget);
  }

  Future popPushFade(Widget widget, {int milliseconds = 500}) async {
    pop();
    return pushFade(widget, milliseconds: milliseconds);
  }

  Future popPushScale(Widget widget, {int milliseconds = 500}) async {
    pop();
    return pushScale(widget, milliseconds: milliseconds);
  }

  void pop<T extends Object?>([T? result]) => OneContext().pop(result);

  void popDialog<T extends Object>([T? result]) => OneContext().popDialog(result);
  void popAllDialogs() => OneContext().popAllDialogs();

  void gotoLogin(BuildContext context) {
    ConnectSocket().destory();
    SqliteManager().close();
    UserModel.clear();
    MessageModel().clear();
    GroupScoped.instance.clear();
    FriendScoped.instance.clear();
    NewFriendModel.instance.clear();
    // ChatInputModel.dispose();
    // ChatCancelList().clear();
    // OneContext().popDialog();
    // EasyLoading.dismiss();
    // FocusScope.of(OneContext().context!).requestFocus(FocusNode());
    RoomMembersModel().clear();
    RedPacketModel().clear();
    pushReplacement(InitPage(), milliseconds: 1000);
  }

  void gotpHome(BuildContext context, {HomeViewIndex index = HomeViewIndex.MESSAGE}) {
    // ChatInputModel.dispose();
    // OneContext().popDialog();
    // EasyLoading.dismiss();
    // FocusScope.of(OneContext().context!).requestFocus(FocusNode());
    int tag = HomeViewIndex.values.indexOf(index);
    pushReplacement(HomePage(currentIndex: tag));
  }

  Future<T?> showDialog<T>(
      {required Widget Function(BuildContext) builder,
      bool barrierDismissible = true,
      bool useRootNavigator = true}) {
    return OneContext().showDialog(
        builder: builder,
        barrierDismissible: barrierDismissible,
        useRootNavigator: useRootNavigator);
  }

  Future<T?> showModalBottomSheet<T>({
    required Widget Function(BuildContext) builder,
    Color backgroundColor = const Color(0x00FFFFFF),
    double? elevation,
    ShapeBorder? shape,
    Clip? clipBehavior,
    bool isScrollControlled = true,
    bool useRootNavigator = false,
    bool isDismissible = true,
  }) {
    return OneContext().showModalBottomSheet(
      backgroundColor: backgroundColor,
      elevation: elevation,
      shape: shape,
      clipBehavior: clipBehavior,
      isScrollControlled: isScrollControlled,
      useRootNavigator: useRootNavigator,
      isDismissible: isDismissible,
      builder: builder,
    );
  }

  @override
  void didPush(Route<dynamic> route, Route<dynamic>? previousRoute) {
    super.didPush(route, previousRoute);
    // Debug.w('didPush $route ------ $previousRoute');
    String newRouteName = 'null';
    String oldRouteName = 'null';
    newRouteName = route.settings.name ?? 'null';
    if (previousRoute != null) oldRouteName = previousRoute.settings.name ?? 'null';
    Debug.w(' NavObserverDidPush-Current:' + newRouteName + '  Previous:' + oldRouteName);
    _currentSettings = route.settings;
    print('didPush $_currentSettings');
    notifyListeners();
  }

  @override
  void didPop(Route<dynamic> route, Route<dynamic>? previousRoute) {
    super.didPop(route, previousRoute);
    // Debug.w('didPop $route ------ $previousRoute');
    String newRouteName = 'null';
    String oldRouteName = 'null';
    newRouteName = route.settings.name ?? 'null';
    if (previousRoute != null) oldRouteName = previousRoute.settings.name ?? 'null';

    Debug.w('NavObserverDidPop--Current:' + oldRouteName + '  Previous:' + newRouteName);
    _currentSettings = previousRoute?.settings;

    print('didPop $_currentSettings');
    notifyListeners();
  }

  @override
  void didRemove(Route<dynamic> route, Route<dynamic>? previousRoute) {
    super.didRemove(route, previousRoute);
    // Debug.w('didRemove $route ------ $previousRoute');
    String newRouteName = 'null';
    String oldRouteName = 'null';
    newRouteName = route.settings.name ?? 'null';
    if (previousRoute != null) oldRouteName = previousRoute.settings.name ?? 'null';
    Debug.w('NavObserverDidRemove--Current:' + newRouteName + '  Previous:' + oldRouteName);
    _currentSettings = previousRoute?.settings;
  }

  @override
  void didReplace({Route<dynamic>? newRoute, Route<dynamic>? oldRoute}) {
    super.didReplace(newRoute: newRoute, oldRoute: oldRoute);

    String newRouteName = 'null';
    String oldRouteName = 'null';
    if (newRoute != null) newRouteName = newRoute.settings.name ?? 'null';
    if (oldRoute != null) oldRouteName = oldRoute.settings.name ?? 'null';

    Debug.w('NavObserverDidRemove--Current:' + newRouteName + '  Previous:' + oldRouteName);
    _currentSettings = newRoute?.settings;
    print('didPush $_currentSettings');
    notifyListeners();
  }

  @override
  void didStartUserGesture(Route<dynamic> route, Route<dynamic>? previousRoute) {
    super.didStartUserGesture(route, previousRoute);
    // Debug.w('didStartUserGesture $route ------ $previousRoute');
    String newRouteName = 'null';
    String oldRouteName = 'null';
    route.settings.name ?? 'null';
    if (previousRoute != null) oldRouteName = previousRoute.settings.name ?? 'null';
    Debug.w(
        'NavObserverDidStartUserGesture--Current:' + newRouteName + '  Previous:' + oldRouteName);
  }

  @override
  void didStopUserGesture() {
    super.didStopUserGesture();
    Debug.w('NavObserverDidStopUserGesture');
  }
}

Route pageRoute(Widget widget) {
  // Debug.w('''pageRounte:{
  //   runtimeType: ${widget.runtimeType},
  //   runtimeTypeString: ${widget.runtimeType.toString()},
  //   Short:${widget.toStringShort()},
  //   String: ${widget.toString()}
  // }''');
  return PageRouteBuilder(
    settings: RouteSettings(name: CustomRoute.getTypeName(widget), arguments: widget),
    transitionDuration: const Duration(milliseconds: 300),
    pageBuilder: (context, animation, secondaryAnimation) {
      return widget;
    },
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      const begin = Offset(1.0, 0.0);
      const end = Offset.zero;
      const curve = Curves.fastOutSlowIn;

      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}

Route fadeRoute(Widget widget, {int milliseconds = 500}) {
  return PageRouteBuilder(
    settings: RouteSettings(name: CustomRoute.getTypeName(widget), arguments: widget),
    transitionDuration: Duration(milliseconds: milliseconds),
    pageBuilder: (BuildContext context, animation, secondaryAnimation) {
      return widget;
    },
    transitionsBuilder: (BuildContext context, animation, secondaryAnimation, Widget child) {
      return FadeTransition(
        opacity: Tween(begin: 0.0, end: 1.0)
            .animate(CurvedAnimation(parent: animation, curve: Curves.fastOutSlowIn)),
        child: child,
      );
    },
  );
}

Route scaleRoute(Widget widget) {
  return PageRouteBuilder(
    settings: RouteSettings(name: CustomRoute.getTypeName(widget), arguments: widget),
    transitionDuration: const Duration(milliseconds: 500),
    pageBuilder: (BuildContext context, animation, secondaryAnimation) {
      return widget;
    },
    transitionsBuilder: (BuildContext context, animation, secondaryAnimation, Widget child) {
      return ScaleTransition(
          scale: Tween(begin: 0.0, end: 1.0)
              .animate(CurvedAnimation(parent: animation, curve: Curves.fastOutSlowIn)),
          child: child);
    },
  );
}

gotoLogin(BuildContext context) {
  ConnectSocket().destory();
  SqliteManager().close();
  UserModel.clear();
  MessageModel().clear();
  GroupScoped.instance.clear();
  FriendScoped.instance.clear();
  NewFriendModel.instance.clear();
  // ChatInputModel.dispose();
  // ChatCancelList().clear();
  // OneContext().popDialog();
  // EasyLoading.dismiss();
  // LogicChat().clear();
  // FocusScope.of(OneContext().context!).requestFocus(FocusNode());
  RoomMembersModel().clear();
  RedPacketModel().clear();
  OneContext().pushReplacement(fadeRoute(InitPage(), milliseconds: 1000));
}

gotpHome(BuildContext context, {HomeViewIndex index = HomeViewIndex.MESSAGE}) {
  // ChatInputModel.dispose();
  // OneContext().popDialog();
  // EasyLoading.dismiss();
  // FocusScope.of(OneContext().context!).requestFocus(FocusNode());
  int tag = HomeViewIndex.values.indexOf(index);
  OneContext().pushReplacement(fadeRoute(HomePage(currentIndex: tag)));
}
