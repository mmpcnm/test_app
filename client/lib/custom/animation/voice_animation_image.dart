// 帧动画Image
import 'dart:async';
import 'package:flutter/material.dart';

class VoiceAnimationImage extends StatefulWidget {
  List<String> assetList;
  double width;
  double height;
  int interval;
  bool isRunting;
  bool isLoop;
  bool isFlipX;
  VoidCallback? onAnimationEnd;
  VoiceAnimationImageControll? controll;

  VoiceAnimationImage(
    this.assetList, {
    Key? key,
    this.isLoop = false,
    required this.width,
    required this.height,
    this.isFlipX = false,
    this.isRunting = false,
    this.interval = 200,
    this.controll,
    this.onAnimationEnd,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    StateVoiceAnimationImage state = StateVoiceAnimationImage();
    if (controll != null) {
      controll!._stateVoiceAnimationImage = state;
    }
    return state;
  }
}

class StateVoiceAnimationImage extends State<VoiceAnimationImage> {
  VoiceAnimationImageState _mState = VoiceAnimationImageState.UNKNOWN;
  int _mCurrentIndex = 0;
  List<ImageProvider> _mImageProviders = [];
  Timer? _mTimer;

  @override
  void initState() {
    super.initState();
    for (int i = 0; i < widget.assetList.length; i++) {
      String path = widget.assetList[i];
      _mImageProviders.add(AssetImage(path));
    }

    if (widget.isRunting) {
      _mCurrentIndex = 0;
      _createTimer();
    }
  }

  @override
  void dispose() {
    _disposeTimer();
    super.dispose();
  }

  void start() {
    if (_mState == VoiceAnimationImageState.RUNTING) return;
    _mState = VoiceAnimationImageState.RUNTING;
    _mCurrentIndex = 0;
    _createTimer();
    _reSetState();
  }

  void restart() {
    _mState = VoiceAnimationImageState.UNKNOWN;
    start();
  }

  void stop({bool isReset = false}) {
    _mState = VoiceAnimationImageState.UNKNOWN;
    if (isReset) _mCurrentIndex = 0;
    _disposeTimer();
    _reSetState();
  }

  void paused() {
    _mState = VoiceAnimationImageState.PAUSED;
    _disposeTimer();
    _reSetState();
  }

  void recover() {
    if (_mState == VoiceAnimationImageState.PAUSED) {
      _mState = VoiceAnimationImageState.RUNTING;
      _mCurrentIndex = 0;
      _createTimer();
    }
  }

  void _createTimer() {
    if (_mTimer == null) {
      _mTimer = Timer.periodic(Duration(milliseconds: widget.interval), (timer) {
        updateTime();
      });
    }
  }

  void _disposeTimer() {
    if (_mTimer != null) {
      _mTimer!.cancel();
      _mTimer = null;
    }
  }

  void _reSetState() {
    Future.delayed(Duration(milliseconds: 1), () {
      setState(() {});
    });
  }

  void updateTime() {
    if (_mState != VoiceAnimationImageState.RUNTING) return;
    setState(() {
      int index = _mCurrentIndex + 1;
      if (index >= widget.assetList.length) {
        index = 0;
        if (!widget.isLoop) {
          _disposeTimer();
          if (widget.onAnimationEnd != null) widget.onAnimationEnd!.call();
        }
      }
      _mCurrentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (widget.controll != null) widget.controll!._stateVoiceAnimationImage = this;
    return Image(
      image: _mImageProviders[_mCurrentIndex],
      width: widget.width,
      height: widget.height,
    );
  }
}

enum VoiceAnimationImageState {
  UNKNOWN,
  RUNTING,
  PAUSED,
}

class VoiceAnimationImageControll {
  // GlobalKey<StateClockText> _key = GlobalKey<StateClockText>();
  StateVoiceAnimationImage? _stateVoiceAnimationImage;
  void start() {
    if (_stateVoiceAnimationImage != null) {
      _stateVoiceAnimationImage!.start();
    }
  }

  void stop({bool isReset = false}) {
    if (_stateVoiceAnimationImage != null) {
      _stateVoiceAnimationImage!.stop(isReset: isReset);
    }
  }

  void paused() {
    if (_stateVoiceAnimationImage != null) {
      _stateVoiceAnimationImage!.paused();
    }
  }

  void recover() {
    if (_stateVoiceAnimationImage != null) {
      _stateVoiceAnimationImage!.recover();
    }
  }
}
