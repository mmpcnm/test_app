// ignore_for_file: file_names, no_logic_in_create_state

import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:flutter/material.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:flutter/services.dart';

class CustomTextField extends StatefulWidget {
  final Widget? icon;
  final int maxLength;
  final String hintText;
  final String initialValue;
  final TextInputType keyboardType;
  final bool isEyeIcon;
  final List<TextInputFormatter>? inputFormatters;
  final ValueChanged<String>? onChanged;
  final FormFieldSetter<String>? onSaved;
  final FormFieldValidator<String>? validator;

  const CustomTextField({
    Key? key,
    this.icon,
    this.maxLength = 16,
    this.hintText = '',
    this.initialValue = '',
    this.keyboardType = TextInputType.text,
    this.isEyeIcon = false,
    this.inputFormatters,
    this.onChanged,
    this.onSaved,
    this.validator,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => StateCustomTextField(TextEditingController(text: initialValue));
}

class StateCustomTextField extends State<CustomTextField> {
  bool _obscureText = true;
  final TextEditingController _controller;
  StateCustomTextField(this._controller);

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: widget.keyboardType,
      obscureText: widget.isEyeIcon ? _obscureText : false,
      obscuringCharacter: '*',
      style: defTextStyle,
      maxLength: widget.maxLength,
      controller: _controller,
      // cursorHeight: defFontSize,
      // cursorColor: defFontColor,
      textAlignVertical: TextAlignVertical.center,
      // textCapitalization: TextCapitalization.characters,
      inputFormatters: widget.inputFormatters,
      decoration: InputDecoration(
        hintText: widget.hintText,
        hintStyle: TextStyle(fontSize: defFontSize, color: grayColor),
        counterText: '',
        icon: widget.icon,
        suffixIcon: _buildRightIcons(),
      ),
      onChanged: (String value) {
        setState(() {
          if (widget.onChanged != null) widget.onChanged!.call(value);
        });
      },
      onSaved: widget.onSaved,
      validator: widget.validator,
    );
  }

  Widget _buildRightIcons() {
    List<Widget> icons = [
      SizedBox(width: 5.sp),
    ];

    if (_controller.text.isNotEmpty) {
      icons.add(
        Button(
          padding: EdgeInsets.all(5.sp),
          borderCircular: 50,
          child: Icon(Icons.cancel, color: Colors.grey, size: defFontSize),
          onPressed: () {
            setState(() {
              _controller.clear();
              if (widget.onChanged != null) widget.onChanged!.call('');
            });
          },
        ),
      );
      icons.add(SizedBox(width: 5.sp));
    }
    if (widget.isEyeIcon) {
      icons.add(
        Button(
          padding: const EdgeInsets.all(0),
          borderCircular: 50,
          child: Icon(_obscureText ? Icons.visibility_off_outlined : Icons.visibility_outlined, size: 48.sp, color: grayColor),
          onPressed: () {
            setState(() {
              _obscureText = !_obscureText;
            });
          },
        ),
      );
      icons.add(SizedBox(width: 5.sp));
    }
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.end,
      children: icons,
    );
  }
}
