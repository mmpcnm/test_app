// ignore_for_file: file_names

import 'dart:math';

import 'package:flutter/services.dart';

class CustomTextFieldNumFormatter extends TextInputFormatter {
  int digit;
  CustomTextFieldNumFormatter({this.digit = -1});
  bool isNum(String value) {
    return double.tryParse(value) != null;
  }

  int getValueDigit(String value) {
    if (value.contains('.')) {
      return value.split('.')[1].length;
    }
    return -1;
  }

  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    String value = newValue.text;
    int selecitonIndex = newValue.selection.end;

    if (value.isNotEmpty) {
      if (value == '.') {
        value = '0.';
        selecitonIndex++;
      } else if (value == '-') {
        value = '-';
        selecitonIndex++;
      } else if (!isNum(value)) {
        value = oldValue.text;
        selecitonIndex = oldValue.selection.end;
      } else if (value.length > 1) {
        if (value[0] == '0' && value[1] != '.') {
          // value = oldValue.text;
          value = value.substring(1);
          if (selecitonIndex > 0) {
            selecitonIndex--;
          }
        } else if (value[0] == '.') {
          value = '0$value';
          selecitonIndex++;
        }
      } else if (getValueDigit(value) > digit) {
        value = oldValue.text;
        selecitonIndex = oldValue.selection.end;
      }
    }

    return TextEditingValue(
      text: value,
      selection: TextSelection.collapsed(offset: selecitonIndex),
    );
  }
}

class CustomTextFieldSpaceFormatter extends TextInputFormatter {
  CustomTextFieldSpaceFormatter();
  bool isNum(String value) {
    return double.tryParse(value) != null;
  }

  int getValueDigit(String value) {
    if (value.contains('.')) {
      return value.split('.')[1].length;
    }
    return -1;
  }

  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    String value = newValue.text;
    int selecitonIndex = newValue.selection.end;
    String newStr = value.replaceAll(RegExp(r'\s+\b|\b\s'), '');
    selecitonIndex = max(newStr.length - value.length, 0) + selecitonIndex;
    return TextEditingValue(
      text: newStr,
      selection: TextSelection.collapsed(offset: selecitonIndex),
    );
  }
}
