// ignore_for_file: prefer_const_declarations

import 'dart:math' as math;
import 'package:extended_text_library/extended_text_library.dart';
import 'package:fim_app/custom/text/src/global_extended_text_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
// import 'package:url_launcher/url_launcher.dart';

class TextSelectionToolbarItemData {
  const TextSelectionToolbarItemData({
    required this.label,
    required this.onPressed,
  });

  final String label;
  final VoidCallback? onPressed;
}

const double _kHandleSize = 22.0;

const double _kToolbarContentDistanceBelow = _kHandleSize - 2.0;
const double _kToolbarContentDistance = 8.0;

const List<TextSelectionToolbarItemData> _defToolbarItems = <TextSelectionToolbarItemData>[];

class ExtendedTextSelectionControls extends TextSelectionControls {
  ExtendedTextSelectionControls({
    this.key,
    this.joinZeroWidthSpace = false,
    this.selectionColor = Colors.green,
    this.toolbarItems = _defToolbarItems,
  });
  final bool joinZeroWidthSpace;
  final Color selectionColor;
  final List<TextSelectionToolbarItemData> toolbarItems;

  final Key? key;

  late TextSelectionDelegate? _delegate;

  // bool _isVisible = true;
  // set isVisible(bool visible) {
  //   _isVisible = visible;
  // }

  void hideToolbar([bool hideHandles = true]) {
    try {
      if (_delegate != null) {
        final TextEditingValue value = _delegate!.textEditingValue;
        _delegate?.userUpdateTextEditingValue(
            TextEditingValue(
              text: value.text,
              selection: TextSelection.collapsed(offset: value.selection.end),
            ),
            SelectionChangedCause.toolbar);
        _delegate?.bringIntoView(_delegate!.textEditingValue.selection.extent);
        _delegate?.hideToolbar(hideHandles);
        _delegate = null;
        if (GlobalExtendedTextModel().currentCentrols == this) {
          GlobalExtendedTextModel().currentCentrols = null;
        }
      }
    } catch (e) {
      print('ExtendedTextSelectionControls error hideToolbar');
    }
  }

  @override
  bool operator ==(Object other) {
    if (other == null || other.runtimeType != runtimeType) return false;
    if (other is ExtendedTextSelectionControls) {
      print('ExtendedTextSelectionControls:$key, other:${other.key}');
      return other.key == key;
    }

    return false;
  }

  void _handleClick(TextSelectionDelegate delegate, {VoidCallback? onPressed}) {
    delegate.hideToolbar();
    if (GlobalExtendedTextModel().currentCentrols == this) {
      GlobalExtendedTextModel().currentCentrols = null;
    }
    if (onPressed != null) {
      onPressed.call();
    }
  }

  @override
  void handleCopy(TextSelectionDelegate delegate, ClipboardStatusNotifier? clipboardStatus) {
    final TextEditingValue value = delegate.textEditingValue;
    _delegate = null;

    String data = value.selection.textInside(value.text);
    // remove zeroWidthSpace
    if (joinZeroWidthSpace) {
      data = data.replaceAll(zeroWidthSpace, '');
    }
    Clipboard.setData(ClipboardData(
      text: value.selection.textInside(value.text),
    ));
    clipboardStatus?.update();
    delegate.userUpdateTextEditingValue(
        TextEditingValue(
          text: value.text,
          selection: TextSelection.collapsed(offset: value.selection.end),
        ),
        SelectionChangedCause.toolbar);
    delegate.bringIntoView(delegate.textEditingValue.selection.extent);

    delegate.hideToolbar();
    if (GlobalExtendedTextModel().currentCentrols == this) {
      GlobalExtendedTextModel().currentCentrols = null;
    }
  }

  @override
  Size getHandleSize(double textLineHeight) => const Size(_kHandleSize, _kHandleSize);

  /// Builder for material-style copy/paste text selection toolbar.
  @override
  Widget buildToolbar(
    BuildContext context,
    Rect globalEditableRegion,
    double textLineHeight,
    Offset selectionMidpoint,
    List<TextSelectionPoint> endpoints,
    TextSelectionDelegate delegate,
    ClipboardStatusNotifier clipboardStatus,
    Offset? lastSecondaryTapDownPosition,
  ) {
    _delegate = delegate;
    Future.delayed(const Duration(milliseconds: 5), () {
      if (GlobalExtendedTextModel().currentCentrols != this) {
        GlobalExtendedTextModel().currentCentrols = this;
      }
    });
    return _TextSelectionControlsToolbar(
      globalEditableRegion: globalEditableRegion,
      textLineHeight: textLineHeight,
      selectionMidpoint: selectionMidpoint,
      endpoints: endpoints,
      delegate: delegate,
      clipboardStatus: clipboardStatus,
      handleCut: canCut(delegate) ? () => handleCut(delegate, clipboardStatus) : null,
      handleCopy: canCopy(delegate) ? () => handleCopy(delegate, clipboardStatus) : null,
      handlePaste: canPaste(delegate) ? () => handlePaste(delegate) : null,
      handleSelectAll: canSelectAll(delegate) ? () => handleSelectAll(delegate) : null,
      handleLike: () {
        // launch('mailto:zmtzawqlp@live.com?subject=extended_text_share&body=${delegate.textEditingValue.text}');
        delegate.hideToolbar();
        delegate.userUpdateTextEditingValue(
            delegate.textEditingValue.copyWith(selection: const TextSelection.collapsed(offset: 0)), SelectionChangedCause.toolbar);
      },
      toolbarItems: toolbarItems.asMap().entries.map(
        (MapEntry<int, TextSelectionToolbarItemData> entry) {
          return TextSelectionToolbarItemData(
            label: entry.value.label,
            onPressed: () => _handleClick(delegate, onPressed: entry.value.onPressed),
          );
        },
      ).toList(),
      // toolbarItems: toolbarItems,
    );
  }

  @override
  Widget buildHandle(BuildContext context, TextSelectionHandleType type, double textLineHeight,
      [VoidCallback? onTap, double? startGlyphHeight, double? endGlyphHeight]) {
    final Widget handle = Icon(
      Icons.location_on_rounded,
      size: _kHandleSize,
      color: selectionColor,
    );

    switch (type) {
      case TextSelectionHandleType.left: // points up-right
        return Transform.translate(
          offset: const Offset(_kHandleSize / 2, 0),
          child: Transform.rotate(
            // angle: math.pi / 4.0,
            angle: math.pi,
            child: handle,
          ),
        );
      case TextSelectionHandleType.right: // points up-left
        return Transform.translate(
          offset: const Offset(-_kHandleSize / 2, 0),
          child: Transform.rotate(
            // angle: -math.pi / 4.0,
            angle: math.pi,
            child: handle,
          ),
        );
      case TextSelectionHandleType.collapsed: // points up
        return handle;
    }
  }

  @override
  Offset getHandleAnchor(TextSelectionHandleType type, double textLineHeight, [double? startGlyphHeight, double? endGlyphHeight]) {
    switch (type) {
      case TextSelectionHandleType.left:
        return const Offset(_kHandleSize, 0);
      case TextSelectionHandleType.right:
        return Offset.zero;
      default:
        return const Offset(_kHandleSize / 2, -4);
    }
  }

  @override
  bool canSelectAll(TextSelectionDelegate delegate) {
    final TextEditingValue value = delegate.textEditingValue;
    return delegate.selectAllEnabled && value.text.isNotEmpty && !(value.selection.start == 0 && value.selection.end == value.text.length);
  }
}

class _TextSelectionControlsToolbar extends StatefulWidget {
  const _TextSelectionControlsToolbar({
    Key? key,
    required this.clipboardStatus,
    required this.delegate,
    required this.endpoints,
    required this.globalEditableRegion,
    required this.handleCut,
    required this.handleCopy,
    required this.handlePaste,
    required this.handleSelectAll,
    required this.selectionMidpoint,
    required this.textLineHeight,
    required this.handleLike,
    this.toolbarItems = _defToolbarItems,
  }) : super(key: key);

  final ClipboardStatusNotifier clipboardStatus;
  final TextSelectionDelegate delegate;
  final List<TextSelectionPoint> endpoints;
  final Rect globalEditableRegion;
  final VoidCallback? handleCut;
  final VoidCallback? handleCopy;
  final VoidCallback? handlePaste;
  final VoidCallback? handleSelectAll;
  final VoidCallback handleLike;
  final Offset selectionMidpoint;
  final double textLineHeight;
  final List<TextSelectionToolbarItemData> toolbarItems;

  @override
  _TextSelectionControlsToolbarState createState() => _TextSelectionControlsToolbarState();
}

class _TextSelectionControlsToolbarState extends State<_TextSelectionControlsToolbar> with TickerProviderStateMixin {
  void _onChangedClipboardStatus() {
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    widget.clipboardStatus.addListener(_onChangedClipboardStatus);
    widget.clipboardStatus.update();
  }

  @override
  void didUpdateWidget(_TextSelectionControlsToolbar oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.clipboardStatus != oldWidget.clipboardStatus) {
      widget.clipboardStatus.addListener(_onChangedClipboardStatus);
      oldWidget.clipboardStatus.removeListener(_onChangedClipboardStatus);
    }
    widget.clipboardStatus.update();
  }

  @override
  void dispose() {
    super.dispose();
    if (!widget.clipboardStatus.disposed) {
      widget.clipboardStatus.removeListener(_onChangedClipboardStatus);
    }
  }

  @override
  Widget build(BuildContext context) {
    // If there are no buttons to be shown, don't render anything.
    if (widget.handleCut == null &&
        widget.handleCopy == null &&
        widget.handlePaste == null &&
        widget.handleSelectAll == null &&
        widget.toolbarItems.isEmpty) {
      return const SizedBox.shrink();
    }
    if (widget.handlePaste != null && widget.clipboardStatus.value == ClipboardStatus.unknown) {
      return const SizedBox.shrink();
    }

    final TextSelectionPoint startTextSelectionPoint = widget.endpoints[0];
    final TextSelectionPoint endTextSelectionPoint = widget.endpoints.length > 1 ? widget.endpoints[1] : widget.endpoints[0];
    final Offset anchorAbove = Offset(widget.globalEditableRegion.left + widget.selectionMidpoint.dx,
        widget.globalEditableRegion.top + startTextSelectionPoint.point.dy - widget.textLineHeight - _kToolbarContentDistance);
    final Offset anchorBelow = Offset(
      widget.globalEditableRegion.left + widget.selectionMidpoint.dx,
      widget.globalEditableRegion.top + endTextSelectionPoint.point.dy + _kToolbarContentDistanceBelow,
    );

    assert(debugCheckHasMaterialLocalizations(context));
    final MaterialLocalizations localizations = MaterialLocalizations.of(context);
    List<TextSelectionToolbarItemData> itemDatas = <TextSelectionToolbarItemData>[
      if (widget.handleCut != null)
        TextSelectionToolbarItemData(
          label: localizations.cutButtonLabel,
          onPressed: widget.handleCut,
        ),
      if (widget.handleCopy != null)
        TextSelectionToolbarItemData(
          label: localizations.copyButtonLabel,
          onPressed: widget.handleCopy,
        ),
      if (widget.handlePaste != null && widget.clipboardStatus.value == ClipboardStatus.pasteable)
        TextSelectionToolbarItemData(
          label: localizations.pasteButtonLabel,
          onPressed: widget.handlePaste,
        ),
      if (widget.handleSelectAll != null)
        TextSelectionToolbarItemData(
          label: localizations.selectAllButtonLabel,
          onPressed: widget.handleSelectAll,
        ),
      // TextSelectionToolbarItemData(
      //   label: 'like',
      //   onPressed: widget.handleLike,
      // ),
    ];

    if (widget.toolbarItems.isNotEmpty) {
      itemDatas.addAll(widget.toolbarItems);
    }

    if (itemDatas.isEmpty) {
      return const SizedBox(width: 0.0, height: 0.0);
    }

    return TextSelectionToolbar(
      anchorAbove: anchorAbove,
      anchorBelow: anchorBelow,
      children: itemDatas.asMap().entries.map((MapEntry<int, TextSelectionToolbarItemData> entry) {
        return TextSelectionToolbarTextButton(
          padding: TextSelectionToolbarTextButton.getPadding(entry.key, itemDatas.length),
          onPressed: entry.value.onPressed,
          child: entry.value.label == 'like' ? const Icon(Icons.favorite) : Text(entry.value.label),
        );
      }).toList(),
    );
  }
}
