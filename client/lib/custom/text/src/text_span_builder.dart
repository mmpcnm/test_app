import 'dart:convert';

import 'package:extended_text_library/extended_text_library.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/material.dart';

// import 'at_text.dart';
// import 'at_text.dart';
import 'emoji_icon.dart';
// import 'emoji_text.dart';

class TextData {
  final String type;
  final int start;
  final String flag;
  final String endFlag;
  final dynamic data;

  const TextData(
      {required this.type,
      required this.start,
      required this.flag,
      required this.endFlag,
      required this.data});

  String get text {
    if (type == 'Emoji') {
      return flag + data + endFlag;
    } else if (type == '@') {
      return flag + (data as ATData).toString() + endFlag;
    } else {
      return data;
    }
  }

  InlineSpan getSpan({required TextStyle style, double iconSize = 26}) {
    if (type == 'Emoji') {
      return WidgetSpan(
          child: Image.asset(ResEmoji(data), width: iconSize.sp, height: iconSize.sp));
    } else if (type == '@') {
      return WidgetSpan(
        child: Container(
          color: Colors.blue.withOpacity(0.15),
          child: Text('@${data.nick} ', style: defTextStyle),
        ),
      );
    }
    return TextSpan(text: data, style: style);
  }
}

class ATData {
  Int64 userId;
  String nick;
  String avatarUrl;
  ATData({required this.userId, required this.nick, required this.avatarUrl});

  Map<String, dynamic> toJson() => {
        'userId': userId.toInt(),
        'nick': nick,
      };

  static ATData? form(String json) {
    try {
      var atMapData = jsonDecode(json);
      if (atMapData['nick'] != null && atMapData['userId'] != null) {
        int id = atMapData['userId'];
        return ATData(userId: Int64(id), nick: atMapData['nick'], avatarUrl: '');
      }
    } catch (e) {
      Debug.w('ATDialogUserData form error $json');
    }
    return null;
  }

  @override
  String toString() {
    return jsonEncode(toJson());
  }
}

//扩展
class TextSpanBuilder extends SpecialTextSpanBuilder {
  /// whether show background for @somebody
  bool showAtBackground;
  bool isAt;
  TextSpanBuilder({this.showAtBackground = false, this.isAt = false});

  @override
  TextSpan build(String data, {textStyle, onTap}) {
    // var textSpan = super.build(data, textStyle: textStyle, onTap: onTap);
    // return textSpan;
    // print('TextSpanBuilder build: ----$data-----');

    final List<InlineSpan> inlineList = <InlineSpan>[];
    List<TextData> items = decodeText(data);

    for (var i = 0; i < items.length; i++) {
      TextData textData = items[i];
      InlineSpan inlineSpan;
      if (textData.type == '@') {
        ATData atData = textData.data;
        inlineSpan = showAtBackground
            ? BackgroundTextSpan(
                background: Paint()..color = Colors.blue.withOpacity(0.15),
                text: '@${atData.nick}',
                actualText: textData.text,
                start: textData.start,
                deleteAll: true,
                style: textStyle,
              )
            : SpecialTextSpan(
                text: '@${atData.nick}',
                actualText: textData.text,
                start: textData.start,
                style: textStyle,
                deleteAll: true,
              );
      } else if (textData.type == 'Emoji') {
        double size = textStyle?.fontSize ?? defFontSize;
        inlineSpan = ImageSpan(
          AssetImage(ResEmoji(textData.data)),
          actualText: textData.text,
          imageWidth: size,
          imageHeight: size,
          start: textData.start,
          fit: BoxFit.scaleDown,
          // margin: EdgeInsets.symmetric(horizontal: SUT_W(1)),
        );
      } else {
        inlineSpan = TextSpan(text: textData.text, style: textStyle);
      }
      inlineList.add(inlineSpan);
    }

    return TextSpan(children: inlineList, style: textStyle);
  }

  @override
  SpecialText? createSpecialText(String flag,
      {TextStyle? textStyle, SpecialTextGestureTapCallback? onTap, required int index}) {
    return null;
  }

  // @override
  // SpecialText? createSpecialText(String flag, {TextStyle? textStyle, SpecialTextGestureTapCallback? onTap, required int index}) {
  //   if (flag.isEmpty) return null;
  //   print('debug 1 createSpecialText flag: $flag');

  //   ///index is end index of start flag, so text start index should be index-(flag.length-1)
  //   if (isAt && isStart(flag, AtText.flag)) {

  //     print('debug 2 createSpecialText AtText flag: $flag');
  //     return AtText(textStyle!, start: index, showAtBackground: showAtBackground);
  //   } else if (isStart(flag, EmojiText.flag)) {
  //     return EmojiText(textStyle!, start: index);
  //   }
  //   print('debug 3 createSpecialText flag: $flag');
  //   return null;
  // }

  ///解析文本(isEnter 是否容许换行)
  static List<TextData> decodeText(String value, {bool isEnter = true}) {
    List<TextData> ret = [];
    String text = value;
    if (!isEnter) {
      var split = value.split('\n');
      if (split.length == 1) {
        text = split[0];
      } else {
        text = split[0] + '...';
      }
    }

    //  !isEnter ? value.replaceAll('\n', '') : value;

    //标记查找的开始位置
    int _startTx = 0;

    //表情flag标记
    int _emojiIndex = -1;

    //@flag标记
    int _atIndex = -1;
    for (int index = 0; index < text.length; ++index) {
      if (text[index] == '[') {
        _emojiIndex = index;
      } else if (text[index] == ']') {
        if (_emojiIndex != -1) {
          int mjEnd = index;
          String select = text.substring(_emojiIndex + 1, mjEnd);
          if (EmojiData.containsKey(select)) {
            //添加表情前面的文本
            if (_emojiIndex > 0) {
              ret.add(TextData(
                  type: 'Text',
                  start: _startTx,
                  flag: '',
                  endFlag: '',
                  data: text.substring(_startTx, _emojiIndex)));
            }

            //添加表情
            ret.add(
                TextData(type: 'Emoji', start: _emojiIndex, flag: '[', endFlag: ']', data: select));

            //是表情删除AT标记
            _atIndex = -1;

            //记录查找坐标
            _startTx = index + 1;
          }
        }

        //清除表情标记
        _emojiIndex = -1;
      } else if (text[index] == '@') {
        //记录@标记
        _atIndex = index;
      } else if (text[index] == ' ') {
        if (_atIndex != -1) {
          int userIdEnd = index;
          String select = text.substring(_atIndex + 1, userIdEnd);
          try {
            ATData? atData = ATData.form(select);
            if (atData != null) {
              //添加@前面的文本
              if (_atIndex > 0) {
                ret.add(TextData(
                    type: 'Text',
                    start: _startTx,
                    flag: '',
                    endFlag: '',
                    data: text.substring(_startTx, _atIndex)));
              }

              //添加@用户
              ret.add(TextData(type: '@', start: _atIndex, flag: '@', endFlag: ' ', data: atData));

              //删除表情flag
              _emojiIndex = -1;

              //记录查找坐标
              _startTx = index + 1;
            }
          } catch (e) {
            Debug.e('解析 @ 玩家 $select');
          }
        }

        //清除@标记
        _atIndex = -1;
      }
    }

    if (_startTx < text.length) {
      String str = _startTx == 0 ? text : text.substring(_startTx);
      ret.add(TextData(type: 'Text', start: _startTx, flag: '', endFlag: '', data: str));
    }
    return ret;
  }
}
