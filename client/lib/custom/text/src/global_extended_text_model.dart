import 'package:fim_app/custom/text/src/extended_text_selection_controls.dart';
import 'package:fim_app/model/ModelBase.dart';

class GlobalExtendedTextModel extends ModelBase {
  static final GlobalExtendedTextModel _instance = GlobalExtendedTextModel._();
  GlobalExtendedTextModel._();
  factory GlobalExtendedTextModel() => _instance;

  // Key? _currentKey;
  // set currentKey(Key? key) {
  //   _currentKey;
  //   notifyListeners();
  // }

  // Key? get currentKey => _currentKey;

  ExtendedTextSelectionControls? _currentCentrols;
  set currentCentrols(ExtendedTextSelectionControls? _controls) {
    _currentCentrols = _controls;
    notifyListeners();
  }

  ExtendedTextSelectionControls? get currentCentrols => _currentCentrols;

  void setToolbarVisible(bool isVisible) {}
}
