import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:flutter/material.dart';

///表情对象
class Emoji {
  final String name;
  final String key;
  final int strKey;

  ///标识是否是emoji表情,true是,默认false
  final bool isEmoji;

  Emoji(this.strKey, this.name, this.key, {this.isEmoji = false});
}

///数据类
class EmojiData {
  static final EmojiData _instatnce = EmojiData();
  static EmojiData get instatnce => _instatnce;
  static int get count => _instatnce._emojiList.length;

  ///表情路径
  final List<Emoji> _emojiList = [
    Emoji(10052, '微笑', 'hehe'),
    Emoji(10053, '撇嘴', 'piezui'),
    Emoji(10054, '色', 'se'),
    Emoji(10055, '发呆', 'fadai'),
    Emoji(10056, '得意', 'deyi'),
    Emoji(10057, '流泪', 'liulei'),
    Emoji(10058, '害羞', 'haixiu'),
    Emoji(10059, '闭嘴', 'bizui'),
    Emoji(10060, '睡', 'shui'),
    Emoji(10061, '大哭', 'daku'),
    Emoji(10062, '尴尬', 'ganga'),
    Emoji(10063, '发怒', 'fanu'),
    Emoji(10064, '调皮', 'tiaopi'),
    Emoji(10065, '呲牙', 'ciya'),
    Emoji(10066, '惊讶', 'jingya'),
    Emoji(10067, '难过', 'nanguo'),
    Emoji(10068, '囧', 'jiong'),
    Emoji(10069, '抓狂', 'zhuakuang'),
    Emoji(10070, '吐', 'tu'),
    Emoji(10071, '偷笑', 'touxiao'),
    Emoji(10072, '愉快', 'yukuai'),
    Emoji(10073, '白眼', 'baiyan'),
    Emoji(10074, '傲慢', 'aoman'),
    Emoji(10075, '困', 'kun'),
    Emoji(10076, '惊恐', 'jingkong'),
    Emoji(10077, '流汗', 'liuhan'),
    Emoji(10078, '憨笑', 'hanxiao'),
    Emoji(10079, '悠闲', 'youxian'),
    Emoji(10080, '奋斗', 'fendou'),
    Emoji(10081, '咒骂', 'zhouma'),
    Emoji(10082, '疑问', 'yiwen'),
    Emoji(10083, '嘘', 'xu'),
    Emoji(10084, '晕', 'yun'),
    Emoji(10085, '衰', 'sui'),
    Emoji(10086, '骷髅', 'kulou'),
    Emoji(10087, '敲打', 'qiaoda'),
    Emoji(10088, '再见', 'zaininmadejian'),
    Emoji(10089, '擦汗', 'cahan'),
    Emoji(10090, '抠鼻', 'koubi'),
    Emoji(10091, '鼓掌', 'guzhang'),
    Emoji(10092, '坏笑', 'huaixiao'),
    Emoji(10093, '左哼哼', 'zuohengheng'),
    Emoji(10094, '右哼哼', 'youhengheng'),
    Emoji(10095, '哈欠', 'haqian'),
    Emoji(10096, '鄙视', 'bishi'),
    Emoji(10097, '委屈', 'weiqu'),
    Emoji(10098, '快哭了', 'kuaikule'),
    Emoji(10099, '阴险', 'yinxian'),
    Emoji(10100, '亲亲', 'qinqin'),
    Emoji(10101, '可怜', 'kelian'),
    Emoji(10102, '菜刀', 'caidao'),
    Emoji(10103, '西瓜', 'xigua'),
    Emoji(10104, '啤酒', 'pijiu'),
    Emoji(10105, '咖啡', 'kafei'),
    Emoji(10106, '猪头', 'zhutou'),
    Emoji(10107, '玫瑰', 'meigui'),
    Emoji(10108, '凋谢', 'diaoxie'),
    Emoji(10109, '嘴唇', 'zuichun'),
    Emoji(10110, '爱心', 'aixin'),
    Emoji(10111, '心碎', 'xinsui'),
    Emoji(10112, '蛋糕', 'dangao'),
    Emoji(10113, '炸弹', 'zhadan'),
    Emoji(10114, '便便', 'bianbian'),
    Emoji(10115, '月亮', 'yueliang'),
    Emoji(10116, '太阳', 'taiyang'),
    Emoji(10117, '拥抱', 'yongbao'),
    Emoji(10118, '强', 'qiang'),
    Emoji(10119, '弱', 'ruo'),
    Emoji(10120, '握手', 'woshou'),
    Emoji(10121, '胜利', 'shengli'),
    Emoji(10122, '抱拳', 'baoquan'),
    Emoji(10123, '勾引', 'gouyin'),
    Emoji(10124, '拳头', 'quantou'),
    Emoji(10125, 'OK', 'ok'),
    Emoji(10126, '跳跳', 'tiaotiao'),
    Emoji(10127, '发抖', 'fadou'),
    Emoji(10128, '怄火', 'ohuo'),
    Emoji(10129, '转圈', 'zhuanquan'),
    Emoji(10130, '嘿哈', 'heiha'),
    Emoji(10131, '捂脸', 'wulian'),
    Emoji(10132, '奸笑', 'jianxiao'),
    Emoji(10133, '机智', 'jizhi'),
    Emoji(10134, '皱眉', 'zhoumei'),
    Emoji(10135, '耶', 'ye'),
    Emoji(10136, '蜡烛', 'lazhu'),
    Emoji(10137, '红包', 'hongbao'),
    Emoji(10138, '吃瓜', 'chigua'),
    Emoji(10139, '加油', 'jiayou'),
    Emoji(10140, '汗', 'han'),
    Emoji(10141, '天啊', 'tiana'),
    Emoji(10142, 'Emm', 'emm'),
    Emoji(10143, '社会社会', 'shehuishehui'),
    Emoji(10144, '旺柴', 'wangchai'),
    Emoji(10145, '好的', 'haode'),
    Emoji(10146, '打脸', 'dalian'),
    Emoji(10147, '加油加油', 'jiayoujiayou'),
    Emoji(10148, '哇', 'wa'),
    Emoji(10149, '發', 'fa'),
    Emoji(10150, '福', 'fu'),
    Emoji(10151, '鸡', 'ji'),
    Emoji(10152, '眼睛', 'eyes'),
    Emoji(10153, '鲜花', 'rose'),
    Emoji(10154, '鼓掌', 'maleficeent'),
    Emoji(10155, '肌肉', 'muscle'),
    Emoji(10156, '出租车', 'taxi'),
    Emoji(10157, '礼物', 'gift'),
    Emoji(10158, '药丸', 'pill'),
    Emoji(10159, '啤酒', 'celebrate'),
    Emoji(10160, '鬼', 'ghost'),
    Emoji(10161, '庆祝', 'fireworks'),
    Emoji(10162, '气球', 'balloon'),
  ];
  final Map<String, Emoji> _emojiMap = {};

  EmojiData() {
    for (var item in _emojiList) {
      _emojiMap[item.key] = item;
    }
  }

  static Emoji atEmoji(int index) {
    return _instatnce._emojiList[index];
  }

  static bool containsKey(String key) {
    return instatnce._emojiMap.containsKey(key);
  }

  static Emoji? getEmoji(String key) {
    return instatnce._emojiMap[key];
  }
}

typedef EmojiCallBack = void Function(String key);

class EmojiIcon extends StatelessWidget {
  String emojiKey;
  double? size;
  EmojiCallBack? onPressed;
  EmojiCallBack? onLongPress;
  EdgeInsets? padinng;
  EmojiIcon(this.emojiKey, {this.size, this.padinng, this.onPressed, this.onLongPress});
  @override
  Widget build(BuildContext context) {
    double iconSize = size ?? 64.sp;

    return Button(
      onPressed: () {
        if (onPressed != null) {
          onPressed!(emojiKey);
        }
      },
      onLongPress: onLongPress != null
          ? () {
              if (onLongPress != null) {
                onLongPress!(emojiKey);
              }
            }
          : null,
      padding: EdgeInsets.zero,
      child: Image(width: iconSize, height: iconSize, image: AssetImage(ResEmoji(emojiKey))),
    );
  }
}
