import 'dart:ui' as ui;

import 'package:extended_text/extended_text.dart';
import 'package:fim_app/custom/text/src/global_extended_text_model.dart';
import 'package:fim_app/custom/text/src/extended_text_selection_controls.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

const List<TextSelectionToolbarItemData> _defToolbarItems = [];

class CustomExtendedText extends StatefulWidget {
  final TextOverflowWidget? overflowWidget;
  final ui.BoxHeightStyle selectionHeightStyle;
  final ui.BoxWidthStyle selectionWidthStyle;
  // final ExtendedTextSelectionControls? selectionControls;
  final DragStartBehavior dragStartBehavior;
  final Color? selectionColor;
  final GestureTapCallback? onTap;
  final bool selectionEnabled;
  final String data;
  final SpecialTextSpanBuilder? specialTextSpanBuilder;
  final SpecialTextGestureTapCallback? onSpecialTextTap;
  // final InlineSpan? textSpan;
  final TextStyle? style;
  final StrutStyle? strutStyle;
  final TextAlign? textAlign;
  final TextDirection? textDirection;
  final Locale? locale;
  final bool? softWrap;
  final TextOverflow? overflow;
  final double? textScaleFactor;
  final int? maxLines;
  final String? semanticsLabel;
  final TextWidthBasis? textWidthBasis;
  final ui.TextHeightBehavior? textHeightBehavior;
  final bool joinZeroWidthSpace;
  final ui.Color selectToolbarColor;
  final List<TextSelectionToolbarItemData> toolbarItems;

  const CustomExtendedText(
    this.data, {
    required Key key,
    this.style,
    this.strutStyle,
    this.textAlign,
    this.textDirection,
    this.locale,
    this.softWrap,
    this.overflow,
    this.textScaleFactor,
    this.maxLines,
    this.semanticsLabel,
    this.textWidthBasis,
    this.textHeightBehavior,
    this.specialTextSpanBuilder,
    this.onSpecialTextTap,
    this.selectionEnabled = false,
    this.onTap,
    this.selectionColor,
    this.dragStartBehavior = DragStartBehavior.start,
    // this.selectionControls,
    this.selectionHeightStyle = ui.BoxHeightStyle.tight,
    this.selectionWidthStyle = ui.BoxWidthStyle.tight,
    this.overflowWidget,
    this.joinZeroWidthSpace = false,
    this.selectToolbarColor = Colors.blue,
    this.toolbarItems = _defToolbarItems,
  }) : super(key: key);
  @override
  State<StatefulWidget> createState() => _CustomExtendedText();
}

class _CustomExtendedText extends State<CustomExtendedText> {
  ExtendedTextSelectionControls? _selectionControls;

  @override
  void initState() {
    super.initState();
    _selectionControls = ExtendedTextSelectionControls(
      key: widget.key,
      joinZeroWidthSpace: widget.joinZeroWidthSpace,
      selectionColor: widget.selectToolbarColor,
      toolbarItems: widget.toolbarItems,
    );
    GlobalExtendedTextModel().addListener(_onListener);
  }

  @override
  void dispose() {
    GlobalExtendedTextModel().removeListener(_onListener);
    super.dispose();
  }

  void _onListener() {
    var currentCentrols = GlobalExtendedTextModel().currentCentrols;
    if (currentCentrols == null || currentCentrols != _selectionControls) {
      print('debug CustomExtendedText _onListener ohter');
      _selectionControls?.hideToolbar();
    } else {
      print('debug CustomExtendedText _onListener self');
    }
  }

  @override
  Widget build(BuildContext context) {
    return ExtendedText(
      widget.data,
      key: widget.key,
      style: widget.style,
      strutStyle: widget.strutStyle,
      textAlign: widget.textAlign,
      textDirection: widget.textDirection,
      locale: widget.locale,
      softWrap: widget.softWrap,
      overflow: widget.overflow,
      textScaleFactor: widget.textScaleFactor,
      maxLines: widget.maxLines,
      semanticsLabel: widget.semanticsLabel,
      textWidthBasis: widget.textWidthBasis,
      textHeightBehavior: widget.textHeightBehavior,
      specialTextSpanBuilder: widget.specialTextSpanBuilder,
      onSpecialTextTap: widget.onSpecialTextTap,
      selectionEnabled: widget.selectionEnabled,
      onTap: widget.onTap,
      selectionColor: widget.selectionColor,
      dragStartBehavior: widget.dragStartBehavior,
      selectionControls: _selectionControls,
      selectionHeightStyle: widget.selectionHeightStyle,
      selectionWidthStyle: widget.selectionWidthStyle,
      overflowWidget: widget.overflowWidget,
      joinZeroWidthSpace: widget.joinZeroWidthSpace,
    );
  }
}
