import 'dart:io';
import 'dart:ui' as ui;

import 'package:fim_app/configs/api.dart';
import 'package:fim_app/package/net/http/http_utils.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

class ImageUtils {
  /// 选取多张图片
  static pickMultiImage({void Function(List<XFile> images)? onCallBack}) async {
    final ImagePicker _picker = ImagePicker();
    final List<XFile>? images = await _picker.pickMultiImage();
    if (onCallBack != null) {
      onCallBack.call(images ?? []);
    }
    return images;
  }

  /// 选取一张照片
  static pickImage({required ImageSource source, void Function(XFile? image)? onCallBack}) async {
    final ImagePicker _picker = ImagePicker();
    XFile? image = await _picker.pickImage(source: source);
    if (onCallBack != null) {
      onCallBack.call(image);
    }
    return image;
  }

  /// 选取一个视频
  static pickVideo({required ImageSource source, void Function(XFile? video)? onCallBack}) async {
    final ImagePicker _picker = ImagePicker();
    XFile? video = await _picker.pickVideo(source: source);
    if (onCallBack != null) {
      onCallBack.call(video);
    }
    return video;
  }

  /// 图片剪裁
  static cropImage({required String filePath, bool circle = false, void Function(File? croppedFile)? onCallBack}) async {
    File? croppedFile = await ImageCropper.cropImage(
      // maxWidth: SUT_W(500).toInt(),
      // maxHeight: SUT_H(500).toInt(),
      sourcePath: filePath,
      cropStyle: circle ? CropStyle.circle : CropStyle.rectangle,
      // compressQuality: 100,
      aspectRatioPresets: [CropAspectRatioPreset.original],
      // aspectRatioPresets: [
      //   CropAspectRatioPreset.square,
      //   CropAspectRatioPreset.ratio3x2,
      //   CropAspectRatioPreset.original,
      //   CropAspectRatioPreset.ratio4x3,
      //   CropAspectRatioPreset.ratio16x9
      // ],
      androidUiSettings: AndroidUiSettings(
          // toolbarTitle: 'Cropper',
          toolbarColor: Colors.deepOrange,
          toolbarWidgetColor: Colors.white,
          // initAspectRatio: CropAspectRatioPreset.original,
          lockAspectRatio: false),
      iosUiSettings: IOSUiSettings(
        minimumAspectRatio: 1.0,
      ),
    );

    if (onCallBack != null) {
      onCallBack.call(croppedFile);
    }
    return croppedFile;
  }

  /// 选择并且剪切图片
  static pickAndCropImage({ImageSource source = ImageSource.gallery, void Function(File? croppedFile)? onCallBack}) async {
    await ImageUtils.pickImage(
      source: source,
      onCallBack: (xfile) {
        if (xfile != null) {
          ImageUtils.cropImage(filePath: xfile.path, onCallBack: onCallBack);
        }
      },
    );
  }

  /// 选择并且剪切图片上传到服务器
  static pickAndCropImageUpService(
      {ImageSource source = ImageSource.gallery, void Function(String url, double widht, double height)? onSucceed, void Function(String errorCode)? onError}) async {
    XFile? xFile = await ImageUtils.pickImage(
      source: source,
    );
    if (xFile != null) {
      File? file = await ImageUtils.cropImage(filePath: xFile.path);
      if (file != null) {
        Image image = Image.file(file);
        double fileSize = (await file.length()).toDouble();
        print('pickAndCropImageUpService fileSize: $fileSize');
        // if(fileSize / 1024 / 1024 >  )  {

        // }
        image.image.resolve(ImageConfiguration()).addListener(
              ImageStreamListener(
                (ImageInfo info, bool _) {
                  final imageWidth = info.image.width.toDouble();
                  final imageHeight = info.image.height.toDouble();
                  print('Image size{width:${imageWidth}, height:${imageHeight}}');
                  onSucceed?.call(file.path, imageWidth, imageHeight);
                  // HttpUtils.upMultipartFile(
                  //   requestUrl: AppConfig.apiUrl,
                  //   filePath: file.path,
                  //   onSucceed: (dynamic response) {
                  //     if (response['code'] == 0 && response['data'] != null) {
                  //       String url = response['data']['url'];
                  //       if (onSucceed != null) onSucceed.call(url, imageWidth, imageHeight);
                  //       // pb.Voice voice = pb.Voice(url: response['data']['url'], duration: (duration * 1000).toInt());
                  //       // sendMessage(context, voice.writeToBuffer());
                  //     } else {
                  //       // EasyLoading.showToast('${STR(10197)}：${response['code']}');
                  //       EasyLoading.showError('图片上传失败！');
                  //     }

                  //     // if (response['code'] == 1 && response['data'] != null) {
                  //     //   if (onSucceed != null) onSucceed.call(response['data'], imageWidth, imageHeight);
                  //     // } else {
                  //     //   // if (onError != null) onError.call('无数据');
                  //     //   EasyLoading.showError('图片上传失败！');
                  //     // }
                  //   },
                  //   onError: onError,
                  // );
                },
                onError: (exception, stackTrace) {
                  // if (onError != null) onError('未知错误！');
                },
              ),
            );

        return;
      }
    }

    // if (onError != null) onError('未知错误！');
  }
}
