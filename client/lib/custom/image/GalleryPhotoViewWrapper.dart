// ignore_for_file: must_be_immutable, no_logic_in_create_state, file_names

import 'dart:math';
import 'dart:io' as io;

import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/custom/image/NetworkImage.dart' as custom;
import 'package:fim_app/custom/wdiget/custom_list_table.dart';
import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/package/util/screen_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:one_context/one_context.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

class GalleryPhotoViewWrapper extends StatefulWidget {
  List<pb.Image> mImageUrls;
  int mInitialIndex;
  GalleryPhotoViewWrapper({Key? key, this.mInitialIndex = 0, required this.mImageUrls}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _GalleryPhotoViewWrapper(mCurrentIndex: mInitialIndex);
}

class _GalleryPhotoViewWrapper extends State<GalleryPhotoViewWrapper> {
  int mCurrentIndex;
  _GalleryPhotoViewWrapper({required this.mCurrentIndex});

  Future saveImage() async {
    io.File file = await DefaultCacheManager().getSingleFile(widget.mImageUrls[mCurrentIndex].url);
    await ImageGallerySaver.saveImage(file.readAsBytesSync());
    //'图片已保存至相册'
    EasyLoading.showToast(STR(10170), toastPosition: EasyLoadingToastPosition.center);
  }

  @override
  Widget build(BuildContext context) {
    // int count = widget.mImageUrls.length;
    // int currentIndex = min(mCurrentIndex + 1, count);

    return Scaffold(
      backgroundColor: Colors.black,
      body: Stack(
        alignment: AlignmentDirectional.topCenter,
        children: [
          Positioned(
            top: ScreenUtil.statusBarHeight,
            right: 0,
            left: 0,
            bottom: ScreenUtil.bottomBarHeight,
            child: Container(
              color: Colors.black,
              constraints: BoxConstraints(maxHeight: SUT_S_HEIGHT, maxWidth: SUT_S_WIDTH),
              child: PhotoViewGallery.builder(
                itemCount: widget.mImageUrls.length,
                scrollPhysics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                builder: (BuildContext context, int index) {
                  Debug.d('${widget.mImageUrls[index]}');
                  // return PhotoViewGalleryPageOptions.customChild(
                  //   // initialScale: 1.0,
                  //   minScale: 0.3,
                  //   maxScale: 3.0,
                  //   filterQuality: FilterQuality.high,
                  //   child: Image.network(
                  //     widget.mImageUrls[index],
                  //     fit: BoxFit.contain,
                  //     // filterQuality: FilterQuality.high,
                  //   ),
                  // );
                  pb.Image image = widget.mImageUrls[index];
                  double maxWidth = SUT_S_WIDTH - 2.sp;
                  double maxHeight = SUT_S_HEIGHT - ScreenUtil.bottomBarHeight - ScreenUtil.statusBarHeight;
                  double initialScale = min(1.0, min(maxWidth / image.width, maxHeight / image.height));

                  double minScale = initialScale * 0.3;
                  double maxScale = initialScale * 3.0;

                  return PhotoViewGalleryPageOptions(
                    initialScale: initialScale,
                    minScale: minScale,
                    maxScale: maxScale,
                    // filterQuality: FilterQuality.medium,
                    imageProvider: custom.NetworkImage(image.url), //NetImage.netImageProvider(image.url),
                    onTapUp: (BuildContext context, TapUpDetails tapUpDetails, PhotoViewControllerValue controllerValue) {
                      OneContext().pop();
                    },
                  );
                },
                loadingBuilder: (context, event) => Center(
                  child: SizedBox(
                    width: 50.sp,
                    height: 50.sp,
                    child: CircularProgressIndicator(
                      value: event == null ? 0 : event.cumulativeBytesLoaded / (event.expectedTotalBytes ?? 1),
                    ),
                  ),
                ),
                // backgroundDecoration: widget.backgroundDecoration,
                // pageController: widget.pageController,
                pageController: PageController(initialPage: mCurrentIndex),
                onPageChanged: (int index) {
                  setState(() {
                    mCurrentIndex = index;
                  });
                },
              ),
            ),
          ),
          Positioned(
            top: ScreenUtil.statusBarHeight,
            right: 0,
            left: 0,
            child: SizedBox(
              height: 78.sp,
              child: CustomListTile(
                leading: Button(
                  child: Icon(Icons.arrow_back_ios, color: Colors.white, size: defIconSize),
                  padding: const EdgeInsets.all(0),
                  onPressed: () => OneContext().pop(),
                ),
                middletitle: Text(
                  '${min(mCurrentIndex + 1, widget.mImageUrls.length)}/${widget.mImageUrls.length}',
                  style: TextStyle(color: Colors.white, fontSize: defFontSize),
                ),
              ),
            ),
          ),
          Positioned(
            bottom: ScreenUtil.bottomBarHeight,
            right: 0,
            left: 0,
            child: SizedBox(
              height: 78.sp,
              child: CustomListTile(
                middletitle: Text(
                  '${widget.mImageUrls[mCurrentIndex].width}x${widget.mImageUrls[mCurrentIndex].height}',
                  style: TextStyle(color: Colors.white, fontSize: defFontSize),
                ),
                trailing: Button(
                  padding: EdgeInsets.all(5.sp),
                  child: Icon(Icons.file_download_outlined,
                      color: whiteColor, size: defIconSize), //Text("保存", style: TextStyle(color: whiteColor, fontSize: defFontSize)),
                  onPressed: () async {
                    saveImage();
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
