import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/custom_popup_menu/im_popup_menu.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/pages/group/new_group_page.dart';
import 'package:fim_app/pages/scan/ScanCodePage.dart';
import 'package:fim_app/pages/search/search_page.dart';
import 'package:flutter/material.dart';

class HomeTitlePop extends StatelessWidget {
  const HomeTitlePop({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ImPopUpMenu(
      axis: Axis.vertical,
      axisSpacing: defPaddingSize,
      color: defBackgroundColor,
      paninng: EdgeInsets.all(25.sp),
      icon: Icon(Icons.add_circle_outline, size: defIconSize, color: blackColor),
      items: <ImPopupItemModel>[
        //添加好友
        ImPopupItemModel(
          icon: Icons.group_add_outlined,
          text: '  ${STR(10033)}',
          onPressed: () => SearchPage.showSearchPage(),
        ),
        //发起群聊
        ImPopupItemModel(
          icon: Icons.chat_bubble_outline,
          text: '  ${STR(10164)}',
          onPressed: () => CustomRoute().pushPage(NewGroupPage()),
        ),
        //扫一扫
        ImPopupItemModel(
          icon: Icons.qr_code_scanner_outlined,
          text: '  ${STR(10226)}',
          onPressed: () => CustomRoute().pushPage(const ScanCodePage()),
        ),
      ],
    );
  }
}
