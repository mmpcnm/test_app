// ignore_for_file: use_key_in_widget_constructors

import 'package:custom_pop_up_menu/custom_pop_up_menu.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:flutter/material.dart';

class ImPopupItemModel {
  // final T value;
  final String text;
  final IconData? icon;
  final double? iconSize;
  final double? fontSize;
  final Color? fontColor;
  final VoidCallback? onPressed;

  const ImPopupItemModel(
      {
      // required this.value,
      required this.text,
      this.fontSize,
      this.fontColor,
      this.icon,
      this.iconSize,
      this.onPressed});
}

class ImPopUpMenu extends StatefulWidget {
  final Widget icon;
  final PressType pressType;
  final Color? color;
  final Axis axis;
  final double axisSpacing;
  final bool showArrow;
  final EdgeInsets? paninng;
  final List<ImPopupItemModel> items;
  // final void Function(T value)? onSelectTap;

  const ImPopUpMenu({
    required this.icon,
    this.pressType = PressType.singleClick,
    this.color,
    this.axis = Axis.vertical,
    this.axisSpacing = 5,
    this.paninng,
    this.showArrow = true,
    required this.items,
    // this.onSelectTap
  });

  @override
  State<StatefulWidget> createState() {
    return _ImPopUpMenu();
  }
}

class _ImPopUpMenu extends State<ImPopUpMenu> {
  final CustomPopupMenuController _controller = CustomPopupMenuController();

  List<Widget> _buildChildren() {
    List<Widget> children = [];
    double spacing = 0;
    for (int i = 0; i < widget.items.length; ++i) {
      children.add(
        Container(
          padding: widget.axis == Axis.horizontal ? const EdgeInsets.only(left: 0) : EdgeInsets.only(top: spacing),
          child: _ImPopupItem(
            widget.items[i],
            onPressed: (item) {
              item.onPressed?.call();
              _controller.hideMenu();
            },
          ),
        ),
      );
      spacing = widget.axisSpacing;
      if (widget.axis == Axis.horizontal && i < widget.items.length - 1) {
        children.add(SizedBox(height: 30.h, child: VerticalDivider(width: spacing, color: Colors.black)));
      }
    }

    return children;
  }

  @override
  Widget build(BuildContext context) {
    return CustomPopupMenu(
      arrowColor: widget.color ?? grayColor,
      child: widget.icon,
      showArrow: widget.showArrow,
      menuBuilder: () => Card(
        color: widget.color ?? grayColor,
        shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5))),
        margin: const EdgeInsets.all(0),
        // padding: paninng ?? EdgeInsets.all(SUT_W(15)),
        child: Container(
          padding: widget.paninng ?? EdgeInsets.all(SUT_W(15)),
          child: widget.axis == Axis.horizontal
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: _buildChildren(),
                )
              : Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: _buildChildren(),
                ),
        ),
      ),
      pressType: widget.pressType,
      verticalMargin: widget.axis == Axis.vertical ? 0 : 0,
      horizontalMargin: widget.axis == Axis.horizontal ? 0 : 10,
      controller: _controller,
    );
  }
}

class _ImPopupItem extends StatelessWidget {
  final ImPopupItemModel item;
  final void Function(ImPopupItemModel item)? onPressed;

  const _ImPopupItem(this.item, {required this.onPressed});

  @override
  Widget build(BuildContext context) {
    List<InlineSpan> children = [];

    if (item.icon != null) {
      children.add(WidgetSpan(child: Icon(item.icon, size: item.iconSize ?? defIconSize), alignment: PlaceholderAlignment.middle));
    }

    children.add(TextSpan(text: item.text, style: TextStyle(fontSize: item.fontSize ?? defFontSize, color: item.fontColor ?? defFontColor)));

    return InkWell(
      child: Text.rich(TextSpan(children: children)),
      onTap: onPressed == null ? null : () => onPressed?.call(item),
    );
  }
}
