// ignore_for_file: use_key_in_widget_constructors, file_names, unused_element

import 'package:flutter/material.dart';

abstract class StatefulWidgetBase extends StatefulWidget {
  const StatefulWidgetBase({Key? key}) : super(key: key);
}

abstract class StatelessWidgetBase extends StatelessWidget {
  const StatelessWidgetBase({Key? key}) : super(key: key);
}
