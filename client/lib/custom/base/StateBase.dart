// ignore_for_file: file_names

import 'package:flutter/cupertino.dart';
// import 'package:flutter/services.dart';

abstract class StateBase<T extends StatefulWidget> extends State<T> {
  @override
  void activate() {
    super.activate();
    print('StateBase activate');
  }

  @override
  void deactivate() {
    super.deactivate();
    print('StateBase deactivate');
  }

  @override
  void initState() {
    super.initState();
    // WidgetsBinding.instance?.addObserver(this);
    print('StateBase initState');
  }

  // @override
  // void didChangeAppLifecycleState(AppLifecycleState state) {
  //   super.didChangeAppLifecycleState(state);
  //   // SystemChannels.lifecycle;
  //   print('StateBase didChangeAppLifecycleState $state');
  // }

  @override
  void didUpdateWidget(covariant T oldWidget) {
    super.didUpdateWidget(oldWidget);
    print('StateBase didUpdateWidget');
  }

  @override
  void reassemble() {
    super.reassemble();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print('StateBase didChangeDependencies');
  }
}
