// ignore_for_file: constant_identifier_names, file_names

import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:one_context/one_context.dart';

// import 'package:flutter_picker/flutter_picker.dart' as picker;
// double get _buttonHeight => 82.sp;
const double _dialogTitleFontSize = 30;
const double _dialogMessageFontSize = 26;
const double _dialogButtonFontSize = 28;

enum MessageBoxAlignment {
  Center,
  Top,
  Bottom,
}

Widget _buildText(String text, {required TextStyle style}) {
  return DefaultTextStyle(
    style: style,
    child: Semantics(
      child: Text(text),
      namesRoute: true,
      container: true,
    ),
  );
}

class MessageBoxButton {
  String text;
  Color textColor;
  VoidCallback? onPressed;
  MessageBoxButton(this.text, {this.textColor = Colors.black, this.onPressed}) {
    VoidCallback? _callback = onPressed;
    if (_callback != null) {
      onPressed = () {
        OneContext().popDialog();
        _callback.call();
      };
    } else {
      onPressed = _popDialog;
    }
  }
  static void _popDialog() => OneContext().popDialog();
}

void showMessageDialogEx(BuildContext context,
    {String? title,
    String content = '',
    bool barrierDismissible = true,
    VoidCallback? confirmCallback}) {
  showMessageBox(
    title: title,
    content: content,
    barrierDismissible: barrierDismissible,
    buttons: [
      MessageBoxButton(STR(10032)),
      MessageBoxButton(STR(10186), onPressed: confirmCallback),
    ],
  );
}

void showMessageDialog(BuildContext context,
    {String? title, String content = '', bool barrierDismissible = true, VoidCallback? onPressed}) {
  showMessageBox(
    title: title,
    content: content,
    barrierDismissible: barrierDismissible,
    buttons: [
      MessageBoxButton(STR(10186), onPressed: onPressed),
    ],
  );
}

void showMessageBox({
  String? title,
  String content = '',
  MessageBoxAlignment alignment = MessageBoxAlignment.Center,
  bool barrierDismissible = true,
  List<MessageBoxButton>? buttons,
}) {
  switch (alignment) {
    case MessageBoxAlignment.Top:
      _showMessageBox(
          title: title,
          content: content,
          alignment: Alignment.topCenter,
          barrierDismissible: barrierDismissible,
          buttons: buttons);
      break;
    case MessageBoxAlignment.Center:
      _showMessageBox(
          title: title,
          content: content,
          alignment: Alignment.center,
          barrierDismissible: barrierDismissible,
          buttons: buttons);
      break;
    case MessageBoxAlignment.Bottom:
      _showBottomMessageBox(
          title: title, content: content, barrierDismissible: barrierDismissible, buttons: buttons);
      break;
    default:
      _showMessageBox(
          title: title,
          content: content,
          alignment: Alignment.center,
          barrierDismissible: barrierDismissible,
          buttons: buttons);
  }
}

void _showMessageBox(
    {String? title,
    String content = '',
    Alignment alignment = Alignment.center,
    bool barrierDismissible = true,
    List<MessageBoxButton>? buttons}) {
  print('_showCenterMessageBox');
  List<CupertinoActionSheetAction> actions = [];
  if (buttons != null && buttons.isNotEmpty) {
    for (var i = 0; i < buttons.length; i++) {
      var item = buttons[i];
      actions.add(CupertinoActionSheetAction(
        onPressed: item.onPressed!,
        child: _buildText(
          item.text,
          style: TextStyle(
            color: item.textColor,
            fontSize: _dialogButtonFontSize.sp,
          ),
        ),
      ));
    }
  }
  OneContext().showDialog(
    barrierDismissible: barrierDismissible,
    builder: (context) {
      return CupertinoAlertDialog(
        title: Center(
          child: _buildText(
            title ?? STR(10185),
            style: TextStyle(
              fontSize: _dialogTitleFontSize.sp,
              color: defFontColor,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        content: Center(
          child: _buildText(
            content,
            style: TextStyle(fontSize: _dialogMessageFontSize.sp, color: defFontColor),
          ),
        ),
        actions: actions,
      );
    },
  );
}

void _showBottomMessageBox(
    {String? title,
    String content = '',
    bool barrierDismissible = true,
    List<MessageBoxButton>? buttons}) {
  print('_showBottomMessageBox');
  List<CupertinoActionSheetAction> actions = [];
  if (buttons != null && buttons.isNotEmpty) {
    for (var i = 0; i < buttons.length; i++) {
      var item = buttons[i];
      actions.add(CupertinoActionSheetAction(
        onPressed: item.onPressed!,
        child: _buildText(
          item.text,
          style: TextStyle(
            color: item.textColor,
            fontSize: _dialogButtonFontSize.sp,
          ),
        ),
      ));
    }
  }

  OneContext().showModalBottomSheet(
    backgroundColor: const Color(0x00FFFFFF),
    isScrollControlled: true,
    isDismissible: barrierDismissible,
    builder: (BuildContext context) {
      return CupertinoActionSheet(
        title: title != null
            ? _buildText(
                title,
                style: TextStyle(
                  fontSize: _dialogTitleFontSize.sp,
                  color: defFontColor,
                  fontWeight: FontWeight.bold,
                ),
              )
            : null,
        message: _buildText(
          content,
          style: TextStyle(
            fontSize: _dialogMessageFontSize.sp,
            color: defFontColor,
          ),
        ),
        actions: actions,
      );
    },
  );
}

void showLoding(String text,
    {Duration? duration, EasyLoadingMaskType maskType = EasyLoadingMaskType.clear}) {
  if (text.isNotEmpty) {
    EasyLoading.show(status: text, maskType: EasyLoadingMaskType.clear);
  } else {
    EasyLoading.show(maskType: EasyLoadingMaskType.clear);
  }
  if (null != duration) {
    Future.delayed(duration, () {
      EasyLoading.dismiss();
    });
  }
}
