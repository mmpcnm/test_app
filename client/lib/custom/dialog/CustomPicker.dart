import 'dart:convert';
import 'dart:math';

import 'package:fim_app/configs/const_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_picker/Picker.dart';

enum PickerDateType {
  /// 1976年1月
  YM,

  /// 1976年1月1日
  YMD,
}

class PickerSuffix {
  final String year;
  final String month;
  final String day;
  const PickerSuffix({this.year = '', this.month = '', this.day = ''});
}

class CustomPicker {
  ///必须 (topTime < bottomTime == true)
  static void showDatePicker(
    BuildContext context, {
    String? cancelText,
    String? confirmText,
    PickerDateType dateType = PickerDateType.YM,
    PickerSuffix suffix = const PickerSuffix(year: '/', month: '/', day: ''),
    DateTime? topTime,
    DateTime? bottomTime,
    DateTime? selectDate,
    void Function(List<int> values)? onConfirm,
  }) {
    topTime = topTime ?? DateTime(2021, 10, 1, 0, 0);
    bottomTime = bottomTime ?? DateTime.now();
    print(topTime.toString());
    print(bottomTime.toString());
    assert(topTime.millisecondsSinceEpoch < bottomTime.millisecondsSinceEpoch, 'topTime < bottomTime != true');
    List<int> selIndexs = [0, 0, 0];
    selectDate = selectDate ?? bottomTime;

    List<dynamic> years = [];

    ///年
    for (int year = topTime.year; year <= bottomTime.year; ++year) {
      if (selectDate.year == year) {
        selIndexs[0] = year - topTime.year;
      }
      List<dynamic> months = [];
      int monthStart = 1;
      int monthEnd = 12;

      if (topTime.year == year) {
        monthStart = topTime.month;
      }
      if (bottomTime.year == year) {
        if (topTime.year == year) {
          monthStart = topTime.month;
          monthEnd = max(bottomTime.month, topTime.month);
        } else {
          monthStart = 1;
          monthEnd = bottomTime.month;
        }
      }

      ///月
      for (int month = monthStart; month <= monthEnd; ++month) {
        if (selectDate.year == year && selectDate.month == month) {
          selIndexs[1] = month - monthStart;
        }
        if (dateType == PickerDateType.YMD) {
          List<dynamic> days = [];

          int millisecondsSinceEpoch = DateTime(year, month + 1, 1).millisecondsSinceEpoch - 60000 * 60;
          DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(millisecondsSinceEpoch);

          int dayStart = 1;
          int dayEnd = dateTime.day;

          if (topTime.year == year && topTime.month == month) {
            dayStart = topTime.day;
          }

          if (bottomTime.year == year && bottomTime.month == bottomTime.month) {
            if (topTime.year == year && topTime.month == month) {
              dayStart = topTime.day;
            } else {
              dayStart = 1;
            }
          }

          ///日
          for (int day = dayStart; day <= dayEnd; ++day) {
            if (selectDate.year == year && selectDate.month == month && selectDate.day == day) {
              selIndexs[2] = day - dayStart;
            }
            days.add('${day}${suffix.day}');
          }
          months.add({'${month}${suffix.month}': days});
        } else {
          months.add('${month}${suffix.month}');
        }
      }

      years.add({'${year}${suffix.year}': months});
    }

    print(jsonEncode(years));

    List<int> selecteds = [];
    if (dateType == PickerDateType.YM) {
      selecteds = [selIndexs[0], selIndexs[1]];
    } else if (dateType == PickerDateType.YMD) {
      selecteds = [selIndexs[0], selIndexs[1], selIndexs[1]];
    }
    // print(selecteds);
    Picker(
      adapter: PickerDataAdapter<String>(
        pickerdata: years,
        isArray: false,
      ),
      selecteds: selecteds,
      changeToFirst: true,
      cancelText: cancelText ?? '取消',
      cancelTextStyle: defTextStyle,
      confirmText: confirmText ?? '确定',
      confirmTextStyle: defTextStyle,
      textStyle: defTextStyle,
      onSelect: (Picker picker, int index, List<int> selcet) {},
      onConfirm: (Picker picker, List value) {
        // print(value.toString());
        List<dynamic> selectValues = picker.getSelectedValues();
        // print(selectValues);
        List<int> values = [];
        try {
          if (selectValues.length > 0) {
            int year = int.parse(selectValues[0].replaceAll(suffix.year, ''));
            values.add(year);
          }
          if (selectValues.length > 1) {
            int month = int.parse(selectValues[1].replaceAll(suffix.month, ''));
            values.add(month);
          }
          if (selectValues.length > 2) {
            int day = int.parse(selectValues[2].replaceAll(suffix.day, ''));
            values.add(day);
          }
        } catch (e) {
          values = [];
        }

        if (onConfirm != null) onConfirm.call(values);
      },
    ).showModal(context);
  }
}
