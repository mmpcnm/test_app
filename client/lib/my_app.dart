import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/LocalNotifications/LocalNotifications.dart';
import 'package:fim_app/package/localizations/language_delegate.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/pages/loading/init_page.dart';
import 'package:fim_app/model/device_info.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/preferences/prefers.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/material.dart';
import 'package:one_context/one_context.dart';

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyApp();
  }
}

class _MyApp extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    //获取设备信息
    DeviceInfo.init();

    //读取本地化语言表
    STRManager.initConfig();

    //读取用户存储信息
    Prefers.init();

    //初始化应用路径
    Res.initPath();
    LocalNotifications.instance.init();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: DeviceInfo.appName,
      theme: ThemeData(
        brightness: Brightness.light,
      ),
      navigatorKey: OneContext().key,
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        LanguageDelegate(),
      ],
      // supportedLocales: const [
      //   Locale('zh', 'CN'), // 简体中文
      //   Locale('en', 'US'), // 美国英语
      // ],
      home: InitPage(),
      // routes: {
      //   '/': (context) {
      //     return InitPage();
      //   }
      // },
      // onGenerateRoute: (RouteSettings settings) {
      //   print('\x1B[31m:onGenerateRoute name: ${settings.name}\x1B[0m');
      // },
      // onUnknownRoute: (RouteSettings settings) {
      //   print('\x1B[31m:onUnknownRoute name: ${settings.name}\x1B[0m');
      // },
      navigatorObservers: <NavigatorObserver>[
        // CustomNavigatorObserver(),
        CustomRoute(),
      ],
      builder: EasyLoading.init(builder: OneContext().builder),
    );
  }
}
