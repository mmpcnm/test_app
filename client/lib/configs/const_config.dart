import 'package:fim_app/package/util/screen_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

int get passwordMinLength => 1;
int get passwordMaxLength => 8;

int get accountMinLength => 6;
int get accountMaxLength => 16;

const int nickMinLen = 3;
const int nickMaxLen = 8;

///26.sp
double get defFontSize => 26.sp;

///42.sp
final double defIconSize = 42.sp;

///78.sp
final double defAvatarSize = 78.sp;
double get defAvatarCircular => defAvatarSize * 0.1;

///24.sp
double get defMinFontSize => 22.sp;
// const double _defMinFontSize = 22;

///默认边距与间距 15.sp
double get defPaddingSize => 15.sp;
double get defPaddingSizeH => 15.sp;
double get defPaddingSizeV => 10.sp;

double get defTabelHeight => 78.sp;

double get defTabelSplitHeight => 30.sp;

//colors
Color get defBackgroundColor => Colors.grey.shade200;

Color get defBtnColor => Colors.grey.shade800;
Color get defBtnFontColor => Colors.white;

Color get defBtnColorL => Colors.grey.shade300;
Color get defBtnFontColorL => Colors.white;

Color get lineColor => Colors.grey.shade400;
Color get defFontColor => Colors.black; //Colors.grey.shade800;
Color get defEditorBgColor => Colors.grey.shade300;

Color get lightGrayColor => Colors.grey.shade600;
Color get grayColor => Colors.grey;
Color get whiteColor => Colors.white;
Color get blackColor => Colors.black;

TextStyle get defTextStyle => TextStyle(color: defFontColor, fontSize: defFontSize);

TextStyle get defTitleTextStyle => TextStyle(fontWeight: FontWeight.bold, color: blackColor, fontSize: SUT_SP(30));

TextStyle get defBtnTextStyle => TextStyle(fontSize: defFontSize, color: defBtnFontColor);

TextStyle get defBtnTextStyleL => TextStyle(fontSize: defFontSize, color: defBtnFontColorL);

///用户名最大长度 10
const int constUserNameMaxLength = 10;

///签名最大长度 20
const int constSignatureMaxLength = 20;

///公告最大长度 30
const int constNoticeMaxLength = 50;

///chat最大长度 30
const int constChatMaxLength = 150;
