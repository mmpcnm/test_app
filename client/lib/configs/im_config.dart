import 'package:fim_app/package/util/res_manager.dart';

enum USER_SEX {
  ///未知
  UNKNOWN,

  ///男
  MALE,

  ///女
  WOMAN
}

extension SexIntExtension on int {
  USER_SEX get sex {
    if (this >= 0 && this < USER_SEX.values.length) {
      return USER_SEX.values[this];
    } else {
      return USER_SEX.UNKNOWN;
    }
  }

  String get sex_text {
    switch (sex) {
      case USER_SEX.UNKNOWN:
        return "未知";
      case USER_SEX.MALE:
        return "男";
      case USER_SEX.WOMAN:
        return "女";
    }
  }

  String get sex_image {
    switch (sex) {
      case USER_SEX.UNKNOWN:
        return ResImgs('male_ico_new');
      case USER_SEX.MALE:
        return ResImgs('male_ico_new');
      case USER_SEX.WOMAN:
        return ResImgs('female_icon_new');
    }
  }
}
