import 'package:fixnum/fixnum.dart';

class AppConfig {
  /// 是否开启debug界面
  static const bool isDebug = true;

  ///
  static void clear() {
    _connectIp = '66.203.152.106';
    _connectPort = 8083;
    _logicIp = '66.203.152.106';
    _logicPort = 50001;
  }

  // ///推送服
  static String _connectIp = '66.203.152.106';
  static int _connectPort = 8083;

  ///logic
  static String _logicIp = '66.203.152.106';
  static int _logicPort = 50001;

  ///
  static String get connectIp => _connectIp;
  static int get connectPort => _connectPort;

  ///
  static String get logicIp => _logicIp;
  static int get logicPort => _logicPort;

  /// 渠道号
  static Int64 get channelId => Int64(101);

  ///
  static set connectIp(value) {
    if (isDebug) _connectIp = value;
  }

  ///
  static set connectPort(value) {
    if (isDebug) _connectPort = value;
  }

  ///
  static set logicIp(value) {
    if (isDebug) _logicIp = value;
  }

  ///
  static set logicPort(value) {
    if (isDebug) _logicPort = value;
  }

  ///音频地址
  static const String apiUrl =
      'http://66.203.152.106:8092/upload'; //'http://66.203.153.70:8092/upload';

  ///图片地址
  // static const String picUrl = 'http://new.xbxb555.com/user/operation/uploadChessPic';
}
