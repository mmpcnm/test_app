///
//  Generated code. Do not modify.
//  source: logic.ext.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use memberTypeDescriptor instead')
const MemberType$json = const {
  '1': 'MemberType',
  '2': const [
    const {'1': 'GMT_UNKNOWN', '2': 0},
    const {'1': 'GMT_ADMIN', '2': 1},
    const {'1': 'GMT_MEMBER', '2': 2},
  ],
};

/// Descriptor for `MemberType`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List memberTypeDescriptor = $convert.base64Decode('CgpNZW1iZXJUeXBlEg8KC0dNVF9VTktOT1dOEAASDQoJR01UX0FETUlOEAESDgoKR01UX01FTUJFUhAC');
@$core.Deprecated('Use registerReqDescriptor instead')
const RegisterReq$json = const {
  '1': 'RegisterReq',
  '2': const [
    const {'1': 'account', '3': 1, '4': 1, '5': 9, '10': 'account'},
    const {'1': 'password', '3': 2, '4': 1, '5': 9, '10': 'password'},
    const {'1': 'nick', '3': 3, '4': 1, '5': 9, '10': 'nick'},
    const {'1': 'machine_id', '3': 4, '4': 1, '5': 9, '10': 'machineId'},
    const {'1': 'type', '3': 5, '4': 1, '5': 5, '10': 'type'},
    const {'1': 'brand', '3': 6, '4': 1, '5': 9, '10': 'brand'},
    const {'1': 'model', '3': 7, '4': 1, '5': 9, '10': 'model'},
    const {'1': 'system_version', '3': 8, '4': 1, '5': 9, '10': 'systemVersion'},
    const {'1': 'sdk_version', '3': 9, '4': 1, '5': 9, '10': 'sdkVersion'},
    const {'1': 'channel', '3': 10, '4': 1, '5': 3, '10': 'channel'},
  ],
};

/// Descriptor for `RegisterReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List registerReqDescriptor = $convert.base64Decode('CgtSZWdpc3RlclJlcRIYCgdhY2NvdW50GAEgASgJUgdhY2NvdW50EhoKCHBhc3N3b3JkGAIgASgJUghwYXNzd29yZBISCgRuaWNrGAMgASgJUgRuaWNrEh0KCm1hY2hpbmVfaWQYBCABKAlSCW1hY2hpbmVJZBISCgR0eXBlGAUgASgFUgR0eXBlEhQKBWJyYW5kGAYgASgJUgVicmFuZBIUCgVtb2RlbBgHIAEoCVIFbW9kZWwSJQoOc3lzdGVtX3ZlcnNpb24YCCABKAlSDXN5c3RlbVZlcnNpb24SHwoLc2RrX3ZlcnNpb24YCSABKAlSCnNka1ZlcnNpb24SGAoHY2hhbm5lbBgKIAEoA1IHY2hhbm5lbA==');
@$core.Deprecated('Use registerRespDescriptor instead')
const RegisterResp$json = const {
  '1': 'RegisterResp',
  '2': const [
    const {'1': 'code', '3': 1, '4': 1, '5': 5, '10': 'code'},
  ],
};

/// Descriptor for `RegisterResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List registerRespDescriptor = $convert.base64Decode('CgxSZWdpc3RlclJlc3ASEgoEY29kZRgBIAEoBVIEY29kZQ==');
@$core.Deprecated('Use signInReqDescriptor instead')
const SignInReq$json = const {
  '1': 'SignInReq',
  '2': const [
    const {'1': 'account', '3': 1, '4': 1, '5': 9, '10': 'account'},
    const {'1': 'password', '3': 2, '4': 1, '5': 9, '10': 'password'},
    const {'1': 'machine_id', '3': 3, '4': 1, '5': 9, '10': 'machineId'},
    const {'1': 'type', '3': 4, '4': 1, '5': 5, '10': 'type'},
    const {'1': 'brand', '3': 5, '4': 1, '5': 9, '10': 'brand'},
    const {'1': 'model', '3': 6, '4': 1, '5': 9, '10': 'model'},
    const {'1': 'system_version', '3': 7, '4': 1, '5': 9, '10': 'systemVersion'},
    const {'1': 'sdk_version', '3': 8, '4': 1, '5': 9, '10': 'sdkVersion'},
    const {'1': 'channel', '3': 9, '4': 1, '5': 3, '10': 'channel'},
  ],
};

/// Descriptor for `SignInReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List signInReqDescriptor = $convert.base64Decode('CglTaWduSW5SZXESGAoHYWNjb3VudBgBIAEoCVIHYWNjb3VudBIaCghwYXNzd29yZBgCIAEoCVIIcGFzc3dvcmQSHQoKbWFjaGluZV9pZBgDIAEoCVIJbWFjaGluZUlkEhIKBHR5cGUYBCABKAVSBHR5cGUSFAoFYnJhbmQYBSABKAlSBWJyYW5kEhQKBW1vZGVsGAYgASgJUgVtb2RlbBIlCg5zeXN0ZW1fdmVyc2lvbhgHIAEoCVINc3lzdGVtVmVyc2lvbhIfCgtzZGtfdmVyc2lvbhgIIAEoCVIKc2RrVmVyc2lvbhIYCgdjaGFubmVsGAkgASgDUgdjaGFubmVs');
@$core.Deprecated('Use signInRespDescriptor instead')
const SignInResp$json = const {
  '1': 'SignInResp',
  '2': const [
    const {'1': 'user_id', '3': 1, '4': 1, '5': 3, '10': 'userId'},
    const {'1': 'token', '3': 2, '4': 1, '5': 9, '10': 'token'},
  ],
};

/// Descriptor for `SignInResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List signInRespDescriptor = $convert.base64Decode('CgpTaWduSW5SZXNwEhcKB3VzZXJfaWQYASABKANSBnVzZXJJZBIUCgV0b2tlbhgCIAEoCVIFdG9rZW4=');
@$core.Deprecated('Use sendMessageReqDescriptor instead')
const SendMessageReq$json = const {
  '1': 'SendMessageReq',
  '2': const [
    const {'1': 'receiver_type', '3': 1, '4': 1, '5': 14, '6': '.pb.ReceiverType', '10': 'receiverType'},
    const {'1': 'receiver_id', '3': 2, '4': 1, '5': 3, '10': 'receiverId'},
    const {'1': 'to_user_ids', '3': 3, '4': 3, '5': 3, '10': 'toUserIds'},
    const {'1': 'message_type', '3': 4, '4': 1, '5': 14, '6': '.pb.MessageType', '10': 'messageType'},
    const {'1': 'message_content', '3': 5, '4': 1, '5': 12, '10': 'messageContent'},
    const {'1': 'send_time', '3': 6, '4': 1, '5': 3, '10': 'sendTime'},
    const {'1': 'is_persist', '3': 7, '4': 1, '5': 8, '10': 'isPersist'},
  ],
};

/// Descriptor for `SendMessageReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List sendMessageReqDescriptor = $convert.base64Decode('Cg5TZW5kTWVzc2FnZVJlcRI1Cg1yZWNlaXZlcl90eXBlGAEgASgOMhAucGIuUmVjZWl2ZXJUeXBlUgxyZWNlaXZlclR5cGUSHwoLcmVjZWl2ZXJfaWQYAiABKANSCnJlY2VpdmVySWQSHgoLdG9fdXNlcl9pZHMYAyADKANSCXRvVXNlcklkcxIyCgxtZXNzYWdlX3R5cGUYBCABKA4yDy5wYi5NZXNzYWdlVHlwZVILbWVzc2FnZVR5cGUSJwoPbWVzc2FnZV9jb250ZW50GAUgASgMUg5tZXNzYWdlQ29udGVudBIbCglzZW5kX3RpbWUYBiABKANSCHNlbmRUaW1lEh0KCmlzX3BlcnNpc3QYByABKAhSCWlzUGVyc2lzdA==');
@$core.Deprecated('Use sendMessageRespDescriptor instead')
const SendMessageResp$json = const {
  '1': 'SendMessageResp',
  '2': const [
    const {'1': 'seq', '3': 1, '4': 1, '5': 3, '10': 'seq'},
  ],
};

/// Descriptor for `SendMessageResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List sendMessageRespDescriptor = $convert.base64Decode('Cg9TZW5kTWVzc2FnZVJlc3ASEAoDc2VxGAEgASgDUgNzZXE=');
@$core.Deprecated('Use cancelMessageReqDescriptor instead')
const CancelMessageReq$json = const {
  '1': 'CancelMessageReq',
  '2': const [
    const {'1': 'receiver_type', '3': 1, '4': 1, '5': 14, '6': '.pb.ReceiverType', '10': 'receiverType'},
    const {'1': 'receiver_id', '3': 2, '4': 1, '5': 3, '10': 'receiverId'},
    const {'1': 'seq_id', '3': 3, '4': 1, '5': 3, '10': 'seqId'},
  ],
};

/// Descriptor for `CancelMessageReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List cancelMessageReqDescriptor = $convert.base64Decode('ChBDYW5jZWxNZXNzYWdlUmVxEjUKDXJlY2VpdmVyX3R5cGUYASABKA4yEC5wYi5SZWNlaXZlclR5cGVSDHJlY2VpdmVyVHlwZRIfCgtyZWNlaXZlcl9pZBgCIAEoA1IKcmVjZWl2ZXJJZBIVCgZzZXFfaWQYAyABKANSBXNlcUlk');
@$core.Deprecated('Use cancelMessageRespDescriptor instead')
const CancelMessageResp$json = const {
  '1': 'CancelMessageResp',
};

/// Descriptor for `CancelMessageResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List cancelMessageRespDescriptor = $convert.base64Decode('ChFDYW5jZWxNZXNzYWdlUmVzcA==');
@$core.Deprecated('Use pushRoomReqDescriptor instead')
const PushRoomReq$json = const {
  '1': 'PushRoomReq',
  '2': const [
    const {'1': 'room_id', '3': 1, '4': 1, '5': 3, '10': 'roomId'},
    const {'1': 'message_type', '3': 2, '4': 1, '5': 14, '6': '.pb.MessageType', '10': 'messageType'},
    const {'1': 'message_content', '3': 3, '4': 1, '5': 12, '10': 'messageContent'},
    const {'1': 'send_time', '3': 4, '4': 1, '5': 3, '10': 'sendTime'},
    const {'1': 'is_persist', '3': 5, '4': 1, '5': 8, '10': 'isPersist'},
  ],
};

/// Descriptor for `PushRoomReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List pushRoomReqDescriptor = $convert.base64Decode('CgtQdXNoUm9vbVJlcRIXCgdyb29tX2lkGAEgASgDUgZyb29tSWQSMgoMbWVzc2FnZV90eXBlGAIgASgOMg8ucGIuTWVzc2FnZVR5cGVSC21lc3NhZ2VUeXBlEicKD21lc3NhZ2VfY29udGVudBgDIAEoDFIObWVzc2FnZUNvbnRlbnQSGwoJc2VuZF90aW1lGAQgASgDUghzZW5kVGltZRIdCgppc19wZXJzaXN0GAUgASgIUglpc1BlcnNpc3Q=');
@$core.Deprecated('Use pushRoomRespDescriptor instead')
const PushRoomResp$json = const {
  '1': 'PushRoomResp',
  '2': const [
    const {'1': 'seq', '3': 1, '4': 1, '5': 3, '10': 'seq'},
  ],
};

/// Descriptor for `PushRoomResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List pushRoomRespDescriptor = $convert.base64Decode('CgxQdXNoUm9vbVJlc3ASEAoDc2VxGAEgASgDUgNzZXE=');
@$core.Deprecated('Use addFriendReqDescriptor instead')
const AddFriendReq$json = const {
  '1': 'AddFriendReq',
  '2': const [
    const {'1': 'friend_id', '3': 1, '4': 1, '5': 3, '10': 'friendId'},
    const {'1': 'remarks', '3': 2, '4': 1, '5': 9, '10': 'remarks'},
    const {'1': 'description', '3': 3, '4': 1, '5': 9, '10': 'description'},
  ],
};

/// Descriptor for `AddFriendReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List addFriendReqDescriptor = $convert.base64Decode('CgxBZGRGcmllbmRSZXESGwoJZnJpZW5kX2lkGAEgASgDUghmcmllbmRJZBIYCgdyZW1hcmtzGAIgASgJUgdyZW1hcmtzEiAKC2Rlc2NyaXB0aW9uGAMgASgJUgtkZXNjcmlwdGlvbg==');
@$core.Deprecated('Use addFriendRespDescriptor instead')
const AddFriendResp$json = const {
  '1': 'AddFriendResp',
};

/// Descriptor for `AddFriendResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List addFriendRespDescriptor = $convert.base64Decode('Cg1BZGRGcmllbmRSZXNw');
@$core.Deprecated('Use delFriendReqDescriptor instead')
const DelFriendReq$json = const {
  '1': 'DelFriendReq',
  '2': const [
    const {'1': 'friend_id', '3': 1, '4': 1, '5': 3, '10': 'friendId'},
  ],
};

/// Descriptor for `DelFriendReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List delFriendReqDescriptor = $convert.base64Decode('CgxEZWxGcmllbmRSZXESGwoJZnJpZW5kX2lkGAEgASgDUghmcmllbmRJZA==');
@$core.Deprecated('Use delFriendRespDescriptor instead')
const DelFriendResp$json = const {
  '1': 'DelFriendResp',
};

/// Descriptor for `DelFriendResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List delFriendRespDescriptor = $convert.base64Decode('Cg1EZWxGcmllbmRSZXNw');
@$core.Deprecated('Use agreeAddFriendReqDescriptor instead')
const AgreeAddFriendReq$json = const {
  '1': 'AgreeAddFriendReq',
  '2': const [
    const {'1': 'user_id', '3': 1, '4': 1, '5': 3, '10': 'userId'},
    const {'1': 'remarks', '3': 2, '4': 1, '5': 9, '10': 'remarks'},
  ],
};

/// Descriptor for `AgreeAddFriendReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List agreeAddFriendReqDescriptor = $convert.base64Decode('ChFBZ3JlZUFkZEZyaWVuZFJlcRIXCgd1c2VyX2lkGAEgASgDUgZ1c2VySWQSGAoHcmVtYXJrcxgCIAEoCVIHcmVtYXJrcw==');
@$core.Deprecated('Use agreeAddFriendRespDescriptor instead')
const AgreeAddFriendResp$json = const {
  '1': 'AgreeAddFriendResp',
};

/// Descriptor for `AgreeAddFriendResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List agreeAddFriendRespDescriptor = $convert.base64Decode('ChJBZ3JlZUFkZEZyaWVuZFJlc3A=');
@$core.Deprecated('Use setFriendReqDescriptor instead')
const SetFriendReq$json = const {
  '1': 'SetFriendReq',
  '2': const [
    const {'1': 'friend_id', '3': 1, '4': 1, '5': 3, '10': 'friendId'},
    const {'1': 'remarks', '3': 2, '4': 1, '5': 9, '10': 'remarks'},
    const {'1': 'extra', '3': 8, '4': 1, '5': 9, '10': 'extra'},
  ],
};

/// Descriptor for `SetFriendReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List setFriendReqDescriptor = $convert.base64Decode('CgxTZXRGcmllbmRSZXESGwoJZnJpZW5kX2lkGAEgASgDUghmcmllbmRJZBIYCgdyZW1hcmtzGAIgASgJUgdyZW1hcmtzEhQKBWV4dHJhGAggASgJUgVleHRyYQ==');
@$core.Deprecated('Use setFriendRespDescriptor instead')
const SetFriendResp$json = const {
  '1': 'SetFriendResp',
  '2': const [
    const {'1': 'friend_id', '3': 1, '4': 1, '5': 3, '10': 'friendId'},
    const {'1': 'remarks', '3': 2, '4': 1, '5': 9, '10': 'remarks'},
    const {'1': 'extra', '3': 8, '4': 1, '5': 9, '10': 'extra'},
  ],
};

/// Descriptor for `SetFriendResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List setFriendRespDescriptor = $convert.base64Decode('Cg1TZXRGcmllbmRSZXNwEhsKCWZyaWVuZF9pZBgBIAEoA1IIZnJpZW5kSWQSGAoHcmVtYXJrcxgCIAEoCVIHcmVtYXJrcxIUCgVleHRyYRgIIAEoCVIFZXh0cmE=');
@$core.Deprecated('Use friendDescriptor instead')
const Friend$json = const {
  '1': 'Friend',
  '2': const [
    const {'1': 'user_id', '3': 1, '4': 1, '5': 3, '10': 'userId'},
    const {'1': 'phone_number', '3': 2, '4': 1, '5': 9, '10': 'phoneNumber'},
    const {'1': 'nickname', '3': 3, '4': 1, '5': 9, '10': 'nickname'},
    const {'1': 'sex', '3': 4, '4': 1, '5': 5, '10': 'sex'},
    const {'1': 'avatar_url', '3': 5, '4': 1, '5': 9, '10': 'avatarUrl'},
    const {'1': 'user_extra', '3': 6, '4': 1, '5': 9, '10': 'userExtra'},
    const {'1': 'remarks', '3': 7, '4': 1, '5': 9, '10': 'remarks'},
    const {'1': 'extra', '3': 8, '4': 1, '5': 9, '10': 'extra'},
  ],
};

/// Descriptor for `Friend`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List friendDescriptor = $convert.base64Decode('CgZGcmllbmQSFwoHdXNlcl9pZBgBIAEoA1IGdXNlcklkEiEKDHBob25lX251bWJlchgCIAEoCVILcGhvbmVOdW1iZXISGgoIbmlja25hbWUYAyABKAlSCG5pY2tuYW1lEhAKA3NleBgEIAEoBVIDc2V4Eh0KCmF2YXRhcl91cmwYBSABKAlSCWF2YXRhclVybBIdCgp1c2VyX2V4dHJhGAYgASgJUgl1c2VyRXh0cmESGAoHcmVtYXJrcxgHIAEoCVIHcmVtYXJrcxIUCgVleHRyYRgIIAEoCVIFZXh0cmE=');
@$core.Deprecated('Use getFriendsReqDescriptor instead')
const GetFriendsReq$json = const {
  '1': 'GetFriendsReq',
};

/// Descriptor for `GetFriendsReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getFriendsReqDescriptor = $convert.base64Decode('Cg1HZXRGcmllbmRzUmVx');
@$core.Deprecated('Use getFriendsRespDescriptor instead')
const GetFriendsResp$json = const {
  '1': 'GetFriendsResp',
  '2': const [
    const {'1': 'friends', '3': 1, '4': 3, '5': 11, '6': '.pb.Friend', '10': 'friends'},
  ],
};

/// Descriptor for `GetFriendsResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getFriendsRespDescriptor = $convert.base64Decode('Cg5HZXRGcmllbmRzUmVzcBIkCgdmcmllbmRzGAEgAygLMgoucGIuRnJpZW5kUgdmcmllbmRz');
@$core.Deprecated('Use createGroupReqDescriptor instead')
const CreateGroupReq$json = const {
  '1': 'CreateGroupReq',
  '2': const [
    const {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'avatar_url', '3': 2, '4': 1, '5': 9, '10': 'avatarUrl'},
    const {'1': 'introduction', '3': 3, '4': 1, '5': 9, '10': 'introduction'},
    const {'1': 'extra', '3': 4, '4': 1, '5': 9, '10': 'extra'},
    const {'1': 'member_ids', '3': 5, '4': 3, '5': 3, '10': 'memberIds'},
  ],
};

/// Descriptor for `CreateGroupReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List createGroupReqDescriptor = $convert.base64Decode('Cg5DcmVhdGVHcm91cFJlcRISCgRuYW1lGAEgASgJUgRuYW1lEh0KCmF2YXRhcl91cmwYAiABKAlSCWF2YXRhclVybBIiCgxpbnRyb2R1Y3Rpb24YAyABKAlSDGludHJvZHVjdGlvbhIUCgVleHRyYRgEIAEoCVIFZXh0cmESHQoKbWVtYmVyX2lkcxgFIAMoA1IJbWVtYmVySWRz');
@$core.Deprecated('Use createGroupRespDescriptor instead')
const CreateGroupResp$json = const {
  '1': 'CreateGroupResp',
  '2': const [
    const {'1': 'group_id', '3': 1, '4': 1, '5': 3, '10': 'groupId'},
  ],
};

/// Descriptor for `CreateGroupResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List createGroupRespDescriptor = $convert.base64Decode('Cg9DcmVhdGVHcm91cFJlc3ASGQoIZ3JvdXBfaWQYASABKANSB2dyb3VwSWQ=');
@$core.Deprecated('Use updateGroupReqDescriptor instead')
const UpdateGroupReq$json = const {
  '1': 'UpdateGroupReq',
  '2': const [
    const {'1': 'group_id', '3': 1, '4': 1, '5': 3, '10': 'groupId'},
    const {'1': 'avatar_url', '3': 2, '4': 1, '5': 9, '10': 'avatarUrl'},
    const {'1': 'name', '3': 3, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'introduction', '3': 4, '4': 1, '5': 9, '10': 'introduction'},
    const {'1': 'extra', '3': 5, '4': 1, '5': 9, '10': 'extra'},
  ],
};

/// Descriptor for `UpdateGroupReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updateGroupReqDescriptor = $convert.base64Decode('Cg5VcGRhdGVHcm91cFJlcRIZCghncm91cF9pZBgBIAEoA1IHZ3JvdXBJZBIdCgphdmF0YXJfdXJsGAIgASgJUglhdmF0YXJVcmwSEgoEbmFtZRgDIAEoCVIEbmFtZRIiCgxpbnRyb2R1Y3Rpb24YBCABKAlSDGludHJvZHVjdGlvbhIUCgVleHRyYRgFIAEoCVIFZXh0cmE=');
@$core.Deprecated('Use updateGroupRespDescriptor instead')
const UpdateGroupResp$json = const {
  '1': 'UpdateGroupResp',
};

/// Descriptor for `UpdateGroupResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updateGroupRespDescriptor = $convert.base64Decode('Cg9VcGRhdGVHcm91cFJlc3A=');
@$core.Deprecated('Use getGroupReqDescriptor instead')
const GetGroupReq$json = const {
  '1': 'GetGroupReq',
  '2': const [
    const {'1': 'group_id', '3': 1, '4': 1, '5': 3, '10': 'groupId'},
  ],
};

/// Descriptor for `GetGroupReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getGroupReqDescriptor = $convert.base64Decode('CgtHZXRHcm91cFJlcRIZCghncm91cF9pZBgBIAEoA1IHZ3JvdXBJZA==');
@$core.Deprecated('Use getGroupRespDescriptor instead')
const GetGroupResp$json = const {
  '1': 'GetGroupResp',
  '2': const [
    const {'1': 'group', '3': 1, '4': 1, '5': 11, '6': '.pb.Group', '10': 'group'},
  ],
};

/// Descriptor for `GetGroupResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getGroupRespDescriptor = $convert.base64Decode('CgxHZXRHcm91cFJlc3ASHwoFZ3JvdXAYASABKAsyCS5wYi5Hcm91cFIFZ3JvdXA=');
@$core.Deprecated('Use groupDescriptor instead')
const Group$json = const {
  '1': 'Group',
  '2': const [
    const {'1': 'group_id', '3': 1, '4': 1, '5': 3, '10': 'groupId'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'avatar_url', '3': 3, '4': 1, '5': 9, '10': 'avatarUrl'},
    const {'1': 'introduction', '3': 4, '4': 1, '5': 9, '10': 'introduction'},
    const {'1': 'user_mum', '3': 5, '4': 1, '5': 5, '10': 'userMum'},
    const {'1': 'extra', '3': 6, '4': 1, '5': 9, '10': 'extra'},
    const {'1': 'create_time', '3': 7, '4': 1, '5': 3, '10': 'createTime'},
    const {'1': 'update_time', '3': 8, '4': 1, '5': 3, '10': 'updateTime'},
  ],
};

/// Descriptor for `Group`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List groupDescriptor = $convert.base64Decode('CgVHcm91cBIZCghncm91cF9pZBgBIAEoA1IHZ3JvdXBJZBISCgRuYW1lGAIgASgJUgRuYW1lEh0KCmF2YXRhcl91cmwYAyABKAlSCWF2YXRhclVybBIiCgxpbnRyb2R1Y3Rpb24YBCABKAlSDGludHJvZHVjdGlvbhIZCgh1c2VyX211bRgFIAEoBVIHdXNlck11bRIUCgVleHRyYRgGIAEoCVIFZXh0cmESHwoLY3JlYXRlX3RpbWUYByABKANSCmNyZWF0ZVRpbWUSHwoLdXBkYXRlX3RpbWUYCCABKANSCnVwZGF0ZVRpbWU=');
@$core.Deprecated('Use getGroupsReqDescriptor instead')
const GetGroupsReq$json = const {
  '1': 'GetGroupsReq',
};

/// Descriptor for `GetGroupsReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getGroupsReqDescriptor = $convert.base64Decode('CgxHZXRHcm91cHNSZXE=');
@$core.Deprecated('Use getGroupsRespDescriptor instead')
const GetGroupsResp$json = const {
  '1': 'GetGroupsResp',
  '2': const [
    const {'1': 'groups', '3': 1, '4': 3, '5': 11, '6': '.pb.Group', '10': 'groups'},
  ],
};

/// Descriptor for `GetGroupsResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getGroupsRespDescriptor = $convert.base64Decode('Cg1HZXRHcm91cHNSZXNwEiEKBmdyb3VwcxgBIAMoCzIJLnBiLkdyb3VwUgZncm91cHM=');
@$core.Deprecated('Use addGroupMembersReqDescriptor instead')
const AddGroupMembersReq$json = const {
  '1': 'AddGroupMembersReq',
  '2': const [
    const {'1': 'group_id', '3': 1, '4': 1, '5': 3, '10': 'groupId'},
    const {'1': 'user_ids', '3': 2, '4': 3, '5': 3, '10': 'userIds'},
  ],
};

/// Descriptor for `AddGroupMembersReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List addGroupMembersReqDescriptor = $convert.base64Decode('ChJBZGRHcm91cE1lbWJlcnNSZXESGQoIZ3JvdXBfaWQYASABKANSB2dyb3VwSWQSGQoIdXNlcl9pZHMYAiADKANSB3VzZXJJZHM=');
@$core.Deprecated('Use addGroupMembersRespDescriptor instead')
const AddGroupMembersResp$json = const {
  '1': 'AddGroupMembersResp',
  '2': const [
    const {'1': 'user_ids', '3': 1, '4': 3, '5': 3, '10': 'userIds'},
  ],
};

/// Descriptor for `AddGroupMembersResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List addGroupMembersRespDescriptor = $convert.base64Decode('ChNBZGRHcm91cE1lbWJlcnNSZXNwEhkKCHVzZXJfaWRzGAEgAygDUgd1c2VySWRz');
@$core.Deprecated('Use updateGroupMemberReqDescriptor instead')
const UpdateGroupMemberReq$json = const {
  '1': 'UpdateGroupMemberReq',
  '2': const [
    const {'1': 'group_id', '3': 1, '4': 1, '5': 3, '10': 'groupId'},
    const {'1': 'user_id', '3': 2, '4': 1, '5': 3, '10': 'userId'},
    const {'1': 'member_type', '3': 3, '4': 1, '5': 14, '6': '.pb.MemberType', '10': 'memberType'},
    const {'1': 'remarks', '3': 4, '4': 1, '5': 9, '10': 'remarks'},
    const {'1': 'extra', '3': 5, '4': 1, '5': 9, '10': 'extra'},
  ],
};

/// Descriptor for `UpdateGroupMemberReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updateGroupMemberReqDescriptor = $convert.base64Decode('ChRVcGRhdGVHcm91cE1lbWJlclJlcRIZCghncm91cF9pZBgBIAEoA1IHZ3JvdXBJZBIXCgd1c2VyX2lkGAIgASgDUgZ1c2VySWQSLwoLbWVtYmVyX3R5cGUYAyABKA4yDi5wYi5NZW1iZXJUeXBlUgptZW1iZXJUeXBlEhgKB3JlbWFya3MYBCABKAlSB3JlbWFya3MSFAoFZXh0cmEYBSABKAlSBWV4dHJh');
@$core.Deprecated('Use updateGroupMemberRespDescriptor instead')
const UpdateGroupMemberResp$json = const {
  '1': 'UpdateGroupMemberResp',
};

/// Descriptor for `UpdateGroupMemberResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updateGroupMemberRespDescriptor = $convert.base64Decode('ChVVcGRhdGVHcm91cE1lbWJlclJlc3A=');
@$core.Deprecated('Use deleteGroupMemberReqDescriptor instead')
const DeleteGroupMemberReq$json = const {
  '1': 'DeleteGroupMemberReq',
  '2': const [
    const {'1': 'group_id', '3': 1, '4': 1, '5': 3, '10': 'groupId'},
    const {'1': 'user_id', '3': 2, '4': 1, '5': 3, '10': 'userId'},
  ],
};

/// Descriptor for `DeleteGroupMemberReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List deleteGroupMemberReqDescriptor = $convert.base64Decode('ChREZWxldGVHcm91cE1lbWJlclJlcRIZCghncm91cF9pZBgBIAEoA1IHZ3JvdXBJZBIXCgd1c2VyX2lkGAIgASgDUgZ1c2VySWQ=');
@$core.Deprecated('Use deleteGroupMemberRespDescriptor instead')
const DeleteGroupMemberResp$json = const {
  '1': 'DeleteGroupMemberResp',
};

/// Descriptor for `DeleteGroupMemberResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List deleteGroupMemberRespDescriptor = $convert.base64Decode('ChVEZWxldGVHcm91cE1lbWJlclJlc3A=');
@$core.Deprecated('Use exitGroupReqDescriptor instead')
const ExitGroupReq$json = const {
  '1': 'ExitGroupReq',
  '2': const [
    const {'1': 'group_id', '3': 1, '4': 1, '5': 3, '10': 'groupId'},
  ],
};

/// Descriptor for `ExitGroupReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List exitGroupReqDescriptor = $convert.base64Decode('CgxFeGl0R3JvdXBSZXESGQoIZ3JvdXBfaWQYASABKANSB2dyb3VwSWQ=');
@$core.Deprecated('Use exitGroupRespDescriptor instead')
const ExitGroupResp$json = const {
  '1': 'ExitGroupResp',
};

/// Descriptor for `ExitGroupResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List exitGroupRespDescriptor = $convert.base64Decode('Cg1FeGl0R3JvdXBSZXNw');
@$core.Deprecated('Use getGroupMembersReqDescriptor instead')
const GetGroupMembersReq$json = const {
  '1': 'GetGroupMembersReq',
  '2': const [
    const {'1': 'group_id', '3': 1, '4': 1, '5': 3, '10': 'groupId'},
  ],
};

/// Descriptor for `GetGroupMembersReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getGroupMembersReqDescriptor = $convert.base64Decode('ChJHZXRHcm91cE1lbWJlcnNSZXESGQoIZ3JvdXBfaWQYASABKANSB2dyb3VwSWQ=');
@$core.Deprecated('Use getGroupMembersRespDescriptor instead')
const GetGroupMembersResp$json = const {
  '1': 'GetGroupMembersResp',
  '2': const [
    const {'1': 'members', '3': 1, '4': 3, '5': 11, '6': '.pb.GroupMember', '10': 'members'},
  ],
};

/// Descriptor for `GetGroupMembersResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getGroupMembersRespDescriptor = $convert.base64Decode('ChNHZXRHcm91cE1lbWJlcnNSZXNwEikKB21lbWJlcnMYASADKAsyDy5wYi5Hcm91cE1lbWJlclIHbWVtYmVycw==');
@$core.Deprecated('Use groupMemberDescriptor instead')
const GroupMember$json = const {
  '1': 'GroupMember',
  '2': const [
    const {'1': 'user_id', '3': 1, '4': 1, '5': 3, '10': 'userId'},
    const {'1': 'nickname', '3': 2, '4': 1, '5': 9, '10': 'nickname'},
    const {'1': 'sex', '3': 3, '4': 1, '5': 5, '10': 'sex'},
    const {'1': 'avatar_url', '3': 4, '4': 1, '5': 9, '10': 'avatarUrl'},
    const {'1': 'user_extra', '3': 5, '4': 1, '5': 9, '10': 'userExtra'},
    const {'1': 'member_type', '3': 6, '4': 1, '5': 14, '6': '.pb.MemberType', '10': 'memberType'},
    const {'1': 'remarks', '3': 7, '4': 1, '5': 9, '10': 'remarks'},
    const {'1': 'extra', '3': 8, '4': 1, '5': 9, '10': 'extra'},
  ],
};

/// Descriptor for `GroupMember`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List groupMemberDescriptor = $convert.base64Decode('CgtHcm91cE1lbWJlchIXCgd1c2VyX2lkGAEgASgDUgZ1c2VySWQSGgoIbmlja25hbWUYAiABKAlSCG5pY2tuYW1lEhAKA3NleBgDIAEoBVIDc2V4Eh0KCmF2YXRhcl91cmwYBCABKAlSCWF2YXRhclVybBIdCgp1c2VyX2V4dHJhGAUgASgJUgl1c2VyRXh0cmESLwoLbWVtYmVyX3R5cGUYBiABKA4yDi5wYi5NZW1iZXJUeXBlUgptZW1iZXJUeXBlEhgKB3JlbWFya3MYByABKAlSB3JlbWFya3MSFAoFZXh0cmEYCCABKAlSBWV4dHJh');
@$core.Deprecated('Use userDescriptor instead')
const User$json = const {
  '1': 'User',
  '2': const [
    const {'1': 'user_id', '3': 1, '4': 1, '5': 3, '10': 'userId'},
    const {'1': 'account', '3': 2, '4': 1, '5': 9, '10': 'account'},
    const {'1': 'nickname', '3': 3, '4': 1, '5': 9, '10': 'nickname'},
    const {'1': 'sex', '3': 4, '4': 1, '5': 5, '10': 'sex'},
    const {'1': 'avatar_url', '3': 5, '4': 1, '5': 9, '10': 'avatarUrl'},
    const {'1': 'extra', '3': 6, '4': 1, '5': 9, '10': 'extra'},
    const {'1': 'isWalletPwdSet', '3': 7, '4': 1, '5': 8, '10': 'isWalletPwdSet'},
    const {'1': 'phone', '3': 8, '4': 1, '5': 9, '10': 'phone'},
    const {'1': 'channel', '3': 9, '4': 1, '5': 3, '10': 'channel'},
    const {'1': 'create_time', '3': 10, '4': 1, '5': 3, '10': 'createTime'},
  ],
};

/// Descriptor for `User`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List userDescriptor = $convert.base64Decode('CgRVc2VyEhcKB3VzZXJfaWQYASABKANSBnVzZXJJZBIYCgdhY2NvdW50GAIgASgJUgdhY2NvdW50EhoKCG5pY2tuYW1lGAMgASgJUghuaWNrbmFtZRIQCgNzZXgYBCABKAVSA3NleBIdCgphdmF0YXJfdXJsGAUgASgJUglhdmF0YXJVcmwSFAoFZXh0cmEYBiABKAlSBWV4dHJhEiYKDmlzV2FsbGV0UHdkU2V0GAcgASgIUg5pc1dhbGxldFB3ZFNldBIUCgVwaG9uZRgIIAEoCVIFcGhvbmUSGAoHY2hhbm5lbBgJIAEoA1IHY2hhbm5lbBIfCgtjcmVhdGVfdGltZRgKIAEoA1IKY3JlYXRlVGltZQ==');
@$core.Deprecated('Use getUserReqDescriptor instead')
const GetUserReq$json = const {
  '1': 'GetUserReq',
  '2': const [
    const {'1': 'user_id', '3': 1, '4': 1, '5': 3, '10': 'userId'},
  ],
};

/// Descriptor for `GetUserReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getUserReqDescriptor = $convert.base64Decode('CgpHZXRVc2VyUmVxEhcKB3VzZXJfaWQYASABKANSBnVzZXJJZA==');
@$core.Deprecated('Use getUserRespDescriptor instead')
const GetUserResp$json = const {
  '1': 'GetUserResp',
  '2': const [
    const {'1': 'user', '3': 1, '4': 1, '5': 11, '6': '.pb.User', '10': 'user'},
  ],
};

/// Descriptor for `GetUserResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getUserRespDescriptor = $convert.base64Decode('CgtHZXRVc2VyUmVzcBIcCgR1c2VyGAEgASgLMggucGIuVXNlclIEdXNlcg==');
@$core.Deprecated('Use updateUserReqDescriptor instead')
const UpdateUserReq$json = const {
  '1': 'UpdateUserReq',
  '2': const [
    const {'1': 'nickname', '3': 1, '4': 1, '5': 9, '10': 'nickname'},
    const {'1': 'sex', '3': 2, '4': 1, '5': 5, '10': 'sex'},
    const {'1': 'avatar_url', '3': 3, '4': 1, '5': 9, '10': 'avatarUrl'},
    const {'1': 'extra', '3': 4, '4': 1, '5': 9, '10': 'extra'},
  ],
};

/// Descriptor for `UpdateUserReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updateUserReqDescriptor = $convert.base64Decode('Cg1VcGRhdGVVc2VyUmVxEhoKCG5pY2tuYW1lGAEgASgJUghuaWNrbmFtZRIQCgNzZXgYAiABKAVSA3NleBIdCgphdmF0YXJfdXJsGAMgASgJUglhdmF0YXJVcmwSFAoFZXh0cmEYBCABKAlSBWV4dHJh');
@$core.Deprecated('Use updateUserRespDescriptor instead')
const UpdateUserResp$json = const {
  '1': 'UpdateUserResp',
};

/// Descriptor for `UpdateUserResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updateUserRespDescriptor = $convert.base64Decode('Cg5VcGRhdGVVc2VyUmVzcA==');
@$core.Deprecated('Use searchUserReqDescriptor instead')
const SearchUserReq$json = const {
  '1': 'SearchUserReq',
  '2': const [
    const {'1': 'key', '3': 1, '4': 1, '5': 9, '10': 'key'},
  ],
};

/// Descriptor for `SearchUserReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List searchUserReqDescriptor = $convert.base64Decode('Cg1TZWFyY2hVc2VyUmVxEhAKA2tleRgBIAEoCVIDa2V5');
@$core.Deprecated('Use searchUserRespDescriptor instead')
const SearchUserResp$json = const {
  '1': 'SearchUserResp',
  '2': const [
    const {'1': 'users', '3': 1, '4': 3, '5': 11, '6': '.pb.User', '10': 'users'},
  ],
};

/// Descriptor for `SearchUserResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List searchUserRespDescriptor = $convert.base64Decode('Cg5TZWFyY2hVc2VyUmVzcBIeCgV1c2VycxgBIAMoCzIILnBiLlVzZXJSBXVzZXJz');
@$core.Deprecated('Use setWalletPwdReqDescriptor instead')
const SetWalletPwdReq$json = const {
  '1': 'SetWalletPwdReq',
  '2': const [
    const {'1': 'pwd', '3': 1, '4': 1, '5': 9, '10': 'pwd'},
    const {'1': 'new_pwd', '3': 2, '4': 1, '5': 9, '10': 'newPwd'},
  ],
};

/// Descriptor for `SetWalletPwdReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List setWalletPwdReqDescriptor = $convert.base64Decode('Cg9TZXRXYWxsZXRQd2RSZXESEAoDcHdkGAEgASgJUgNwd2QSFwoHbmV3X3B3ZBgCIAEoCVIGbmV3UHdk');
@$core.Deprecated('Use setWalletPwdRespDescriptor instead')
const SetWalletPwdResp$json = const {
  '1': 'SetWalletPwdResp',
};

/// Descriptor for `SetWalletPwdResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List setWalletPwdRespDescriptor = $convert.base64Decode('ChBTZXRXYWxsZXRQd2RSZXNw');
@$core.Deprecated('Use modifyPwdReqDescriptor instead')
const ModifyPwdReq$json = const {
  '1': 'ModifyPwdReq',
  '2': const [
    const {'1': 'pwd', '3': 1, '4': 1, '5': 9, '10': 'pwd'},
    const {'1': 'new_pwd', '3': 2, '4': 1, '5': 9, '10': 'newPwd'},
  ],
};

/// Descriptor for `ModifyPwdReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List modifyPwdReqDescriptor = $convert.base64Decode('CgxNb2RpZnlQd2RSZXESEAoDcHdkGAEgASgJUgNwd2QSFwoHbmV3X3B3ZBgCIAEoCVIGbmV3UHdk');
@$core.Deprecated('Use modifyPwdRespDescriptor instead')
const ModifyPwdResp$json = const {
  '1': 'ModifyPwdResp',
};

/// Descriptor for `ModifyPwdResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List modifyPwdRespDescriptor = $convert.base64Decode('Cg1Nb2RpZnlQd2RSZXNw');
@$core.Deprecated('Use openWalletReqDescriptor instead')
const OpenWalletReq$json = const {
  '1': 'OpenWalletReq',
};

/// Descriptor for `OpenWalletReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List openWalletReqDescriptor = $convert.base64Decode('Cg1PcGVuV2FsbGV0UmVx');
@$core.Deprecated('Use openWalletRespDescriptor instead')
const OpenWalletResp$json = const {
  '1': 'OpenWalletResp',
  '2': const [
    const {'1': 'money', '3': 1, '4': 1, '5': 3, '10': 'money'},
  ],
};

/// Descriptor for `OpenWalletResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List openWalletRespDescriptor = $convert.base64Decode('Cg5PcGVuV2FsbGV0UmVzcBIUCgVtb25leRgBIAEoA1IFbW9uZXk=');
@$core.Deprecated('Use bindPhoneReqDescriptor instead')
const BindPhoneReq$json = const {
  '1': 'BindPhoneReq',
  '2': const [
    const {'1': 'phone', '3': 1, '4': 1, '5': 9, '10': 'phone'},
  ],
};

/// Descriptor for `BindPhoneReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List bindPhoneReqDescriptor = $convert.base64Decode('CgxCaW5kUGhvbmVSZXESFAoFcGhvbmUYASABKAlSBXBob25l');
@$core.Deprecated('Use bindPhoneRespDescriptor instead')
const BindPhoneResp$json = const {
  '1': 'BindPhoneResp',
};

/// Descriptor for `BindPhoneResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List bindPhoneRespDescriptor = $convert.base64Decode('Cg1CaW5kUGhvbmVSZXNw');
@$core.Deprecated('Use bindAlipayReqDescriptor instead')
const BindAlipayReq$json = const {
  '1': 'BindAlipayReq',
  '2': const [
    const {'1': 'realname', '3': 1, '4': 1, '5': 9, '10': 'realname'},
    const {'1': 'alipay_account', '3': 2, '4': 1, '5': 9, '10': 'alipayAccount'},
  ],
};

/// Descriptor for `BindAlipayReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List bindAlipayReqDescriptor = $convert.base64Decode('Cg1CaW5kQWxpcGF5UmVxEhoKCHJlYWxuYW1lGAEgASgJUghyZWFsbmFtZRIlCg5hbGlwYXlfYWNjb3VudBgCIAEoCVINYWxpcGF5QWNjb3VudA==');
@$core.Deprecated('Use bindAlipayRespDescriptor instead')
const BindAlipayResp$json = const {
  '1': 'BindAlipayResp',
};

/// Descriptor for `BindAlipayResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List bindAlipayRespDescriptor = $convert.base64Decode('Cg5CaW5kQWxpcGF5UmVzcA==');
@$core.Deprecated('Use bindPromoterReqDescriptor instead')
const BindPromoterReq$json = const {
  '1': 'BindPromoterReq',
  '2': const [
    const {'1': 'promoterId', '3': 1, '4': 1, '5': 3, '10': 'promoterId'},
  ],
};

/// Descriptor for `BindPromoterReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List bindPromoterReqDescriptor = $convert.base64Decode('Cg9CaW5kUHJvbW90ZXJSZXESHgoKcHJvbW90ZXJJZBgBIAEoA1IKcHJvbW90ZXJJZA==');
@$core.Deprecated('Use bindPromoterRespDescriptor instead')
const BindPromoterResp$json = const {
  '1': 'BindPromoterResp',
};

/// Descriptor for `BindPromoterResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List bindPromoterRespDescriptor = $convert.base64Decode('ChBCaW5kUHJvbW90ZXJSZXNw');
@$core.Deprecated('Use queryBillReqDescriptor instead')
const QueryBillReq$json = const {
  '1': 'QueryBillReq',
  '2': const [
    const {'1': 'month', '3': 1, '4': 1, '5': 5, '10': 'month'},
  ],
};

/// Descriptor for `QueryBillReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List queryBillReqDescriptor = $convert.base64Decode('CgxRdWVyeUJpbGxSZXESFAoFbW9udGgYASABKAVSBW1vbnRo');
@$core.Deprecated('Use billItemDescriptor instead')
const BillItem$json = const {
  '1': 'BillItem',
  '2': const [
    const {'1': 'icon', '3': 1, '4': 1, '5': 9, '10': 'icon'},
    const {'1': 'productName', '3': 2, '4': 1, '5': 9, '10': 'productName'},
    const {'1': 'param', '3': 3, '4': 1, '5': 9, '10': 'param'},
    const {'1': 'type', '3': 4, '4': 1, '5': 5, '10': 'type'},
    const {'1': 'status', '3': 5, '4': 1, '5': 5, '10': 'status'},
    const {'1': 'way', '3': 6, '4': 1, '5': 5, '10': 'way'},
    const {'1': 'money', '3': 7, '4': 1, '5': 3, '10': 'money'},
    const {'1': 'afterMoney', '3': 8, '4': 1, '5': 3, '10': 'afterMoney'},
    const {'1': 'opId', '3': 9, '4': 1, '5': 3, '10': 'opId'},
    const {'1': 'innerOrderId', '3': 10, '4': 1, '5': 9, '10': 'innerOrderId'},
    const {'1': 'outerOrderId', '3': 11, '4': 1, '5': 9, '10': 'outerOrderId'},
    const {'1': 'time', '3': 12, '4': 1, '5': 3, '10': 'time'},
  ],
};

/// Descriptor for `BillItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List billItemDescriptor = $convert.base64Decode('CghCaWxsSXRlbRISCgRpY29uGAEgASgJUgRpY29uEiAKC3Byb2R1Y3ROYW1lGAIgASgJUgtwcm9kdWN0TmFtZRIUCgVwYXJhbRgDIAEoCVIFcGFyYW0SEgoEdHlwZRgEIAEoBVIEdHlwZRIWCgZzdGF0dXMYBSABKAVSBnN0YXR1cxIQCgN3YXkYBiABKAVSA3dheRIUCgVtb25leRgHIAEoA1IFbW9uZXkSHgoKYWZ0ZXJNb25leRgIIAEoA1IKYWZ0ZXJNb25leRISCgRvcElkGAkgASgDUgRvcElkEiIKDGlubmVyT3JkZXJJZBgKIAEoCVIMaW5uZXJPcmRlcklkEiIKDG91dGVyT3JkZXJJZBgLIAEoCVIMb3V0ZXJPcmRlcklkEhIKBHRpbWUYDCABKANSBHRpbWU=');
@$core.Deprecated('Use queryBillRespDescriptor instead')
const QueryBillResp$json = const {
  '1': 'QueryBillResp',
  '2': const [
    const {'1': 'items', '3': 1, '4': 3, '5': 11, '6': '.pb.BillItem', '10': 'items'},
  ],
};

/// Descriptor for `QueryBillResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List queryBillRespDescriptor = $convert.base64Decode('Cg1RdWVyeUJpbGxSZXNwEiIKBWl0ZW1zGAEgAygLMgwucGIuQmlsbEl0ZW1SBWl0ZW1z');
@$core.Deprecated('Use getRedPacketRobAuthReqDescriptor instead')
const GetRedPacketRobAuthReq$json = const {
  '1': 'GetRedPacketRobAuthReq',
};

/// Descriptor for `GetRedPacketRobAuthReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getRedPacketRobAuthReqDescriptor = $convert.base64Decode('ChZHZXRSZWRQYWNrZXRSb2JBdXRoUmVx');
@$core.Deprecated('Use getRedPacketRobAuthRespDescriptor instead')
const GetRedPacketRobAuthResp$json = const {
  '1': 'GetRedPacketRobAuthResp',
  '2': const [
    const {'1': 'items', '3': 1, '4': 3, '5': 11, '6': '.pb.RobAuthItem', '10': 'items'},
  ],
};

/// Descriptor for `GetRedPacketRobAuthResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getRedPacketRobAuthRespDescriptor = $convert.base64Decode('ChdHZXRSZWRQYWNrZXRSb2JBdXRoUmVzcBIlCgVpdGVtcxgBIAMoCzIPLnBiLlJvYkF1dGhJdGVtUgVpdGVtcw==');
@$core.Deprecated('Use getAllRoomReqDescriptor instead')
const GetAllRoomReq$json = const {
  '1': 'GetAllRoomReq',
};

/// Descriptor for `GetAllRoomReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getAllRoomReqDescriptor = $convert.base64Decode('Cg1HZXRBbGxSb29tUmVx');
@$core.Deprecated('Use getAllRoomRespDescriptor instead')
const GetAllRoomResp$json = const {
  '1': 'GetAllRoomResp',
  '2': const [
    const {'1': 'rooms', '3': 1, '4': 3, '5': 3, '10': 'rooms'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'avatar_url', '3': 3, '4': 1, '5': 9, '10': 'avatarUrl'},
    const {'1': 'introduction', '3': 4, '4': 1, '5': 9, '10': 'introduction'},
    const {'1': 'extra', '3': 5, '4': 1, '5': 9, '10': 'extra'},
  ],
};

/// Descriptor for `GetAllRoomResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getAllRoomRespDescriptor = $convert.base64Decode('Cg5HZXRBbGxSb29tUmVzcBIUCgVyb29tcxgBIAMoA1IFcm9vbXMSEgoEbmFtZRgCIAEoCVIEbmFtZRIdCgphdmF0YXJfdXJsGAMgASgJUglhdmF0YXJVcmwSIgoMaW50cm9kdWN0aW9uGAQgASgJUgxpbnRyb2R1Y3Rpb24SFAoFZXh0cmEYBSABKAlSBWV4dHJh');
@$core.Deprecated('Use roomMemberDescriptor instead')
const RoomMember$json = const {
  '1': 'RoomMember',
  '2': const [
    const {'1': 'user_id', '3': 1, '4': 1, '5': 3, '10': 'userId'},
    const {'1': 'nickname', '3': 2, '4': 1, '5': 9, '10': 'nickname'},
    const {'1': 'sex', '3': 3, '4': 1, '5': 5, '10': 'sex'},
    const {'1': 'avatar_url', '3': 4, '4': 1, '5': 9, '10': 'avatarUrl'},
    const {'1': 'extra', '3': 5, '4': 1, '5': 9, '10': 'extra'},
  ],
};

/// Descriptor for `RoomMember`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List roomMemberDescriptor = $convert.base64Decode('CgpSb29tTWVtYmVyEhcKB3VzZXJfaWQYASABKANSBnVzZXJJZBIaCghuaWNrbmFtZRgCIAEoCVIIbmlja25hbWUSEAoDc2V4GAMgASgFUgNzZXgSHQoKYXZhdGFyX3VybBgEIAEoCVIJYXZhdGFyVXJsEhQKBWV4dHJhGAUgASgJUgVleHRyYQ==');
@$core.Deprecated('Use getRoomUserListReqDescriptor instead')
const GetRoomUserListReq$json = const {
  '1': 'GetRoomUserListReq',
  '2': const [
    const {'1': 'room_id', '3': 1, '4': 1, '5': 3, '10': 'roomId'},
  ],
};

/// Descriptor for `GetRoomUserListReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getRoomUserListReqDescriptor = $convert.base64Decode('ChJHZXRSb29tVXNlckxpc3RSZXESFwoHcm9vbV9pZBgBIAEoA1IGcm9vbUlk');
@$core.Deprecated('Use getRoomUserListRespDescriptor instead')
const GetRoomUserListResp$json = const {
  '1': 'GetRoomUserListResp',
  '2': const [
    const {'1': 'userList', '3': 1, '4': 3, '5': 11, '6': '.pb.RoomMember', '10': 'userList'},
  ],
};

/// Descriptor for `GetRoomUserListResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getRoomUserListRespDescriptor = $convert.base64Decode('ChNHZXRSb29tVXNlckxpc3RSZXNwEioKCHVzZXJMaXN0GAEgAygLMg4ucGIuUm9vbU1lbWJlclIIdXNlckxpc3Q=');
