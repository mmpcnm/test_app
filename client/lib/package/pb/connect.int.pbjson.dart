///
//  Generated code. Do not modify.
//  source: connect.int.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use deliverMessageReqDescriptor instead')
const DeliverMessageReq$json = const {
  '1': 'DeliverMessageReq',
  '2': const [
    const {'1': 'user_id', '3': 1, '4': 1, '5': 3, '10': 'userId'},
    const {'1': 'message_send', '3': 2, '4': 1, '5': 11, '6': '.pb.MessageSend', '10': 'messageSend'},
  ],
};

/// Descriptor for `DeliverMessageReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List deliverMessageReqDescriptor = $convert.base64Decode('ChFEZWxpdmVyTWVzc2FnZVJlcRIXCgd1c2VyX2lkGAEgASgDUgZ1c2VySWQSMgoMbWVzc2FnZV9zZW5kGAIgASgLMg8ucGIuTWVzc2FnZVNlbmRSC21lc3NhZ2VTZW5k');
@$core.Deprecated('Use deliverMessageRespDescriptor instead')
const DeliverMessageResp$json = const {
  '1': 'DeliverMessageResp',
};

/// Descriptor for `DeliverMessageResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List deliverMessageRespDescriptor = $convert.base64Decode('ChJEZWxpdmVyTWVzc2FnZVJlc3A=');
@$core.Deprecated('Use pushRoomMsgDescriptor instead')
const PushRoomMsg$json = const {
  '1': 'PushRoomMsg',
  '2': const [
    const {'1': 'room_id', '3': 1, '4': 1, '5': 3, '10': 'roomId'},
    const {'1': 'message_send', '3': 2, '4': 1, '5': 11, '6': '.pb.MessageSend', '10': 'messageSend'},
  ],
};

/// Descriptor for `PushRoomMsg`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List pushRoomMsgDescriptor = $convert.base64Decode('CgtQdXNoUm9vbU1zZxIXCgdyb29tX2lkGAEgASgDUgZyb29tSWQSMgoMbWVzc2FnZV9zZW5kGAIgASgLMg8ucGIuTWVzc2FnZVNlbmRSC21lc3NhZ2VTZW5k');
@$core.Deprecated('Use pushAllMsgDescriptor instead')
const PushAllMsg$json = const {
  '1': 'PushAllMsg',
  '2': const [
    const {'1': 'message_send', '3': 2, '4': 1, '5': 11, '6': '.pb.MessageSend', '10': 'messageSend'},
  ],
};

/// Descriptor for `PushAllMsg`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List pushAllMsgDescriptor = $convert.base64Decode('CgpQdXNoQWxsTXNnEjIKDG1lc3NhZ2Vfc2VuZBgCIAEoCzIPLnBiLk1lc3NhZ2VTZW5kUgttZXNzYWdlU2VuZA==');
