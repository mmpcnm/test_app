///
//  Generated code. Do not modify.
//  source: connect.int.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'connect.int.pb.dart' as $1;
export 'connect.int.pb.dart';

class ConnectIntClient extends $grpc.Client {
  static final _$deliverMessage =
      $grpc.ClientMethod<$1.DeliverMessageReq, $1.DeliverMessageResp>(
          '/pb.ConnectInt/DeliverMessage',
          ($1.DeliverMessageReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $1.DeliverMessageResp.fromBuffer(value));

  ConnectIntClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$1.DeliverMessageResp> deliverMessage(
      $1.DeliverMessageReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deliverMessage, request, options: options);
  }
}

abstract class ConnectIntServiceBase extends $grpc.Service {
  $core.String get $name => 'pb.ConnectInt';

  ConnectIntServiceBase() {
    $addMethod($grpc.ServiceMethod<$1.DeliverMessageReq, $1.DeliverMessageResp>(
        'DeliverMessage',
        deliverMessage_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.DeliverMessageReq.fromBuffer(value),
        ($1.DeliverMessageResp value) => value.writeToBuffer()));
  }

  $async.Future<$1.DeliverMessageResp> deliverMessage_Pre(
      $grpc.ServiceCall call,
      $async.Future<$1.DeliverMessageReq> request) async {
    return deliverMessage(call, await request);
  }

  $async.Future<$1.DeliverMessageResp> deliverMessage(
      $grpc.ServiceCall call, $1.DeliverMessageReq request);
}
