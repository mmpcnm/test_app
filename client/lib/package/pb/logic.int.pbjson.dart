///
//  Generated code. Do not modify.
//  source: logic.int.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use connSignInReqDescriptor instead')
const ConnSignInReq$json = const {
  '1': 'ConnSignInReq',
  '2': const [
    const {'1': 'machine_id', '3': 1, '4': 1, '5': 9, '10': 'machineId'},
    const {'1': 'user_id', '3': 2, '4': 1, '5': 3, '10': 'userId'},
    const {'1': 'token', '3': 3, '4': 1, '5': 9, '10': 'token'},
    const {'1': 'conn_addr', '3': 4, '4': 1, '5': 9, '10': 'connAddr'},
    const {'1': 'client_addr', '3': 5, '4': 1, '5': 9, '10': 'clientAddr'},
  ],
};

/// Descriptor for `ConnSignInReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List connSignInReqDescriptor = $convert.base64Decode('Cg1Db25uU2lnbkluUmVxEh0KCm1hY2hpbmVfaWQYASABKAlSCW1hY2hpbmVJZBIXCgd1c2VyX2lkGAIgASgDUgZ1c2VySWQSFAoFdG9rZW4YAyABKAlSBXRva2VuEhsKCWNvbm5fYWRkchgEIAEoCVIIY29ubkFkZHISHwoLY2xpZW50X2FkZHIYBSABKAlSCmNsaWVudEFkZHI=');
@$core.Deprecated('Use connSignInRespDescriptor instead')
const ConnSignInResp$json = const {
  '1': 'ConnSignInResp',
};

/// Descriptor for `ConnSignInResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List connSignInRespDescriptor = $convert.base64Decode('Cg5Db25uU2lnbkluUmVzcA==');
@$core.Deprecated('Use syncReqDescriptor instead')
const SyncReq$json = const {
  '1': 'SyncReq',
  '2': const [
    const {'1': 'user_id', '3': 1, '4': 1, '5': 3, '10': 'userId'},
    const {'1': 'machine_id', '3': 2, '4': 1, '5': 9, '10': 'machineId'},
    const {'1': 'seq', '3': 3, '4': 1, '5': 3, '10': 'seq'},
  ],
};

/// Descriptor for `SyncReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List syncReqDescriptor = $convert.base64Decode('CgdTeW5jUmVxEhcKB3VzZXJfaWQYASABKANSBnVzZXJJZBIdCgptYWNoaW5lX2lkGAIgASgJUgltYWNoaW5lSWQSEAoDc2VxGAMgASgDUgNzZXE=');
@$core.Deprecated('Use syncRespDescriptor instead')
const SyncResp$json = const {
  '1': 'SyncResp',
  '2': const [
    const {'1': 'messages', '3': 1, '4': 3, '5': 11, '6': '.pb.Message', '10': 'messages'},
    const {'1': 'has_more', '3': 2, '4': 1, '5': 8, '10': 'hasMore'},
  ],
};

/// Descriptor for `SyncResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List syncRespDescriptor = $convert.base64Decode('CghTeW5jUmVzcBInCghtZXNzYWdlcxgBIAMoCzILLnBiLk1lc3NhZ2VSCG1lc3NhZ2VzEhkKCGhhc19tb3JlGAIgASgIUgdoYXNNb3Jl');
@$core.Deprecated('Use messageACKReqDescriptor instead')
const MessageACKReq$json = const {
  '1': 'MessageACKReq',
  '2': const [
    const {'1': 'user_id', '3': 1, '4': 1, '5': 3, '10': 'userId'},
    const {'1': 'machine_id', '3': 2, '4': 1, '5': 9, '10': 'machineId'},
    const {'1': 'device_ack', '3': 3, '4': 1, '5': 3, '10': 'deviceAck'},
    const {'1': 'receive_time', '3': 4, '4': 1, '5': 3, '10': 'receiveTime'},
  ],
};

/// Descriptor for `MessageACKReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List messageACKReqDescriptor = $convert.base64Decode('Cg1NZXNzYWdlQUNLUmVxEhcKB3VzZXJfaWQYASABKANSBnVzZXJJZBIdCgptYWNoaW5lX2lkGAIgASgJUgltYWNoaW5lSWQSHQoKZGV2aWNlX2FjaxgDIAEoA1IJZGV2aWNlQWNrEiEKDHJlY2VpdmVfdGltZRgEIAEoA1ILcmVjZWl2ZVRpbWU=');
@$core.Deprecated('Use messageACKRespDescriptor instead')
const MessageACKResp$json = const {
  '1': 'MessageACKResp',
};

/// Descriptor for `MessageACKResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List messageACKRespDescriptor = $convert.base64Decode('Cg5NZXNzYWdlQUNLUmVzcA==');
@$core.Deprecated('Use offlineReqDescriptor instead')
const OfflineReq$json = const {
  '1': 'OfflineReq',
  '2': const [
    const {'1': 'user_id', '3': 1, '4': 1, '5': 3, '10': 'userId'},
    const {'1': 'machine_id', '3': 2, '4': 1, '5': 9, '10': 'machineId'},
    const {'1': 'client_addr', '3': 3, '4': 1, '5': 9, '10': 'clientAddr'},
    const {'1': 'room_id', '3': 4, '4': 1, '5': 3, '10': 'roomId'},
  ],
};

/// Descriptor for `OfflineReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List offlineReqDescriptor = $convert.base64Decode('CgpPZmZsaW5lUmVxEhcKB3VzZXJfaWQYASABKANSBnVzZXJJZBIdCgptYWNoaW5lX2lkGAIgASgJUgltYWNoaW5lSWQSHwoLY2xpZW50X2FkZHIYAyABKAlSCmNsaWVudEFkZHISFwoHcm9vbV9pZBgEIAEoA1IGcm9vbUlk');
@$core.Deprecated('Use offlineRespDescriptor instead')
const OfflineResp$json = const {
  '1': 'OfflineResp',
};

/// Descriptor for `OfflineResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List offlineRespDescriptor = $convert.base64Decode('CgtPZmZsaW5lUmVzcA==');
@$core.Deprecated('Use subscribeRoomReqDescriptor instead')
const SubscribeRoomReq$json = const {
  '1': 'SubscribeRoomReq',
  '2': const [
    const {'1': 'user_id', '3': 1, '4': 1, '5': 3, '10': 'userId'},
    const {'1': 'machine_id', '3': 2, '4': 1, '5': 9, '10': 'machineId'},
    const {'1': 'room_id', '3': 3, '4': 1, '5': 3, '10': 'roomId'},
    const {'1': 'seq', '3': 4, '4': 1, '5': 3, '10': 'seq'},
    const {'1': 'conn_addr', '3': 5, '4': 1, '5': 9, '10': 'connAddr'},
  ],
};

/// Descriptor for `SubscribeRoomReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List subscribeRoomReqDescriptor = $convert.base64Decode('ChBTdWJzY3JpYmVSb29tUmVxEhcKB3VzZXJfaWQYASABKANSBnVzZXJJZBIdCgptYWNoaW5lX2lkGAIgASgJUgltYWNoaW5lSWQSFwoHcm9vbV9pZBgDIAEoA1IGcm9vbUlkEhAKA3NlcRgEIAEoA1IDc2VxEhsKCWNvbm5fYWRkchgFIAEoCVIIY29ubkFkZHI=');
@$core.Deprecated('Use subscribeRoomRespDescriptor instead')
const SubscribeRoomResp$json = const {
  '1': 'SubscribeRoomResp',
};

/// Descriptor for `SubscribeRoomResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List subscribeRoomRespDescriptor = $convert.base64Decode('ChFTdWJzY3JpYmVSb29tUmVzcA==');
@$core.Deprecated('Use pushAllReqDescriptor instead')
const PushAllReq$json = const {
  '1': 'PushAllReq',
  '2': const [
    const {'1': 'message_type', '3': 1, '4': 1, '5': 14, '6': '.pb.MessageType', '10': 'messageType'},
    const {'1': 'message_content', '3': 2, '4': 1, '5': 12, '10': 'messageContent'},
    const {'1': 'send_time', '3': 3, '4': 1, '5': 3, '10': 'sendTime'},
  ],
};

/// Descriptor for `PushAllReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List pushAllReqDescriptor = $convert.base64Decode('CgpQdXNoQWxsUmVxEjIKDG1lc3NhZ2VfdHlwZRgBIAEoDjIPLnBiLk1lc3NhZ2VUeXBlUgttZXNzYWdlVHlwZRInCg9tZXNzYWdlX2NvbnRlbnQYAiABKAxSDm1lc3NhZ2VDb250ZW50EhsKCXNlbmRfdGltZRgDIAEoA1IIc2VuZFRpbWU=');
@$core.Deprecated('Use pushAllRespDescriptor instead')
const PushAllResp$json = const {
  '1': 'PushAllResp',
};

/// Descriptor for `PushAllResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List pushAllRespDescriptor = $convert.base64Decode('CgtQdXNoQWxsUmVzcA==');
@$core.Deprecated('Use getUsersReqDescriptor instead')
const GetUsersReq$json = const {
  '1': 'GetUsersReq',
  '2': const [
    const {'1': 'user_ids', '3': 1, '4': 3, '5': 11, '6': '.pb.GetUsersReq.UserIdsEntry', '10': 'userIds'},
  ],
  '3': const [GetUsersReq_UserIdsEntry$json],
};

@$core.Deprecated('Use getUsersReqDescriptor instead')
const GetUsersReq_UserIdsEntry$json = const {
  '1': 'UserIdsEntry',
  '2': const [
    const {'1': 'key', '3': 1, '4': 1, '5': 3, '10': 'key'},
    const {'1': 'value', '3': 2, '4': 1, '5': 5, '10': 'value'},
  ],
  '7': const {'7': true},
};

/// Descriptor for `GetUsersReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getUsersReqDescriptor = $convert.base64Decode('CgtHZXRVc2Vyc1JlcRI3Cgh1c2VyX2lkcxgBIAMoCzIcLnBiLkdldFVzZXJzUmVxLlVzZXJJZHNFbnRyeVIHdXNlcklkcxo6CgxVc2VySWRzRW50cnkSEAoDa2V5GAEgASgDUgNrZXkSFAoFdmFsdWUYAiABKAVSBXZhbHVlOgI4AQ==');
@$core.Deprecated('Use getUsersRespDescriptor instead')
const GetUsersResp$json = const {
  '1': 'GetUsersResp',
  '2': const [
    const {'1': 'users', '3': 1, '4': 3, '5': 11, '6': '.pb.GetUsersResp.UsersEntry', '10': 'users'},
  ],
  '3': const [GetUsersResp_UsersEntry$json],
};

@$core.Deprecated('Use getUsersRespDescriptor instead')
const GetUsersResp_UsersEntry$json = const {
  '1': 'UsersEntry',
  '2': const [
    const {'1': 'key', '3': 1, '4': 1, '5': 3, '10': 'key'},
    const {'1': 'value', '3': 2, '4': 1, '5': 11, '6': '.pb.User', '10': 'value'},
  ],
  '7': const {'7': true},
};

/// Descriptor for `GetUsersResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getUsersRespDescriptor = $convert.base64Decode('CgxHZXRVc2Vyc1Jlc3ASMQoFdXNlcnMYASADKAsyGy5wYi5HZXRVc2Vyc1Jlc3AuVXNlcnNFbnRyeVIFdXNlcnMaQgoKVXNlcnNFbnRyeRIQCgNrZXkYASABKANSA2tleRIeCgV2YWx1ZRgCIAEoCzIILnBiLlVzZXJSBXZhbHVlOgI4AQ==');
@$core.Deprecated('Use serverStopReqDescriptor instead')
const ServerStopReq$json = const {
  '1': 'ServerStopReq',
  '2': const [
    const {'1': 'conn_addr', '3': 1, '4': 1, '5': 9, '10': 'connAddr'},
  ],
};

/// Descriptor for `ServerStopReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List serverStopReqDescriptor = $convert.base64Decode('Cg1TZXJ2ZXJTdG9wUmVxEhsKCWNvbm5fYWRkchgBIAEoCVIIY29ubkFkZHI=');
@$core.Deprecated('Use serverStopRespDescriptor instead')
const ServerStopResp$json = const {
  '1': 'ServerStopResp',
};

/// Descriptor for `ServerStopResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List serverStopRespDescriptor = $convert.base64Decode('Cg5TZXJ2ZXJTdG9wUmVzcA==');
@$core.Deprecated('Use setUserFlagReqDescriptor instead')
const SetUserFlagReq$json = const {
  '1': 'SetUserFlagReq',
  '2': const [
    const {'1': 'user_id', '3': 1, '4': 1, '5': 3, '10': 'userId'},
    const {'1': 'type', '3': 2, '4': 1, '5': 14, '6': '.pb.ReceiverType', '10': 'type'},
    const {'1': 'receive_id', '3': 3, '4': 1, '5': 3, '10': 'receiveId'},
    const {'1': 'flag', '3': 4, '4': 1, '5': 3, '10': 'flag'},
  ],
};

/// Descriptor for `SetUserFlagReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List setUserFlagReqDescriptor = $convert.base64Decode('Cg5TZXRVc2VyRmxhZ1JlcRIXCgd1c2VyX2lkGAEgASgDUgZ1c2VySWQSJAoEdHlwZRgCIAEoDjIQLnBiLlJlY2VpdmVyVHlwZVIEdHlwZRIdCgpyZWNlaXZlX2lkGAMgASgDUglyZWNlaXZlSWQSEgoEZmxhZxgEIAEoA1IEZmxhZw==');
@$core.Deprecated('Use setUserFlagRespDescriptor instead')
const SetUserFlagResp$json = const {
  '1': 'SetUserFlagResp',
};

/// Descriptor for `SetUserFlagResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List setUserFlagRespDescriptor = $convert.base64Decode('Cg9TZXRVc2VyRmxhZ1Jlc3A=');
@$core.Deprecated('Use bindAlipayIntReqDescriptor instead')
const BindAlipayIntReq$json = const {
  '1': 'BindAlipayIntReq',
  '2': const [
    const {'1': 'userId', '3': 1, '4': 1, '5': 3, '10': 'userId'},
    const {'1': 'realname', '3': 2, '4': 1, '5': 9, '10': 'realname'},
    const {'1': 'alipay_account', '3': 3, '4': 1, '5': 9, '10': 'alipayAccount'},
  ],
};

/// Descriptor for `BindAlipayIntReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List bindAlipayIntReqDescriptor = $convert.base64Decode('ChBCaW5kQWxpcGF5SW50UmVxEhYKBnVzZXJJZBgBIAEoA1IGdXNlcklkEhoKCHJlYWxuYW1lGAIgASgJUghyZWFsbmFtZRIlCg5hbGlwYXlfYWNjb3VudBgDIAEoCVINYWxpcGF5QWNjb3VudA==');
@$core.Deprecated('Use bindAlipayIntRespDescriptor instead')
const BindAlipayIntResp$json = const {
  '1': 'BindAlipayIntResp',
};

/// Descriptor for `BindAlipayIntResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List bindAlipayIntRespDescriptor = $convert.base64Decode('ChFCaW5kQWxpcGF5SW50UmVzcA==');
@$core.Deprecated('Use bindPromoterIdIntReqDescriptor instead')
const BindPromoterIdIntReq$json = const {
  '1': 'BindPromoterIdIntReq',
  '2': const [
    const {'1': 'userId', '3': 1, '4': 1, '5': 3, '10': 'userId'},
    const {'1': 'promoterId', '3': 2, '4': 1, '5': 3, '10': 'promoterId'},
  ],
};

/// Descriptor for `BindPromoterIdIntReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List bindPromoterIdIntReqDescriptor = $convert.base64Decode('ChRCaW5kUHJvbW90ZXJJZEludFJlcRIWCgZ1c2VySWQYASABKANSBnVzZXJJZBIeCgpwcm9tb3RlcklkGAIgASgDUgpwcm9tb3Rlcklk');
@$core.Deprecated('Use bindPromoterIdIntRespDescriptor instead')
const BindPromoterIdIntResp$json = const {
  '1': 'BindPromoterIdIntResp',
};

/// Descriptor for `BindPromoterIdIntResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List bindPromoterIdIntRespDescriptor = $convert.base64Decode('ChVCaW5kUHJvbW90ZXJJZEludFJlc3A=');
@$core.Deprecated('Use resetPwdReqDescriptor instead')
const ResetPwdReq$json = const {
  '1': 'ResetPwdReq',
  '2': const [
    const {'1': 'userId', '3': 1, '4': 1, '5': 3, '10': 'userId'},
    const {'1': 'newPwd', '3': 2, '4': 1, '5': 9, '10': 'newPwd'},
  ],
};

/// Descriptor for `ResetPwdReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List resetPwdReqDescriptor = $convert.base64Decode('CgtSZXNldFB3ZFJlcRIWCgZ1c2VySWQYASABKANSBnVzZXJJZBIWCgZuZXdQd2QYAiABKAlSBm5ld1B3ZA==');
@$core.Deprecated('Use resetPwdRespDescriptor instead')
const ResetPwdResp$json = const {
  '1': 'ResetPwdResp',
};

/// Descriptor for `ResetPwdResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List resetPwdRespDescriptor = $convert.base64Decode('CgxSZXNldFB3ZFJlc3A=');
@$core.Deprecated('Use setRedPacketGroupBaseConfigReqDescriptor instead')
const SetRedPacketGroupBaseConfigReq$json = const {
  '1': 'SetRedPacketGroupBaseConfigReq',
  '2': const [
    const {'1': 'channel', '3': 1, '4': 1, '5': 3, '10': 'channel'},
    const {'1': 'min_num', '3': 2, '4': 1, '5': 5, '10': 'minNum'},
    const {'1': 'max_num', '3': 3, '4': 1, '5': 5, '10': 'maxNum'},
    const {'1': 'max_amount', '3': 4, '4': 1, '5': 3, '10': 'maxAmount'},
  ],
};

/// Descriptor for `SetRedPacketGroupBaseConfigReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List setRedPacketGroupBaseConfigReqDescriptor = $convert.base64Decode('Ch5TZXRSZWRQYWNrZXRHcm91cEJhc2VDb25maWdSZXESGAoHY2hhbm5lbBgBIAEoA1IHY2hhbm5lbBIXCgdtaW5fbnVtGAIgASgFUgZtaW5OdW0SFwoHbWF4X251bRgDIAEoBVIGbWF4TnVtEh0KCm1heF9hbW91bnQYBCABKANSCW1heEFtb3VudA==');
@$core.Deprecated('Use setRedPacketGroupBaseConfigRespDescriptor instead')
const SetRedPacketGroupBaseConfigResp$json = const {
  '1': 'SetRedPacketGroupBaseConfigResp',
};

/// Descriptor for `SetRedPacketGroupBaseConfigResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List setRedPacketGroupBaseConfigRespDescriptor = $convert.base64Decode('Ch9TZXRSZWRQYWNrZXRHcm91cEJhc2VDb25maWdSZXNw');
@$core.Deprecated('Use registerFakeRoomReqDescriptor instead')
const RegisterFakeRoomReq$json = const {
  '1': 'RegisterFakeRoomReq',
  '2': const [
    const {'1': 'Channel', '3': 1, '4': 1, '5': 3, '10': 'Channel'},
    const {'1': 'Name', '3': 2, '4': 1, '5': 9, '10': 'Name'},
    const {'1': 'AvatarUrl', '3': 3, '4': 1, '5': 9, '10': 'AvatarUrl'},
    const {'1': 'Introduction', '3': 4, '4': 1, '5': 9, '10': 'Introduction'},
    const {'1': 'Extra', '3': 5, '4': 1, '5': 9, '10': 'Extra'},
  ],
};

/// Descriptor for `RegisterFakeRoomReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List registerFakeRoomReqDescriptor = $convert.base64Decode('ChNSZWdpc3RlckZha2VSb29tUmVxEhgKB0NoYW5uZWwYASABKANSB0NoYW5uZWwSEgoETmFtZRgCIAEoCVIETmFtZRIcCglBdmF0YXJVcmwYAyABKAlSCUF2YXRhclVybBIiCgxJbnRyb2R1Y3Rpb24YBCABKAlSDEludHJvZHVjdGlvbhIUCgVFeHRyYRgFIAEoCVIFRXh0cmE=');
@$core.Deprecated('Use registerFakeRoomRespDescriptor instead')
const RegisterFakeRoomResp$json = const {
  '1': 'RegisterFakeRoomResp',
};

/// Descriptor for `RegisterFakeRoomResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List registerFakeRoomRespDescriptor = $convert.base64Decode('ChRSZWdpc3RlckZha2VSb29tUmVzcA==');
@$core.Deprecated('Use unregisterFakeRoomReqDescriptor instead')
const UnregisterFakeRoomReq$json = const {
  '1': 'UnregisterFakeRoomReq',
  '2': const [
    const {'1': 'Channel', '3': 1, '4': 1, '5': 3, '10': 'Channel'},
  ],
};

/// Descriptor for `UnregisterFakeRoomReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List unregisterFakeRoomReqDescriptor = $convert.base64Decode('ChVVbnJlZ2lzdGVyRmFrZVJvb21SZXESGAoHQ2hhbm5lbBgBIAEoA1IHQ2hhbm5lbA==');
@$core.Deprecated('Use unregisterFakeRoomRespDescriptor instead')
const UnregisterFakeRoomResp$json = const {
  '1': 'UnregisterFakeRoomResp',
};

/// Descriptor for `UnregisterFakeRoomResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List unregisterFakeRoomRespDescriptor = $convert.base64Decode('ChZVbnJlZ2lzdGVyRmFrZVJvb21SZXNw');
