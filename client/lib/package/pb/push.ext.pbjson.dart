///
//  Generated code. Do not modify.
//  source: push.ext.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use pushCodeDescriptor instead')
const PushCode$json = const {
  '1': 'PushCode',
  '2': const [
    const {'1': 'PC_ADD_DEFAULT', '2': 0},
    const {'1': 'PC_ADD_FRIEND', '2': 100},
    const {'1': 'PC_AGREE_ADD_FRIEND', '2': 101},
    const {'1': 'PC_UPDATE_GROUP', '2': 110},
    const {'1': 'PC_ADD_GROUP_MEMBERS', '2': 120},
    const {'1': 'PC_REMOVE_GROUP_MEMBER', '2': 121},
    const {'1': 'PC_CANCEL_MESSAGE', '2': 131},
    const {'1': 'PC_REFUND_NOTIFY', '2': 141},
  ],
};

/// Descriptor for `PushCode`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List pushCodeDescriptor = $convert.base64Decode('CghQdXNoQ29kZRISCg5QQ19BRERfREVGQVVMVBAAEhEKDVBDX0FERF9GUklFTkQQZBIXChNQQ19BR1JFRV9BRERfRlJJRU5EEGUSEwoPUENfVVBEQVRFX0dST1VQEG4SGAoUUENfQUREX0dST1VQX01FTUJFUlMQeBIaChZQQ19SRU1PVkVfR1JPVVBfTUVNQkVSEHkSFgoRUENfQ0FOQ0VMX01FU1NBR0UQgwESFQoQUENfUkVGVU5EX05PVElGWRCNAQ==');
@$core.Deprecated('Use addFriendPushDescriptor instead')
const AddFriendPush$json = const {
  '1': 'AddFriendPush',
  '2': const [
    const {'1': 'friend_id', '3': 1, '4': 1, '5': 3, '10': 'friendId'},
    const {'1': 'nickname', '3': 2, '4': 1, '5': 9, '10': 'nickname'},
    const {'1': 'avatar_url', '3': 3, '4': 1, '5': 9, '10': 'avatarUrl'},
    const {'1': 'description', '3': 4, '4': 1, '5': 9, '10': 'description'},
  ],
};

/// Descriptor for `AddFriendPush`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List addFriendPushDescriptor = $convert.base64Decode('Cg1BZGRGcmllbmRQdXNoEhsKCWZyaWVuZF9pZBgBIAEoA1IIZnJpZW5kSWQSGgoIbmlja25hbWUYAiABKAlSCG5pY2tuYW1lEh0KCmF2YXRhcl91cmwYAyABKAlSCWF2YXRhclVybBIgCgtkZXNjcmlwdGlvbhgEIAEoCVILZGVzY3JpcHRpb24=');
@$core.Deprecated('Use agreeAddFriendPushDescriptor instead')
const AgreeAddFriendPush$json = const {
  '1': 'AgreeAddFriendPush',
  '2': const [
    const {'1': 'friend_id', '3': 1, '4': 1, '5': 3, '10': 'friendId'},
    const {'1': 'nickname', '3': 2, '4': 1, '5': 9, '10': 'nickname'},
    const {'1': 'avatar_url', '3': 3, '4': 1, '5': 9, '10': 'avatarUrl'},
  ],
};

/// Descriptor for `AgreeAddFriendPush`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List agreeAddFriendPushDescriptor = $convert.base64Decode('ChJBZ3JlZUFkZEZyaWVuZFB1c2gSGwoJZnJpZW5kX2lkGAEgASgDUghmcmllbmRJZBIaCghuaWNrbmFtZRgCIAEoCVIIbmlja25hbWUSHQoKYXZhdGFyX3VybBgDIAEoCVIJYXZhdGFyVXJs');
@$core.Deprecated('Use updateGroupPushDescriptor instead')
const UpdateGroupPush$json = const {
  '1': 'UpdateGroupPush',
  '2': const [
    const {'1': 'opt_id', '3': 1, '4': 1, '5': 3, '10': 'optId'},
    const {'1': 'opt_name', '3': 2, '4': 1, '5': 9, '10': 'optName'},
    const {'1': 'group_id', '3': 3, '4': 1, '5': 3, '10': 'groupId'},
    const {'1': 'name', '3': 4, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'avatar_url', '3': 5, '4': 1, '5': 9, '10': 'avatarUrl'},
    const {'1': 'introduction', '3': 6, '4': 1, '5': 9, '10': 'introduction'},
    const {'1': 'extra', '3': 7, '4': 1, '5': 9, '10': 'extra'},
  ],
};

/// Descriptor for `UpdateGroupPush`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updateGroupPushDescriptor = $convert.base64Decode('Cg9VcGRhdGVHcm91cFB1c2gSFQoGb3B0X2lkGAEgASgDUgVvcHRJZBIZCghvcHRfbmFtZRgCIAEoCVIHb3B0TmFtZRIZCghncm91cF9pZBgDIAEoA1IHZ3JvdXBJZBISCgRuYW1lGAQgASgJUgRuYW1lEh0KCmF2YXRhcl91cmwYBSABKAlSCWF2YXRhclVybBIiCgxpbnRyb2R1Y3Rpb24YBiABKAlSDGludHJvZHVjdGlvbhIUCgVleHRyYRgHIAEoCVIFZXh0cmE=');
@$core.Deprecated('Use addGroupMembersPushDescriptor instead')
const AddGroupMembersPush$json = const {
  '1': 'AddGroupMembersPush',
  '2': const [
    const {'1': 'opt_id', '3': 1, '4': 1, '5': 3, '10': 'optId'},
    const {'1': 'opt_name', '3': 2, '4': 1, '5': 9, '10': 'optName'},
    const {'1': 'group_id', '3': 3, '4': 1, '5': 3, '10': 'groupId'},
    const {'1': 'members', '3': 4, '4': 3, '5': 11, '6': '.pb.GroupMember', '10': 'members'},
  ],
};

/// Descriptor for `AddGroupMembersPush`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List addGroupMembersPushDescriptor = $convert.base64Decode('ChNBZGRHcm91cE1lbWJlcnNQdXNoEhUKBm9wdF9pZBgBIAEoA1IFb3B0SWQSGQoIb3B0X25hbWUYAiABKAlSB29wdE5hbWUSGQoIZ3JvdXBfaWQYAyABKANSB2dyb3VwSWQSKQoHbWVtYmVycxgEIAMoCzIPLnBiLkdyb3VwTWVtYmVyUgdtZW1iZXJz');
@$core.Deprecated('Use removeGroupMemberPushDescriptor instead')
const RemoveGroupMemberPush$json = const {
  '1': 'RemoveGroupMemberPush',
  '2': const [
    const {'1': 'opt_id', '3': 1, '4': 1, '5': 3, '10': 'optId'},
    const {'1': 'opt_name', '3': 2, '4': 1, '5': 9, '10': 'optName'},
    const {'1': 'group_id', '3': 3, '4': 1, '5': 3, '10': 'groupId'},
    const {'1': 'deleted_user_id', '3': 4, '4': 1, '5': 3, '10': 'deletedUserId'},
  ],
};

/// Descriptor for `RemoveGroupMemberPush`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List removeGroupMemberPushDescriptor = $convert.base64Decode('ChVSZW1vdmVHcm91cE1lbWJlclB1c2gSFQoGb3B0X2lkGAEgASgDUgVvcHRJZBIZCghvcHRfbmFtZRgCIAEoCVIHb3B0TmFtZRIZCghncm91cF9pZBgDIAEoA1IHZ3JvdXBJZBImCg9kZWxldGVkX3VzZXJfaWQYBCABKANSDWRlbGV0ZWRVc2VySWQ=');
@$core.Deprecated('Use cancelMessagePushDescriptor instead')
const CancelMessagePush$json = const {
  '1': 'CancelMessagePush',
  '2': const [
    const {'1': 'opt_id', '3': 1, '4': 1, '5': 3, '10': 'optId'},
    const {'1': 'opt_name', '3': 2, '4': 1, '5': 9, '10': 'optName'},
    const {'1': 'receiver_type', '3': 3, '4': 1, '5': 14, '6': '.pb.ReceiverType', '10': 'receiverType'},
    const {'1': 'receiver_id', '3': 4, '4': 1, '5': 3, '10': 'receiverId'},
    const {'1': 'seq_id', '3': 5, '4': 1, '5': 3, '10': 'seqId'},
  ],
};

/// Descriptor for `CancelMessagePush`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List cancelMessagePushDescriptor = $convert.base64Decode('ChFDYW5jZWxNZXNzYWdlUHVzaBIVCgZvcHRfaWQYASABKANSBW9wdElkEhkKCG9wdF9uYW1lGAIgASgJUgdvcHROYW1lEjUKDXJlY2VpdmVyX3R5cGUYAyABKA4yEC5wYi5SZWNlaXZlclR5cGVSDHJlY2VpdmVyVHlwZRIfCgtyZWNlaXZlcl9pZBgEIAEoA1IKcmVjZWl2ZXJJZBIVCgZzZXFfaWQYBSABKANSBXNlcUlk');
@$core.Deprecated('Use refundRedPacketPushDescriptor instead')
const RefundRedPacketPush$json = const {
  '1': 'RefundRedPacketPush',
  '2': const [
    const {'1': 'receiver_type', '3': 1, '4': 1, '5': 14, '6': '.pb.ReceiverType', '10': 'receiverType'},
    const {'1': 'receiver_id', '3': 2, '4': 1, '5': 3, '10': 'receiverId'},
    const {'1': 'user_id', '3': 3, '4': 1, '5': 3, '10': 'userId'},
    const {'1': 'nick', '3': 4, '4': 1, '5': 9, '10': 'nick'},
    const {'1': 'rid', '3': 5, '4': 1, '5': 9, '10': 'rid'},
    const {'1': 'money', '3': 6, '4': 1, '5': 3, '10': 'money'},
  ],
};

/// Descriptor for `RefundRedPacketPush`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List refundRedPacketPushDescriptor = $convert.base64Decode('ChNSZWZ1bmRSZWRQYWNrZXRQdXNoEjUKDXJlY2VpdmVyX3R5cGUYASABKA4yEC5wYi5SZWNlaXZlclR5cGVSDHJlY2VpdmVyVHlwZRIfCgtyZWNlaXZlcl9pZBgCIAEoA1IKcmVjZWl2ZXJJZBIXCgd1c2VyX2lkGAMgASgDUgZ1c2VySWQSEgoEbmljaxgEIAEoCVIEbmljaxIQCgNyaWQYBSABKAlSA3JpZBIUCgVtb25leRgGIAEoA1IFbW9uZXk=');
