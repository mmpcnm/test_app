///
//  Generated code. Do not modify.
//  source: business.int.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import 'connect.ext.pb.dart' as $4;

import 'connect.ext.pbenum.dart' as $4;

class SendRedPacketReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SendRedPacketReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'rid')
    ..aInt64(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'avatar')
    ..e<$4.ReceiverType>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'type', $pb.PbFieldType.OE, defaultOrMaker: $4.ReceiverType.RT_UNKNOWN, valueOf: $4.ReceiverType.valueOf, enumValues: $4.ReceiverType.values)
    ..aInt64(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'receiveId')
    ..aInt64(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'amount')
    ..a<$core.int>(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'num', $pb.PbFieldType.O3)
    ..aOS(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'blessing')
    ..a<$core.int>(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'level', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  SendRedPacketReq._() : super();
  factory SendRedPacketReq({
    $core.String? rid,
    $fixnum.Int64? userId,
    $core.String? name,
    $core.String? avatar,
    $4.ReceiverType? type,
    $fixnum.Int64? receiveId,
    $fixnum.Int64? amount,
    $core.int? num,
    $core.String? blessing,
    $core.int? level,
  }) {
    final _result = create();
    if (rid != null) {
      _result.rid = rid;
    }
    if (userId != null) {
      _result.userId = userId;
    }
    if (name != null) {
      _result.name = name;
    }
    if (avatar != null) {
      _result.avatar = avatar;
    }
    if (type != null) {
      _result.type = type;
    }
    if (receiveId != null) {
      _result.receiveId = receiveId;
    }
    if (amount != null) {
      _result.amount = amount;
    }
    if (num != null) {
      _result.num = num;
    }
    if (blessing != null) {
      _result.blessing = blessing;
    }
    if (level != null) {
      _result.level = level;
    }
    return _result;
  }
  factory SendRedPacketReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SendRedPacketReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SendRedPacketReq clone() => SendRedPacketReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SendRedPacketReq copyWith(void Function(SendRedPacketReq) updates) => super.copyWith((message) => updates(message as SendRedPacketReq)) as SendRedPacketReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SendRedPacketReq create() => SendRedPacketReq._();
  SendRedPacketReq createEmptyInstance() => create();
  static $pb.PbList<SendRedPacketReq> createRepeated() => $pb.PbList<SendRedPacketReq>();
  @$core.pragma('dart2js:noInline')
  static SendRedPacketReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SendRedPacketReq>(create);
  static SendRedPacketReq? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get rid => $_getSZ(0);
  @$pb.TagNumber(1)
  set rid($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasRid() => $_has(0);
  @$pb.TagNumber(1)
  void clearRid() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get userId => $_getI64(1);
  @$pb.TagNumber(2)
  set userId($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasUserId() => $_has(1);
  @$pb.TagNumber(2)
  void clearUserId() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get name => $_getSZ(2);
  @$pb.TagNumber(3)
  set name($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasName() => $_has(2);
  @$pb.TagNumber(3)
  void clearName() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get avatar => $_getSZ(3);
  @$pb.TagNumber(4)
  set avatar($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasAvatar() => $_has(3);
  @$pb.TagNumber(4)
  void clearAvatar() => clearField(4);

  @$pb.TagNumber(5)
  $4.ReceiverType get type => $_getN(4);
  @$pb.TagNumber(5)
  set type($4.ReceiverType v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasType() => $_has(4);
  @$pb.TagNumber(5)
  void clearType() => clearField(5);

  @$pb.TagNumber(6)
  $fixnum.Int64 get receiveId => $_getI64(5);
  @$pb.TagNumber(6)
  set receiveId($fixnum.Int64 v) { $_setInt64(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasReceiveId() => $_has(5);
  @$pb.TagNumber(6)
  void clearReceiveId() => clearField(6);

  @$pb.TagNumber(7)
  $fixnum.Int64 get amount => $_getI64(6);
  @$pb.TagNumber(7)
  set amount($fixnum.Int64 v) { $_setInt64(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasAmount() => $_has(6);
  @$pb.TagNumber(7)
  void clearAmount() => clearField(7);

  @$pb.TagNumber(8)
  $core.int get num => $_getIZ(7);
  @$pb.TagNumber(8)
  set num($core.int v) { $_setSignedInt32(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasNum() => $_has(7);
  @$pb.TagNumber(8)
  void clearNum() => clearField(8);

  @$pb.TagNumber(9)
  $core.String get blessing => $_getSZ(8);
  @$pb.TagNumber(9)
  set blessing($core.String v) { $_setString(8, v); }
  @$pb.TagNumber(9)
  $core.bool hasBlessing() => $_has(8);
  @$pb.TagNumber(9)
  void clearBlessing() => clearField(9);

  @$pb.TagNumber(10)
  $core.int get level => $_getIZ(9);
  @$pb.TagNumber(10)
  set level($core.int v) { $_setSignedInt32(9, v); }
  @$pb.TagNumber(10)
  $core.bool hasLevel() => $_has(9);
  @$pb.TagNumber(10)
  void clearLevel() => clearField(10);
}

class SendRedPacketResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SendRedPacketResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..e<$4.ReceiverType>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'type', $pb.PbFieldType.OE, defaultOrMaker: $4.ReceiverType.RT_UNKNOWN, valueOf: $4.ReceiverType.valueOf, enumValues: $4.ReceiverType.values)
    ..aInt64(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'seq')
    ..hasRequiredFields = false
  ;

  SendRedPacketResp._() : super();
  factory SendRedPacketResp({
    $4.ReceiverType? type,
    $fixnum.Int64? seq,
  }) {
    final _result = create();
    if (type != null) {
      _result.type = type;
    }
    if (seq != null) {
      _result.seq = seq;
    }
    return _result;
  }
  factory SendRedPacketResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SendRedPacketResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SendRedPacketResp clone() => SendRedPacketResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SendRedPacketResp copyWith(void Function(SendRedPacketResp) updates) => super.copyWith((message) => updates(message as SendRedPacketResp)) as SendRedPacketResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SendRedPacketResp create() => SendRedPacketResp._();
  SendRedPacketResp createEmptyInstance() => create();
  static $pb.PbList<SendRedPacketResp> createRepeated() => $pb.PbList<SendRedPacketResp>();
  @$core.pragma('dart2js:noInline')
  static SendRedPacketResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SendRedPacketResp>(create);
  static SendRedPacketResp? _defaultInstance;

  @$pb.TagNumber(1)
  $4.ReceiverType get type => $_getN(0);
  @$pb.TagNumber(1)
  set type($4.ReceiverType v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasType() => $_has(0);
  @$pb.TagNumber(1)
  void clearType() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get seq => $_getI64(1);
  @$pb.TagNumber(2)
  set seq($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasSeq() => $_has(1);
  @$pb.TagNumber(2)
  void clearSeq() => clearField(2);
}

class RobRedPacketReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RobRedPacketReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..e<$4.ReceiverType>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'receiverType', $pb.PbFieldType.OE, defaultOrMaker: $4.ReceiverType.RT_UNKNOWN, valueOf: $4.ReceiverType.valueOf, enumValues: $4.ReceiverType.values)
    ..aInt64(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'receiverId')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'rid')
    ..aInt64(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'avatar')
    ..hasRequiredFields = false
  ;

  RobRedPacketReq._() : super();
  factory RobRedPacketReq({
    $4.ReceiverType? receiverType,
    $fixnum.Int64? receiverId,
    $core.String? rid,
    $fixnum.Int64? userId,
    $core.String? name,
    $core.String? avatar,
  }) {
    final _result = create();
    if (receiverType != null) {
      _result.receiverType = receiverType;
    }
    if (receiverId != null) {
      _result.receiverId = receiverId;
    }
    if (rid != null) {
      _result.rid = rid;
    }
    if (userId != null) {
      _result.userId = userId;
    }
    if (name != null) {
      _result.name = name;
    }
    if (avatar != null) {
      _result.avatar = avatar;
    }
    return _result;
  }
  factory RobRedPacketReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RobRedPacketReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RobRedPacketReq clone() => RobRedPacketReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RobRedPacketReq copyWith(void Function(RobRedPacketReq) updates) => super.copyWith((message) => updates(message as RobRedPacketReq)) as RobRedPacketReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RobRedPacketReq create() => RobRedPacketReq._();
  RobRedPacketReq createEmptyInstance() => create();
  static $pb.PbList<RobRedPacketReq> createRepeated() => $pb.PbList<RobRedPacketReq>();
  @$core.pragma('dart2js:noInline')
  static RobRedPacketReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RobRedPacketReq>(create);
  static RobRedPacketReq? _defaultInstance;

  @$pb.TagNumber(1)
  $4.ReceiverType get receiverType => $_getN(0);
  @$pb.TagNumber(1)
  set receiverType($4.ReceiverType v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasReceiverType() => $_has(0);
  @$pb.TagNumber(1)
  void clearReceiverType() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get receiverId => $_getI64(1);
  @$pb.TagNumber(2)
  set receiverId($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasReceiverId() => $_has(1);
  @$pb.TagNumber(2)
  void clearReceiverId() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get rid => $_getSZ(2);
  @$pb.TagNumber(3)
  set rid($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasRid() => $_has(2);
  @$pb.TagNumber(3)
  void clearRid() => clearField(3);

  @$pb.TagNumber(4)
  $fixnum.Int64 get userId => $_getI64(3);
  @$pb.TagNumber(4)
  set userId($fixnum.Int64 v) { $_setInt64(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasUserId() => $_has(3);
  @$pb.TagNumber(4)
  void clearUserId() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get name => $_getSZ(4);
  @$pb.TagNumber(5)
  set name($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasName() => $_has(4);
  @$pb.TagNumber(5)
  void clearName() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get avatar => $_getSZ(5);
  @$pb.TagNumber(6)
  set avatar($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasAvatar() => $_has(5);
  @$pb.TagNumber(6)
  void clearAvatar() => clearField(6);
}

class RobRedPacketResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RobRedPacketResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'amount')
    ..aInt64(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'avatar')
    ..hasRequiredFields = false
  ;

  RobRedPacketResp._() : super();
  factory RobRedPacketResp({
    $fixnum.Int64? amount,
    $fixnum.Int64? userId,
    $core.String? name,
    $core.String? avatar,
  }) {
    final _result = create();
    if (amount != null) {
      _result.amount = amount;
    }
    if (userId != null) {
      _result.userId = userId;
    }
    if (name != null) {
      _result.name = name;
    }
    if (avatar != null) {
      _result.avatar = avatar;
    }
    return _result;
  }
  factory RobRedPacketResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RobRedPacketResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RobRedPacketResp clone() => RobRedPacketResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RobRedPacketResp copyWith(void Function(RobRedPacketResp) updates) => super.copyWith((message) => updates(message as RobRedPacketResp)) as RobRedPacketResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RobRedPacketResp create() => RobRedPacketResp._();
  RobRedPacketResp createEmptyInstance() => create();
  static $pb.PbList<RobRedPacketResp> createRepeated() => $pb.PbList<RobRedPacketResp>();
  @$core.pragma('dart2js:noInline')
  static RobRedPacketResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RobRedPacketResp>(create);
  static RobRedPacketResp? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get amount => $_getI64(0);
  @$pb.TagNumber(1)
  set amount($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasAmount() => $_has(0);
  @$pb.TagNumber(1)
  void clearAmount() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get userId => $_getI64(1);
  @$pb.TagNumber(2)
  set userId($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasUserId() => $_has(1);
  @$pb.TagNumber(2)
  void clearUserId() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get name => $_getSZ(2);
  @$pb.TagNumber(3)
  set name($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasName() => $_has(2);
  @$pb.TagNumber(3)
  void clearName() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get avatar => $_getSZ(3);
  @$pb.TagNumber(4)
  set avatar($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasAvatar() => $_has(3);
  @$pb.TagNumber(4)
  void clearAvatar() => clearField(4);
}

class LookUpRedPacketReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'LookUpRedPacketReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'rid')
    ..hasRequiredFields = false
  ;

  LookUpRedPacketReq._() : super();
  factory LookUpRedPacketReq({
    $core.String? rid,
  }) {
    final _result = create();
    if (rid != null) {
      _result.rid = rid;
    }
    return _result;
  }
  factory LookUpRedPacketReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory LookUpRedPacketReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  LookUpRedPacketReq clone() => LookUpRedPacketReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  LookUpRedPacketReq copyWith(void Function(LookUpRedPacketReq) updates) => super.copyWith((message) => updates(message as LookUpRedPacketReq)) as LookUpRedPacketReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LookUpRedPacketReq create() => LookUpRedPacketReq._();
  LookUpRedPacketReq createEmptyInstance() => create();
  static $pb.PbList<LookUpRedPacketReq> createRepeated() => $pb.PbList<LookUpRedPacketReq>();
  @$core.pragma('dart2js:noInline')
  static LookUpRedPacketReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<LookUpRedPacketReq>(create);
  static LookUpRedPacketReq? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get rid => $_getSZ(0);
  @$pb.TagNumber(1)
  set rid($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasRid() => $_has(0);
  @$pb.TagNumber(1)
  void clearRid() => clearField(1);
}

class LookUpRedPacketResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'LookUpRedPacketResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..pc<$4.RedPacketItem>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'messages', $pb.PbFieldType.PM, subBuilder: $4.RedPacketItem.create)
    ..hasRequiredFields = false
  ;

  LookUpRedPacketResp._() : super();
  factory LookUpRedPacketResp({
    $core.Iterable<$4.RedPacketItem>? messages,
  }) {
    final _result = create();
    if (messages != null) {
      _result.messages.addAll(messages);
    }
    return _result;
  }
  factory LookUpRedPacketResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory LookUpRedPacketResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  LookUpRedPacketResp clone() => LookUpRedPacketResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  LookUpRedPacketResp copyWith(void Function(LookUpRedPacketResp) updates) => super.copyWith((message) => updates(message as LookUpRedPacketResp)) as LookUpRedPacketResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static LookUpRedPacketResp create() => LookUpRedPacketResp._();
  LookUpRedPacketResp createEmptyInstance() => create();
  static $pb.PbList<LookUpRedPacketResp> createRepeated() => $pb.PbList<LookUpRedPacketResp>();
  @$core.pragma('dart2js:noInline')
  static LookUpRedPacketResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<LookUpRedPacketResp>(create);
  static LookUpRedPacketResp? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$4.RedPacketItem> get messages => $_getList(0);
}

class RedPacketAuthItem extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RedPacketAuthItem', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'level', $pb.PbFieldType.O3)
    ..aInt64(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'minMoney')
    ..aInt64(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'maxMoney')
    ..hasRequiredFields = false
  ;

  RedPacketAuthItem._() : super();
  factory RedPacketAuthItem({
    $core.int? level,
    $fixnum.Int64? minMoney,
    $fixnum.Int64? maxMoney,
  }) {
    final _result = create();
    if (level != null) {
      _result.level = level;
    }
    if (minMoney != null) {
      _result.minMoney = minMoney;
    }
    if (maxMoney != null) {
      _result.maxMoney = maxMoney;
    }
    return _result;
  }
  factory RedPacketAuthItem.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RedPacketAuthItem.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RedPacketAuthItem clone() => RedPacketAuthItem()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RedPacketAuthItem copyWith(void Function(RedPacketAuthItem) updates) => super.copyWith((message) => updates(message as RedPacketAuthItem)) as RedPacketAuthItem; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RedPacketAuthItem create() => RedPacketAuthItem._();
  RedPacketAuthItem createEmptyInstance() => create();
  static $pb.PbList<RedPacketAuthItem> createRepeated() => $pb.PbList<RedPacketAuthItem>();
  @$core.pragma('dart2js:noInline')
  static RedPacketAuthItem getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RedPacketAuthItem>(create);
  static RedPacketAuthItem? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get level => $_getIZ(0);
  @$pb.TagNumber(1)
  set level($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasLevel() => $_has(0);
  @$pb.TagNumber(1)
  void clearLevel() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get minMoney => $_getI64(1);
  @$pb.TagNumber(2)
  set minMoney($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMinMoney() => $_has(1);
  @$pb.TagNumber(2)
  void clearMinMoney() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get maxMoney => $_getI64(2);
  @$pb.TagNumber(3)
  set maxMoney($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasMaxMoney() => $_has(2);
  @$pb.TagNumber(3)
  void clearMaxMoney() => clearField(3);
}

class SetRedPacketAuthConfigReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SetRedPacketAuthConfigReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'channel')
    ..pc<RedPacketAuthItem>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'items', $pb.PbFieldType.PM, subBuilder: RedPacketAuthItem.create)
    ..hasRequiredFields = false
  ;

  SetRedPacketAuthConfigReq._() : super();
  factory SetRedPacketAuthConfigReq({
    $fixnum.Int64? channel,
    $core.Iterable<RedPacketAuthItem>? items,
  }) {
    final _result = create();
    if (channel != null) {
      _result.channel = channel;
    }
    if (items != null) {
      _result.items.addAll(items);
    }
    return _result;
  }
  factory SetRedPacketAuthConfigReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SetRedPacketAuthConfigReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SetRedPacketAuthConfigReq clone() => SetRedPacketAuthConfigReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SetRedPacketAuthConfigReq copyWith(void Function(SetRedPacketAuthConfigReq) updates) => super.copyWith((message) => updates(message as SetRedPacketAuthConfigReq)) as SetRedPacketAuthConfigReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SetRedPacketAuthConfigReq create() => SetRedPacketAuthConfigReq._();
  SetRedPacketAuthConfigReq createEmptyInstance() => create();
  static $pb.PbList<SetRedPacketAuthConfigReq> createRepeated() => $pb.PbList<SetRedPacketAuthConfigReq>();
  @$core.pragma('dart2js:noInline')
  static SetRedPacketAuthConfigReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SetRedPacketAuthConfigReq>(create);
  static SetRedPacketAuthConfigReq? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get channel => $_getI64(0);
  @$pb.TagNumber(1)
  set channel($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasChannel() => $_has(0);
  @$pb.TagNumber(1)
  void clearChannel() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<RedPacketAuthItem> get items => $_getList(1);
}

class SetRedPacketAuthConfigResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SetRedPacketAuthConfigResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  SetRedPacketAuthConfigResp._() : super();
  factory SetRedPacketAuthConfigResp() => create();
  factory SetRedPacketAuthConfigResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SetRedPacketAuthConfigResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SetRedPacketAuthConfigResp clone() => SetRedPacketAuthConfigResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SetRedPacketAuthConfigResp copyWith(void Function(SetRedPacketAuthConfigResp) updates) => super.copyWith((message) => updates(message as SetRedPacketAuthConfigResp)) as SetRedPacketAuthConfigResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SetRedPacketAuthConfigResp create() => SetRedPacketAuthConfigResp._();
  SetRedPacketAuthConfigResp createEmptyInstance() => create();
  static $pb.PbList<SetRedPacketAuthConfigResp> createRepeated() => $pb.PbList<SetRedPacketAuthConfigResp>();
  @$core.pragma('dart2js:noInline')
  static SetRedPacketAuthConfigResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SetRedPacketAuthConfigResp>(create);
  static SetRedPacketAuthConfigResp? _defaultInstance;
}

class UpdateRobAuthReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'UpdateRobAuthReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId')
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'level', $pb.PbFieldType.O3)
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'num', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  UpdateRobAuthReq._() : super();
  factory UpdateRobAuthReq({
    $fixnum.Int64? userId,
    $core.int? level,
    $core.int? num,
  }) {
    final _result = create();
    if (userId != null) {
      _result.userId = userId;
    }
    if (level != null) {
      _result.level = level;
    }
    if (num != null) {
      _result.num = num;
    }
    return _result;
  }
  factory UpdateRobAuthReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory UpdateRobAuthReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  UpdateRobAuthReq clone() => UpdateRobAuthReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  UpdateRobAuthReq copyWith(void Function(UpdateRobAuthReq) updates) => super.copyWith((message) => updates(message as UpdateRobAuthReq)) as UpdateRobAuthReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static UpdateRobAuthReq create() => UpdateRobAuthReq._();
  UpdateRobAuthReq createEmptyInstance() => create();
  static $pb.PbList<UpdateRobAuthReq> createRepeated() => $pb.PbList<UpdateRobAuthReq>();
  @$core.pragma('dart2js:noInline')
  static UpdateRobAuthReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<UpdateRobAuthReq>(create);
  static UpdateRobAuthReq? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get userId => $_getI64(0);
  @$pb.TagNumber(1)
  set userId($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUserId() => $_has(0);
  @$pb.TagNumber(1)
  void clearUserId() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get level => $_getIZ(1);
  @$pb.TagNumber(2)
  set level($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasLevel() => $_has(1);
  @$pb.TagNumber(2)
  void clearLevel() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get num => $_getIZ(2);
  @$pb.TagNumber(3)
  set num($core.int v) { $_setSignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasNum() => $_has(2);
  @$pb.TagNumber(3)
  void clearNum() => clearField(3);
}

class UpdateRobAuthResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'UpdateRobAuthResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  UpdateRobAuthResp._() : super();
  factory UpdateRobAuthResp() => create();
  factory UpdateRobAuthResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory UpdateRobAuthResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  UpdateRobAuthResp clone() => UpdateRobAuthResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  UpdateRobAuthResp copyWith(void Function(UpdateRobAuthResp) updates) => super.copyWith((message) => updates(message as UpdateRobAuthResp)) as UpdateRobAuthResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static UpdateRobAuthResp create() => UpdateRobAuthResp._();
  UpdateRobAuthResp createEmptyInstance() => create();
  static $pb.PbList<UpdateRobAuthResp> createRepeated() => $pb.PbList<UpdateRobAuthResp>();
  @$core.pragma('dart2js:noInline')
  static UpdateRobAuthResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<UpdateRobAuthResp>(create);
  static UpdateRobAuthResp? _defaultInstance;
}

class GetRobAuthReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetRobAuthReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId')
    ..hasRequiredFields = false
  ;

  GetRobAuthReq._() : super();
  factory GetRobAuthReq({
    $fixnum.Int64? userId,
  }) {
    final _result = create();
    if (userId != null) {
      _result.userId = userId;
    }
    return _result;
  }
  factory GetRobAuthReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetRobAuthReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetRobAuthReq clone() => GetRobAuthReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetRobAuthReq copyWith(void Function(GetRobAuthReq) updates) => super.copyWith((message) => updates(message as GetRobAuthReq)) as GetRobAuthReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetRobAuthReq create() => GetRobAuthReq._();
  GetRobAuthReq createEmptyInstance() => create();
  static $pb.PbList<GetRobAuthReq> createRepeated() => $pb.PbList<GetRobAuthReq>();
  @$core.pragma('dart2js:noInline')
  static GetRobAuthReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetRobAuthReq>(create);
  static GetRobAuthReq? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get userId => $_getI64(0);
  @$pb.TagNumber(1)
  set userId($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUserId() => $_has(0);
  @$pb.TagNumber(1)
  void clearUserId() => clearField(1);
}

class RobAuthItem extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RobAuthItem', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'level', $pb.PbFieldType.O3)
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'num', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  RobAuthItem._() : super();
  factory RobAuthItem({
    $core.int? level,
    $core.int? num,
  }) {
    final _result = create();
    if (level != null) {
      _result.level = level;
    }
    if (num != null) {
      _result.num = num;
    }
    return _result;
  }
  factory RobAuthItem.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RobAuthItem.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RobAuthItem clone() => RobAuthItem()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RobAuthItem copyWith(void Function(RobAuthItem) updates) => super.copyWith((message) => updates(message as RobAuthItem)) as RobAuthItem; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RobAuthItem create() => RobAuthItem._();
  RobAuthItem createEmptyInstance() => create();
  static $pb.PbList<RobAuthItem> createRepeated() => $pb.PbList<RobAuthItem>();
  @$core.pragma('dart2js:noInline')
  static RobAuthItem getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RobAuthItem>(create);
  static RobAuthItem? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get level => $_getIZ(0);
  @$pb.TagNumber(1)
  set level($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasLevel() => $_has(0);
  @$pb.TagNumber(1)
  void clearLevel() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get num => $_getIZ(1);
  @$pb.TagNumber(2)
  set num($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasNum() => $_has(1);
  @$pb.TagNumber(2)
  void clearNum() => clearField(2);
}

class GetRobAuthResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetRobAuthResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..pc<RobAuthItem>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'items', $pb.PbFieldType.PM, subBuilder: RobAuthItem.create)
    ..hasRequiredFields = false
  ;

  GetRobAuthResp._() : super();
  factory GetRobAuthResp({
    $core.Iterable<RobAuthItem>? items,
  }) {
    final _result = create();
    if (items != null) {
      _result.items.addAll(items);
    }
    return _result;
  }
  factory GetRobAuthResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetRobAuthResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetRobAuthResp clone() => GetRobAuthResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetRobAuthResp copyWith(void Function(GetRobAuthResp) updates) => super.copyWith((message) => updates(message as GetRobAuthResp)) as GetRobAuthResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetRobAuthResp create() => GetRobAuthResp._();
  GetRobAuthResp createEmptyInstance() => create();
  static $pb.PbList<GetRobAuthResp> createRepeated() => $pb.PbList<GetRobAuthResp>();
  @$core.pragma('dart2js:noInline')
  static GetRobAuthResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetRobAuthResp>(create);
  static GetRobAuthResp? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<RobAuthItem> get items => $_getList(0);
}

class RefundRedPacketReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RefundRedPacketReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  RefundRedPacketReq._() : super();
  factory RefundRedPacketReq() => create();
  factory RefundRedPacketReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RefundRedPacketReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RefundRedPacketReq clone() => RefundRedPacketReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RefundRedPacketReq copyWith(void Function(RefundRedPacketReq) updates) => super.copyWith((message) => updates(message as RefundRedPacketReq)) as RefundRedPacketReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RefundRedPacketReq create() => RefundRedPacketReq._();
  RefundRedPacketReq createEmptyInstance() => create();
  static $pb.PbList<RefundRedPacketReq> createRepeated() => $pb.PbList<RefundRedPacketReq>();
  @$core.pragma('dart2js:noInline')
  static RefundRedPacketReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RefundRedPacketReq>(create);
  static RefundRedPacketReq? _defaultInstance;
}

class RefundItem extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RefundItem', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'rid')
    ..aInt64(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId')
    ..aInt64(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'money')
    ..e<$4.ReceiverType>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'type', $pb.PbFieldType.OE, defaultOrMaker: $4.ReceiverType.RT_UNKNOWN, valueOf: $4.ReceiverType.valueOf, enumValues: $4.ReceiverType.values)
    ..aInt64(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'receiveId', protoName: 'receiveId')
    ..hasRequiredFields = false
  ;

  RefundItem._() : super();
  factory RefundItem({
    $core.String? rid,
    $fixnum.Int64? userId,
    $fixnum.Int64? money,
    $4.ReceiverType? type,
    $fixnum.Int64? receiveId,
  }) {
    final _result = create();
    if (rid != null) {
      _result.rid = rid;
    }
    if (userId != null) {
      _result.userId = userId;
    }
    if (money != null) {
      _result.money = money;
    }
    if (type != null) {
      _result.type = type;
    }
    if (receiveId != null) {
      _result.receiveId = receiveId;
    }
    return _result;
  }
  factory RefundItem.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RefundItem.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RefundItem clone() => RefundItem()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RefundItem copyWith(void Function(RefundItem) updates) => super.copyWith((message) => updates(message as RefundItem)) as RefundItem; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RefundItem create() => RefundItem._();
  RefundItem createEmptyInstance() => create();
  static $pb.PbList<RefundItem> createRepeated() => $pb.PbList<RefundItem>();
  @$core.pragma('dart2js:noInline')
  static RefundItem getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RefundItem>(create);
  static RefundItem? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get rid => $_getSZ(0);
  @$pb.TagNumber(1)
  set rid($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasRid() => $_has(0);
  @$pb.TagNumber(1)
  void clearRid() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get userId => $_getI64(1);
  @$pb.TagNumber(2)
  set userId($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasUserId() => $_has(1);
  @$pb.TagNumber(2)
  void clearUserId() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get money => $_getI64(2);
  @$pb.TagNumber(3)
  set money($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasMoney() => $_has(2);
  @$pb.TagNumber(3)
  void clearMoney() => clearField(3);

  @$pb.TagNumber(4)
  $4.ReceiverType get type => $_getN(3);
  @$pb.TagNumber(4)
  set type($4.ReceiverType v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasType() => $_has(3);
  @$pb.TagNumber(4)
  void clearType() => clearField(4);

  @$pb.TagNumber(5)
  $fixnum.Int64 get receiveId => $_getI64(4);
  @$pb.TagNumber(5)
  set receiveId($fixnum.Int64 v) { $_setInt64(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasReceiveId() => $_has(4);
  @$pb.TagNumber(5)
  void clearReceiveId() => clearField(5);
}

class RefundRedPacketResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RefundRedPacketResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..pc<RefundItem>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'items', $pb.PbFieldType.PM, subBuilder: RefundItem.create)
    ..hasRequiredFields = false
  ;

  RefundRedPacketResp._() : super();
  factory RefundRedPacketResp({
    $core.Iterable<RefundItem>? items,
  }) {
    final _result = create();
    if (items != null) {
      _result.items.addAll(items);
    }
    return _result;
  }
  factory RefundRedPacketResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RefundRedPacketResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RefundRedPacketResp clone() => RefundRedPacketResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RefundRedPacketResp copyWith(void Function(RefundRedPacketResp) updates) => super.copyWith((message) => updates(message as RefundRedPacketResp)) as RefundRedPacketResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RefundRedPacketResp create() => RefundRedPacketResp._();
  RefundRedPacketResp createEmptyInstance() => create();
  static $pb.PbList<RefundRedPacketResp> createRepeated() => $pb.PbList<RefundRedPacketResp>();
  @$core.pragma('dart2js:noInline')
  static RefundRedPacketResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RefundRedPacketResp>(create);
  static RefundRedPacketResp? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<RefundItem> get items => $_getList(0);
}

class RefundRedPacketResultReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RefundRedPacketResultReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..pPS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'items')
    ..hasRequiredFields = false
  ;

  RefundRedPacketResultReq._() : super();
  factory RefundRedPacketResultReq({
    $core.Iterable<$core.String>? items,
  }) {
    final _result = create();
    if (items != null) {
      _result.items.addAll(items);
    }
    return _result;
  }
  factory RefundRedPacketResultReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RefundRedPacketResultReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RefundRedPacketResultReq clone() => RefundRedPacketResultReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RefundRedPacketResultReq copyWith(void Function(RefundRedPacketResultReq) updates) => super.copyWith((message) => updates(message as RefundRedPacketResultReq)) as RefundRedPacketResultReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RefundRedPacketResultReq create() => RefundRedPacketResultReq._();
  RefundRedPacketResultReq createEmptyInstance() => create();
  static $pb.PbList<RefundRedPacketResultReq> createRepeated() => $pb.PbList<RefundRedPacketResultReq>();
  @$core.pragma('dart2js:noInline')
  static RefundRedPacketResultReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RefundRedPacketResultReq>(create);
  static RefundRedPacketResultReq? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.String> get items => $_getList(0);
}

class RefundRedPacketResultResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RefundRedPacketResultResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  RefundRedPacketResultResp._() : super();
  factory RefundRedPacketResultResp() => create();
  factory RefundRedPacketResultResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RefundRedPacketResultResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RefundRedPacketResultResp clone() => RefundRedPacketResultResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RefundRedPacketResultResp copyWith(void Function(RefundRedPacketResultResp) updates) => super.copyWith((message) => updates(message as RefundRedPacketResultResp)) as RefundRedPacketResultResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RefundRedPacketResultResp create() => RefundRedPacketResultResp._();
  RefundRedPacketResultResp createEmptyInstance() => create();
  static $pb.PbList<RefundRedPacketResultResp> createRepeated() => $pb.PbList<RefundRedPacketResultResp>();
  @$core.pragma('dart2js:noInline')
  static RefundRedPacketResultResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RefundRedPacketResultResp>(create);
  static RefundRedPacketResultResp? _defaultInstance;
}

