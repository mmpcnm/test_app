///
//  Generated code. Do not modify.
//  source: business.int.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use sendRedPacketReqDescriptor instead')
const SendRedPacketReq$json = const {
  '1': 'SendRedPacketReq',
  '2': const [
    const {'1': 'rid', '3': 1, '4': 1, '5': 9, '10': 'rid'},
    const {'1': 'user_id', '3': 2, '4': 1, '5': 3, '10': 'userId'},
    const {'1': 'name', '3': 3, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'avatar', '3': 4, '4': 1, '5': 9, '10': 'avatar'},
    const {'1': 'type', '3': 5, '4': 1, '5': 14, '6': '.pb.ReceiverType', '10': 'type'},
    const {'1': 'receive_id', '3': 6, '4': 1, '5': 3, '10': 'receiveId'},
    const {'1': 'amount', '3': 7, '4': 1, '5': 3, '10': 'amount'},
    const {'1': 'num', '3': 8, '4': 1, '5': 5, '10': 'num'},
    const {'1': 'blessing', '3': 9, '4': 1, '5': 9, '10': 'blessing'},
    const {'1': 'level', '3': 10, '4': 1, '5': 5, '10': 'level'},
  ],
};

/// Descriptor for `SendRedPacketReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List sendRedPacketReqDescriptor = $convert.base64Decode('ChBTZW5kUmVkUGFja2V0UmVxEhAKA3JpZBgBIAEoCVIDcmlkEhcKB3VzZXJfaWQYAiABKANSBnVzZXJJZBISCgRuYW1lGAMgASgJUgRuYW1lEhYKBmF2YXRhchgEIAEoCVIGYXZhdGFyEiQKBHR5cGUYBSABKA4yEC5wYi5SZWNlaXZlclR5cGVSBHR5cGUSHQoKcmVjZWl2ZV9pZBgGIAEoA1IJcmVjZWl2ZUlkEhYKBmFtb3VudBgHIAEoA1IGYW1vdW50EhAKA251bRgIIAEoBVIDbnVtEhoKCGJsZXNzaW5nGAkgASgJUghibGVzc2luZxIUCgVsZXZlbBgKIAEoBVIFbGV2ZWw=');
@$core.Deprecated('Use sendRedPacketRespDescriptor instead')
const SendRedPacketResp$json = const {
  '1': 'SendRedPacketResp',
  '2': const [
    const {'1': 'type', '3': 1, '4': 1, '5': 14, '6': '.pb.ReceiverType', '10': 'type'},
    const {'1': 'seq', '3': 2, '4': 1, '5': 3, '10': 'seq'},
  ],
};

/// Descriptor for `SendRedPacketResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List sendRedPacketRespDescriptor = $convert.base64Decode('ChFTZW5kUmVkUGFja2V0UmVzcBIkCgR0eXBlGAEgASgOMhAucGIuUmVjZWl2ZXJUeXBlUgR0eXBlEhAKA3NlcRgCIAEoA1IDc2Vx');
@$core.Deprecated('Use robRedPacketReqDescriptor instead')
const RobRedPacketReq$json = const {
  '1': 'RobRedPacketReq',
  '2': const [
    const {'1': 'receiver_type', '3': 1, '4': 1, '5': 14, '6': '.pb.ReceiverType', '10': 'receiverType'},
    const {'1': 'receiver_id', '3': 2, '4': 1, '5': 3, '10': 'receiverId'},
    const {'1': 'rid', '3': 3, '4': 1, '5': 9, '10': 'rid'},
    const {'1': 'user_id', '3': 4, '4': 1, '5': 3, '10': 'userId'},
    const {'1': 'name', '3': 5, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'avatar', '3': 6, '4': 1, '5': 9, '10': 'avatar'},
  ],
};

/// Descriptor for `RobRedPacketReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List robRedPacketReqDescriptor = $convert.base64Decode('Cg9Sb2JSZWRQYWNrZXRSZXESNQoNcmVjZWl2ZXJfdHlwZRgBIAEoDjIQLnBiLlJlY2VpdmVyVHlwZVIMcmVjZWl2ZXJUeXBlEh8KC3JlY2VpdmVyX2lkGAIgASgDUgpyZWNlaXZlcklkEhAKA3JpZBgDIAEoCVIDcmlkEhcKB3VzZXJfaWQYBCABKANSBnVzZXJJZBISCgRuYW1lGAUgASgJUgRuYW1lEhYKBmF2YXRhchgGIAEoCVIGYXZhdGFy');
@$core.Deprecated('Use robRedPacketRespDescriptor instead')
const RobRedPacketResp$json = const {
  '1': 'RobRedPacketResp',
  '2': const [
    const {'1': 'amount', '3': 1, '4': 1, '5': 3, '10': 'amount'},
    const {'1': 'user_id', '3': 2, '4': 1, '5': 3, '10': 'userId'},
    const {'1': 'name', '3': 3, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'avatar', '3': 4, '4': 1, '5': 9, '10': 'avatar'},
  ],
};

/// Descriptor for `RobRedPacketResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List robRedPacketRespDescriptor = $convert.base64Decode('ChBSb2JSZWRQYWNrZXRSZXNwEhYKBmFtb3VudBgBIAEoA1IGYW1vdW50EhcKB3VzZXJfaWQYAiABKANSBnVzZXJJZBISCgRuYW1lGAMgASgJUgRuYW1lEhYKBmF2YXRhchgEIAEoCVIGYXZhdGFy');
@$core.Deprecated('Use lookUpRedPacketReqDescriptor instead')
const LookUpRedPacketReq$json = const {
  '1': 'LookUpRedPacketReq',
  '2': const [
    const {'1': 'rid', '3': 1, '4': 1, '5': 9, '10': 'rid'},
  ],
};

/// Descriptor for `LookUpRedPacketReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List lookUpRedPacketReqDescriptor = $convert.base64Decode('ChJMb29rVXBSZWRQYWNrZXRSZXESEAoDcmlkGAEgASgJUgNyaWQ=');
@$core.Deprecated('Use lookUpRedPacketRespDescriptor instead')
const LookUpRedPacketResp$json = const {
  '1': 'LookUpRedPacketResp',
  '2': const [
    const {'1': 'messages', '3': 1, '4': 3, '5': 11, '6': '.pb.RedPacketItem', '10': 'messages'},
  ],
};

/// Descriptor for `LookUpRedPacketResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List lookUpRedPacketRespDescriptor = $convert.base64Decode('ChNMb29rVXBSZWRQYWNrZXRSZXNwEi0KCG1lc3NhZ2VzGAEgAygLMhEucGIuUmVkUGFja2V0SXRlbVIIbWVzc2FnZXM=');
@$core.Deprecated('Use redPacketAuthItemDescriptor instead')
const RedPacketAuthItem$json = const {
  '1': 'RedPacketAuthItem',
  '2': const [
    const {'1': 'level', '3': 1, '4': 1, '5': 5, '10': 'level'},
    const {'1': 'min_money', '3': 2, '4': 1, '5': 3, '10': 'minMoney'},
    const {'1': 'max_money', '3': 3, '4': 1, '5': 3, '10': 'maxMoney'},
  ],
};

/// Descriptor for `RedPacketAuthItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List redPacketAuthItemDescriptor = $convert.base64Decode('ChFSZWRQYWNrZXRBdXRoSXRlbRIUCgVsZXZlbBgBIAEoBVIFbGV2ZWwSGwoJbWluX21vbmV5GAIgASgDUghtaW5Nb25leRIbCgltYXhfbW9uZXkYAyABKANSCG1heE1vbmV5');
@$core.Deprecated('Use setRedPacketAuthConfigReqDescriptor instead')
const SetRedPacketAuthConfigReq$json = const {
  '1': 'SetRedPacketAuthConfigReq',
  '2': const [
    const {'1': 'channel', '3': 1, '4': 1, '5': 3, '10': 'channel'},
    const {'1': 'items', '3': 2, '4': 3, '5': 11, '6': '.pb.RedPacketAuthItem', '10': 'items'},
  ],
};

/// Descriptor for `SetRedPacketAuthConfigReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List setRedPacketAuthConfigReqDescriptor = $convert.base64Decode('ChlTZXRSZWRQYWNrZXRBdXRoQ29uZmlnUmVxEhgKB2NoYW5uZWwYASABKANSB2NoYW5uZWwSKwoFaXRlbXMYAiADKAsyFS5wYi5SZWRQYWNrZXRBdXRoSXRlbVIFaXRlbXM=');
@$core.Deprecated('Use setRedPacketAuthConfigRespDescriptor instead')
const SetRedPacketAuthConfigResp$json = const {
  '1': 'SetRedPacketAuthConfigResp',
};

/// Descriptor for `SetRedPacketAuthConfigResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List setRedPacketAuthConfigRespDescriptor = $convert.base64Decode('ChpTZXRSZWRQYWNrZXRBdXRoQ29uZmlnUmVzcA==');
@$core.Deprecated('Use updateRobAuthReqDescriptor instead')
const UpdateRobAuthReq$json = const {
  '1': 'UpdateRobAuthReq',
  '2': const [
    const {'1': 'user_id', '3': 1, '4': 1, '5': 3, '10': 'userId'},
    const {'1': 'level', '3': 2, '4': 1, '5': 5, '10': 'level'},
    const {'1': 'num', '3': 3, '4': 1, '5': 5, '10': 'num'},
  ],
};

/// Descriptor for `UpdateRobAuthReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updateRobAuthReqDescriptor = $convert.base64Decode('ChBVcGRhdGVSb2JBdXRoUmVxEhcKB3VzZXJfaWQYASABKANSBnVzZXJJZBIUCgVsZXZlbBgCIAEoBVIFbGV2ZWwSEAoDbnVtGAMgASgFUgNudW0=');
@$core.Deprecated('Use updateRobAuthRespDescriptor instead')
const UpdateRobAuthResp$json = const {
  '1': 'UpdateRobAuthResp',
};

/// Descriptor for `UpdateRobAuthResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List updateRobAuthRespDescriptor = $convert.base64Decode('ChFVcGRhdGVSb2JBdXRoUmVzcA==');
@$core.Deprecated('Use getRobAuthReqDescriptor instead')
const GetRobAuthReq$json = const {
  '1': 'GetRobAuthReq',
  '2': const [
    const {'1': 'user_id', '3': 1, '4': 1, '5': 3, '10': 'userId'},
  ],
};

/// Descriptor for `GetRobAuthReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getRobAuthReqDescriptor = $convert.base64Decode('Cg1HZXRSb2JBdXRoUmVxEhcKB3VzZXJfaWQYASABKANSBnVzZXJJZA==');
@$core.Deprecated('Use robAuthItemDescriptor instead')
const RobAuthItem$json = const {
  '1': 'RobAuthItem',
  '2': const [
    const {'1': 'level', '3': 1, '4': 1, '5': 5, '10': 'level'},
    const {'1': 'num', '3': 2, '4': 1, '5': 5, '10': 'num'},
  ],
};

/// Descriptor for `RobAuthItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List robAuthItemDescriptor = $convert.base64Decode('CgtSb2JBdXRoSXRlbRIUCgVsZXZlbBgBIAEoBVIFbGV2ZWwSEAoDbnVtGAIgASgFUgNudW0=');
@$core.Deprecated('Use getRobAuthRespDescriptor instead')
const GetRobAuthResp$json = const {
  '1': 'GetRobAuthResp',
  '2': const [
    const {'1': 'items', '3': 1, '4': 3, '5': 11, '6': '.pb.RobAuthItem', '10': 'items'},
  ],
};

/// Descriptor for `GetRobAuthResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List getRobAuthRespDescriptor = $convert.base64Decode('Cg5HZXRSb2JBdXRoUmVzcBIlCgVpdGVtcxgBIAMoCzIPLnBiLlJvYkF1dGhJdGVtUgVpdGVtcw==');
@$core.Deprecated('Use refundRedPacketReqDescriptor instead')
const RefundRedPacketReq$json = const {
  '1': 'RefundRedPacketReq',
};

/// Descriptor for `RefundRedPacketReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List refundRedPacketReqDescriptor = $convert.base64Decode('ChJSZWZ1bmRSZWRQYWNrZXRSZXE=');
@$core.Deprecated('Use refundItemDescriptor instead')
const RefundItem$json = const {
  '1': 'RefundItem',
  '2': const [
    const {'1': 'rid', '3': 1, '4': 1, '5': 9, '10': 'rid'},
    const {'1': 'user_id', '3': 2, '4': 1, '5': 3, '10': 'userId'},
    const {'1': 'money', '3': 3, '4': 1, '5': 3, '10': 'money'},
    const {'1': 'type', '3': 4, '4': 1, '5': 14, '6': '.pb.ReceiverType', '10': 'type'},
    const {'1': 'receiveId', '3': 5, '4': 1, '5': 3, '10': 'receiveId'},
  ],
};

/// Descriptor for `RefundItem`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List refundItemDescriptor = $convert.base64Decode('CgpSZWZ1bmRJdGVtEhAKA3JpZBgBIAEoCVIDcmlkEhcKB3VzZXJfaWQYAiABKANSBnVzZXJJZBIUCgVtb25leRgDIAEoA1IFbW9uZXkSJAoEdHlwZRgEIAEoDjIQLnBiLlJlY2VpdmVyVHlwZVIEdHlwZRIcCglyZWNlaXZlSWQYBSABKANSCXJlY2VpdmVJZA==');
@$core.Deprecated('Use refundRedPacketRespDescriptor instead')
const RefundRedPacketResp$json = const {
  '1': 'RefundRedPacketResp',
  '2': const [
    const {'1': 'items', '3': 1, '4': 3, '5': 11, '6': '.pb.RefundItem', '10': 'items'},
  ],
};

/// Descriptor for `RefundRedPacketResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List refundRedPacketRespDescriptor = $convert.base64Decode('ChNSZWZ1bmRSZWRQYWNrZXRSZXNwEiQKBWl0ZW1zGAEgAygLMg4ucGIuUmVmdW5kSXRlbVIFaXRlbXM=');
@$core.Deprecated('Use refundRedPacketResultReqDescriptor instead')
const RefundRedPacketResultReq$json = const {
  '1': 'RefundRedPacketResultReq',
  '2': const [
    const {'1': 'items', '3': 1, '4': 3, '5': 9, '10': 'items'},
  ],
};

/// Descriptor for `RefundRedPacketResultReq`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List refundRedPacketResultReqDescriptor = $convert.base64Decode('ChhSZWZ1bmRSZWRQYWNrZXRSZXN1bHRSZXESFAoFaXRlbXMYASADKAlSBWl0ZW1z');
@$core.Deprecated('Use refundRedPacketResultRespDescriptor instead')
const RefundRedPacketResultResp$json = const {
  '1': 'RefundRedPacketResultResp',
};

/// Descriptor for `RefundRedPacketResultResp`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List refundRedPacketResultRespDescriptor = $convert.base64Decode('ChlSZWZ1bmRSZWRQYWNrZXRSZXN1bHRSZXNw');
