///
//  Generated code. Do not modify.
//  source: logic.int.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import 'connect.ext.pb.dart' as $4;
import 'logic.ext.pb.dart' as $2;

import 'connect.ext.pbenum.dart' as $4;

class ConnSignInReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ConnSignInReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'machineId')
    ..aInt64(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'token')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'connAddr')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'clientAddr')
    ..hasRequiredFields = false
  ;

  ConnSignInReq._() : super();
  factory ConnSignInReq({
    $core.String? machineId,
    $fixnum.Int64? userId,
    $core.String? token,
    $core.String? connAddr,
    $core.String? clientAddr,
  }) {
    final _result = create();
    if (machineId != null) {
      _result.machineId = machineId;
    }
    if (userId != null) {
      _result.userId = userId;
    }
    if (token != null) {
      _result.token = token;
    }
    if (connAddr != null) {
      _result.connAddr = connAddr;
    }
    if (clientAddr != null) {
      _result.clientAddr = clientAddr;
    }
    return _result;
  }
  factory ConnSignInReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ConnSignInReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ConnSignInReq clone() => ConnSignInReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ConnSignInReq copyWith(void Function(ConnSignInReq) updates) => super.copyWith((message) => updates(message as ConnSignInReq)) as ConnSignInReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ConnSignInReq create() => ConnSignInReq._();
  ConnSignInReq createEmptyInstance() => create();
  static $pb.PbList<ConnSignInReq> createRepeated() => $pb.PbList<ConnSignInReq>();
  @$core.pragma('dart2js:noInline')
  static ConnSignInReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ConnSignInReq>(create);
  static ConnSignInReq? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get machineId => $_getSZ(0);
  @$pb.TagNumber(1)
  set machineId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMachineId() => $_has(0);
  @$pb.TagNumber(1)
  void clearMachineId() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get userId => $_getI64(1);
  @$pb.TagNumber(2)
  set userId($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasUserId() => $_has(1);
  @$pb.TagNumber(2)
  void clearUserId() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get token => $_getSZ(2);
  @$pb.TagNumber(3)
  set token($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasToken() => $_has(2);
  @$pb.TagNumber(3)
  void clearToken() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get connAddr => $_getSZ(3);
  @$pb.TagNumber(4)
  set connAddr($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasConnAddr() => $_has(3);
  @$pb.TagNumber(4)
  void clearConnAddr() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get clientAddr => $_getSZ(4);
  @$pb.TagNumber(5)
  set clientAddr($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasClientAddr() => $_has(4);
  @$pb.TagNumber(5)
  void clearClientAddr() => clearField(5);
}

class ConnSignInResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ConnSignInResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  ConnSignInResp._() : super();
  factory ConnSignInResp() => create();
  factory ConnSignInResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ConnSignInResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ConnSignInResp clone() => ConnSignInResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ConnSignInResp copyWith(void Function(ConnSignInResp) updates) => super.copyWith((message) => updates(message as ConnSignInResp)) as ConnSignInResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ConnSignInResp create() => ConnSignInResp._();
  ConnSignInResp createEmptyInstance() => create();
  static $pb.PbList<ConnSignInResp> createRepeated() => $pb.PbList<ConnSignInResp>();
  @$core.pragma('dart2js:noInline')
  static ConnSignInResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ConnSignInResp>(create);
  static ConnSignInResp? _defaultInstance;
}

class SyncReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SyncReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'machineId')
    ..aInt64(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'seq')
    ..hasRequiredFields = false
  ;

  SyncReq._() : super();
  factory SyncReq({
    $fixnum.Int64? userId,
    $core.String? machineId,
    $fixnum.Int64? seq,
  }) {
    final _result = create();
    if (userId != null) {
      _result.userId = userId;
    }
    if (machineId != null) {
      _result.machineId = machineId;
    }
    if (seq != null) {
      _result.seq = seq;
    }
    return _result;
  }
  factory SyncReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SyncReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SyncReq clone() => SyncReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SyncReq copyWith(void Function(SyncReq) updates) => super.copyWith((message) => updates(message as SyncReq)) as SyncReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SyncReq create() => SyncReq._();
  SyncReq createEmptyInstance() => create();
  static $pb.PbList<SyncReq> createRepeated() => $pb.PbList<SyncReq>();
  @$core.pragma('dart2js:noInline')
  static SyncReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SyncReq>(create);
  static SyncReq? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get userId => $_getI64(0);
  @$pb.TagNumber(1)
  set userId($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUserId() => $_has(0);
  @$pb.TagNumber(1)
  void clearUserId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get machineId => $_getSZ(1);
  @$pb.TagNumber(2)
  set machineId($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMachineId() => $_has(1);
  @$pb.TagNumber(2)
  void clearMachineId() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get seq => $_getI64(2);
  @$pb.TagNumber(3)
  set seq($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasSeq() => $_has(2);
  @$pb.TagNumber(3)
  void clearSeq() => clearField(3);
}

class SyncResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SyncResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..pc<$4.Message>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'messages', $pb.PbFieldType.PM, subBuilder: $4.Message.create)
    ..aOB(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'hasMore')
    ..hasRequiredFields = false
  ;

  SyncResp._() : super();
  factory SyncResp({
    $core.Iterable<$4.Message>? messages,
    $core.bool? hasMore,
  }) {
    final _result = create();
    if (messages != null) {
      _result.messages.addAll(messages);
    }
    if (hasMore != null) {
      _result.hasMore = hasMore;
    }
    return _result;
  }
  factory SyncResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SyncResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SyncResp clone() => SyncResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SyncResp copyWith(void Function(SyncResp) updates) => super.copyWith((message) => updates(message as SyncResp)) as SyncResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SyncResp create() => SyncResp._();
  SyncResp createEmptyInstance() => create();
  static $pb.PbList<SyncResp> createRepeated() => $pb.PbList<SyncResp>();
  @$core.pragma('dart2js:noInline')
  static SyncResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SyncResp>(create);
  static SyncResp? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$4.Message> get messages => $_getList(0);

  @$pb.TagNumber(2)
  $core.bool get hasMore => $_getBF(1);
  @$pb.TagNumber(2)
  set hasMore($core.bool v) { $_setBool(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasHasMore() => $_has(1);
  @$pb.TagNumber(2)
  void clearHasMore() => clearField(2);
}

class MessageACKReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MessageACKReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'machineId')
    ..aInt64(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deviceAck')
    ..aInt64(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'receiveTime')
    ..hasRequiredFields = false
  ;

  MessageACKReq._() : super();
  factory MessageACKReq({
    $fixnum.Int64? userId,
    $core.String? machineId,
    $fixnum.Int64? deviceAck,
    $fixnum.Int64? receiveTime,
  }) {
    final _result = create();
    if (userId != null) {
      _result.userId = userId;
    }
    if (machineId != null) {
      _result.machineId = machineId;
    }
    if (deviceAck != null) {
      _result.deviceAck = deviceAck;
    }
    if (receiveTime != null) {
      _result.receiveTime = receiveTime;
    }
    return _result;
  }
  factory MessageACKReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MessageACKReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MessageACKReq clone() => MessageACKReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MessageACKReq copyWith(void Function(MessageACKReq) updates) => super.copyWith((message) => updates(message as MessageACKReq)) as MessageACKReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MessageACKReq create() => MessageACKReq._();
  MessageACKReq createEmptyInstance() => create();
  static $pb.PbList<MessageACKReq> createRepeated() => $pb.PbList<MessageACKReq>();
  @$core.pragma('dart2js:noInline')
  static MessageACKReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MessageACKReq>(create);
  static MessageACKReq? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get userId => $_getI64(0);
  @$pb.TagNumber(1)
  set userId($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUserId() => $_has(0);
  @$pb.TagNumber(1)
  void clearUserId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get machineId => $_getSZ(1);
  @$pb.TagNumber(2)
  set machineId($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMachineId() => $_has(1);
  @$pb.TagNumber(2)
  void clearMachineId() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get deviceAck => $_getI64(2);
  @$pb.TagNumber(3)
  set deviceAck($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasDeviceAck() => $_has(2);
  @$pb.TagNumber(3)
  void clearDeviceAck() => clearField(3);

  @$pb.TagNumber(4)
  $fixnum.Int64 get receiveTime => $_getI64(3);
  @$pb.TagNumber(4)
  set receiveTime($fixnum.Int64 v) { $_setInt64(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasReceiveTime() => $_has(3);
  @$pb.TagNumber(4)
  void clearReceiveTime() => clearField(4);
}

class MessageACKResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MessageACKResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  MessageACKResp._() : super();
  factory MessageACKResp() => create();
  factory MessageACKResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MessageACKResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MessageACKResp clone() => MessageACKResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MessageACKResp copyWith(void Function(MessageACKResp) updates) => super.copyWith((message) => updates(message as MessageACKResp)) as MessageACKResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MessageACKResp create() => MessageACKResp._();
  MessageACKResp createEmptyInstance() => create();
  static $pb.PbList<MessageACKResp> createRepeated() => $pb.PbList<MessageACKResp>();
  @$core.pragma('dart2js:noInline')
  static MessageACKResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MessageACKResp>(create);
  static MessageACKResp? _defaultInstance;
}

class OfflineReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'OfflineReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'machineId')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'clientAddr')
    ..aInt64(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'roomId')
    ..hasRequiredFields = false
  ;

  OfflineReq._() : super();
  factory OfflineReq({
    $fixnum.Int64? userId,
    $core.String? machineId,
    $core.String? clientAddr,
    $fixnum.Int64? roomId,
  }) {
    final _result = create();
    if (userId != null) {
      _result.userId = userId;
    }
    if (machineId != null) {
      _result.machineId = machineId;
    }
    if (clientAddr != null) {
      _result.clientAddr = clientAddr;
    }
    if (roomId != null) {
      _result.roomId = roomId;
    }
    return _result;
  }
  factory OfflineReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory OfflineReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  OfflineReq clone() => OfflineReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  OfflineReq copyWith(void Function(OfflineReq) updates) => super.copyWith((message) => updates(message as OfflineReq)) as OfflineReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static OfflineReq create() => OfflineReq._();
  OfflineReq createEmptyInstance() => create();
  static $pb.PbList<OfflineReq> createRepeated() => $pb.PbList<OfflineReq>();
  @$core.pragma('dart2js:noInline')
  static OfflineReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<OfflineReq>(create);
  static OfflineReq? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get userId => $_getI64(0);
  @$pb.TagNumber(1)
  set userId($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUserId() => $_has(0);
  @$pb.TagNumber(1)
  void clearUserId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get machineId => $_getSZ(1);
  @$pb.TagNumber(2)
  set machineId($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMachineId() => $_has(1);
  @$pb.TagNumber(2)
  void clearMachineId() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get clientAddr => $_getSZ(2);
  @$pb.TagNumber(3)
  set clientAddr($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasClientAddr() => $_has(2);
  @$pb.TagNumber(3)
  void clearClientAddr() => clearField(3);

  @$pb.TagNumber(4)
  $fixnum.Int64 get roomId => $_getI64(3);
  @$pb.TagNumber(4)
  set roomId($fixnum.Int64 v) { $_setInt64(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasRoomId() => $_has(3);
  @$pb.TagNumber(4)
  void clearRoomId() => clearField(4);
}

class OfflineResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'OfflineResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  OfflineResp._() : super();
  factory OfflineResp() => create();
  factory OfflineResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory OfflineResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  OfflineResp clone() => OfflineResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  OfflineResp copyWith(void Function(OfflineResp) updates) => super.copyWith((message) => updates(message as OfflineResp)) as OfflineResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static OfflineResp create() => OfflineResp._();
  OfflineResp createEmptyInstance() => create();
  static $pb.PbList<OfflineResp> createRepeated() => $pb.PbList<OfflineResp>();
  @$core.pragma('dart2js:noInline')
  static OfflineResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<OfflineResp>(create);
  static OfflineResp? _defaultInstance;
}

class SubscribeRoomReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SubscribeRoomReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'machineId')
    ..aInt64(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'roomId')
    ..aInt64(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'seq')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'connAddr')
    ..hasRequiredFields = false
  ;

  SubscribeRoomReq._() : super();
  factory SubscribeRoomReq({
    $fixnum.Int64? userId,
    $core.String? machineId,
    $fixnum.Int64? roomId,
    $fixnum.Int64? seq,
    $core.String? connAddr,
  }) {
    final _result = create();
    if (userId != null) {
      _result.userId = userId;
    }
    if (machineId != null) {
      _result.machineId = machineId;
    }
    if (roomId != null) {
      _result.roomId = roomId;
    }
    if (seq != null) {
      _result.seq = seq;
    }
    if (connAddr != null) {
      _result.connAddr = connAddr;
    }
    return _result;
  }
  factory SubscribeRoomReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SubscribeRoomReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SubscribeRoomReq clone() => SubscribeRoomReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SubscribeRoomReq copyWith(void Function(SubscribeRoomReq) updates) => super.copyWith((message) => updates(message as SubscribeRoomReq)) as SubscribeRoomReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SubscribeRoomReq create() => SubscribeRoomReq._();
  SubscribeRoomReq createEmptyInstance() => create();
  static $pb.PbList<SubscribeRoomReq> createRepeated() => $pb.PbList<SubscribeRoomReq>();
  @$core.pragma('dart2js:noInline')
  static SubscribeRoomReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SubscribeRoomReq>(create);
  static SubscribeRoomReq? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get userId => $_getI64(0);
  @$pb.TagNumber(1)
  set userId($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUserId() => $_has(0);
  @$pb.TagNumber(1)
  void clearUserId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get machineId => $_getSZ(1);
  @$pb.TagNumber(2)
  set machineId($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMachineId() => $_has(1);
  @$pb.TagNumber(2)
  void clearMachineId() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get roomId => $_getI64(2);
  @$pb.TagNumber(3)
  set roomId($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasRoomId() => $_has(2);
  @$pb.TagNumber(3)
  void clearRoomId() => clearField(3);

  @$pb.TagNumber(4)
  $fixnum.Int64 get seq => $_getI64(3);
  @$pb.TagNumber(4)
  set seq($fixnum.Int64 v) { $_setInt64(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasSeq() => $_has(3);
  @$pb.TagNumber(4)
  void clearSeq() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get connAddr => $_getSZ(4);
  @$pb.TagNumber(5)
  set connAddr($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasConnAddr() => $_has(4);
  @$pb.TagNumber(5)
  void clearConnAddr() => clearField(5);
}

class SubscribeRoomResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SubscribeRoomResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  SubscribeRoomResp._() : super();
  factory SubscribeRoomResp() => create();
  factory SubscribeRoomResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SubscribeRoomResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SubscribeRoomResp clone() => SubscribeRoomResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SubscribeRoomResp copyWith(void Function(SubscribeRoomResp) updates) => super.copyWith((message) => updates(message as SubscribeRoomResp)) as SubscribeRoomResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SubscribeRoomResp create() => SubscribeRoomResp._();
  SubscribeRoomResp createEmptyInstance() => create();
  static $pb.PbList<SubscribeRoomResp> createRepeated() => $pb.PbList<SubscribeRoomResp>();
  @$core.pragma('dart2js:noInline')
  static SubscribeRoomResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SubscribeRoomResp>(create);
  static SubscribeRoomResp? _defaultInstance;
}

class PushAllReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PushAllReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..e<$4.MessageType>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'messageType', $pb.PbFieldType.OE, defaultOrMaker: $4.MessageType.MT_UNKNOWN, valueOf: $4.MessageType.valueOf, enumValues: $4.MessageType.values)
    ..a<$core.List<$core.int>>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'messageContent', $pb.PbFieldType.OY)
    ..aInt64(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'sendTime')
    ..hasRequiredFields = false
  ;

  PushAllReq._() : super();
  factory PushAllReq({
    $4.MessageType? messageType,
    $core.List<$core.int>? messageContent,
    $fixnum.Int64? sendTime,
  }) {
    final _result = create();
    if (messageType != null) {
      _result.messageType = messageType;
    }
    if (messageContent != null) {
      _result.messageContent = messageContent;
    }
    if (sendTime != null) {
      _result.sendTime = sendTime;
    }
    return _result;
  }
  factory PushAllReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PushAllReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PushAllReq clone() => PushAllReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PushAllReq copyWith(void Function(PushAllReq) updates) => super.copyWith((message) => updates(message as PushAllReq)) as PushAllReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PushAllReq create() => PushAllReq._();
  PushAllReq createEmptyInstance() => create();
  static $pb.PbList<PushAllReq> createRepeated() => $pb.PbList<PushAllReq>();
  @$core.pragma('dart2js:noInline')
  static PushAllReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PushAllReq>(create);
  static PushAllReq? _defaultInstance;

  @$pb.TagNumber(1)
  $4.MessageType get messageType => $_getN(0);
  @$pb.TagNumber(1)
  set messageType($4.MessageType v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasMessageType() => $_has(0);
  @$pb.TagNumber(1)
  void clearMessageType() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get messageContent => $_getN(1);
  @$pb.TagNumber(2)
  set messageContent($core.List<$core.int> v) { $_setBytes(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMessageContent() => $_has(1);
  @$pb.TagNumber(2)
  void clearMessageContent() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get sendTime => $_getI64(2);
  @$pb.TagNumber(3)
  set sendTime($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasSendTime() => $_has(2);
  @$pb.TagNumber(3)
  void clearSendTime() => clearField(3);
}

class PushAllResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PushAllResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  PushAllResp._() : super();
  factory PushAllResp() => create();
  factory PushAllResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PushAllResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PushAllResp clone() => PushAllResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PushAllResp copyWith(void Function(PushAllResp) updates) => super.copyWith((message) => updates(message as PushAllResp)) as PushAllResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PushAllResp create() => PushAllResp._();
  PushAllResp createEmptyInstance() => create();
  static $pb.PbList<PushAllResp> createRepeated() => $pb.PbList<PushAllResp>();
  @$core.pragma('dart2js:noInline')
  static PushAllResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PushAllResp>(create);
  static PushAllResp? _defaultInstance;
}

class GetUsersReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetUsersReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..m<$fixnum.Int64, $core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userIds', entryClassName: 'GetUsersReq.UserIdsEntry', keyFieldType: $pb.PbFieldType.O6, valueFieldType: $pb.PbFieldType.O3, packageName: const $pb.PackageName('pb'))
    ..hasRequiredFields = false
  ;

  GetUsersReq._() : super();
  factory GetUsersReq({
    $core.Map<$fixnum.Int64, $core.int>? userIds,
  }) {
    final _result = create();
    if (userIds != null) {
      _result.userIds.addAll(userIds);
    }
    return _result;
  }
  factory GetUsersReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetUsersReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetUsersReq clone() => GetUsersReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetUsersReq copyWith(void Function(GetUsersReq) updates) => super.copyWith((message) => updates(message as GetUsersReq)) as GetUsersReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetUsersReq create() => GetUsersReq._();
  GetUsersReq createEmptyInstance() => create();
  static $pb.PbList<GetUsersReq> createRepeated() => $pb.PbList<GetUsersReq>();
  @$core.pragma('dart2js:noInline')
  static GetUsersReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetUsersReq>(create);
  static GetUsersReq? _defaultInstance;

  @$pb.TagNumber(1)
  $core.Map<$fixnum.Int64, $core.int> get userIds => $_getMap(0);
}

class GetUsersResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'GetUsersResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..m<$fixnum.Int64, $2.User>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'users', entryClassName: 'GetUsersResp.UsersEntry', keyFieldType: $pb.PbFieldType.O6, valueFieldType: $pb.PbFieldType.OM, valueCreator: $2.User.create, packageName: const $pb.PackageName('pb'))
    ..hasRequiredFields = false
  ;

  GetUsersResp._() : super();
  factory GetUsersResp({
    $core.Map<$fixnum.Int64, $2.User>? users,
  }) {
    final _result = create();
    if (users != null) {
      _result.users.addAll(users);
    }
    return _result;
  }
  factory GetUsersResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetUsersResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  GetUsersResp clone() => GetUsersResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  GetUsersResp copyWith(void Function(GetUsersResp) updates) => super.copyWith((message) => updates(message as GetUsersResp)) as GetUsersResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetUsersResp create() => GetUsersResp._();
  GetUsersResp createEmptyInstance() => create();
  static $pb.PbList<GetUsersResp> createRepeated() => $pb.PbList<GetUsersResp>();
  @$core.pragma('dart2js:noInline')
  static GetUsersResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetUsersResp>(create);
  static GetUsersResp? _defaultInstance;

  @$pb.TagNumber(1)
  $core.Map<$fixnum.Int64, $2.User> get users => $_getMap(0);
}

class ServerStopReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ServerStopReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'connAddr')
    ..hasRequiredFields = false
  ;

  ServerStopReq._() : super();
  factory ServerStopReq({
    $core.String? connAddr,
  }) {
    final _result = create();
    if (connAddr != null) {
      _result.connAddr = connAddr;
    }
    return _result;
  }
  factory ServerStopReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ServerStopReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ServerStopReq clone() => ServerStopReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ServerStopReq copyWith(void Function(ServerStopReq) updates) => super.copyWith((message) => updates(message as ServerStopReq)) as ServerStopReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ServerStopReq create() => ServerStopReq._();
  ServerStopReq createEmptyInstance() => create();
  static $pb.PbList<ServerStopReq> createRepeated() => $pb.PbList<ServerStopReq>();
  @$core.pragma('dart2js:noInline')
  static ServerStopReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ServerStopReq>(create);
  static ServerStopReq? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get connAddr => $_getSZ(0);
  @$pb.TagNumber(1)
  set connAddr($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasConnAddr() => $_has(0);
  @$pb.TagNumber(1)
  void clearConnAddr() => clearField(1);
}

class ServerStopResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ServerStopResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  ServerStopResp._() : super();
  factory ServerStopResp() => create();
  factory ServerStopResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ServerStopResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ServerStopResp clone() => ServerStopResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ServerStopResp copyWith(void Function(ServerStopResp) updates) => super.copyWith((message) => updates(message as ServerStopResp)) as ServerStopResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ServerStopResp create() => ServerStopResp._();
  ServerStopResp createEmptyInstance() => create();
  static $pb.PbList<ServerStopResp> createRepeated() => $pb.PbList<ServerStopResp>();
  @$core.pragma('dart2js:noInline')
  static ServerStopResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ServerStopResp>(create);
  static ServerStopResp? _defaultInstance;
}

class SetUserFlagReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SetUserFlagReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId')
    ..e<$4.ReceiverType>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'type', $pb.PbFieldType.OE, defaultOrMaker: $4.ReceiverType.RT_UNKNOWN, valueOf: $4.ReceiverType.valueOf, enumValues: $4.ReceiverType.values)
    ..aInt64(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'receiveId')
    ..aInt64(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'flag')
    ..hasRequiredFields = false
  ;

  SetUserFlagReq._() : super();
  factory SetUserFlagReq({
    $fixnum.Int64? userId,
    $4.ReceiverType? type,
    $fixnum.Int64? receiveId,
    $fixnum.Int64? flag,
  }) {
    final _result = create();
    if (userId != null) {
      _result.userId = userId;
    }
    if (type != null) {
      _result.type = type;
    }
    if (receiveId != null) {
      _result.receiveId = receiveId;
    }
    if (flag != null) {
      _result.flag = flag;
    }
    return _result;
  }
  factory SetUserFlagReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SetUserFlagReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SetUserFlagReq clone() => SetUserFlagReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SetUserFlagReq copyWith(void Function(SetUserFlagReq) updates) => super.copyWith((message) => updates(message as SetUserFlagReq)) as SetUserFlagReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SetUserFlagReq create() => SetUserFlagReq._();
  SetUserFlagReq createEmptyInstance() => create();
  static $pb.PbList<SetUserFlagReq> createRepeated() => $pb.PbList<SetUserFlagReq>();
  @$core.pragma('dart2js:noInline')
  static SetUserFlagReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SetUserFlagReq>(create);
  static SetUserFlagReq? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get userId => $_getI64(0);
  @$pb.TagNumber(1)
  set userId($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUserId() => $_has(0);
  @$pb.TagNumber(1)
  void clearUserId() => clearField(1);

  @$pb.TagNumber(2)
  $4.ReceiverType get type => $_getN(1);
  @$pb.TagNumber(2)
  set type($4.ReceiverType v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasType() => $_has(1);
  @$pb.TagNumber(2)
  void clearType() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get receiveId => $_getI64(2);
  @$pb.TagNumber(3)
  set receiveId($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasReceiveId() => $_has(2);
  @$pb.TagNumber(3)
  void clearReceiveId() => clearField(3);

  @$pb.TagNumber(4)
  $fixnum.Int64 get flag => $_getI64(3);
  @$pb.TagNumber(4)
  set flag($fixnum.Int64 v) { $_setInt64(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasFlag() => $_has(3);
  @$pb.TagNumber(4)
  void clearFlag() => clearField(4);
}

class SetUserFlagResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SetUserFlagResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  SetUserFlagResp._() : super();
  factory SetUserFlagResp() => create();
  factory SetUserFlagResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SetUserFlagResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SetUserFlagResp clone() => SetUserFlagResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SetUserFlagResp copyWith(void Function(SetUserFlagResp) updates) => super.copyWith((message) => updates(message as SetUserFlagResp)) as SetUserFlagResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SetUserFlagResp create() => SetUserFlagResp._();
  SetUserFlagResp createEmptyInstance() => create();
  static $pb.PbList<SetUserFlagResp> createRepeated() => $pb.PbList<SetUserFlagResp>();
  @$core.pragma('dart2js:noInline')
  static SetUserFlagResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SetUserFlagResp>(create);
  static SetUserFlagResp? _defaultInstance;
}

class BindAlipayIntReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BindAlipayIntReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId', protoName: 'userId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'realname')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'alipayAccount')
    ..hasRequiredFields = false
  ;

  BindAlipayIntReq._() : super();
  factory BindAlipayIntReq({
    $fixnum.Int64? userId,
    $core.String? realname,
    $core.String? alipayAccount,
  }) {
    final _result = create();
    if (userId != null) {
      _result.userId = userId;
    }
    if (realname != null) {
      _result.realname = realname;
    }
    if (alipayAccount != null) {
      _result.alipayAccount = alipayAccount;
    }
    return _result;
  }
  factory BindAlipayIntReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BindAlipayIntReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BindAlipayIntReq clone() => BindAlipayIntReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BindAlipayIntReq copyWith(void Function(BindAlipayIntReq) updates) => super.copyWith((message) => updates(message as BindAlipayIntReq)) as BindAlipayIntReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BindAlipayIntReq create() => BindAlipayIntReq._();
  BindAlipayIntReq createEmptyInstance() => create();
  static $pb.PbList<BindAlipayIntReq> createRepeated() => $pb.PbList<BindAlipayIntReq>();
  @$core.pragma('dart2js:noInline')
  static BindAlipayIntReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BindAlipayIntReq>(create);
  static BindAlipayIntReq? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get userId => $_getI64(0);
  @$pb.TagNumber(1)
  set userId($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUserId() => $_has(0);
  @$pb.TagNumber(1)
  void clearUserId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get realname => $_getSZ(1);
  @$pb.TagNumber(2)
  set realname($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasRealname() => $_has(1);
  @$pb.TagNumber(2)
  void clearRealname() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get alipayAccount => $_getSZ(2);
  @$pb.TagNumber(3)
  set alipayAccount($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasAlipayAccount() => $_has(2);
  @$pb.TagNumber(3)
  void clearAlipayAccount() => clearField(3);
}

class BindAlipayIntResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BindAlipayIntResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  BindAlipayIntResp._() : super();
  factory BindAlipayIntResp() => create();
  factory BindAlipayIntResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BindAlipayIntResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BindAlipayIntResp clone() => BindAlipayIntResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BindAlipayIntResp copyWith(void Function(BindAlipayIntResp) updates) => super.copyWith((message) => updates(message as BindAlipayIntResp)) as BindAlipayIntResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BindAlipayIntResp create() => BindAlipayIntResp._();
  BindAlipayIntResp createEmptyInstance() => create();
  static $pb.PbList<BindAlipayIntResp> createRepeated() => $pb.PbList<BindAlipayIntResp>();
  @$core.pragma('dart2js:noInline')
  static BindAlipayIntResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BindAlipayIntResp>(create);
  static BindAlipayIntResp? _defaultInstance;
}

class BindPromoterIdIntReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BindPromoterIdIntReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId', protoName: 'userId')
    ..aInt64(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'promoterId', protoName: 'promoterId')
    ..hasRequiredFields = false
  ;

  BindPromoterIdIntReq._() : super();
  factory BindPromoterIdIntReq({
    $fixnum.Int64? userId,
    $fixnum.Int64? promoterId,
  }) {
    final _result = create();
    if (userId != null) {
      _result.userId = userId;
    }
    if (promoterId != null) {
      _result.promoterId = promoterId;
    }
    return _result;
  }
  factory BindPromoterIdIntReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BindPromoterIdIntReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BindPromoterIdIntReq clone() => BindPromoterIdIntReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BindPromoterIdIntReq copyWith(void Function(BindPromoterIdIntReq) updates) => super.copyWith((message) => updates(message as BindPromoterIdIntReq)) as BindPromoterIdIntReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BindPromoterIdIntReq create() => BindPromoterIdIntReq._();
  BindPromoterIdIntReq createEmptyInstance() => create();
  static $pb.PbList<BindPromoterIdIntReq> createRepeated() => $pb.PbList<BindPromoterIdIntReq>();
  @$core.pragma('dart2js:noInline')
  static BindPromoterIdIntReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BindPromoterIdIntReq>(create);
  static BindPromoterIdIntReq? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get userId => $_getI64(0);
  @$pb.TagNumber(1)
  set userId($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUserId() => $_has(0);
  @$pb.TagNumber(1)
  void clearUserId() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get promoterId => $_getI64(1);
  @$pb.TagNumber(2)
  set promoterId($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPromoterId() => $_has(1);
  @$pb.TagNumber(2)
  void clearPromoterId() => clearField(2);
}

class BindPromoterIdIntResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'BindPromoterIdIntResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  BindPromoterIdIntResp._() : super();
  factory BindPromoterIdIntResp() => create();
  factory BindPromoterIdIntResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BindPromoterIdIntResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BindPromoterIdIntResp clone() => BindPromoterIdIntResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BindPromoterIdIntResp copyWith(void Function(BindPromoterIdIntResp) updates) => super.copyWith((message) => updates(message as BindPromoterIdIntResp)) as BindPromoterIdIntResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static BindPromoterIdIntResp create() => BindPromoterIdIntResp._();
  BindPromoterIdIntResp createEmptyInstance() => create();
  static $pb.PbList<BindPromoterIdIntResp> createRepeated() => $pb.PbList<BindPromoterIdIntResp>();
  @$core.pragma('dart2js:noInline')
  static BindPromoterIdIntResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BindPromoterIdIntResp>(create);
  static BindPromoterIdIntResp? _defaultInstance;
}

class ResetPwdReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ResetPwdReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId', protoName: 'userId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'newPwd', protoName: 'newPwd')
    ..hasRequiredFields = false
  ;

  ResetPwdReq._() : super();
  factory ResetPwdReq({
    $fixnum.Int64? userId,
    $core.String? newPwd,
  }) {
    final _result = create();
    if (userId != null) {
      _result.userId = userId;
    }
    if (newPwd != null) {
      _result.newPwd = newPwd;
    }
    return _result;
  }
  factory ResetPwdReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ResetPwdReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ResetPwdReq clone() => ResetPwdReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ResetPwdReq copyWith(void Function(ResetPwdReq) updates) => super.copyWith((message) => updates(message as ResetPwdReq)) as ResetPwdReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ResetPwdReq create() => ResetPwdReq._();
  ResetPwdReq createEmptyInstance() => create();
  static $pb.PbList<ResetPwdReq> createRepeated() => $pb.PbList<ResetPwdReq>();
  @$core.pragma('dart2js:noInline')
  static ResetPwdReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ResetPwdReq>(create);
  static ResetPwdReq? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get userId => $_getI64(0);
  @$pb.TagNumber(1)
  set userId($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUserId() => $_has(0);
  @$pb.TagNumber(1)
  void clearUserId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get newPwd => $_getSZ(1);
  @$pb.TagNumber(2)
  set newPwd($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasNewPwd() => $_has(1);
  @$pb.TagNumber(2)
  void clearNewPwd() => clearField(2);
}

class ResetPwdResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ResetPwdResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  ResetPwdResp._() : super();
  factory ResetPwdResp() => create();
  factory ResetPwdResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ResetPwdResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ResetPwdResp clone() => ResetPwdResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ResetPwdResp copyWith(void Function(ResetPwdResp) updates) => super.copyWith((message) => updates(message as ResetPwdResp)) as ResetPwdResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ResetPwdResp create() => ResetPwdResp._();
  ResetPwdResp createEmptyInstance() => create();
  static $pb.PbList<ResetPwdResp> createRepeated() => $pb.PbList<ResetPwdResp>();
  @$core.pragma('dart2js:noInline')
  static ResetPwdResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ResetPwdResp>(create);
  static ResetPwdResp? _defaultInstance;
}

class SetRedPacketGroupBaseConfigReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SetRedPacketGroupBaseConfigReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'channel')
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'minNum', $pb.PbFieldType.O3)
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'maxNum', $pb.PbFieldType.O3)
    ..aInt64(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'maxAmount')
    ..hasRequiredFields = false
  ;

  SetRedPacketGroupBaseConfigReq._() : super();
  factory SetRedPacketGroupBaseConfigReq({
    $fixnum.Int64? channel,
    $core.int? minNum,
    $core.int? maxNum,
    $fixnum.Int64? maxAmount,
  }) {
    final _result = create();
    if (channel != null) {
      _result.channel = channel;
    }
    if (minNum != null) {
      _result.minNum = minNum;
    }
    if (maxNum != null) {
      _result.maxNum = maxNum;
    }
    if (maxAmount != null) {
      _result.maxAmount = maxAmount;
    }
    return _result;
  }
  factory SetRedPacketGroupBaseConfigReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SetRedPacketGroupBaseConfigReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SetRedPacketGroupBaseConfigReq clone() => SetRedPacketGroupBaseConfigReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SetRedPacketGroupBaseConfigReq copyWith(void Function(SetRedPacketGroupBaseConfigReq) updates) => super.copyWith((message) => updates(message as SetRedPacketGroupBaseConfigReq)) as SetRedPacketGroupBaseConfigReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SetRedPacketGroupBaseConfigReq create() => SetRedPacketGroupBaseConfigReq._();
  SetRedPacketGroupBaseConfigReq createEmptyInstance() => create();
  static $pb.PbList<SetRedPacketGroupBaseConfigReq> createRepeated() => $pb.PbList<SetRedPacketGroupBaseConfigReq>();
  @$core.pragma('dart2js:noInline')
  static SetRedPacketGroupBaseConfigReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SetRedPacketGroupBaseConfigReq>(create);
  static SetRedPacketGroupBaseConfigReq? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get channel => $_getI64(0);
  @$pb.TagNumber(1)
  set channel($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasChannel() => $_has(0);
  @$pb.TagNumber(1)
  void clearChannel() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get minNum => $_getIZ(1);
  @$pb.TagNumber(2)
  set minNum($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMinNum() => $_has(1);
  @$pb.TagNumber(2)
  void clearMinNum() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get maxNum => $_getIZ(2);
  @$pb.TagNumber(3)
  set maxNum($core.int v) { $_setSignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasMaxNum() => $_has(2);
  @$pb.TagNumber(3)
  void clearMaxNum() => clearField(3);

  @$pb.TagNumber(4)
  $fixnum.Int64 get maxAmount => $_getI64(3);
  @$pb.TagNumber(4)
  set maxAmount($fixnum.Int64 v) { $_setInt64(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasMaxAmount() => $_has(3);
  @$pb.TagNumber(4)
  void clearMaxAmount() => clearField(4);
}

class SetRedPacketGroupBaseConfigResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'SetRedPacketGroupBaseConfigResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  SetRedPacketGroupBaseConfigResp._() : super();
  factory SetRedPacketGroupBaseConfigResp() => create();
  factory SetRedPacketGroupBaseConfigResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SetRedPacketGroupBaseConfigResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SetRedPacketGroupBaseConfigResp clone() => SetRedPacketGroupBaseConfigResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SetRedPacketGroupBaseConfigResp copyWith(void Function(SetRedPacketGroupBaseConfigResp) updates) => super.copyWith((message) => updates(message as SetRedPacketGroupBaseConfigResp)) as SetRedPacketGroupBaseConfigResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SetRedPacketGroupBaseConfigResp create() => SetRedPacketGroupBaseConfigResp._();
  SetRedPacketGroupBaseConfigResp createEmptyInstance() => create();
  static $pb.PbList<SetRedPacketGroupBaseConfigResp> createRepeated() => $pb.PbList<SetRedPacketGroupBaseConfigResp>();
  @$core.pragma('dart2js:noInline')
  static SetRedPacketGroupBaseConfigResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SetRedPacketGroupBaseConfigResp>(create);
  static SetRedPacketGroupBaseConfigResp? _defaultInstance;
}

class RegisterFakeRoomReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RegisterFakeRoomReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Channel', protoName: 'Channel')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Name', protoName: 'Name')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'AvatarUrl', protoName: 'AvatarUrl')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Introduction', protoName: 'Introduction')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Extra', protoName: 'Extra')
    ..hasRequiredFields = false
  ;

  RegisterFakeRoomReq._() : super();
  factory RegisterFakeRoomReq({
    $fixnum.Int64? channel,
    $core.String? name,
    $core.String? avatarUrl,
    $core.String? introduction,
    $core.String? extra,
  }) {
    final _result = create();
    if (channel != null) {
      _result.channel = channel;
    }
    if (name != null) {
      _result.name = name;
    }
    if (avatarUrl != null) {
      _result.avatarUrl = avatarUrl;
    }
    if (introduction != null) {
      _result.introduction = introduction;
    }
    if (extra != null) {
      _result.extra = extra;
    }
    return _result;
  }
  factory RegisterFakeRoomReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RegisterFakeRoomReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RegisterFakeRoomReq clone() => RegisterFakeRoomReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RegisterFakeRoomReq copyWith(void Function(RegisterFakeRoomReq) updates) => super.copyWith((message) => updates(message as RegisterFakeRoomReq)) as RegisterFakeRoomReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RegisterFakeRoomReq create() => RegisterFakeRoomReq._();
  RegisterFakeRoomReq createEmptyInstance() => create();
  static $pb.PbList<RegisterFakeRoomReq> createRepeated() => $pb.PbList<RegisterFakeRoomReq>();
  @$core.pragma('dart2js:noInline')
  static RegisterFakeRoomReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RegisterFakeRoomReq>(create);
  static RegisterFakeRoomReq? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get channel => $_getI64(0);
  @$pb.TagNumber(1)
  set channel($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasChannel() => $_has(0);
  @$pb.TagNumber(1)
  void clearChannel() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get avatarUrl => $_getSZ(2);
  @$pb.TagNumber(3)
  set avatarUrl($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasAvatarUrl() => $_has(2);
  @$pb.TagNumber(3)
  void clearAvatarUrl() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get introduction => $_getSZ(3);
  @$pb.TagNumber(4)
  set introduction($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasIntroduction() => $_has(3);
  @$pb.TagNumber(4)
  void clearIntroduction() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get extra => $_getSZ(4);
  @$pb.TagNumber(5)
  set extra($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasExtra() => $_has(4);
  @$pb.TagNumber(5)
  void clearExtra() => clearField(5);
}

class RegisterFakeRoomResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RegisterFakeRoomResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  RegisterFakeRoomResp._() : super();
  factory RegisterFakeRoomResp() => create();
  factory RegisterFakeRoomResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RegisterFakeRoomResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RegisterFakeRoomResp clone() => RegisterFakeRoomResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RegisterFakeRoomResp copyWith(void Function(RegisterFakeRoomResp) updates) => super.copyWith((message) => updates(message as RegisterFakeRoomResp)) as RegisterFakeRoomResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RegisterFakeRoomResp create() => RegisterFakeRoomResp._();
  RegisterFakeRoomResp createEmptyInstance() => create();
  static $pb.PbList<RegisterFakeRoomResp> createRepeated() => $pb.PbList<RegisterFakeRoomResp>();
  @$core.pragma('dart2js:noInline')
  static RegisterFakeRoomResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RegisterFakeRoomResp>(create);
  static RegisterFakeRoomResp? _defaultInstance;
}

class UnregisterFakeRoomReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'UnregisterFakeRoomReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Channel', protoName: 'Channel')
    ..hasRequiredFields = false
  ;

  UnregisterFakeRoomReq._() : super();
  factory UnregisterFakeRoomReq({
    $fixnum.Int64? channel,
  }) {
    final _result = create();
    if (channel != null) {
      _result.channel = channel;
    }
    return _result;
  }
  factory UnregisterFakeRoomReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory UnregisterFakeRoomReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  UnregisterFakeRoomReq clone() => UnregisterFakeRoomReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  UnregisterFakeRoomReq copyWith(void Function(UnregisterFakeRoomReq) updates) => super.copyWith((message) => updates(message as UnregisterFakeRoomReq)) as UnregisterFakeRoomReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static UnregisterFakeRoomReq create() => UnregisterFakeRoomReq._();
  UnregisterFakeRoomReq createEmptyInstance() => create();
  static $pb.PbList<UnregisterFakeRoomReq> createRepeated() => $pb.PbList<UnregisterFakeRoomReq>();
  @$core.pragma('dart2js:noInline')
  static UnregisterFakeRoomReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<UnregisterFakeRoomReq>(create);
  static UnregisterFakeRoomReq? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get channel => $_getI64(0);
  @$pb.TagNumber(1)
  set channel($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasChannel() => $_has(0);
  @$pb.TagNumber(1)
  void clearChannel() => clearField(1);
}

class UnregisterFakeRoomResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'UnregisterFakeRoomResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  UnregisterFakeRoomResp._() : super();
  factory UnregisterFakeRoomResp() => create();
  factory UnregisterFakeRoomResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory UnregisterFakeRoomResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  UnregisterFakeRoomResp clone() => UnregisterFakeRoomResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  UnregisterFakeRoomResp copyWith(void Function(UnregisterFakeRoomResp) updates) => super.copyWith((message) => updates(message as UnregisterFakeRoomResp)) as UnregisterFakeRoomResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static UnregisterFakeRoomResp create() => UnregisterFakeRoomResp._();
  UnregisterFakeRoomResp createEmptyInstance() => create();
  static $pb.PbList<UnregisterFakeRoomResp> createRepeated() => $pb.PbList<UnregisterFakeRoomResp>();
  @$core.pragma('dart2js:noInline')
  static UnregisterFakeRoomResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<UnregisterFakeRoomResp>(create);
  static UnregisterFakeRoomResp? _defaultInstance;
}

