///
//  Generated code. Do not modify.
//  source: connect.int.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import 'connect.ext.pb.dart' as $4;

class DeliverMessageReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'DeliverMessageReq', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId')
    ..aOM<$4.MessageSend>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'messageSend', subBuilder: $4.MessageSend.create)
    ..hasRequiredFields = false
  ;

  DeliverMessageReq._() : super();
  factory DeliverMessageReq({
    $fixnum.Int64? userId,
    $4.MessageSend? messageSend,
  }) {
    final _result = create();
    if (userId != null) {
      _result.userId = userId;
    }
    if (messageSend != null) {
      _result.messageSend = messageSend;
    }
    return _result;
  }
  factory DeliverMessageReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DeliverMessageReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  DeliverMessageReq clone() => DeliverMessageReq()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  DeliverMessageReq copyWith(void Function(DeliverMessageReq) updates) => super.copyWith((message) => updates(message as DeliverMessageReq)) as DeliverMessageReq; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DeliverMessageReq create() => DeliverMessageReq._();
  DeliverMessageReq createEmptyInstance() => create();
  static $pb.PbList<DeliverMessageReq> createRepeated() => $pb.PbList<DeliverMessageReq>();
  @$core.pragma('dart2js:noInline')
  static DeliverMessageReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DeliverMessageReq>(create);
  static DeliverMessageReq? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get userId => $_getI64(0);
  @$pb.TagNumber(1)
  set userId($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUserId() => $_has(0);
  @$pb.TagNumber(1)
  void clearUserId() => clearField(1);

  @$pb.TagNumber(2)
  $4.MessageSend get messageSend => $_getN(1);
  @$pb.TagNumber(2)
  set messageSend($4.MessageSend v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasMessageSend() => $_has(1);
  @$pb.TagNumber(2)
  void clearMessageSend() => clearField(2);
  @$pb.TagNumber(2)
  $4.MessageSend ensureMessageSend() => $_ensure(1);
}

class DeliverMessageResp extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'DeliverMessageResp', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  DeliverMessageResp._() : super();
  factory DeliverMessageResp() => create();
  factory DeliverMessageResp.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DeliverMessageResp.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  DeliverMessageResp clone() => DeliverMessageResp()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  DeliverMessageResp copyWith(void Function(DeliverMessageResp) updates) => super.copyWith((message) => updates(message as DeliverMessageResp)) as DeliverMessageResp; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DeliverMessageResp create() => DeliverMessageResp._();
  DeliverMessageResp createEmptyInstance() => create();
  static $pb.PbList<DeliverMessageResp> createRepeated() => $pb.PbList<DeliverMessageResp>();
  @$core.pragma('dart2js:noInline')
  static DeliverMessageResp getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DeliverMessageResp>(create);
  static DeliverMessageResp? _defaultInstance;
}

class PushRoomMsg extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PushRoomMsg', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'roomId')
    ..aOM<$4.MessageSend>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'messageSend', subBuilder: $4.MessageSend.create)
    ..hasRequiredFields = false
  ;

  PushRoomMsg._() : super();
  factory PushRoomMsg({
    $fixnum.Int64? roomId,
    $4.MessageSend? messageSend,
  }) {
    final _result = create();
    if (roomId != null) {
      _result.roomId = roomId;
    }
    if (messageSend != null) {
      _result.messageSend = messageSend;
    }
    return _result;
  }
  factory PushRoomMsg.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PushRoomMsg.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PushRoomMsg clone() => PushRoomMsg()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PushRoomMsg copyWith(void Function(PushRoomMsg) updates) => super.copyWith((message) => updates(message as PushRoomMsg)) as PushRoomMsg; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PushRoomMsg create() => PushRoomMsg._();
  PushRoomMsg createEmptyInstance() => create();
  static $pb.PbList<PushRoomMsg> createRepeated() => $pb.PbList<PushRoomMsg>();
  @$core.pragma('dart2js:noInline')
  static PushRoomMsg getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PushRoomMsg>(create);
  static PushRoomMsg? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get roomId => $_getI64(0);
  @$pb.TagNumber(1)
  set roomId($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasRoomId() => $_has(0);
  @$pb.TagNumber(1)
  void clearRoomId() => clearField(1);

  @$pb.TagNumber(2)
  $4.MessageSend get messageSend => $_getN(1);
  @$pb.TagNumber(2)
  set messageSend($4.MessageSend v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasMessageSend() => $_has(1);
  @$pb.TagNumber(2)
  void clearMessageSend() => clearField(2);
  @$pb.TagNumber(2)
  $4.MessageSend ensureMessageSend() => $_ensure(1);
}

class PushAllMsg extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'PushAllMsg', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aOM<$4.MessageSend>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'messageSend', subBuilder: $4.MessageSend.create)
    ..hasRequiredFields = false
  ;

  PushAllMsg._() : super();
  factory PushAllMsg({
    $4.MessageSend? messageSend,
  }) {
    final _result = create();
    if (messageSend != null) {
      _result.messageSend = messageSend;
    }
    return _result;
  }
  factory PushAllMsg.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PushAllMsg.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PushAllMsg clone() => PushAllMsg()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PushAllMsg copyWith(void Function(PushAllMsg) updates) => super.copyWith((message) => updates(message as PushAllMsg)) as PushAllMsg; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PushAllMsg create() => PushAllMsg._();
  PushAllMsg createEmptyInstance() => create();
  static $pb.PbList<PushAllMsg> createRepeated() => $pb.PbList<PushAllMsg>();
  @$core.pragma('dart2js:noInline')
  static PushAllMsg getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PushAllMsg>(create);
  static PushAllMsg? _defaultInstance;

  @$pb.TagNumber(2)
  $4.MessageSend get messageSend => $_getN(0);
  @$pb.TagNumber(2)
  set messageSend($4.MessageSend v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasMessageSend() => $_has(0);
  @$pb.TagNumber(2)
  void clearMessageSend() => clearField(2);
  @$pb.TagNumber(2)
  $4.MessageSend ensureMessageSend() => $_ensure(0);
}

