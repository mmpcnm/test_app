///
//  Generated code. Do not modify.
//  source: logic.int.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'logic.int.pb.dart' as $3;
import 'logic.ext.pb.dart' as $2;
import 'business.int.pb.dart' as $0;
export 'logic.int.pb.dart';

class LogicIntClient extends $grpc.Client {
  static final _$connSignIn =
      $grpc.ClientMethod<$3.ConnSignInReq, $3.ConnSignInResp>(
          '/pb.LogicInt/ConnSignIn',
          ($3.ConnSignInReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $3.ConnSignInResp.fromBuffer(value));
  static final _$sync = $grpc.ClientMethod<$3.SyncReq, $3.SyncResp>(
      '/pb.LogicInt/Sync',
      ($3.SyncReq value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $3.SyncResp.fromBuffer(value));
  static final _$messageACK =
      $grpc.ClientMethod<$3.MessageACKReq, $3.MessageACKResp>(
          '/pb.LogicInt/MessageACK',
          ($3.MessageACKReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $3.MessageACKResp.fromBuffer(value));
  static final _$offline = $grpc.ClientMethod<$3.OfflineReq, $3.OfflineResp>(
      '/pb.LogicInt/Offline',
      ($3.OfflineReq value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $3.OfflineResp.fromBuffer(value));
  static final _$subscribeRoom =
      $grpc.ClientMethod<$3.SubscribeRoomReq, $3.SubscribeRoomResp>(
          '/pb.LogicInt/SubscribeRoom',
          ($3.SubscribeRoomReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $3.SubscribeRoomResp.fromBuffer(value));
  static final _$sendMessage =
      $grpc.ClientMethod<$2.SendMessageReq, $2.SendMessageResp>(
          '/pb.LogicInt/SendMessage',
          ($2.SendMessageReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $2.SendMessageResp.fromBuffer(value));
  static final _$pushRoom = $grpc.ClientMethod<$2.PushRoomReq, $2.PushRoomResp>(
      '/pb.LogicInt/PushRoom',
      ($2.PushRoomReq value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $2.PushRoomResp.fromBuffer(value));
  static final _$pushAll = $grpc.ClientMethod<$3.PushAllReq, $3.PushAllResp>(
      '/pb.LogicInt/PushAll',
      ($3.PushAllReq value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $3.PushAllResp.fromBuffer(value));
  static final _$getUser = $grpc.ClientMethod<$2.GetUserReq, $2.GetUserResp>(
      '/pb.LogicInt/GetUser',
      ($2.GetUserReq value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $2.GetUserResp.fromBuffer(value));
  static final _$getUsers = $grpc.ClientMethod<$3.GetUsersReq, $3.GetUsersResp>(
      '/pb.LogicInt/GetUsers',
      ($3.GetUsersReq value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $3.GetUsersResp.fromBuffer(value));
  static final _$setUserFlag =
      $grpc.ClientMethod<$3.SetUserFlagReq, $3.SetUserFlagResp>(
          '/pb.LogicInt/SetUserFlag',
          ($3.SetUserFlagReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $3.SetUserFlagResp.fromBuffer(value));
  static final _$bindAlipay =
      $grpc.ClientMethod<$3.BindAlipayIntReq, $3.BindAlipayIntResp>(
          '/pb.LogicInt/BindAlipay',
          ($3.BindAlipayIntReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $3.BindAlipayIntResp.fromBuffer(value));
  static final _$bindPromoterId =
      $grpc.ClientMethod<$3.BindPromoterIdIntReq, $3.BindPromoterIdIntResp>(
          '/pb.LogicInt/BindPromoterId',
          ($3.BindPromoterIdIntReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $3.BindPromoterIdIntResp.fromBuffer(value));
  static final _$resetPwd = $grpc.ClientMethod<$3.ResetPwdReq, $3.ResetPwdResp>(
      '/pb.LogicInt/ResetPwd',
      ($3.ResetPwdReq value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $3.ResetPwdResp.fromBuffer(value));
  static final _$serverStop =
      $grpc.ClientMethod<$3.ServerStopReq, $3.ServerStopResp>(
          '/pb.LogicInt/ServerStop',
          ($3.ServerStopReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $3.ServerStopResp.fromBuffer(value));
  static final _$setRedPacketGroupBaseConfig = $grpc.ClientMethod<
          $3.SetRedPacketGroupBaseConfigReq,
          $3.SetRedPacketGroupBaseConfigResp>(
      '/pb.LogicInt/SetRedPacketGroupBaseConfig',
      ($3.SetRedPacketGroupBaseConfigReq value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $3.SetRedPacketGroupBaseConfigResp.fromBuffer(value));
  static final _$sendRedPacket =
      $grpc.ClientMethod<$0.SendRedPacketReq, $0.SendRedPacketResp>(
          '/pb.LogicInt/SendRedPacket',
          ($0.SendRedPacketReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.SendRedPacketResp.fromBuffer(value));
  static final _$registerFakeRoom =
      $grpc.ClientMethod<$3.RegisterFakeRoomReq, $3.RegisterFakeRoomResp>(
          '/pb.LogicInt/RegisterFakeRoom',
          ($3.RegisterFakeRoomReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $3.RegisterFakeRoomResp.fromBuffer(value));
  static final _$unregisterFakeRoom =
      $grpc.ClientMethod<$3.UnregisterFakeRoomReq, $3.UnregisterFakeRoomResp>(
          '/pb.LogicInt/UnregisterFakeRoom',
          ($3.UnregisterFakeRoomReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $3.UnregisterFakeRoomResp.fromBuffer(value));

  LogicIntClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$3.ConnSignInResp> connSignIn($3.ConnSignInReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$connSignIn, request, options: options);
  }

  $grpc.ResponseFuture<$3.SyncResp> sync($3.SyncReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$sync, request, options: options);
  }

  $grpc.ResponseFuture<$3.MessageACKResp> messageACK($3.MessageACKReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$messageACK, request, options: options);
  }

  $grpc.ResponseFuture<$3.OfflineResp> offline($3.OfflineReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$offline, request, options: options);
  }

  $grpc.ResponseFuture<$3.SubscribeRoomResp> subscribeRoom(
      $3.SubscribeRoomReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$subscribeRoom, request, options: options);
  }

  $grpc.ResponseFuture<$2.SendMessageResp> sendMessage(
      $2.SendMessageReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$sendMessage, request, options: options);
  }

  $grpc.ResponseFuture<$2.PushRoomResp> pushRoom($2.PushRoomReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$pushRoom, request, options: options);
  }

  $grpc.ResponseFuture<$3.PushAllResp> pushAll($3.PushAllReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$pushAll, request, options: options);
  }

  $grpc.ResponseFuture<$2.GetUserResp> getUser($2.GetUserReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getUser, request, options: options);
  }

  $grpc.ResponseFuture<$3.GetUsersResp> getUsers($3.GetUsersReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getUsers, request, options: options);
  }

  $grpc.ResponseFuture<$3.SetUserFlagResp> setUserFlag(
      $3.SetUserFlagReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$setUserFlag, request, options: options);
  }

  $grpc.ResponseFuture<$3.BindAlipayIntResp> bindAlipay(
      $3.BindAlipayIntReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$bindAlipay, request, options: options);
  }

  $grpc.ResponseFuture<$3.BindPromoterIdIntResp> bindPromoterId(
      $3.BindPromoterIdIntReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$bindPromoterId, request, options: options);
  }

  $grpc.ResponseFuture<$3.ResetPwdResp> resetPwd($3.ResetPwdReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$resetPwd, request, options: options);
  }

  $grpc.ResponseFuture<$3.ServerStopResp> serverStop($3.ServerStopReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$serverStop, request, options: options);
  }

  $grpc.ResponseFuture<$3.SetRedPacketGroupBaseConfigResp>
      setRedPacketGroupBaseConfig($3.SetRedPacketGroupBaseConfigReq request,
          {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$setRedPacketGroupBaseConfig, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.SendRedPacketResp> sendRedPacket(
      $0.SendRedPacketReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$sendRedPacket, request, options: options);
  }

  $grpc.ResponseFuture<$3.RegisterFakeRoomResp> registerFakeRoom(
      $3.RegisterFakeRoomReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$registerFakeRoom, request, options: options);
  }

  $grpc.ResponseFuture<$3.UnregisterFakeRoomResp> unregisterFakeRoom(
      $3.UnregisterFakeRoomReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$unregisterFakeRoom, request, options: options);
  }
}

abstract class LogicIntServiceBase extends $grpc.Service {
  $core.String get $name => 'pb.LogicInt';

  LogicIntServiceBase() {
    $addMethod($grpc.ServiceMethod<$3.ConnSignInReq, $3.ConnSignInResp>(
        'ConnSignIn',
        connSignIn_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $3.ConnSignInReq.fromBuffer(value),
        ($3.ConnSignInResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$3.SyncReq, $3.SyncResp>(
        'Sync',
        sync_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $3.SyncReq.fromBuffer(value),
        ($3.SyncResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$3.MessageACKReq, $3.MessageACKResp>(
        'MessageACK',
        messageACK_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $3.MessageACKReq.fromBuffer(value),
        ($3.MessageACKResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$3.OfflineReq, $3.OfflineResp>(
        'Offline',
        offline_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $3.OfflineReq.fromBuffer(value),
        ($3.OfflineResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$3.SubscribeRoomReq, $3.SubscribeRoomResp>(
        'SubscribeRoom',
        subscribeRoom_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $3.SubscribeRoomReq.fromBuffer(value),
        ($3.SubscribeRoomResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.SendMessageReq, $2.SendMessageResp>(
        'SendMessage',
        sendMessage_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.SendMessageReq.fromBuffer(value),
        ($2.SendMessageResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.PushRoomReq, $2.PushRoomResp>(
        'PushRoom',
        pushRoom_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.PushRoomReq.fromBuffer(value),
        ($2.PushRoomResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$3.PushAllReq, $3.PushAllResp>(
        'PushAll',
        pushAll_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $3.PushAllReq.fromBuffer(value),
        ($3.PushAllResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.GetUserReq, $2.GetUserResp>(
        'GetUser',
        getUser_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.GetUserReq.fromBuffer(value),
        ($2.GetUserResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$3.GetUsersReq, $3.GetUsersResp>(
        'GetUsers',
        getUsers_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $3.GetUsersReq.fromBuffer(value),
        ($3.GetUsersResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$3.SetUserFlagReq, $3.SetUserFlagResp>(
        'SetUserFlag',
        setUserFlag_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $3.SetUserFlagReq.fromBuffer(value),
        ($3.SetUserFlagResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$3.BindAlipayIntReq, $3.BindAlipayIntResp>(
        'BindAlipay',
        bindAlipay_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $3.BindAlipayIntReq.fromBuffer(value),
        ($3.BindAlipayIntResp value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$3.BindPromoterIdIntReq, $3.BindPromoterIdIntResp>(
            'BindPromoterId',
            bindPromoterId_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $3.BindPromoterIdIntReq.fromBuffer(value),
            ($3.BindPromoterIdIntResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$3.ResetPwdReq, $3.ResetPwdResp>(
        'ResetPwd',
        resetPwd_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $3.ResetPwdReq.fromBuffer(value),
        ($3.ResetPwdResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$3.ServerStopReq, $3.ServerStopResp>(
        'ServerStop',
        serverStop_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $3.ServerStopReq.fromBuffer(value),
        ($3.ServerStopResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$3.SetRedPacketGroupBaseConfigReq,
            $3.SetRedPacketGroupBaseConfigResp>(
        'SetRedPacketGroupBaseConfig',
        setRedPacketGroupBaseConfig_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $3.SetRedPacketGroupBaseConfigReq.fromBuffer(value),
        ($3.SetRedPacketGroupBaseConfigResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.SendRedPacketReq, $0.SendRedPacketResp>(
        'SendRedPacket',
        sendRedPacket_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.SendRedPacketReq.fromBuffer(value),
        ($0.SendRedPacketResp value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$3.RegisterFakeRoomReq, $3.RegisterFakeRoomResp>(
            'RegisterFakeRoom',
            registerFakeRoom_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $3.RegisterFakeRoomReq.fromBuffer(value),
            ($3.RegisterFakeRoomResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$3.UnregisterFakeRoomReq,
            $3.UnregisterFakeRoomResp>(
        'UnregisterFakeRoom',
        unregisterFakeRoom_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $3.UnregisterFakeRoomReq.fromBuffer(value),
        ($3.UnregisterFakeRoomResp value) => value.writeToBuffer()));
  }

  $async.Future<$3.ConnSignInResp> connSignIn_Pre(
      $grpc.ServiceCall call, $async.Future<$3.ConnSignInReq> request) async {
    return connSignIn(call, await request);
  }

  $async.Future<$3.SyncResp> sync_Pre(
      $grpc.ServiceCall call, $async.Future<$3.SyncReq> request) async {
    return sync(call, await request);
  }

  $async.Future<$3.MessageACKResp> messageACK_Pre(
      $grpc.ServiceCall call, $async.Future<$3.MessageACKReq> request) async {
    return messageACK(call, await request);
  }

  $async.Future<$3.OfflineResp> offline_Pre(
      $grpc.ServiceCall call, $async.Future<$3.OfflineReq> request) async {
    return offline(call, await request);
  }

  $async.Future<$3.SubscribeRoomResp> subscribeRoom_Pre($grpc.ServiceCall call,
      $async.Future<$3.SubscribeRoomReq> request) async {
    return subscribeRoom(call, await request);
  }

  $async.Future<$2.SendMessageResp> sendMessage_Pre(
      $grpc.ServiceCall call, $async.Future<$2.SendMessageReq> request) async {
    return sendMessage(call, await request);
  }

  $async.Future<$2.PushRoomResp> pushRoom_Pre(
      $grpc.ServiceCall call, $async.Future<$2.PushRoomReq> request) async {
    return pushRoom(call, await request);
  }

  $async.Future<$3.PushAllResp> pushAll_Pre(
      $grpc.ServiceCall call, $async.Future<$3.PushAllReq> request) async {
    return pushAll(call, await request);
  }

  $async.Future<$2.GetUserResp> getUser_Pre(
      $grpc.ServiceCall call, $async.Future<$2.GetUserReq> request) async {
    return getUser(call, await request);
  }

  $async.Future<$3.GetUsersResp> getUsers_Pre(
      $grpc.ServiceCall call, $async.Future<$3.GetUsersReq> request) async {
    return getUsers(call, await request);
  }

  $async.Future<$3.SetUserFlagResp> setUserFlag_Pre(
      $grpc.ServiceCall call, $async.Future<$3.SetUserFlagReq> request) async {
    return setUserFlag(call, await request);
  }

  $async.Future<$3.BindAlipayIntResp> bindAlipay_Pre($grpc.ServiceCall call,
      $async.Future<$3.BindAlipayIntReq> request) async {
    return bindAlipay(call, await request);
  }

  $async.Future<$3.BindPromoterIdIntResp> bindPromoterId_Pre(
      $grpc.ServiceCall call,
      $async.Future<$3.BindPromoterIdIntReq> request) async {
    return bindPromoterId(call, await request);
  }

  $async.Future<$3.ResetPwdResp> resetPwd_Pre(
      $grpc.ServiceCall call, $async.Future<$3.ResetPwdReq> request) async {
    return resetPwd(call, await request);
  }

  $async.Future<$3.ServerStopResp> serverStop_Pre(
      $grpc.ServiceCall call, $async.Future<$3.ServerStopReq> request) async {
    return serverStop(call, await request);
  }

  $async.Future<$3.SetRedPacketGroupBaseConfigResp>
      setRedPacketGroupBaseConfig_Pre($grpc.ServiceCall call,
          $async.Future<$3.SetRedPacketGroupBaseConfigReq> request) async {
    return setRedPacketGroupBaseConfig(call, await request);
  }

  $async.Future<$0.SendRedPacketResp> sendRedPacket_Pre($grpc.ServiceCall call,
      $async.Future<$0.SendRedPacketReq> request) async {
    return sendRedPacket(call, await request);
  }

  $async.Future<$3.RegisterFakeRoomResp> registerFakeRoom_Pre(
      $grpc.ServiceCall call,
      $async.Future<$3.RegisterFakeRoomReq> request) async {
    return registerFakeRoom(call, await request);
  }

  $async.Future<$3.UnregisterFakeRoomResp> unregisterFakeRoom_Pre(
      $grpc.ServiceCall call,
      $async.Future<$3.UnregisterFakeRoomReq> request) async {
    return unregisterFakeRoom(call, await request);
  }

  $async.Future<$3.ConnSignInResp> connSignIn(
      $grpc.ServiceCall call, $3.ConnSignInReq request);
  $async.Future<$3.SyncResp> sync($grpc.ServiceCall call, $3.SyncReq request);
  $async.Future<$3.MessageACKResp> messageACK(
      $grpc.ServiceCall call, $3.MessageACKReq request);
  $async.Future<$3.OfflineResp> offline(
      $grpc.ServiceCall call, $3.OfflineReq request);
  $async.Future<$3.SubscribeRoomResp> subscribeRoom(
      $grpc.ServiceCall call, $3.SubscribeRoomReq request);
  $async.Future<$2.SendMessageResp> sendMessage(
      $grpc.ServiceCall call, $2.SendMessageReq request);
  $async.Future<$2.PushRoomResp> pushRoom(
      $grpc.ServiceCall call, $2.PushRoomReq request);
  $async.Future<$3.PushAllResp> pushAll(
      $grpc.ServiceCall call, $3.PushAllReq request);
  $async.Future<$2.GetUserResp> getUser(
      $grpc.ServiceCall call, $2.GetUserReq request);
  $async.Future<$3.GetUsersResp> getUsers(
      $grpc.ServiceCall call, $3.GetUsersReq request);
  $async.Future<$3.SetUserFlagResp> setUserFlag(
      $grpc.ServiceCall call, $3.SetUserFlagReq request);
  $async.Future<$3.BindAlipayIntResp> bindAlipay(
      $grpc.ServiceCall call, $3.BindAlipayIntReq request);
  $async.Future<$3.BindPromoterIdIntResp> bindPromoterId(
      $grpc.ServiceCall call, $3.BindPromoterIdIntReq request);
  $async.Future<$3.ResetPwdResp> resetPwd(
      $grpc.ServiceCall call, $3.ResetPwdReq request);
  $async.Future<$3.ServerStopResp> serverStop(
      $grpc.ServiceCall call, $3.ServerStopReq request);
  $async.Future<$3.SetRedPacketGroupBaseConfigResp> setRedPacketGroupBaseConfig(
      $grpc.ServiceCall call, $3.SetRedPacketGroupBaseConfigReq request);
  $async.Future<$0.SendRedPacketResp> sendRedPacket(
      $grpc.ServiceCall call, $0.SendRedPacketReq request);
  $async.Future<$3.RegisterFakeRoomResp> registerFakeRoom(
      $grpc.ServiceCall call, $3.RegisterFakeRoomReq request);
  $async.Future<$3.UnregisterFakeRoomResp> unregisterFakeRoom(
      $grpc.ServiceCall call, $3.UnregisterFakeRoomReq request);
}
