///
//  Generated code. Do not modify.
//  source: logic.ext.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'logic.ext.pb.dart' as $2;
import 'business.int.pb.dart' as $0;
export 'logic.ext.pb.dart';

class LogicExtClient extends $grpc.Client {
  static final _$register = $grpc.ClientMethod<$2.RegisterReq, $2.RegisterResp>(
      '/pb.LogicExt/Register',
      ($2.RegisterReq value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $2.RegisterResp.fromBuffer(value));
  static final _$sendMessage =
      $grpc.ClientMethod<$2.SendMessageReq, $2.SendMessageResp>(
          '/pb.LogicExt/SendMessage',
          ($2.SendMessageReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $2.SendMessageResp.fromBuffer(value));
  static final _$pushRoom = $grpc.ClientMethod<$2.PushRoomReq, $2.PushRoomResp>(
      '/pb.LogicExt/PushRoom',
      ($2.PushRoomReq value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $2.PushRoomResp.fromBuffer(value));
  static final _$sendRedPacket =
      $grpc.ClientMethod<$0.SendRedPacketReq, $0.SendRedPacketResp>(
          '/pb.LogicExt/SendRedPacket',
          ($0.SendRedPacketReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.SendRedPacketResp.fromBuffer(value));
  static final _$robRedPacket =
      $grpc.ClientMethod<$0.RobRedPacketReq, $0.RobRedPacketResp>(
          '/pb.LogicExt/RobRedPacket',
          ($0.RobRedPacketReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.RobRedPacketResp.fromBuffer(value));
  static final _$lookUpRedPacket =
      $grpc.ClientMethod<$0.LookUpRedPacketReq, $0.LookUpRedPacketResp>(
          '/pb.LogicExt/LookUpRedPacket',
          ($0.LookUpRedPacketReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.LookUpRedPacketResp.fromBuffer(value));
  static final _$cancelMessage =
      $grpc.ClientMethod<$2.CancelMessageReq, $2.CancelMessageResp>(
          '/pb.LogicExt/CancelMessage',
          ($2.CancelMessageReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $2.CancelMessageResp.fromBuffer(value));
  static final _$addFriend =
      $grpc.ClientMethod<$2.AddFriendReq, $2.AddFriendResp>(
          '/pb.LogicExt/AddFriend',
          ($2.AddFriendReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $2.AddFriendResp.fromBuffer(value));
  static final _$agreeAddFriend =
      $grpc.ClientMethod<$2.AgreeAddFriendReq, $2.AgreeAddFriendResp>(
          '/pb.LogicExt/AgreeAddFriend',
          ($2.AgreeAddFriendReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $2.AgreeAddFriendResp.fromBuffer(value));
  static final _$setFriend =
      $grpc.ClientMethod<$2.SetFriendReq, $2.SetFriendResp>(
          '/pb.LogicExt/SetFriend',
          ($2.SetFriendReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $2.SetFriendResp.fromBuffer(value));
  static final _$getFriends =
      $grpc.ClientMethod<$2.GetFriendsReq, $2.GetFriendsResp>(
          '/pb.LogicExt/GetFriends',
          ($2.GetFriendsReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $2.GetFriendsResp.fromBuffer(value));
  static final _$delFriend =
      $grpc.ClientMethod<$2.DelFriendReq, $2.DelFriendResp>(
          '/pb.LogicExt/DelFriend',
          ($2.DelFriendReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $2.DelFriendResp.fromBuffer(value));
  static final _$createGroup =
      $grpc.ClientMethod<$2.CreateGroupReq, $2.CreateGroupResp>(
          '/pb.LogicExt/CreateGroup',
          ($2.CreateGroupReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $2.CreateGroupResp.fromBuffer(value));
  static final _$updateGroup =
      $grpc.ClientMethod<$2.UpdateGroupReq, $2.UpdateGroupResp>(
          '/pb.LogicExt/UpdateGroup',
          ($2.UpdateGroupReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $2.UpdateGroupResp.fromBuffer(value));
  static final _$getGroup = $grpc.ClientMethod<$2.GetGroupReq, $2.GetGroupResp>(
      '/pb.LogicExt/GetGroup',
      ($2.GetGroupReq value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $2.GetGroupResp.fromBuffer(value));
  static final _$getGroups =
      $grpc.ClientMethod<$2.GetGroupsReq, $2.GetGroupsResp>(
          '/pb.LogicExt/GetGroups',
          ($2.GetGroupsReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $2.GetGroupsResp.fromBuffer(value));
  static final _$addGroupMembers =
      $grpc.ClientMethod<$2.AddGroupMembersReq, $2.AddGroupMembersResp>(
          '/pb.LogicExt/AddGroupMembers',
          ($2.AddGroupMembersReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $2.AddGroupMembersResp.fromBuffer(value));
  static final _$updateGroupMember =
      $grpc.ClientMethod<$2.UpdateGroupMemberReq, $2.UpdateGroupMemberResp>(
          '/pb.LogicExt/UpdateGroupMember',
          ($2.UpdateGroupMemberReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $2.UpdateGroupMemberResp.fromBuffer(value));
  static final _$deleteGroupMember =
      $grpc.ClientMethod<$2.DeleteGroupMemberReq, $2.DeleteGroupMemberResp>(
          '/pb.LogicExt/DeleteGroupMember',
          ($2.DeleteGroupMemberReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $2.DeleteGroupMemberResp.fromBuffer(value));
  static final _$getGroupMembers =
      $grpc.ClientMethod<$2.GetGroupMembersReq, $2.GetGroupMembersResp>(
          '/pb.LogicExt/GetGroupMembers',
          ($2.GetGroupMembersReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $2.GetGroupMembersResp.fromBuffer(value));
  static final _$exitGroup =
      $grpc.ClientMethod<$2.ExitGroupReq, $2.ExitGroupResp>(
          '/pb.LogicExt/ExitGroup',
          ($2.ExitGroupReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $2.ExitGroupResp.fromBuffer(value));
  static final _$signIn = $grpc.ClientMethod<$2.SignInReq, $2.SignInResp>(
      '/pb.LogicExt/SignIn',
      ($2.SignInReq value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $2.SignInResp.fromBuffer(value));
  static final _$getUser = $grpc.ClientMethod<$2.GetUserReq, $2.GetUserResp>(
      '/pb.LogicExt/GetUser',
      ($2.GetUserReq value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $2.GetUserResp.fromBuffer(value));
  static final _$updateUser =
      $grpc.ClientMethod<$2.UpdateUserReq, $2.UpdateUserResp>(
          '/pb.LogicExt/UpdateUser',
          ($2.UpdateUserReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $2.UpdateUserResp.fromBuffer(value));
  static final _$searchUser =
      $grpc.ClientMethod<$2.SearchUserReq, $2.SearchUserResp>(
          '/pb.LogicExt/SearchUser',
          ($2.SearchUserReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $2.SearchUserResp.fromBuffer(value));
  static final _$modifyPwd =
      $grpc.ClientMethod<$2.ModifyPwdReq, $2.ModifyPwdResp>(
          '/pb.LogicExt/ModifyPwd',
          ($2.ModifyPwdReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $2.ModifyPwdResp.fromBuffer(value));
  static final _$openWallet =
      $grpc.ClientMethod<$2.OpenWalletReq, $2.OpenWalletResp>(
          '/pb.LogicExt/OpenWallet',
          ($2.OpenWalletReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $2.OpenWalletResp.fromBuffer(value));
  static final _$setWalletPwd =
      $grpc.ClientMethod<$2.SetWalletPwdReq, $2.SetWalletPwdResp>(
          '/pb.LogicExt/SetWalletPwd',
          ($2.SetWalletPwdReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $2.SetWalletPwdResp.fromBuffer(value));
  static final _$bindPhone =
      $grpc.ClientMethod<$2.BindPhoneReq, $2.BindPhoneResp>(
          '/pb.LogicExt/BindPhone',
          ($2.BindPhoneReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $2.BindPhoneResp.fromBuffer(value));
  static final _$bindAlipay =
      $grpc.ClientMethod<$2.BindAlipayReq, $2.BindAlipayResp>(
          '/pb.LogicExt/BindAlipay',
          ($2.BindAlipayReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $2.BindAlipayResp.fromBuffer(value));
  static final _$bindPromoter =
      $grpc.ClientMethod<$2.BindPromoterReq, $2.BindPromoterResp>(
          '/pb.LogicExt/BindPromoter',
          ($2.BindPromoterReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $2.BindPromoterResp.fromBuffer(value));
  static final _$queryBill =
      $grpc.ClientMethod<$2.QueryBillReq, $2.QueryBillResp>(
          '/pb.LogicExt/QueryBill',
          ($2.QueryBillReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $2.QueryBillResp.fromBuffer(value));
  static final _$getRedPacketRobAuth =
      $grpc.ClientMethod<$2.GetRedPacketRobAuthReq, $2.GetRedPacketRobAuthResp>(
          '/pb.LogicExt/GetRedPacketRobAuth',
          ($2.GetRedPacketRobAuthReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $2.GetRedPacketRobAuthResp.fromBuffer(value));
  static final _$getAllRoom =
      $grpc.ClientMethod<$2.GetAllRoomReq, $2.GetAllRoomResp>(
          '/pb.LogicExt/GetAllRoom',
          ($2.GetAllRoomReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $2.GetAllRoomResp.fromBuffer(value));
  static final _$getRoomUserList =
      $grpc.ClientMethod<$2.GetRoomUserListReq, $2.GetRoomUserListResp>(
          '/pb.LogicExt/GetRoomUserList',
          ($2.GetRoomUserListReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $2.GetRoomUserListResp.fromBuffer(value));

  LogicExtClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$2.RegisterResp> register($2.RegisterReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$register, request, options: options);
  }

  $grpc.ResponseFuture<$2.SendMessageResp> sendMessage(
      $2.SendMessageReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$sendMessage, request, options: options);
  }

  $grpc.ResponseFuture<$2.PushRoomResp> pushRoom($2.PushRoomReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$pushRoom, request, options: options);
  }

  $grpc.ResponseFuture<$0.SendRedPacketResp> sendRedPacket(
      $0.SendRedPacketReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$sendRedPacket, request, options: options);
  }

  $grpc.ResponseFuture<$0.RobRedPacketResp> robRedPacket(
      $0.RobRedPacketReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$robRedPacket, request, options: options);
  }

  $grpc.ResponseFuture<$0.LookUpRedPacketResp> lookUpRedPacket(
      $0.LookUpRedPacketReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$lookUpRedPacket, request, options: options);
  }

  $grpc.ResponseFuture<$2.CancelMessageResp> cancelMessage(
      $2.CancelMessageReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$cancelMessage, request, options: options);
  }

  $grpc.ResponseFuture<$2.AddFriendResp> addFriend($2.AddFriendReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$addFriend, request, options: options);
  }

  $grpc.ResponseFuture<$2.AgreeAddFriendResp> agreeAddFriend(
      $2.AgreeAddFriendReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$agreeAddFriend, request, options: options);
  }

  $grpc.ResponseFuture<$2.SetFriendResp> setFriend($2.SetFriendReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$setFriend, request, options: options);
  }

  $grpc.ResponseFuture<$2.GetFriendsResp> getFriends($2.GetFriendsReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getFriends, request, options: options);
  }

  $grpc.ResponseFuture<$2.DelFriendResp> delFriend($2.DelFriendReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$delFriend, request, options: options);
  }

  $grpc.ResponseFuture<$2.CreateGroupResp> createGroup(
      $2.CreateGroupReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$createGroup, request, options: options);
  }

  $grpc.ResponseFuture<$2.UpdateGroupResp> updateGroup(
      $2.UpdateGroupReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateGroup, request, options: options);
  }

  $grpc.ResponseFuture<$2.GetGroupResp> getGroup($2.GetGroupReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getGroup, request, options: options);
  }

  $grpc.ResponseFuture<$2.GetGroupsResp> getGroups($2.GetGroupsReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getGroups, request, options: options);
  }

  $grpc.ResponseFuture<$2.AddGroupMembersResp> addGroupMembers(
      $2.AddGroupMembersReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$addGroupMembers, request, options: options);
  }

  $grpc.ResponseFuture<$2.UpdateGroupMemberResp> updateGroupMember(
      $2.UpdateGroupMemberReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateGroupMember, request, options: options);
  }

  $grpc.ResponseFuture<$2.DeleteGroupMemberResp> deleteGroupMember(
      $2.DeleteGroupMemberReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$deleteGroupMember, request, options: options);
  }

  $grpc.ResponseFuture<$2.GetGroupMembersResp> getGroupMembers(
      $2.GetGroupMembersReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getGroupMembers, request, options: options);
  }

  $grpc.ResponseFuture<$2.ExitGroupResp> exitGroup($2.ExitGroupReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$exitGroup, request, options: options);
  }

  $grpc.ResponseFuture<$2.SignInResp> signIn($2.SignInReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$signIn, request, options: options);
  }

  $grpc.ResponseFuture<$2.GetUserResp> getUser($2.GetUserReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getUser, request, options: options);
  }

  $grpc.ResponseFuture<$2.UpdateUserResp> updateUser($2.UpdateUserReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateUser, request, options: options);
  }

  $grpc.ResponseFuture<$2.SearchUserResp> searchUser($2.SearchUserReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$searchUser, request, options: options);
  }

  $grpc.ResponseFuture<$2.ModifyPwdResp> modifyPwd($2.ModifyPwdReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$modifyPwd, request, options: options);
  }

  $grpc.ResponseFuture<$2.OpenWalletResp> openWallet($2.OpenWalletReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$openWallet, request, options: options);
  }

  $grpc.ResponseFuture<$2.SetWalletPwdResp> setWalletPwd(
      $2.SetWalletPwdReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$setWalletPwd, request, options: options);
  }

  $grpc.ResponseFuture<$2.BindPhoneResp> bindPhone($2.BindPhoneReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$bindPhone, request, options: options);
  }

  $grpc.ResponseFuture<$2.BindAlipayResp> bindAlipay($2.BindAlipayReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$bindAlipay, request, options: options);
  }

  $grpc.ResponseFuture<$2.BindPromoterResp> bindPromoter(
      $2.BindPromoterReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$bindPromoter, request, options: options);
  }

  $grpc.ResponseFuture<$2.QueryBillResp> queryBill($2.QueryBillReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$queryBill, request, options: options);
  }

  $grpc.ResponseFuture<$2.GetRedPacketRobAuthResp> getRedPacketRobAuth(
      $2.GetRedPacketRobAuthReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getRedPacketRobAuth, request, options: options);
  }

  $grpc.ResponseFuture<$2.GetAllRoomResp> getAllRoom($2.GetAllRoomReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getAllRoom, request, options: options);
  }

  $grpc.ResponseFuture<$2.GetRoomUserListResp> getRoomUserList(
      $2.GetRoomUserListReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getRoomUserList, request, options: options);
  }
}

abstract class LogicExtServiceBase extends $grpc.Service {
  $core.String get $name => 'pb.LogicExt';

  LogicExtServiceBase() {
    $addMethod($grpc.ServiceMethod<$2.RegisterReq, $2.RegisterResp>(
        'Register',
        register_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.RegisterReq.fromBuffer(value),
        ($2.RegisterResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.SendMessageReq, $2.SendMessageResp>(
        'SendMessage',
        sendMessage_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.SendMessageReq.fromBuffer(value),
        ($2.SendMessageResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.PushRoomReq, $2.PushRoomResp>(
        'PushRoom',
        pushRoom_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.PushRoomReq.fromBuffer(value),
        ($2.PushRoomResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.SendRedPacketReq, $0.SendRedPacketResp>(
        'SendRedPacket',
        sendRedPacket_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.SendRedPacketReq.fromBuffer(value),
        ($0.SendRedPacketResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.RobRedPacketReq, $0.RobRedPacketResp>(
        'RobRedPacket',
        robRedPacket_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.RobRedPacketReq.fromBuffer(value),
        ($0.RobRedPacketResp value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.LookUpRedPacketReq, $0.LookUpRedPacketResp>(
            'LookUpRedPacket',
            lookUpRedPacket_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.LookUpRedPacketReq.fromBuffer(value),
            ($0.LookUpRedPacketResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.CancelMessageReq, $2.CancelMessageResp>(
        'CancelMessage',
        cancelMessage_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.CancelMessageReq.fromBuffer(value),
        ($2.CancelMessageResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.AddFriendReq, $2.AddFriendResp>(
        'AddFriend',
        addFriend_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.AddFriendReq.fromBuffer(value),
        ($2.AddFriendResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.AgreeAddFriendReq, $2.AgreeAddFriendResp>(
        'AgreeAddFriend',
        agreeAddFriend_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.AgreeAddFriendReq.fromBuffer(value),
        ($2.AgreeAddFriendResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.SetFriendReq, $2.SetFriendResp>(
        'SetFriend',
        setFriend_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.SetFriendReq.fromBuffer(value),
        ($2.SetFriendResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.GetFriendsReq, $2.GetFriendsResp>(
        'GetFriends',
        getFriends_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.GetFriendsReq.fromBuffer(value),
        ($2.GetFriendsResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.DelFriendReq, $2.DelFriendResp>(
        'DelFriend',
        delFriend_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.DelFriendReq.fromBuffer(value),
        ($2.DelFriendResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.CreateGroupReq, $2.CreateGroupResp>(
        'CreateGroup',
        createGroup_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.CreateGroupReq.fromBuffer(value),
        ($2.CreateGroupResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.UpdateGroupReq, $2.UpdateGroupResp>(
        'UpdateGroup',
        updateGroup_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.UpdateGroupReq.fromBuffer(value),
        ($2.UpdateGroupResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.GetGroupReq, $2.GetGroupResp>(
        'GetGroup',
        getGroup_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.GetGroupReq.fromBuffer(value),
        ($2.GetGroupResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.GetGroupsReq, $2.GetGroupsResp>(
        'GetGroups',
        getGroups_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.GetGroupsReq.fromBuffer(value),
        ($2.GetGroupsResp value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$2.AddGroupMembersReq, $2.AddGroupMembersResp>(
            'AddGroupMembers',
            addGroupMembers_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $2.AddGroupMembersReq.fromBuffer(value),
            ($2.AddGroupMembersResp value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$2.UpdateGroupMemberReq, $2.UpdateGroupMemberResp>(
            'UpdateGroupMember',
            updateGroupMember_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $2.UpdateGroupMemberReq.fromBuffer(value),
            ($2.UpdateGroupMemberResp value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$2.DeleteGroupMemberReq, $2.DeleteGroupMemberResp>(
            'DeleteGroupMember',
            deleteGroupMember_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $2.DeleteGroupMemberReq.fromBuffer(value),
            ($2.DeleteGroupMemberResp value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$2.GetGroupMembersReq, $2.GetGroupMembersResp>(
            'GetGroupMembers',
            getGroupMembers_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $2.GetGroupMembersReq.fromBuffer(value),
            ($2.GetGroupMembersResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.ExitGroupReq, $2.ExitGroupResp>(
        'ExitGroup',
        exitGroup_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.ExitGroupReq.fromBuffer(value),
        ($2.ExitGroupResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.SignInReq, $2.SignInResp>(
        'SignIn',
        signIn_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.SignInReq.fromBuffer(value),
        ($2.SignInResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.GetUserReq, $2.GetUserResp>(
        'GetUser',
        getUser_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.GetUserReq.fromBuffer(value),
        ($2.GetUserResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.UpdateUserReq, $2.UpdateUserResp>(
        'UpdateUser',
        updateUser_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.UpdateUserReq.fromBuffer(value),
        ($2.UpdateUserResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.SearchUserReq, $2.SearchUserResp>(
        'SearchUser',
        searchUser_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.SearchUserReq.fromBuffer(value),
        ($2.SearchUserResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.ModifyPwdReq, $2.ModifyPwdResp>(
        'ModifyPwd',
        modifyPwd_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.ModifyPwdReq.fromBuffer(value),
        ($2.ModifyPwdResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.OpenWalletReq, $2.OpenWalletResp>(
        'OpenWallet',
        openWallet_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.OpenWalletReq.fromBuffer(value),
        ($2.OpenWalletResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.SetWalletPwdReq, $2.SetWalletPwdResp>(
        'SetWalletPwd',
        setWalletPwd_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.SetWalletPwdReq.fromBuffer(value),
        ($2.SetWalletPwdResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.BindPhoneReq, $2.BindPhoneResp>(
        'BindPhone',
        bindPhone_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.BindPhoneReq.fromBuffer(value),
        ($2.BindPhoneResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.BindAlipayReq, $2.BindAlipayResp>(
        'BindAlipay',
        bindAlipay_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.BindAlipayReq.fromBuffer(value),
        ($2.BindAlipayResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.BindPromoterReq, $2.BindPromoterResp>(
        'BindPromoter',
        bindPromoter_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.BindPromoterReq.fromBuffer(value),
        ($2.BindPromoterResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.QueryBillReq, $2.QueryBillResp>(
        'QueryBill',
        queryBill_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.QueryBillReq.fromBuffer(value),
        ($2.QueryBillResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.GetRedPacketRobAuthReq,
            $2.GetRedPacketRobAuthResp>(
        'GetRedPacketRobAuth',
        getRedPacketRobAuth_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $2.GetRedPacketRobAuthReq.fromBuffer(value),
        ($2.GetRedPacketRobAuthResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$2.GetAllRoomReq, $2.GetAllRoomResp>(
        'GetAllRoom',
        getAllRoom_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $2.GetAllRoomReq.fromBuffer(value),
        ($2.GetAllRoomResp value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$2.GetRoomUserListReq, $2.GetRoomUserListResp>(
            'GetRoomUserList',
            getRoomUserList_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $2.GetRoomUserListReq.fromBuffer(value),
            ($2.GetRoomUserListResp value) => value.writeToBuffer()));
  }

  $async.Future<$2.RegisterResp> register_Pre(
      $grpc.ServiceCall call, $async.Future<$2.RegisterReq> request) async {
    return register(call, await request);
  }

  $async.Future<$2.SendMessageResp> sendMessage_Pre(
      $grpc.ServiceCall call, $async.Future<$2.SendMessageReq> request) async {
    return sendMessage(call, await request);
  }

  $async.Future<$2.PushRoomResp> pushRoom_Pre(
      $grpc.ServiceCall call, $async.Future<$2.PushRoomReq> request) async {
    return pushRoom(call, await request);
  }

  $async.Future<$0.SendRedPacketResp> sendRedPacket_Pre($grpc.ServiceCall call,
      $async.Future<$0.SendRedPacketReq> request) async {
    return sendRedPacket(call, await request);
  }

  $async.Future<$0.RobRedPacketResp> robRedPacket_Pre(
      $grpc.ServiceCall call, $async.Future<$0.RobRedPacketReq> request) async {
    return robRedPacket(call, await request);
  }

  $async.Future<$0.LookUpRedPacketResp> lookUpRedPacket_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.LookUpRedPacketReq> request) async {
    return lookUpRedPacket(call, await request);
  }

  $async.Future<$2.CancelMessageResp> cancelMessage_Pre($grpc.ServiceCall call,
      $async.Future<$2.CancelMessageReq> request) async {
    return cancelMessage(call, await request);
  }

  $async.Future<$2.AddFriendResp> addFriend_Pre(
      $grpc.ServiceCall call, $async.Future<$2.AddFriendReq> request) async {
    return addFriend(call, await request);
  }

  $async.Future<$2.AgreeAddFriendResp> agreeAddFriend_Pre(
      $grpc.ServiceCall call,
      $async.Future<$2.AgreeAddFriendReq> request) async {
    return agreeAddFriend(call, await request);
  }

  $async.Future<$2.SetFriendResp> setFriend_Pre(
      $grpc.ServiceCall call, $async.Future<$2.SetFriendReq> request) async {
    return setFriend(call, await request);
  }

  $async.Future<$2.GetFriendsResp> getFriends_Pre(
      $grpc.ServiceCall call, $async.Future<$2.GetFriendsReq> request) async {
    return getFriends(call, await request);
  }

  $async.Future<$2.DelFriendResp> delFriend_Pre(
      $grpc.ServiceCall call, $async.Future<$2.DelFriendReq> request) async {
    return delFriend(call, await request);
  }

  $async.Future<$2.CreateGroupResp> createGroup_Pre(
      $grpc.ServiceCall call, $async.Future<$2.CreateGroupReq> request) async {
    return createGroup(call, await request);
  }

  $async.Future<$2.UpdateGroupResp> updateGroup_Pre(
      $grpc.ServiceCall call, $async.Future<$2.UpdateGroupReq> request) async {
    return updateGroup(call, await request);
  }

  $async.Future<$2.GetGroupResp> getGroup_Pre(
      $grpc.ServiceCall call, $async.Future<$2.GetGroupReq> request) async {
    return getGroup(call, await request);
  }

  $async.Future<$2.GetGroupsResp> getGroups_Pre(
      $grpc.ServiceCall call, $async.Future<$2.GetGroupsReq> request) async {
    return getGroups(call, await request);
  }

  $async.Future<$2.AddGroupMembersResp> addGroupMembers_Pre(
      $grpc.ServiceCall call,
      $async.Future<$2.AddGroupMembersReq> request) async {
    return addGroupMembers(call, await request);
  }

  $async.Future<$2.UpdateGroupMemberResp> updateGroupMember_Pre(
      $grpc.ServiceCall call,
      $async.Future<$2.UpdateGroupMemberReq> request) async {
    return updateGroupMember(call, await request);
  }

  $async.Future<$2.DeleteGroupMemberResp> deleteGroupMember_Pre(
      $grpc.ServiceCall call,
      $async.Future<$2.DeleteGroupMemberReq> request) async {
    return deleteGroupMember(call, await request);
  }

  $async.Future<$2.GetGroupMembersResp> getGroupMembers_Pre(
      $grpc.ServiceCall call,
      $async.Future<$2.GetGroupMembersReq> request) async {
    return getGroupMembers(call, await request);
  }

  $async.Future<$2.ExitGroupResp> exitGroup_Pre(
      $grpc.ServiceCall call, $async.Future<$2.ExitGroupReq> request) async {
    return exitGroup(call, await request);
  }

  $async.Future<$2.SignInResp> signIn_Pre(
      $grpc.ServiceCall call, $async.Future<$2.SignInReq> request) async {
    return signIn(call, await request);
  }

  $async.Future<$2.GetUserResp> getUser_Pre(
      $grpc.ServiceCall call, $async.Future<$2.GetUserReq> request) async {
    return getUser(call, await request);
  }

  $async.Future<$2.UpdateUserResp> updateUser_Pre(
      $grpc.ServiceCall call, $async.Future<$2.UpdateUserReq> request) async {
    return updateUser(call, await request);
  }

  $async.Future<$2.SearchUserResp> searchUser_Pre(
      $grpc.ServiceCall call, $async.Future<$2.SearchUserReq> request) async {
    return searchUser(call, await request);
  }

  $async.Future<$2.ModifyPwdResp> modifyPwd_Pre(
      $grpc.ServiceCall call, $async.Future<$2.ModifyPwdReq> request) async {
    return modifyPwd(call, await request);
  }

  $async.Future<$2.OpenWalletResp> openWallet_Pre(
      $grpc.ServiceCall call, $async.Future<$2.OpenWalletReq> request) async {
    return openWallet(call, await request);
  }

  $async.Future<$2.SetWalletPwdResp> setWalletPwd_Pre(
      $grpc.ServiceCall call, $async.Future<$2.SetWalletPwdReq> request) async {
    return setWalletPwd(call, await request);
  }

  $async.Future<$2.BindPhoneResp> bindPhone_Pre(
      $grpc.ServiceCall call, $async.Future<$2.BindPhoneReq> request) async {
    return bindPhone(call, await request);
  }

  $async.Future<$2.BindAlipayResp> bindAlipay_Pre(
      $grpc.ServiceCall call, $async.Future<$2.BindAlipayReq> request) async {
    return bindAlipay(call, await request);
  }

  $async.Future<$2.BindPromoterResp> bindPromoter_Pre(
      $grpc.ServiceCall call, $async.Future<$2.BindPromoterReq> request) async {
    return bindPromoter(call, await request);
  }

  $async.Future<$2.QueryBillResp> queryBill_Pre(
      $grpc.ServiceCall call, $async.Future<$2.QueryBillReq> request) async {
    return queryBill(call, await request);
  }

  $async.Future<$2.GetRedPacketRobAuthResp> getRedPacketRobAuth_Pre(
      $grpc.ServiceCall call,
      $async.Future<$2.GetRedPacketRobAuthReq> request) async {
    return getRedPacketRobAuth(call, await request);
  }

  $async.Future<$2.GetAllRoomResp> getAllRoom_Pre(
      $grpc.ServiceCall call, $async.Future<$2.GetAllRoomReq> request) async {
    return getAllRoom(call, await request);
  }

  $async.Future<$2.GetRoomUserListResp> getRoomUserList_Pre(
      $grpc.ServiceCall call,
      $async.Future<$2.GetRoomUserListReq> request) async {
    return getRoomUserList(call, await request);
  }

  $async.Future<$2.RegisterResp> register(
      $grpc.ServiceCall call, $2.RegisterReq request);
  $async.Future<$2.SendMessageResp> sendMessage(
      $grpc.ServiceCall call, $2.SendMessageReq request);
  $async.Future<$2.PushRoomResp> pushRoom(
      $grpc.ServiceCall call, $2.PushRoomReq request);
  $async.Future<$0.SendRedPacketResp> sendRedPacket(
      $grpc.ServiceCall call, $0.SendRedPacketReq request);
  $async.Future<$0.RobRedPacketResp> robRedPacket(
      $grpc.ServiceCall call, $0.RobRedPacketReq request);
  $async.Future<$0.LookUpRedPacketResp> lookUpRedPacket(
      $grpc.ServiceCall call, $0.LookUpRedPacketReq request);
  $async.Future<$2.CancelMessageResp> cancelMessage(
      $grpc.ServiceCall call, $2.CancelMessageReq request);
  $async.Future<$2.AddFriendResp> addFriend(
      $grpc.ServiceCall call, $2.AddFriendReq request);
  $async.Future<$2.AgreeAddFriendResp> agreeAddFriend(
      $grpc.ServiceCall call, $2.AgreeAddFriendReq request);
  $async.Future<$2.SetFriendResp> setFriend(
      $grpc.ServiceCall call, $2.SetFriendReq request);
  $async.Future<$2.GetFriendsResp> getFriends(
      $grpc.ServiceCall call, $2.GetFriendsReq request);
  $async.Future<$2.DelFriendResp> delFriend(
      $grpc.ServiceCall call, $2.DelFriendReq request);
  $async.Future<$2.CreateGroupResp> createGroup(
      $grpc.ServiceCall call, $2.CreateGroupReq request);
  $async.Future<$2.UpdateGroupResp> updateGroup(
      $grpc.ServiceCall call, $2.UpdateGroupReq request);
  $async.Future<$2.GetGroupResp> getGroup(
      $grpc.ServiceCall call, $2.GetGroupReq request);
  $async.Future<$2.GetGroupsResp> getGroups(
      $grpc.ServiceCall call, $2.GetGroupsReq request);
  $async.Future<$2.AddGroupMembersResp> addGroupMembers(
      $grpc.ServiceCall call, $2.AddGroupMembersReq request);
  $async.Future<$2.UpdateGroupMemberResp> updateGroupMember(
      $grpc.ServiceCall call, $2.UpdateGroupMemberReq request);
  $async.Future<$2.DeleteGroupMemberResp> deleteGroupMember(
      $grpc.ServiceCall call, $2.DeleteGroupMemberReq request);
  $async.Future<$2.GetGroupMembersResp> getGroupMembers(
      $grpc.ServiceCall call, $2.GetGroupMembersReq request);
  $async.Future<$2.ExitGroupResp> exitGroup(
      $grpc.ServiceCall call, $2.ExitGroupReq request);
  $async.Future<$2.SignInResp> signIn(
      $grpc.ServiceCall call, $2.SignInReq request);
  $async.Future<$2.GetUserResp> getUser(
      $grpc.ServiceCall call, $2.GetUserReq request);
  $async.Future<$2.UpdateUserResp> updateUser(
      $grpc.ServiceCall call, $2.UpdateUserReq request);
  $async.Future<$2.SearchUserResp> searchUser(
      $grpc.ServiceCall call, $2.SearchUserReq request);
  $async.Future<$2.ModifyPwdResp> modifyPwd(
      $grpc.ServiceCall call, $2.ModifyPwdReq request);
  $async.Future<$2.OpenWalletResp> openWallet(
      $grpc.ServiceCall call, $2.OpenWalletReq request);
  $async.Future<$2.SetWalletPwdResp> setWalletPwd(
      $grpc.ServiceCall call, $2.SetWalletPwdReq request);
  $async.Future<$2.BindPhoneResp> bindPhone(
      $grpc.ServiceCall call, $2.BindPhoneReq request);
  $async.Future<$2.BindAlipayResp> bindAlipay(
      $grpc.ServiceCall call, $2.BindAlipayReq request);
  $async.Future<$2.BindPromoterResp> bindPromoter(
      $grpc.ServiceCall call, $2.BindPromoterReq request);
  $async.Future<$2.QueryBillResp> queryBill(
      $grpc.ServiceCall call, $2.QueryBillReq request);
  $async.Future<$2.GetRedPacketRobAuthResp> getRedPacketRobAuth(
      $grpc.ServiceCall call, $2.GetRedPacketRobAuthReq request);
  $async.Future<$2.GetAllRoomResp> getAllRoom(
      $grpc.ServiceCall call, $2.GetAllRoomReq request);
  $async.Future<$2.GetRoomUserListResp> getRoomUserList(
      $grpc.ServiceCall call, $2.GetRoomUserListReq request);
}
