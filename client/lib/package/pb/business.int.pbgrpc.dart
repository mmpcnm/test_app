///
//  Generated code. Do not modify.
//  source: business.int.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'business.int.pb.dart' as $0;
export 'business.int.pb.dart';

class BusinessIntClient extends $grpc.Client {
  static final _$sendOut =
      $grpc.ClientMethod<$0.SendRedPacketReq, $0.SendRedPacketResp>(
          '/pb.BusinessInt/SendOut',
          ($0.SendRedPacketReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.SendRedPacketResp.fromBuffer(value));
  static final _$rob =
      $grpc.ClientMethod<$0.RobRedPacketReq, $0.RobRedPacketResp>(
          '/pb.BusinessInt/Rob',
          ($0.RobRedPacketReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.RobRedPacketResp.fromBuffer(value));
  static final _$lookUp =
      $grpc.ClientMethod<$0.LookUpRedPacketReq, $0.LookUpRedPacketResp>(
          '/pb.BusinessInt/LookUp',
          ($0.LookUpRedPacketReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.LookUpRedPacketResp.fromBuffer(value));
  static final _$setRedPacketAuthConfig = $grpc.ClientMethod<
          $0.SetRedPacketAuthConfigReq, $0.SetRedPacketAuthConfigResp>(
      '/pb.BusinessInt/SetRedPacketAuthConfig',
      ($0.SetRedPacketAuthConfigReq value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.SetRedPacketAuthConfigResp.fromBuffer(value));
  static final _$updateRobAuth =
      $grpc.ClientMethod<$0.UpdateRobAuthReq, $0.UpdateRobAuthResp>(
          '/pb.BusinessInt/UpdateRobAuth',
          ($0.UpdateRobAuthReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.UpdateRobAuthResp.fromBuffer(value));
  static final _$getRobAuth =
      $grpc.ClientMethod<$0.GetRobAuthReq, $0.GetRobAuthResp>(
          '/pb.BusinessInt/GetRobAuth',
          ($0.GetRobAuthReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.GetRobAuthResp.fromBuffer(value));
  static final _$refund =
      $grpc.ClientMethod<$0.RefundRedPacketReq, $0.RefundRedPacketResp>(
          '/pb.BusinessInt/Refund',
          ($0.RefundRedPacketReq value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.RefundRedPacketResp.fromBuffer(value));
  static final _$refundResult = $grpc.ClientMethod<$0.RefundRedPacketResultReq,
          $0.RefundRedPacketResultResp>(
      '/pb.BusinessInt/RefundResult',
      ($0.RefundRedPacketResultReq value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.RefundRedPacketResultResp.fromBuffer(value));

  BusinessIntClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.SendRedPacketResp> sendOut(
      $0.SendRedPacketReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$sendOut, request, options: options);
  }

  $grpc.ResponseFuture<$0.RobRedPacketResp> rob($0.RobRedPacketReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$rob, request, options: options);
  }

  $grpc.ResponseFuture<$0.LookUpRedPacketResp> lookUp(
      $0.LookUpRedPacketReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$lookUp, request, options: options);
  }

  $grpc.ResponseFuture<$0.SetRedPacketAuthConfigResp> setRedPacketAuthConfig(
      $0.SetRedPacketAuthConfigReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$setRedPacketAuthConfig, request,
        options: options);
  }

  $grpc.ResponseFuture<$0.UpdateRobAuthResp> updateRobAuth(
      $0.UpdateRobAuthReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$updateRobAuth, request, options: options);
  }

  $grpc.ResponseFuture<$0.GetRobAuthResp> getRobAuth($0.GetRobAuthReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$getRobAuth, request, options: options);
  }

  $grpc.ResponseFuture<$0.RefundRedPacketResp> refund(
      $0.RefundRedPacketReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$refund, request, options: options);
  }

  $grpc.ResponseFuture<$0.RefundRedPacketResultResp> refundResult(
      $0.RefundRedPacketResultReq request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$refundResult, request, options: options);
  }
}

abstract class BusinessIntServiceBase extends $grpc.Service {
  $core.String get $name => 'pb.BusinessInt';

  BusinessIntServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.SendRedPacketReq, $0.SendRedPacketResp>(
        'SendOut',
        sendOut_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.SendRedPacketReq.fromBuffer(value),
        ($0.SendRedPacketResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.RobRedPacketReq, $0.RobRedPacketResp>(
        'Rob',
        rob_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.RobRedPacketReq.fromBuffer(value),
        ($0.RobRedPacketResp value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.LookUpRedPacketReq, $0.LookUpRedPacketResp>(
            'LookUp',
            lookUp_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.LookUpRedPacketReq.fromBuffer(value),
            ($0.LookUpRedPacketResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.SetRedPacketAuthConfigReq,
            $0.SetRedPacketAuthConfigResp>(
        'SetRedPacketAuthConfig',
        setRedPacketAuthConfig_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.SetRedPacketAuthConfigReq.fromBuffer(value),
        ($0.SetRedPacketAuthConfigResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.UpdateRobAuthReq, $0.UpdateRobAuthResp>(
        'UpdateRobAuth',
        updateRobAuth_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.UpdateRobAuthReq.fromBuffer(value),
        ($0.UpdateRobAuthResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.GetRobAuthReq, $0.GetRobAuthResp>(
        'GetRobAuth',
        getRobAuth_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.GetRobAuthReq.fromBuffer(value),
        ($0.GetRobAuthResp value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.RefundRedPacketReq, $0.RefundRedPacketResp>(
            'Refund',
            refund_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.RefundRedPacketReq.fromBuffer(value),
            ($0.RefundRedPacketResp value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.RefundRedPacketResultReq,
            $0.RefundRedPacketResultResp>(
        'RefundResult',
        refundResult_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.RefundRedPacketResultReq.fromBuffer(value),
        ($0.RefundRedPacketResultResp value) => value.writeToBuffer()));
  }

  $async.Future<$0.SendRedPacketResp> sendOut_Pre($grpc.ServiceCall call,
      $async.Future<$0.SendRedPacketReq> request) async {
    return sendOut(call, await request);
  }

  $async.Future<$0.RobRedPacketResp> rob_Pre(
      $grpc.ServiceCall call, $async.Future<$0.RobRedPacketReq> request) async {
    return rob(call, await request);
  }

  $async.Future<$0.LookUpRedPacketResp> lookUp_Pre($grpc.ServiceCall call,
      $async.Future<$0.LookUpRedPacketReq> request) async {
    return lookUp(call, await request);
  }

  $async.Future<$0.SetRedPacketAuthConfigResp> setRedPacketAuthConfig_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.SetRedPacketAuthConfigReq> request) async {
    return setRedPacketAuthConfig(call, await request);
  }

  $async.Future<$0.UpdateRobAuthResp> updateRobAuth_Pre($grpc.ServiceCall call,
      $async.Future<$0.UpdateRobAuthReq> request) async {
    return updateRobAuth(call, await request);
  }

  $async.Future<$0.GetRobAuthResp> getRobAuth_Pre(
      $grpc.ServiceCall call, $async.Future<$0.GetRobAuthReq> request) async {
    return getRobAuth(call, await request);
  }

  $async.Future<$0.RefundRedPacketResp> refund_Pre($grpc.ServiceCall call,
      $async.Future<$0.RefundRedPacketReq> request) async {
    return refund(call, await request);
  }

  $async.Future<$0.RefundRedPacketResultResp> refundResult_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.RefundRedPacketResultReq> request) async {
    return refundResult(call, await request);
  }

  $async.Future<$0.SendRedPacketResp> sendOut(
      $grpc.ServiceCall call, $0.SendRedPacketReq request);
  $async.Future<$0.RobRedPacketResp> rob(
      $grpc.ServiceCall call, $0.RobRedPacketReq request);
  $async.Future<$0.LookUpRedPacketResp> lookUp(
      $grpc.ServiceCall call, $0.LookUpRedPacketReq request);
  $async.Future<$0.SetRedPacketAuthConfigResp> setRedPacketAuthConfig(
      $grpc.ServiceCall call, $0.SetRedPacketAuthConfigReq request);
  $async.Future<$0.UpdateRobAuthResp> updateRobAuth(
      $grpc.ServiceCall call, $0.UpdateRobAuthReq request);
  $async.Future<$0.GetRobAuthResp> getRobAuth(
      $grpc.ServiceCall call, $0.GetRobAuthReq request);
  $async.Future<$0.RefundRedPacketResp> refund(
      $grpc.ServiceCall call, $0.RefundRedPacketReq request);
  $async.Future<$0.RefundRedPacketResultResp> refundResult(
      $grpc.ServiceCall call, $0.RefundRedPacketResultReq request);
}
