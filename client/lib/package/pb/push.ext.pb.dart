///
//  Generated code. Do not modify.
//  source: push.ext.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import 'logic.ext.pb.dart' as $2;

import 'connect.ext.pbenum.dart' as $4;

export 'push.ext.pbenum.dart';

class AddFriendPush extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AddFriendPush', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'friendId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'nickname')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'avatarUrl')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'description')
    ..hasRequiredFields = false
  ;

  AddFriendPush._() : super();
  factory AddFriendPush({
    $fixnum.Int64? friendId,
    $core.String? nickname,
    $core.String? avatarUrl,
    $core.String? description,
  }) {
    final _result = create();
    if (friendId != null) {
      _result.friendId = friendId;
    }
    if (nickname != null) {
      _result.nickname = nickname;
    }
    if (avatarUrl != null) {
      _result.avatarUrl = avatarUrl;
    }
    if (description != null) {
      _result.description = description;
    }
    return _result;
  }
  factory AddFriendPush.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AddFriendPush.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AddFriendPush clone() => AddFriendPush()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AddFriendPush copyWith(void Function(AddFriendPush) updates) => super.copyWith((message) => updates(message as AddFriendPush)) as AddFriendPush; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AddFriendPush create() => AddFriendPush._();
  AddFriendPush createEmptyInstance() => create();
  static $pb.PbList<AddFriendPush> createRepeated() => $pb.PbList<AddFriendPush>();
  @$core.pragma('dart2js:noInline')
  static AddFriendPush getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AddFriendPush>(create);
  static AddFriendPush? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get friendId => $_getI64(0);
  @$pb.TagNumber(1)
  set friendId($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasFriendId() => $_has(0);
  @$pb.TagNumber(1)
  void clearFriendId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get nickname => $_getSZ(1);
  @$pb.TagNumber(2)
  set nickname($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasNickname() => $_has(1);
  @$pb.TagNumber(2)
  void clearNickname() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get avatarUrl => $_getSZ(2);
  @$pb.TagNumber(3)
  set avatarUrl($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasAvatarUrl() => $_has(2);
  @$pb.TagNumber(3)
  void clearAvatarUrl() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get description => $_getSZ(3);
  @$pb.TagNumber(4)
  set description($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasDescription() => $_has(3);
  @$pb.TagNumber(4)
  void clearDescription() => clearField(4);
}

class AgreeAddFriendPush extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AgreeAddFriendPush', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'friendId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'nickname')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'avatarUrl')
    ..hasRequiredFields = false
  ;

  AgreeAddFriendPush._() : super();
  factory AgreeAddFriendPush({
    $fixnum.Int64? friendId,
    $core.String? nickname,
    $core.String? avatarUrl,
  }) {
    final _result = create();
    if (friendId != null) {
      _result.friendId = friendId;
    }
    if (nickname != null) {
      _result.nickname = nickname;
    }
    if (avatarUrl != null) {
      _result.avatarUrl = avatarUrl;
    }
    return _result;
  }
  factory AgreeAddFriendPush.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AgreeAddFriendPush.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AgreeAddFriendPush clone() => AgreeAddFriendPush()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AgreeAddFriendPush copyWith(void Function(AgreeAddFriendPush) updates) => super.copyWith((message) => updates(message as AgreeAddFriendPush)) as AgreeAddFriendPush; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AgreeAddFriendPush create() => AgreeAddFriendPush._();
  AgreeAddFriendPush createEmptyInstance() => create();
  static $pb.PbList<AgreeAddFriendPush> createRepeated() => $pb.PbList<AgreeAddFriendPush>();
  @$core.pragma('dart2js:noInline')
  static AgreeAddFriendPush getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AgreeAddFriendPush>(create);
  static AgreeAddFriendPush? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get friendId => $_getI64(0);
  @$pb.TagNumber(1)
  set friendId($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasFriendId() => $_has(0);
  @$pb.TagNumber(1)
  void clearFriendId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get nickname => $_getSZ(1);
  @$pb.TagNumber(2)
  set nickname($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasNickname() => $_has(1);
  @$pb.TagNumber(2)
  void clearNickname() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get avatarUrl => $_getSZ(2);
  @$pb.TagNumber(3)
  set avatarUrl($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasAvatarUrl() => $_has(2);
  @$pb.TagNumber(3)
  void clearAvatarUrl() => clearField(3);
}

class UpdateGroupPush extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'UpdateGroupPush', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'optId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'optName')
    ..aInt64(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'groupId')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'avatarUrl')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'introduction')
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'extra')
    ..hasRequiredFields = false
  ;

  UpdateGroupPush._() : super();
  factory UpdateGroupPush({
    $fixnum.Int64? optId,
    $core.String? optName,
    $fixnum.Int64? groupId,
    $core.String? name,
    $core.String? avatarUrl,
    $core.String? introduction,
    $core.String? extra,
  }) {
    final _result = create();
    if (optId != null) {
      _result.optId = optId;
    }
    if (optName != null) {
      _result.optName = optName;
    }
    if (groupId != null) {
      _result.groupId = groupId;
    }
    if (name != null) {
      _result.name = name;
    }
    if (avatarUrl != null) {
      _result.avatarUrl = avatarUrl;
    }
    if (introduction != null) {
      _result.introduction = introduction;
    }
    if (extra != null) {
      _result.extra = extra;
    }
    return _result;
  }
  factory UpdateGroupPush.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory UpdateGroupPush.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  UpdateGroupPush clone() => UpdateGroupPush()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  UpdateGroupPush copyWith(void Function(UpdateGroupPush) updates) => super.copyWith((message) => updates(message as UpdateGroupPush)) as UpdateGroupPush; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static UpdateGroupPush create() => UpdateGroupPush._();
  UpdateGroupPush createEmptyInstance() => create();
  static $pb.PbList<UpdateGroupPush> createRepeated() => $pb.PbList<UpdateGroupPush>();
  @$core.pragma('dart2js:noInline')
  static UpdateGroupPush getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<UpdateGroupPush>(create);
  static UpdateGroupPush? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get optId => $_getI64(0);
  @$pb.TagNumber(1)
  set optId($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasOptId() => $_has(0);
  @$pb.TagNumber(1)
  void clearOptId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get optName => $_getSZ(1);
  @$pb.TagNumber(2)
  set optName($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasOptName() => $_has(1);
  @$pb.TagNumber(2)
  void clearOptName() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get groupId => $_getI64(2);
  @$pb.TagNumber(3)
  set groupId($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasGroupId() => $_has(2);
  @$pb.TagNumber(3)
  void clearGroupId() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get name => $_getSZ(3);
  @$pb.TagNumber(4)
  set name($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasName() => $_has(3);
  @$pb.TagNumber(4)
  void clearName() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get avatarUrl => $_getSZ(4);
  @$pb.TagNumber(5)
  set avatarUrl($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasAvatarUrl() => $_has(4);
  @$pb.TagNumber(5)
  void clearAvatarUrl() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get introduction => $_getSZ(5);
  @$pb.TagNumber(6)
  set introduction($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasIntroduction() => $_has(5);
  @$pb.TagNumber(6)
  void clearIntroduction() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get extra => $_getSZ(6);
  @$pb.TagNumber(7)
  set extra($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasExtra() => $_has(6);
  @$pb.TagNumber(7)
  void clearExtra() => clearField(7);
}

class AddGroupMembersPush extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AddGroupMembersPush', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'optId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'optName')
    ..aInt64(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'groupId')
    ..pc<$2.GroupMember>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'members', $pb.PbFieldType.PM, subBuilder: $2.GroupMember.create)
    ..hasRequiredFields = false
  ;

  AddGroupMembersPush._() : super();
  factory AddGroupMembersPush({
    $fixnum.Int64? optId,
    $core.String? optName,
    $fixnum.Int64? groupId,
    $core.Iterable<$2.GroupMember>? members,
  }) {
    final _result = create();
    if (optId != null) {
      _result.optId = optId;
    }
    if (optName != null) {
      _result.optName = optName;
    }
    if (groupId != null) {
      _result.groupId = groupId;
    }
    if (members != null) {
      _result.members.addAll(members);
    }
    return _result;
  }
  factory AddGroupMembersPush.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AddGroupMembersPush.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AddGroupMembersPush clone() => AddGroupMembersPush()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AddGroupMembersPush copyWith(void Function(AddGroupMembersPush) updates) => super.copyWith((message) => updates(message as AddGroupMembersPush)) as AddGroupMembersPush; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AddGroupMembersPush create() => AddGroupMembersPush._();
  AddGroupMembersPush createEmptyInstance() => create();
  static $pb.PbList<AddGroupMembersPush> createRepeated() => $pb.PbList<AddGroupMembersPush>();
  @$core.pragma('dart2js:noInline')
  static AddGroupMembersPush getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AddGroupMembersPush>(create);
  static AddGroupMembersPush? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get optId => $_getI64(0);
  @$pb.TagNumber(1)
  set optId($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasOptId() => $_has(0);
  @$pb.TagNumber(1)
  void clearOptId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get optName => $_getSZ(1);
  @$pb.TagNumber(2)
  set optName($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasOptName() => $_has(1);
  @$pb.TagNumber(2)
  void clearOptName() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get groupId => $_getI64(2);
  @$pb.TagNumber(3)
  set groupId($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasGroupId() => $_has(2);
  @$pb.TagNumber(3)
  void clearGroupId() => clearField(3);

  @$pb.TagNumber(4)
  $core.List<$2.GroupMember> get members => $_getList(3);
}

class RemoveGroupMemberPush extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RemoveGroupMemberPush', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'optId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'optName')
    ..aInt64(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'groupId')
    ..aInt64(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'deletedUserId')
    ..hasRequiredFields = false
  ;

  RemoveGroupMemberPush._() : super();
  factory RemoveGroupMemberPush({
    $fixnum.Int64? optId,
    $core.String? optName,
    $fixnum.Int64? groupId,
    $fixnum.Int64? deletedUserId,
  }) {
    final _result = create();
    if (optId != null) {
      _result.optId = optId;
    }
    if (optName != null) {
      _result.optName = optName;
    }
    if (groupId != null) {
      _result.groupId = groupId;
    }
    if (deletedUserId != null) {
      _result.deletedUserId = deletedUserId;
    }
    return _result;
  }
  factory RemoveGroupMemberPush.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RemoveGroupMemberPush.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RemoveGroupMemberPush clone() => RemoveGroupMemberPush()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RemoveGroupMemberPush copyWith(void Function(RemoveGroupMemberPush) updates) => super.copyWith((message) => updates(message as RemoveGroupMemberPush)) as RemoveGroupMemberPush; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RemoveGroupMemberPush create() => RemoveGroupMemberPush._();
  RemoveGroupMemberPush createEmptyInstance() => create();
  static $pb.PbList<RemoveGroupMemberPush> createRepeated() => $pb.PbList<RemoveGroupMemberPush>();
  @$core.pragma('dart2js:noInline')
  static RemoveGroupMemberPush getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RemoveGroupMemberPush>(create);
  static RemoveGroupMemberPush? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get optId => $_getI64(0);
  @$pb.TagNumber(1)
  set optId($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasOptId() => $_has(0);
  @$pb.TagNumber(1)
  void clearOptId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get optName => $_getSZ(1);
  @$pb.TagNumber(2)
  set optName($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasOptName() => $_has(1);
  @$pb.TagNumber(2)
  void clearOptName() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get groupId => $_getI64(2);
  @$pb.TagNumber(3)
  set groupId($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasGroupId() => $_has(2);
  @$pb.TagNumber(3)
  void clearGroupId() => clearField(3);

  @$pb.TagNumber(4)
  $fixnum.Int64 get deletedUserId => $_getI64(3);
  @$pb.TagNumber(4)
  set deletedUserId($fixnum.Int64 v) { $_setInt64(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasDeletedUserId() => $_has(3);
  @$pb.TagNumber(4)
  void clearDeletedUserId() => clearField(4);
}

class CancelMessagePush extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'CancelMessagePush', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..aInt64(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'optId')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'optName')
    ..e<$4.ReceiverType>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'receiverType', $pb.PbFieldType.OE, defaultOrMaker: $4.ReceiverType.RT_UNKNOWN, valueOf: $4.ReceiverType.valueOf, enumValues: $4.ReceiverType.values)
    ..aInt64(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'receiverId')
    ..aInt64(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'seqId')
    ..hasRequiredFields = false
  ;

  CancelMessagePush._() : super();
  factory CancelMessagePush({
    $fixnum.Int64? optId,
    $core.String? optName,
    $4.ReceiverType? receiverType,
    $fixnum.Int64? receiverId,
    $fixnum.Int64? seqId,
  }) {
    final _result = create();
    if (optId != null) {
      _result.optId = optId;
    }
    if (optName != null) {
      _result.optName = optName;
    }
    if (receiverType != null) {
      _result.receiverType = receiverType;
    }
    if (receiverId != null) {
      _result.receiverId = receiverId;
    }
    if (seqId != null) {
      _result.seqId = seqId;
    }
    return _result;
  }
  factory CancelMessagePush.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CancelMessagePush.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  CancelMessagePush clone() => CancelMessagePush()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  CancelMessagePush copyWith(void Function(CancelMessagePush) updates) => super.copyWith((message) => updates(message as CancelMessagePush)) as CancelMessagePush; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CancelMessagePush create() => CancelMessagePush._();
  CancelMessagePush createEmptyInstance() => create();
  static $pb.PbList<CancelMessagePush> createRepeated() => $pb.PbList<CancelMessagePush>();
  @$core.pragma('dart2js:noInline')
  static CancelMessagePush getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CancelMessagePush>(create);
  static CancelMessagePush? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get optId => $_getI64(0);
  @$pb.TagNumber(1)
  set optId($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasOptId() => $_has(0);
  @$pb.TagNumber(1)
  void clearOptId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get optName => $_getSZ(1);
  @$pb.TagNumber(2)
  set optName($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasOptName() => $_has(1);
  @$pb.TagNumber(2)
  void clearOptName() => clearField(2);

  @$pb.TagNumber(3)
  $4.ReceiverType get receiverType => $_getN(2);
  @$pb.TagNumber(3)
  set receiverType($4.ReceiverType v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasReceiverType() => $_has(2);
  @$pb.TagNumber(3)
  void clearReceiverType() => clearField(3);

  @$pb.TagNumber(4)
  $fixnum.Int64 get receiverId => $_getI64(3);
  @$pb.TagNumber(4)
  set receiverId($fixnum.Int64 v) { $_setInt64(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasReceiverId() => $_has(3);
  @$pb.TagNumber(4)
  void clearReceiverId() => clearField(4);

  @$pb.TagNumber(5)
  $fixnum.Int64 get seqId => $_getI64(4);
  @$pb.TagNumber(5)
  set seqId($fixnum.Int64 v) { $_setInt64(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasSeqId() => $_has(4);
  @$pb.TagNumber(5)
  void clearSeqId() => clearField(5);
}

class RefundRedPacketPush extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'RefundRedPacketPush', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'pb'), createEmptyInstance: create)
    ..e<$4.ReceiverType>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'receiverType', $pb.PbFieldType.OE, defaultOrMaker: $4.ReceiverType.RT_UNKNOWN, valueOf: $4.ReceiverType.valueOf, enumValues: $4.ReceiverType.values)
    ..aInt64(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'receiverId')
    ..aInt64(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'userId')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'nick')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'rid')
    ..aInt64(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'money')
    ..hasRequiredFields = false
  ;

  RefundRedPacketPush._() : super();
  factory RefundRedPacketPush({
    $4.ReceiverType? receiverType,
    $fixnum.Int64? receiverId,
    $fixnum.Int64? userId,
    $core.String? nick,
    $core.String? rid,
    $fixnum.Int64? money,
  }) {
    final _result = create();
    if (receiverType != null) {
      _result.receiverType = receiverType;
    }
    if (receiverId != null) {
      _result.receiverId = receiverId;
    }
    if (userId != null) {
      _result.userId = userId;
    }
    if (nick != null) {
      _result.nick = nick;
    }
    if (rid != null) {
      _result.rid = rid;
    }
    if (money != null) {
      _result.money = money;
    }
    return _result;
  }
  factory RefundRedPacketPush.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RefundRedPacketPush.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  RefundRedPacketPush clone() => RefundRedPacketPush()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  RefundRedPacketPush copyWith(void Function(RefundRedPacketPush) updates) => super.copyWith((message) => updates(message as RefundRedPacketPush)) as RefundRedPacketPush; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RefundRedPacketPush create() => RefundRedPacketPush._();
  RefundRedPacketPush createEmptyInstance() => create();
  static $pb.PbList<RefundRedPacketPush> createRepeated() => $pb.PbList<RefundRedPacketPush>();
  @$core.pragma('dart2js:noInline')
  static RefundRedPacketPush getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RefundRedPacketPush>(create);
  static RefundRedPacketPush? _defaultInstance;

  @$pb.TagNumber(1)
  $4.ReceiverType get receiverType => $_getN(0);
  @$pb.TagNumber(1)
  set receiverType($4.ReceiverType v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasReceiverType() => $_has(0);
  @$pb.TagNumber(1)
  void clearReceiverType() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get receiverId => $_getI64(1);
  @$pb.TagNumber(2)
  set receiverId($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasReceiverId() => $_has(1);
  @$pb.TagNumber(2)
  void clearReceiverId() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get userId => $_getI64(2);
  @$pb.TagNumber(3)
  set userId($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasUserId() => $_has(2);
  @$pb.TagNumber(3)
  void clearUserId() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get nick => $_getSZ(3);
  @$pb.TagNumber(4)
  set nick($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasNick() => $_has(3);
  @$pb.TagNumber(4)
  void clearNick() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get rid => $_getSZ(4);
  @$pb.TagNumber(5)
  set rid($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasRid() => $_has(4);
  @$pb.TagNumber(5)
  void clearRid() => clearField(5);

  @$pb.TagNumber(6)
  $fixnum.Int64 get money => $_getI64(5);
  @$pb.TagNumber(6)
  set money($fixnum.Int64 v) { $_setInt64(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasMoney() => $_has(5);
  @$pb.TagNumber(6)
  void clearMoney() => clearField(6);
}

