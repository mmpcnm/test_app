import 'package:shared_preferences/shared_preferences.dart';

class Prefers {
  static void init() async {
    _instatnce._loadData();
  }

  static void setInt(String key, int value) => _instatnce._setString(key, value.toString());

  static int getInt(String key, {int defaultValue = 0}) {
    String? value = _instatnce._getString(key);
    if (value != null) {
      return int.tryParse(value) ?? defaultValue;
    }
    return defaultValue;
  }

  static void setDouble(String key, double value) => _instatnce._setString(key, value.toString());
  static double getDouble(String key, {double defaultValue = 0}) {
    String? value = _instatnce._getString(key);
    if (value != null) {
      return double.tryParse(value) ?? defaultValue;
    }
    return defaultValue;
  }

  static void setBool(String key, bool value) => _instatnce._setString(key, value.toString());

  static bool getBool(String key, {bool defaultValue = false}) {
    String? value = _instatnce._getString(key);
    if (value != null) {
      return value == 'true';
    }
    return defaultValue;
  }

  static void setString(String key, String value) => _instatnce._setString(key, value);

  static String getString(String key, {String defaultValue = ''}) {
    return _instatnce._getString(key) ?? defaultValue;
  }

  static void remove(String key) async {
    _instatnce._remove(key);
  }

  void _loadData() async {
    if (_isLoad) return;
    _isLoad = true;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Set<String> keys = prefs.getKeys();
    print('DataManager keys ${keys}');
    for (var element in keys) {
      String? value = prefs.getString(element);
      print('DataManager key $element, value $value');
      if (value != null && value.isNotEmpty) _saveDatas[element] = value;
    }
    print('DataManager ${_saveDatas}');
  }

  //私有
  static final Prefers _instatnce = Prefers();
  final Map<String, String> _saveDatas = {};
  bool _isLoad = false;

  String? _getString(String key) {
    if (_saveDatas.containsKey(key)) {
      return _saveDatas[key];
    } else {
      return null;
    }
  }

  void _setString(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
    _saveDatas[key] = value;
  }

  void _remove(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
    if (_saveDatas.containsKey(key)) {
      _saveDatas.remove(key);
    }
  }
}
