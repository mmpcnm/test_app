import 'dart:io';

// import 'package:fim_app/package/util/res_manager.dart';
import 'package:path_provider/path_provider.dart';

class CCacheManager {
  static Future<double> loadApplicationCache() async {
    //获取文件夹
    // Directory docDirectory = await getApplicationDocumentsDirectory();
    Directory tempDirectory = await getTemporaryDirectory();
    // Directory tempDirectory = Directory(Res.tempPath);
    double size = 0;

    // if (docDirectory.existsSync()) {
    //   size += await getTotalSizeOfFilesInDir(docDirectory);
    // }
    if (tempDirectory.existsSync()) {
      size += await getTotalSizeOfFilesInDir(tempDirectory);
    }
    return size;
  }

  static Future<void> emptyCache() async {
    _deleteDirFiles(await getTemporaryDirectory());
  }

  static Future<void> _deleteDirFiles(FileSystemEntity file) async {
    if (file is File) {
      await file.delete();
    } else if (file is Directory) {
      final List<FileSystemEntity> children = file.listSync();
      for (final FileSystemEntity child in children) {
        await _deleteDirFiles(child);
        // await child.delete();
      }
    }
  }

  static Future<double> getTotalSizeOfFilesInDir(final FileSystemEntity file) async {
    if (file is File) {
      int length = await file.length();
      return double.parse(length.toString());
    }
    if (file is Directory) {
      final List<FileSystemEntity> children = file.listSync();
      double total = 0;
      for (final FileSystemEntity child in children) total += await getTotalSizeOfFilesInDir(child);
      return total;
    }
    return 0;
  }

  static String formatSize(double value) {
    if (value == 0) {
      return '0MB';
    }
    List<String> unitArr = ['B', 'KB', 'MB', 'GB'];
    int index = 0;
    while (value > 1024) {
      index++;
      value = value / 1024;
    }
    String size = value.toStringAsFixed(2);
    return size + unitArr[index];
  }
}
