// ignore_for_file: non_constant_identifier_names, constant_identifier_names

import 'package:flutter/material.dart';

double SUT_W(num width) => width.sp;

double SUT_H(num height) => height.sp;

double SUT_SP(num fontSize) => fontSize.sp;
double get SUT_SCALE => ScreenUtil.scale;

///ScreenWidth 设备
double get SUT_S_WIDTH => ScreenUtil.screenWidth;

///ScreenHeight 设备
double get SUT_S_HEIGHT => ScreenUtil.screenHeight;

///设计尺寸
double get SUT_WIDTH => ScreenUtil.width;
double get SUT_HEIGHT => ScreenUtil.height;

enum ScreenDirection { HORIZONTAL, VERTICAL }

class ScreenUtil {
  static Widget build(
    BuildContext context, {
    double width = 750,
    double height = 1334,
    ScreenDirection direction = ScreenDirection.VERTICAL,
    required Widget Function() build,
  }) {
    MediaQueryData mediaData = MediaQuery.of(context);
    _instance._mediaQueryData = mediaData;
    //物理像素比例
    _instance._pixelRatio = mediaData.devicePixelRatio;
    _instance._screenWidth = mediaData.size.width;
    _instance._screenHeight = mediaData.size.height;
    _instance._statusBarHeight = mediaData.padding.top;
    _instance._bottomBarHeight = mediaData.padding.bottom;
    _instance._textScaleFactor = mediaData.textScaleFactor;

    _instance._width = width;
    _instance._height = _instance._screenHeight / _instance._screenWidth * _instance._width;
    _instance._Init();
    return build.call();
  }

  void _Init() {
    print(
        'width:$_width, height:$_height, pixelRatio:$_pixelRatio, screenWidth:$_screenWidth, screenHeight:$_screenHeight, statusBarHeight$_statusBarHeight, bottomBarHeight$_bottomBarHeight, textScaleFactor$_textScaleFactor');
  }

  static MediaQueryData get mediaQueryData => _instance._mediaQueryData;

  static double get width => _instance._width;
  static double get height => _instance._height;

  //每个逻辑像素的字体像素数，字体缩放比例
  static double get textScaleFactor => _instance._textScaleFactor;

  //设备的像素密度
  static double get pixelRatio => _instance._pixelRatio;

  //当前设备宽度 dp
  static double get screenWidth => _instance._screenWidth;

  //当前设备高度 dp
  static double get screenHeight => _instance._screenHeight;

  //当前设备宽度 px
  static double get screenWidthPx => _instance._screenWidth * _instance._pixelRatio;

  //当前设备高度 px
  static double get screenHeightPx => _instance._screenHeight * _instance._pixelRatio;

  //状态栏高度 刘海屏会更高
  static double get statusBarHeight => _instance._statusBarHeight; // * _instance._pixelRatio;

  //底部安全区距离
  static double get bottomBarHeight => _instance._bottomBarHeight; // * _instance._pixelRatio;

  //实际的dp与设计搞px的比例
  static double get scaleWidth => _instance._screenWidth / _instance._width;
  static double get scaleHeight => scaleWidth; //_instance._screenHeight / _instance._height;

  // static double get scaleText => min(scaleWidth, scaleHeight);

  static double get scale => scaleWidth;

  //根据设计搞的设备宽度适配
  static double toWidth(num value) => value * scaleWidth;

  //根据设计稿的设备高度适配
  static double toHeight(num value) => value * scaleHeight;

  static double toSP(num fontSize) => scaleWidth * fontSize;

  static final ScreenUtil _instance = ScreenUtil();

  double _width = 750;
  double _height = 1334;

  late MediaQueryData _mediaQueryData;
  double _screenWidth = 750;
  double _screenHeight = 1334;
  double _pixelRatio = 1;
  double _statusBarHeight = 0;

  double _bottomBarHeight = 0;
  double _textScaleFactor = 1;
}

extension NumExtension on num {
  ///宽度
  double get w => sp;

  ///高度
  double get h => sp;

  ///字体大小适配
  double get sp => ScreenUtil.toSP(this);

  ///屏幕缩放系数
  double get scl => ScreenUtil.scale * this;
}
