import 'dart:async';

import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:sprintf/sprintf.dart';

import 'package:intl/intl.dart';

class TimeUtils extends Model {
  static TimeUtils instance = TimeUtils();

  late int _serverTime;
  late int _localTime;

  TimeUtils() {
    Timer(const Duration(seconds: 5), _updateTime);
    _localTime = DateTime.now().millisecondsSinceEpoch;
    _serverTime = _localTime;
  }

  ///记录服务器时间
  static set serverTime(int value) {
    instance._localTime = DateTime.now().millisecondsSinceEpoch;
    instance._serverTime = value;
  }

  ///当前服务器时间
  static int get serverTime {
    int dTime = DateTime.now().millisecondsSinceEpoch - instance._localTime;
    int sTime = instance._serverTime + dTime;
    return sTime;
  }

  static DateTime get serverDate {
    return DateTime.fromMillisecondsSinceEpoch(serverTime);
  }

  static int difference() {
    int dTime = instance._localTime - instance._serverTime;
    return dTime;
  }

  static int lToSeverTime(int value) {
    return value - difference();
  }

  static DateTime lToSeverDate(int value) {
    int time = lToSeverTime(value);
    DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(time);
    return dateTime;
  }

  ///服务器时间转换到当前设备时间
  static int sToLocalTime(int systemTime) {
    int localTime = systemTime + difference();
    return localTime;
  }

  static DateTime sToLocalDate(int systemTime) {
    int localTime = sToLocalTime(systemTime);
    DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(localTime);
    return dateTime;
  }

  //时间数据格式转换
  /// 1、当天的消息= null;一小时以内= null;显示分钟;                             格式 1  30分钟前(如果小于1分钟= null;显示刚刚)
  /// 2、当天的消息= null;超出一小时= null;显示上午或下午 + 收发消息的时间;         格式 2  下午 13:54
  /// 3、消息超过一天= null;小于两天= null;昨天 + 显示上午或下午 + 收发消息的时间;   格式 3  昨天 下午17:54
  /// 4、消息超过一天= null;小于一周= null;周x + 显示上午或下午 + 收发消息的时间;    格式 4  周日 下午17:54    不建议使用
  /// 5、超过一年= null;年月日 + 显示上午或下午 + 收发消息的时间;             格式 5  2020年06月01日 下午17:54
  /// 6、其他样式:月日 + 显示上午或下午 + 收发消息的时间;               格式 6  6月01日 下午17:54

  static String chatTime(int time, {bool isSecond = false}) {
    var sTime = DateTime.fromMillisecondsSinceEpoch(time).toLocal();
    var lTime = DateTime.now();

    // int sTime = sToLocalTime(time);
    // DateTime sTime = DateTime.fromMillisecondsSinceEpoch(sTime);
    // DateTime lTime = DateTime.now();
    // int minute = isSecond ? sTime.minute : sTime.minute ~/ 5 * 5;
    // int minute = sTime.minute;
    // String retTime = '';
    String newPattern = 'yyyy年MM月dd日 HH:mm:ss';
    //yyyy年MM月dd日 HH:mm:ss'
    if (sTime.day == lTime.day && sTime.month == lTime.month && sTime.year == lTime.year) {
      //今天
      // if (updateTime.hour == localTime.hour) {
      //   // if (updateTime.minute == localTime.minute) {
      //   //   int second = max(1, localTime.second - updateTime.second);
      //   //   return sprintf('%d秒前', [second]);
      //   // } else {
      //   return sprintf('%d:%02d', [updateTime.minute, updateTime.second]);
      //   // }
      // }
      // retTime = sprintf('今天 %d:%02d', [sTime.hour, minute]);
      newPattern = '今天 HH:mm';
    } else if (sTime.month == lTime.month && sTime.year == lTime.year) {
      int day = lTime.day - sTime.day;
      if (day == 1) {
        //昨天
        // retTime = sprintf('${STR(10187)} %d:%02d', [sTime.hour, minute]);
        newPattern = '${STR(10187)} HH:mm';
      } else {
        //本月
        // retTime = sprintf('%d月%02d日 %d:%02d', [sTime.month, sTime.day, sTime.hour, minute]);
        newPattern = 'M月dd日 HH:mm';
      }
      // return sprintf('昨天 %d:%02d:%02d', [updateTime.day, updateTime.hour, updateTime.minute, updateTime.second]);
    } else if (sTime.year == lTime.year) {
      //今年
      // retTime = sprintf('%d月%02d日 %02d:%02d', [sTime.month, sTime.day, sTime.hour, minute]);
      newPattern = 'MM月dd日 HH:mm';
    } else {
      // retTime = sprintf(
      //     '%d年%02d月%02d日 %02d:%02d', [sTime.year, sTime.month, sTime.day, sTime.hour, minute]);
      newPattern = 'yyyy年MM月dd日 HH:mm';
    }

    if (isSecond) {
      newPattern += ':ss';
    }
    return DateFormat(newPattern).format(sTime);

    // return isSecond ? sprintf('%s:%02d', [retTime, sTime.second]) : retTime;
  }

  static String timeToString(int serverTime) {
    int sTime = sToLocalTime(serverTime);
    DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(sTime);
    return sprintf('%d-%02d-%02d %02d:%02d',
        [dateTime.year, dateTime.month, dateTime.day, dateTime.hour, dateTime.minute]);
  }

  ///时间计时器没5秒更新一次
  void _updateTime() {
    notifyListeners();
  }
}
