import 'dart:io';

import 'package:fim_app/debug/custom_debug.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

String ResImgs(String name, {String ext = '.png'}) {
  return Res.imgs(name, ext: ext);
}

String ResImgFile(String name) {
  return Res.imgFile(name);
}

String ResEmoji(String name) {
  return Res.emoji(name);
}

String ResFiles(String name) {
  return Res.files(name);
}

String ResTemp(String name) {
  return Res.tempImgs(name);
}

String ResAudio(String name) {
  return Res.audioPath(name);
}

class Res {
  static Res _instatnce = Res();
  static Res get instatnce => _instatnce;

  String _tempPath = '';
  static String get tempPath => instatnce._tempPath;

  String _tempAudioPath = '';
  static String get tempAudio => instatnce._tempAudioPath;

  static Future initPath() async {
    Directory temDir = await getTemporaryDirectory();
    Debug.d(temDir.path);
    instatnce._tempPath = join(temDir.path, 'temp');
    Directory dir = Directory(instatnce._tempPath);
    if (!dir.existsSync()) {
      dir.createSync();
    }

    instatnce._tempAudioPath = join(temDir.path, 'Audio');
    dir = Directory(instatnce._tempAudioPath);
    if (!dir.existsSync()) {
      dir.createSync();
    }
  }

  static String imgs(String name, {String ext = '.png'}) {
    return 'assets/images/$name$ext';
  }

  static String imgFile(String file) {
    return 'assets/images/$file';
  }

  static String emoji(String name) {
    return 'assets/emoji/$name.png';
  }

  static String files(String name) {
    return 'assets/files/$name';
  }

  static String tempImgs(String name) {
    return join(instatnce._tempPath, name);
  }

  static String audioPath(String name) {
    return join(instatnce._tempAudioPath, name);
  }

  static File copyFileNameSync(File file, String newPath) {
    File newFile = File(newPath);
    if (newFile.existsSync()) {
      newFile.deleteSync();
    }

    newFile = File(newPath);

    newFile.openWrite();
    newFile.writeAsBytesSync(file.readAsBytesSync());
    return newFile;
  }
}
