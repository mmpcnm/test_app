import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/configs/im_config.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:convert' as convert;

class Utils {
  static GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  static BuildContext get context => navigatorKey.currentState!.overlay!.context;

  // static Text newText(String text, {FontWeight fontWeight = FontWeight.bold, Color? color, double? fontSize}) {
  //   double _fontSize = ScreenUtil.toSP(fontSize ??= defFontSize);
  //   Color fontColor = color ?? defFontColor;

  //   return Text(
  //     text,
  //     style: TextStyle(fontWeight: fontWeight, color: fontColor, fontSize: _fontSize),
  //   );
  // }

  //复制文本到剪切板
  static void clipboard(String text) async {
    Clipboard.setData(ClipboardData(text: text));
  }

  // static String timeStr(int milliseconds) {
  //   DateTime updateTime = DateTime.fromMillisecondsSinceEpoch(milliseconds);
  //   // print('Utils spch:${milliseconds}, updateTime: ${updateTime.toString()}');
  //   String suptime = updateTime.toString().substring(6, 16);
  //   return suptime;
  // }

  static String base64Encode(String data) {
    var content = convert.utf8.encode(data);
    var digest = convert.base64Encode(content);
    return digest;
  }

  static double moenyToDouble(Int64 money) => (money.toDouble() / 100);
}
