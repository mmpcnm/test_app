import 'dart:convert';
import 'dart:io';

import 'package:fim_app/custom/dialog/CustomDialog.dart';
import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/http/http_utils.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/model/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
// import 'package:install_plugin/install_plugin.dart';
import 'package:package_info/package_info.dart';
import 'package:path/path.dart';

class UpdateData {
  String mVersion = '';
  bool mForcedUpdate = true;
  String mAppUrl = '';
  UpdateData({this.mVersion = '', this.mAppUrl = '', this.mForcedUpdate = true});
  UpdateData.form(String jsonData) {
    init(jsonData);
  }
  Future init(String jsonData) async {
    String version = this.mVersion;
    try {
      Map mapList = await jsonDecode(jsonData);
      Map data = {};

      if (Platform.isAndroid) {
        data = mapList['Android'];
        print('Android');
      } else if (Platform.isIOS) {
        data = mapList['IOS'];
      }

      version = data['version'] ?? this.mVersion;
      mForcedUpdate = data['forcedUpdate'] ?? true;
      mAppUrl = data['app_url'] ?? '';
    } catch (e) {
      Debug.w('解析失败 $e');
    }
    mVersion = version;
  }
}

class Update {
  static Update instace = Update();
  UpdateData _mData = UpdateData();
  // String _mAppVersion = '';
  // String _mAppUrl = '';
  // bool _mIsForcedUpdate = true;

  ///检查并更新app
  Future<void> checkUpdate(BuildContext context) async {
    EasyLoading.show(status: '${STR(10256)}...', maskType: EasyLoadingMaskType.black);
    check(onSucceed: (bool isUpdate) {
      EasyLoading.dismiss();
      //有新版本
      if (isUpdate) {
        //强制更新
        if (_mData.mForcedUpdate) {
          showMessageDialog(
            context,
            title: STR(10185),
            content: STR(10258),
            barrierDismissible: false,
            onPressed: () => update(context),
          );
        } else {
          showMessageDialogEx(
            context,
            content: STR(10258),
            barrierDismissible: false,
            confirmCallback: () => update(context),
          );
        }
      } else {
        // EasyLoading.showToast(STR(10257), toastPosition: EasyLoadingToastPosition.center);
        EasyLoading.showToast('当前已是最新版本');
      }
    }, onError: (String message) {
      EasyLoading.dismiss();
      if (message.isNotEmpty)
        EasyLoading.showToast(STR(10255));
      else {
        EasyLoading.showToast('当前已是最新版本');
      }
      // showMessageDialog(context, title: STR(10185), content: STR(10255));
    });
  }

  ///检查app版本
  Future<void> check({
    required void Function(bool isUpdate) onSucceed,
    required void Function(String message) onError,
  }) async {
    //读取本地文件
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    _mData.mVersion = packageInfo.version;
    // _mData.mVersion = DeviceInfo.appVersion;
    Debug.d('app version:${_mData.mVersion}');
    File file = File(join(Res.tempPath, 'app_config.json'));
    if (file.existsSync()) {
      String readString = file.readAsStringSync();
      await _mData.init(readString);
    }

    //删除临时文件
    File tmpFile = File(join(Res.tempPath, 'app_config.tmp'));
    if (tmpFile.existsSync()) {
      tmpFile.deleteSync();
    }

    //下载远程文件
    Future.delayed(Duration(seconds: 1), () async {
      HttpDownload download = HttpDownload();
      await download.download(
        'http://192.168.2.103/app_config.json',
        save_name: 'app_config.tmp',
        onSucceed: (String path) async {
          //读取文件
          File file = File(path);
          String readString = file.readAsStringSync();

          //替换本地文件
          Res.copyFileNameSync(file, join(Res.tempPath, 'app_config.json'));
          file.deleteSync();
          // Debug.d('web appConfig: $readString');
          try {
            //解析远程文件
            UpdateData data = UpdateData();
            await data.init(readString);
            _mData.mAppUrl = data.mAppUrl;
            _mData.mForcedUpdate = data.mForcedUpdate;
            if (_mData.mForcedUpdate) _mData.mVersion = packageInfo.version;
            Debug.d('local version:${_mData.mVersion}');
            Debug.d(
                'web appConfig: {\n \t version:${data.mVersion},\n \t forcedUpdate: ${data.mForcedUpdate},\n \t app_url: ${data.mAppUrl}\n}');
            //判断本地版本是否需要更新
            if (_mData.mVersion.compareTo(data.mVersion) < 0) {
              onSucceed(true);
            } else {
              onSucceed(false);
            }
          } catch (e) {
            Debug.w('app_config.json 解析失败: $e');
            onError('-1');
          }
        },
        onError: (String message) {
          //下载远程文件失败
          Debug.w('Update check Error: $message');
          onError('');
        },
      );
    });
  }

  ///更新app
  Future<void> update(BuildContext context) async {
    if (_mData.mAppUrl.isEmpty) {
      showMessageDialog(context, title: STR(10185), content: STR(10261));
      return;
    }

    if (Platform.isAndroid) {
      String appFileName = 'newApp.apk';

      File file = File(join(Res.tempPath, appFileName));
      if (file.existsSync()) {
        file.deleteSync();
      }

      EasyLoading.show(status: '${STR(10259)}...', maskType: EasyLoadingMaskType.black);
      Future.delayed(
        Duration(seconds: 1),
        () async {
          HttpDownload download = HttpDownload();
          await download.download(
            _mData.mAppUrl,
            save_name: appFileName,
            onSucceed: (String path) {
              EasyLoading.dismiss();
              _InstallApk(context, apkPath: path);
            },
            onError: (String message) {
              EasyLoading.dismiss();
              Debug.w('Update check Error: $message');
              showMessageDialog(context, title: STR(10185), content: STR(10260));
            },
          );
        },
      );
    } else if (Platform.isIOS) {}
  }

  //安装Android包
  void _InstallApk(BuildContext context, {required String apkPath}) {
    Future.delayed(Duration(seconds: 1), () {
      // InstallPlugin.installApk(apkPath, DeviceInfo.packageName).then((String result) {
      //   Debug.d('install apk $result');
      // }).catchError((error) {
      //   Debug.w('install apk error: $error');
      //   showMessageDialog(context, title: STR(10185), content: STR(10261));
      // });
    });
  }
}
