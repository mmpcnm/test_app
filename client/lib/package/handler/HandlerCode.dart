// ignore_for_file: file_names, constant_identifier_names

enum HandlerCode {
  /// IP 更新
  DEBUG_SET_IP,

  ///聊天消息
  ///发送消息
  SEND_CHAT_MESSAGE,
  // ///发送完成
  // SEND_CHAT_END_MESSAGE,
  // ///发送失败
  // SEND_CHAT_ERROR_MESSAGE,

  ///撤回消息
  CANCEL_MESSAGE,

  ///红包退还
  REFUND_RED_MESSAGE,

  /// AT玩家
  // CHAT_AT_USER_START,

  ///上传文件
  UP_MULTIPART_FILE,
}
