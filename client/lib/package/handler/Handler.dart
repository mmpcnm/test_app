// ignore_for_file: file_names

import 'package:fim_app/package/util/time_utils.dart';

import 'HandlerCode.dart';

typedef HandleFunction = void Function(HandlerCode key, dynamic value);

class HandlerMessage {
  HandlerCode key;
  dynamic value;
  HandlerMessage({required this.key, this.value});
}

abstract class HandlerBase<T> {
  final Map<T, List<HandleFunction>> _mListenables = {};

  void claer() {
    _mListenables.clear();
  }

  void sendMessage(T key, {dynamic value}) {
    _notifyListenables(key, value);
  }

  void addListenable(T key, HandleFunction handleFunction) {
    if (_mListenables.containsKey(key)) {
      _mListenables[key]?.add(handleFunction);
    } else {
      _mListenables[key] = [handleFunction];
    }
  }

  void removeListenable(T key, HandleFunction handleFunction) {
    if (_mListenables.containsKey(key)) {
      if (_mListenables[key]!.contains(handleFunction)) {
        _mListenables[key]?.remove(handleFunction);
        if (_mListenables[key]!.isEmpty) {
          _mListenables.remove(key);
        }
      }
    }
  }

  void removeAll(T key) {
    if (_mListenables.containsKey(key)) {
      _mListenables.remove(key);
    }
  }

  void _notifyListenables(T key, dynamic value) {
    if (_mListenables.containsKey(key)) {
      var funs = List.from(_mListenables[key]!);
      for (var fun in funs) {
        try {
          if (fun != null) fun.call(key, value);
        } catch (e) {
          print('error HandlerBase Listenables not call');
        }
      }
    }
  }
}

class GlobalHandler extends HandlerBase<HandlerCode> {
  GlobalHandler._create();
  static GlobalHandler instace = GlobalHandler._create();
  factory GlobalHandler() => instace;
}

class LocalHander extends HandlerBase<String> {}
