import 'dart:convert';

import 'package:fim_app/configs/api.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/pages/chat/chat_page.dart';
import 'package:fim_app/pages/home/home_page.dart';
import 'package:fim_app/model/chat/MessageModel.dart';
import 'package:fim_app/model/user/user_setting.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:one_context/one_context.dart';

///通知类型
enum LocalNotifyChannelName {
  CHANNEL_NAME_MESSAGE,
}

class LocalNotifications {
  static final CHANNEL_NAME_MESSAGE = 'CHANNEL_NAME_MESSAGE';

  static LocalNotifications instance = LocalNotifications();
  final FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  void init() {
    var initializationSettingsAndroid = AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = new IOSInitializationSettings();

    var initializationSettings = new InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOS);

    _flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
  }

  Future onSelectNotification(String? payload) async {
    if (payload == null) {
      print('onSelectNotification payload is null !');
      return;
    }
    print('payload:$payload');
    Map<String, dynamic> jsonData = jsonDecode(payload);
    int channelType = jsonData['chanelName'];
    if (channelType == LocalNotifyChannelName.CHANNEL_NAME_MESSAGE.index) {
      // String value = jsonData['payload'] ?? '-1';
      // Int64 id = Int64.parseInt(value);
      // var model = ChatScoped.instance.getLastMessage(id);
      // if (model != null) {
      //   OneContext().push(navGK.currentContext!, pageRoute(ChatPage(mReceiverId: model.receiverId, mReceiverType: model.receiverType)));
      // }
    }
  }

  ///payload 选中通知接收参数
  Future<void> notificationNoSound(
    LocalNotifyChannelName channelType, {
    required int id,
    required String title,
    required String body,
    String? payload,
  }) async {
    String channelName = '消息通知';

    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      '${AppConfig.channelId}',
      channelName,
      channelDescription: '$channelName',
      playSound: UserSetting.isVoice,
      enableVibration: UserSetting.isShake,
      importance: Importance.max,
      priority: Priority.high,
    );

    var iOSPlatformChannelSpecifics = IOSNotificationDetails(presentSound: UserSetting.isVoice);

    var platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics, iOS: iOSPlatformChannelSpecifics);

    _flutterLocalNotificationsPlugin.show(
      id,
      title,
      body,
      platformChannelSpecifics,
      payload: '{"chanelName":${channelType.index},"payload":"$payload"}',
    );
  }

  Future<void> cancel(int id) async {
    await _flutterLocalNotificationsPlugin.cancel(id);
  }
}
