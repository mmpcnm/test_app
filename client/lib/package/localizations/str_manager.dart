// import 'package:csv/csv.dart';
// ignore_for_file: non_constant_identifier_names

import 'package:fim_app/package/localizations/string_xls.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:sprintf/sprintf.dart';

String STR(int key, [List? args]) {
  String ret = STRManager.instance.getStr(key);
  if (args != null) {
    return sprintf(ret, args);
  } else {
    return ret;
  }
}

// String STR(BuildContext context, int key) {
//   return STRManager.STR( key);
// }

class STRManager extends Model {
  static STRManager instance = STRManager();
  String _languageCode = "zh";
  static void set languageCode(String value) => instance._languageCode = value;
  static String get languageCode => instance._languageCode;

  bool _isLoad = false;
  // final Map<String, Map<int, String>> _strings = {};

  static Future<bool> initConfig() async {
    return instance._initConfig();
  }

  Future<bool> _initConfig() async {
    if (_isLoad) return true;
    _isLoad = true;
    // final data = await rootBundle.loadString(ResFiles('string.csv'));
    // List<String> listStr = data.split('\n');
    // print('$listStr');
    // for (int i = 1; i < listStr.length; ++i) {
    //   String item = listStr[i];
    //   List<String> list = item.split(',');
    //   if (list.length >= 3) {
    //     int key = int.parse(list[0]);
    //     String zhStr = list[1];
    //     String enStr = list[2];
    //     _setValue('zh', key, zhStr);
    //     _setValue('en', key, enStr);
    //   }
    // }
    // // print("STRManager _strings: $_strings");
    // notifyListeners();
    return true;
  }

  String getStr(int key, {String defaultString = ''}) {
    String? value;
    switch (_languageCode) {
      case 'zh':
        value = StringXLS.getString(key, 0);
        break;
      // case 'en':
      //   value = StringSLX.instace.getString(key, 1);
      //   break;
      default:
        value = StringXLS.getString(key, 0);
    }
    // if (StringSLX.instace.getString(key, index)) Map<int, String>? strings = _strings['zh']; //_strings[_languageCode] ?? _strings['zh'];
    // if (strings != null) {
    //   return strings[key] ?? defaultString;
    // }
    return value ?? defaultString;
  }

  // _setValue(String sType, int key, String value) {
  //   var data = _strings[sType] ?? {};
  //   data[key] = value;
  //   _strings[sType] = data;
  // }
}

// extension StringExtension on String {
//   String form([List? args]) {
//     String str = STRManager.getString(this, defaultString: this);
//     if (args != null)
//       return sprintf(str, args);
//     else
//       return str;
//   }
// }

extension StrKeyExtension on int {
  String formString([List? args]) {
    String str = STRManager.instance.getStr(this, defaultString: this.toString());
    if (args != null)
      return sprintf(str, args);
    else
      return str;
  }
}

extension StringKeyExtension on String {
  // String keyForm([List? args]) {
  //   String str = STRManager.getStr(this, defaultString: this.toString());
  //   if (args != null)
  //     return sprintf(str, args);
  //   else
  //     return str;
  // }
}
