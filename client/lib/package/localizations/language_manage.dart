import 'package:flutter/material.dart';

class LanguageManage {
  String languageCode;

  LanguageManage(this.languageCode);

  static LanguageManage of(BuildContext context) {
    return Localizations.of(context, LanguageManage);
  }
}
