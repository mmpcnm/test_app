import 'dart:async';

import 'package:fim_app/package/localizations/language_manage.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';

class LanguageDelegate extends LocalizationsDelegate<LanguageManage> {
  const LanguageDelegate();
  @override
  bool isSupported(Locale locale) => ['zh', 'en'].contains(locale.languageCode);

  @override
  Future<LanguageManage> load(Locale locale) {
    print('LanguageDelegate load: $locale');
    STRManager.languageCode = locale.languageCode;
    return SynchronousFuture(LanguageManage(locale.languageCode));
  }

  @override
  bool shouldReload(covariant LocalizationsDelegate<LanguageManage> old) => false;
}
