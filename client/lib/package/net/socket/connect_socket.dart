import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:connectivity/connectivity.dart';
import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/model/ModelBase.dart';
import 'package:fim_app/package/net/socket/MainController.dart';
import 'package:fim_app/model/ConnectivityModel.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/configs/api.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:protobuf/protobuf.dart';

class ConnectSocket extends Model {
  static ConnectSocket _instance = ConnectSocket._();
  ConnectSocket._();
  factory ConnectSocket() => _instance;

  Socket? _mSocket;
  bool _isConnect = false;
  final List<int> _readBuffer = [];
  final List<List<int>> _writeBuffers = [];
  Timer? _writeTimer;
  // Timer? _netStateTimer;
  Timer? _reConnectTimer;

  ConnectionState _connectionState = ConnectionState.none;

  bool get isConnect => _isConnect;
  ConnectionState get connectionState => _connectionState;

  // void _setConnectionState(ConnectionState state) {
  //   _connectionState = state;
  //   notifyListeners();
  // }

  //链接到服务器
  connect() async {
    _mSocket = null;
    _connectionState = ConnectionState.none;
    clear();
    Debug.d('socket 开始连接 connect ip:${AppConfig.connectIp}:${AppConfig.connectPort}');
    await _connectServer(onError: (e) {
      Debug.e('socket ----连接失败 $e');
      _reConnect();
    });
  }

  void destory() async {
    Debug.d('socket 关闭释放');
    _connectionState = ConnectionState.none;
    clear();
    var pSocket = _mSocket;
    _mSocket = null;
    if (pSocket != null) pSocket.destroy();
    notifyListeners();
  }

  void clear() {
    _readBuffer.clear();
    _writeBuffers.clear();
    _isConnect = false;
    if (_writeTimer != null) {
      _writeTimer!.cancel();
      _writeTimer = null;
    }

    // if (_netStateTimer != null) {
    //   _netStateTimer?.cancel();
    //   _netStateTimer = null;
    // }

    if (_reConnectTimer != null) {
      _reConnectTimer?.cancel();
      _reConnectTimer = null;
    }
  }

  void send(pb.PackageType type, {GeneratedMessage? message, Int64? requestId}) {
    var buffer = encode(type, message: message, requestId: requestId);
    _writeBuffers.add(buffer);
  }

  List<int> encode(pb.PackageType type, {GeneratedMessage? message, Int64? requestId}) {
    // 构建输入流
    var input = pb.Input();
    input.type = type;
    if (requestId == null) {
      input.requestId = Int64(DateTime.now().microsecondsSinceEpoch);
    }
    if (message != null) {
      input.data = message.writeToBuffer();
    }

    var buffer = input.writeToBuffer();
    var length = buffer.length;

    List<int> writeBuffer = [];

    writeBuffer.add(length ~/ 256);
    writeBuffer.add(length % 256);
    writeBuffer.addAll(buffer);

    return writeBuffer;
  }

  void _runTimer() {
    _writeTimer = Timer.periodic(const Duration(milliseconds: 10), _onWriteLisent);
    // _netStateTimer = Timer.periodic(Duration(seconds: 1), _onNetConnectStateHander);
  }

  _reConnect() async {
    _mSocket = null;
    clear();
    _connectionState = ConnectionState.waiting;
    notifyListeners();
    _reConnectTimer = Timer.periodic(const Duration(seconds: 3), (timer) async {
      if (await ConnectivityModel().connectType != ConnectivityResult.none) {
        _reConnectTimer?.cancel();
        _reConnectTimer = null;
        Debug.d('socket 尝试连接到 connect ip:${AppConfig.connectIp}:${AppConfig.connectPort}');
        await _connectServer(onError: (e) {
          Debug.e('socket 2----连接失败 $e');
          _reConnectTimer = Timer.periodic(const Duration(seconds: 5), (timer) async {
            _reConnectTimer?.cancel();
            _reConnectTimer = null;
            _reConnect();
          });
          _connectionState = ConnectionState.none;
          notifyListeners();
        });
      }
    });
  }

  _connectServer({required Function onError}) async {
    await Socket.connect(AppConfig.connectIp, AppConfig.connectPort,
            timeout: const Duration(seconds: 2))
        .then(
      (s) {
        Debug.d('socket 连接成功');
        _isConnect = true;
        _connectionState = ConnectionState.none;
        _mSocket = s;
        _mSocket?.listen(_onData, onError: _onError, onDone: _onDoneHandler, cancelOnError: false);
        _runTimer();
        notifyListeners();
        MainController().onSocketConnectSucceed();
      },
      onError: onError,
    );
  }

  void _onError(error, StackTrace trace) async {
    Debug.e('捕获socket异常信息：error=$error，trace=${trace.toString()}');
    // if (AppConfig.isDebug) {
    //   EasyLoading.showError('Socket Error: $error');
    // }
  }

  void _onDoneHandler() async {
    //断线重连
    Debug.w('socket 链接断开 $_isConnect');
    if (_isConnect) {
      Debug.d('socket 断线重连');
      clear();
      _mSocket = null;
      EasyLoading.showToast('你已离线');
      Future.delayed(Duration(seconds: 1), () {
        _reConnect();
      });
      Debug.d('socket 发送断线消息');
      notifyListeners();
    }
  }

  void _onWriteLisent(Timer timer) {
    if (_writeBuffers.isNotEmpty && _isConnect && _mSocket != null) {
      List<List<int>> buffers = List.from(_writeBuffers);
      _writeBuffers.clear();
      for (var buffer in buffers) {
        _mSocket?.add(buffer);
      }
    }
  }

  // void _onNetConnectStateHander(timer) async {
  //   if (await ConnectivityModel().connectType == ConnectivityResult.none) {
  //     if (_isConnect) {
  //       // _onDoneHandler();
  //     }
  //   }
  // }

  void _onData(Uint8List list) {
    Debug.d('socket 接受到推送服数据 onData');
    _readBuffer.clear();
    _readBuffer.addAll(list);
    try {
      int headerLen = 2;
      if (_readBuffer.length < headerLen) return;
      int header1 = _readBuffer[0];
      int header2 = _readBuffer[1];
      _readBuffer.removeRange(0, headerLen);
      int bodyLength = header1 * 256 + header2;

      Debug.d(
          'readBuffer header1:$header1, header2:$header2, bodyLength:$bodyLength, bufferSize:${_readBuffer.length}');
      if (_readBuffer.length < bodyLength) {
        Debug.e(
            'error readBuffer header1:$header1, header2:$header2, bodyLength:$bodyLength, bufferSize:${_readBuffer.length}');
        // return;
      }

      var body = _readBuffer.getRange(0, bodyLength).toList();
      _readBuffer.removeRange(0, bodyLength);
      MainController().onOutputHandle(pb.Output.fromBuffer(body));
      // _outputHandle(output);
    } catch (e) {
      Debug.e('socket readBuffer error: $e');
    }
  }
}
