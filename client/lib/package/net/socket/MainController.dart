import 'dart:async';

import 'package:fim_app/configs/api.dart';
import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/package/handler/Handler.dart';
import 'package:fim_app/package/handler/HandlerCode.dart';
import 'package:fim_app/package/net/controller/logic_friend.dart';
import 'package:fim_app/package/net/controller/logic_group.dart';
import 'package:fim_app/package/net/socket/connect_socket.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/package/pb/push.ext.pb.dart' as pb;
import 'package:fim_app/package/sqflite/sqlite_new_friend.dart';
import 'package:fim_app/package/util/time_utils.dart';
import 'package:fim_app/model/chat/ChatCancelList.dart';
import 'package:fim_app/model/chat/MessageModel.dart';
import 'package:fim_app/model/device_info.dart' as model;
import 'package:fim_app/model/friend/new_friend_info.dart';
import 'package:fim_app/model/friend/new_friend_model.dart';
import 'package:fim_app/model/group/group_member_scoped.dart';
import 'package:fim_app/model/group/group_scoped.dart';
import 'package:fim_app/model/user/user_model.dart' as model;
import 'package:fim_app/model/user/user_setting.dart';
import 'package:fixnum/fixnum.dart';
import 'package:vibration/vibration.dart';

class MainController {
  static MainController instace = MainController._create();
  MainController._create() {}
  factory MainController() => instace;

  Timer? _heartTimer;

  void runHeart() {
    stopHeart();
    // 触发定时心跳
    _heartTimer = Timer.periodic(const Duration(seconds: 30), (timer) {
      if (!ConnectSocket().isConnect) return;
      Debug.d('socket 发送 心跳包 ${DateTime.now()}');
      ConnectSocket().send(pb.PackageType.PT_HEARTBEAT);
    });
  }

  void stopHeart() {
    if (_heartTimer != null) {
      _heartTimer?.cancel();
    }
    _heartTimer = null;
  }

  void onSocketConnectSucceed() {
    // 长连接登录
    // Debug.d('socket 长连接登录');
    Debug.d('socket 发送验证消息 PT_SIGN_IN');
    var input = pb.SignInInput();
    input.machineId = model.DeviceInfo.machineId;
    input.userId = model.UserModel.userId;
    input.token = model.UserModel.token;
    input.channel = AppConfig.channelId;
    // Debug.d('socket add:${input}');
    ConnectSocket().send(pb.PackageType.PT_SIGN_IN, message: input);

    runHeart();

    Debug.d('socket 发送消息同步 PT_SYNC');
    ConnectSocket()
        .send(pb.PackageType.PT_SYNC, message: pb.SyncInput(seq: model.UserModel.maxSYN));
    Debug.d('socket 发送订阅 PT_SUBSCRIBE_ROOM');
    ConnectSocket().send(pb.PackageType.PT_SUBSCRIBE_ROOM,
        message:
            pb.SubscribeRoomInput(roomId: AppConfig.channelId, seq: model.UserModel.maxRoomSYN));
    MessageModel().insertWelcomeMessage();
  }

  void _messageACK(Int64 requestId, Int64 seq) {
    var ack = pb.MessageACK();
    ack.deviceAck = seq;
    ack.receiveTime = Int64(DateTime.now().millisecondsSinceEpoch);
    ConnectSocket().send(pb.PackageType.PT_MESSAGE, message: ack, requestId: requestId);
  }

  Future onOutputHandle(pb.Output output) async {
    switch (output.type) {
      // 登录
      case pb.PackageType.PT_SIGN_IN:
        if (output.code != 0) {
          Debug.d('socket 登录验证失败 code:${output.code};message:${output.message}');
          return;
        }
        Debug.d('Socket 登录验证成功');
        pb.SignInOutput signInOutput = pb.SignInOutput.fromBuffer(output.data);
        TimeUtils.serverTime = signInOutput.systemTime.toInt();

        Debug.d('socket 发送消息同步 PT_SYNC');
        ConnectSocket()
            .send(pb.PackageType.PT_SYNC, message: pb.SyncInput(seq: model.UserModel.maxSYN));
        Debug.d('socket 发送订阅 PT_SUBSCRIBE_ROOM');
        ConnectSocket().send(pb.PackageType.PT_SUBSCRIBE_ROOM,
            message: pb.SubscribeRoomInput(roomId: AppConfig.channelId, seq: Int64.ZERO));

        break;
      // 消息同步
      case pb.PackageType.PT_SYNC:
        Debug.d('socket 接收到 PT_SYNC');
        var syncOutput = pb.SyncOutput.fromBuffer(output.data);
        if (syncOutput.messages.isEmpty) {
          Debug.w('socket syncOutput.messages.isEmpty');
          return;
        }

        // Int64 seq = Int64();
        Int64 roomSeq = Int64();
        Int64 messageSeq = Int64();

        for (var message in syncOutput.messages) {
          if (message.messageType != pb.MessageType.MT_COMMAND) {
            if (message.receiverId == AppConfig.channelId) {
              roomSeq = message.seq;
            } else {
              messageSeq = message.seq;
            }
          } else {
            messageSeq = message.seq;
          }
          _onHandleMessage(message);
        }

        if (roomSeq != 0) model.UserModel.maxRoomSYN = roomSeq;
        if (messageSeq != 0) model.UserModel.maxSYN = messageSeq;

        _messageACK(output.requestId, syncOutput.messages.last.seq);
        if (syncOutput.hasMore == true) {
          ConnectSocket().send(pb.PackageType.PT_SYNC,
              message: pb.SyncInput(seq: syncOutput.messages.last.seq));
          // _socket.flush();
        }
        break;
      // 心跳
      case pb.PackageType.PT_HEARTBEAT:
        Debug.d('socket 接收到 心跳包 PT_HEARTBEAT');
        break;
      // 消息发送
      case pb.PackageType.PT_MESSAGE:
        Debug.d('socket 接收到 PT_MESSAGE');
        var messageSend = pb.MessageSend.fromBuffer(output.data);

        _messageACK(output.requestId, messageSend.message.seq);

        if (messageSend.message.messageType != pb.MessageType.MT_COMMAND) {
          if (messageSend.message.receiverId == AppConfig.channelId) {
            model.UserModel.maxRoomSYN = messageSend.message.seq;
          } else {
            model.UserModel.maxSYN = messageSend.message.seq;
          }
        } else {
          model.UserModel.maxSYN = messageSend.message.seq;
        }
        _onHandleMessage(messageSend.message);
        break;
      case pb.PackageType.PT_SUBSCRIBE_ROOM:
        Debug.d('socket 接收到 PT_SUBSCRIBE_ROOM');
        break;
      case pb.PackageType.PT_UNKNOWN:
        Debug.d('socket 接收到 PT_UNKNOWN');
        break;
    }
  }

  Future _onHandleMessage(pb.Message msg) async {
    Debug.d('socket handleMessage : $msg');
    //推送指令
    if (msg.messageType == pb.MessageType.MT_COMMAND) {
      Debug.d('socket 推送指令 MessageType.MT_COMMAND');
      pb.Command command = pb.Command.fromBuffer(msg.messageContent);
      if (command.code == pb.PushCode.PC_ADD_FRIEND.value) {
        Debug.d('socket 推送指令 好友邀请 PushCode.PC_ADD_FRIEND');
        // 添加好友请求
        pb.AddFriendPush addFriendPush = pb.AddFriendPush.fromBuffer(command.data);
        // if (!FriendScoped.instance.isFriend(addFriendPush.friendId)) {
        NewFriendInfo newFriendModel = NewFriendInfo.formNewFriend(addFriendPush);
        if (NewFriendModel.instance.push(newFriendModel)) {
          SqliteNewFriend().insert(newFriendModel);
        }
        if (await Vibration.hasVibrator() && UserSetting.isShake) {
          Vibration.vibrate(pattern: [0, 200, 35, 150]);
        }
        // }
      } else if (command.code == pb.PushCode.PC_AGREE_ADD_FRIEND.value) {
        Debug.d('socket 推送指令 同意添加好友 PushCode.PC_AGREE_ADD_FRIEND');
        // 同意添加好友
        LogicFriend.GetFriends();
      } else if (command.code == pb.PushCode.PC_UPDATE_GROUP.value) {
        Debug.d('socket 推送指令 更新群组 PushCode.PC_UPDATE_GROUP');
        pb.UpdateGroupPush push = pb.UpdateGroupPush.fromBuffer(command.data);
        GroupScoped.instance.UPGroupDate(
          group_id: msg.receiverId,
          name: push.name,
          avatar_url: push.avatarUrl,
          introduction: push.introduction,
          extra: push.extra,
        );
      } else if (command.code == pb.PushCode.PC_ADD_GROUP_MEMBERS.value) {
        Debug.d('socket 推送指令 添加群组成员 PushCode.PC_ADD_GROUP_MEMBERS');
        pb.AddGroupMembersPush push = pb.AddGroupMembersPush.fromBuffer(command.data);
        GroupMemberScoped.instance.addMemberAll(push.groupId, push.members);
        LogicGroup.GetGroup(groupId: push.groupId);
      } else if (command.code == pb.PushCode.PC_REMOVE_GROUP_MEMBER.value) {
        Debug.d('socket 推送指令 移除群组成员 PushCode.PC_REMOVE_GROUP_MEMBER');
        pb.RemoveGroupMemberPush push = pb.RemoveGroupMemberPush.fromBuffer(command.data);
        Debug.d('socket 推送指令 移除群组成员 groupId:${push.groupId}, deletedUserId:${push.deletedUserId}');
        GroupMemberScoped.instance.delMember(groupId: push.groupId, userId: push.deletedUserId);
        if (push.deletedUserId == model.UserModel.userId) {
          MessageModel().remove(push.groupId.toString(), isSqlite: true);
          GroupScoped.instance.delGroup(push.groupId);
        } else {
          LogicGroup.GetGroup(groupId: push.groupId);
        }
      } else if (command.code == pb.PushCode.PC_CANCEL_MESSAGE.value) {
        Debug.d('socket 推送指令 撤销消息 PushCode.PC_CANCEL_MESSAGE');
        pb.CancelMessagePush push = pb.CancelMessagePush.fromBuffer(command.data);
        GlobalHandler().sendMessage(HandlerCode.CANCEL_MESSAGE, value: push);
      } else if (command.code == pb.PushCode.PC_REFUND_NOTIFY.value) {
        Debug.d('socket 推送指令 红包退还 MessageType.PC_REFUND_NOTIFY');
        pb.RefundRedPacketPush push = pb.RefundRedPacketPush.fromBuffer(command.data);
        GlobalHandler().sendMessage(HandlerCode.REFUND_RED_MESSAGE, value: push);
      }
    } else if (msg.messageType != pb.MessageType.MT_UNKNOWN) {
      Debug.d('socket 新的消息 --- status: ${msg.status}');
      MessageInfo chatModel = MessageInfo.fromMessage(msg);
      if (ChatCancelList().find(
              sender_id: chatModel.senderId,
              sender_seq: chatModel.senderSeq,
              receiverType: chatModel.receiverType) !=
          null) {
        chatModel.status = pb.MessageStatus.MS_RECALL;
      }
      GlobalHandler().sendMessage(HandlerCode.SEND_CHAT_MESSAGE, value: chatModel);
    }
  }
}
