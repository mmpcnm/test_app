// ignore_for_file: constant_identifier_names, non_constant_identifier_names

import 'dart:io';

import 'package:dio/dio.dart';
import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/package/util/utils.dart';
import 'package:path/path.dart';

///超时时间
const int CONNECT_TIMEOUT = 30000;
const int RECEIVE_TIMEOUT = 30000;
const int SEND_TIMEOUT = 10000;

class HttpUtils {
  static upMultipartFile({
    required String requestUrl,
    required String filePath,
    void Function(dynamic response)? onSucceed,
    void Function(String errorCode)? onError,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    Debug.d('upMultipartFile filePath: $filePath');
    // final name = filePath.substring(filePath.lastIndexOf("/") + 1, filePath.length);

    // var response = await Dio().post('/info', data: formData);
    Response response;
    try {
      var formData = FormData.fromMap({'file': await MultipartFile.fromFile(filePath)});
      response = await Dio().post(
        requestUrl,
        data: formData,
        cancelToken: CancelToken(),
        options: Options(sendTimeout: SEND_TIMEOUT, receiveTimeout: RECEIVE_TIMEOUT),
        onSendProgress: (count, total) {
          print('upMultipartFile onSendProgress(count: $count, total:$total)');
          onSendProgress?.call(count, total);
        },
        // onReceiveProgress: (count, total) {
        //   print('upMultipartFile onReceiveProgress(count: $count, total:$total)');
        //   onReceiveProgress?.call(count, total);
        // },
      );
    } on DioError catch (e) {
      Debug.w('upMultipartFile: ${e}');
      String message = '上传失败！';
      switch (e.type) {
        case DioErrorType.connectTimeout:
          // It occurs when url is opened timeout.
          message = '链接超时！';
          break;
        case DioErrorType.sendTimeout:
          // It occurs when url is sent timeout.
          message = '发送超时！';
          break;
        case DioErrorType.receiveTimeout:
          //It occurs when receiving timeout.
          message = '发送超时！';
          break;
        case DioErrorType.response:
          // When the server response, but with a incorrect status, such as 404, 503...
          message = '远程服务器未响应！';
          break;
        case DioErrorType.cancel:
          // When the request is cancelled, dio will throw a error with this type.
          message = '上传中断！';
          break;
        case DioErrorType.other:
          // Default error type, Some other Error. In this case, you can
          // use the DioError.error if it is not null.
          message = '上传失败！';
          break;
      }

      if (onError != null) onError(message);
      return;
    } catch (e) {
      Debug.w('upMultipartFile: ${e}');
      if (onError != null) onError('上传失败！');
      return;
    }
    try {
      Debug.d('upMultipartFile response: $response');
      if (onSucceed != null) onSucceed(response.data);
      // if (response.data['code'] == 0 && response.data['data'] != null) {

      // } else {
      //   if (onError != null) onError(response.data['message'] ?? '');
      // }
    } catch (e) {
      Debug.w('upMultipartFile: ${e}');
      if (onError != null) onError('参数解析错误！');
    }
  }

  ///打开浏览器
  Future<void> openBrowser(String url) async {}
}

class HttpDownload {
  void Function(double progress)? _onReceiveProgress;
  void Function(String path)? _onSucceed;
  void Function(String message)? _onError;
  Dio? _dio;

  ///save_path:/sdcard/package/temp/path
  ///save_name:file name.txt
  ///save_ext:.txt or .xxx ...
  Future<String?> download(
    String url, {
    String? save_path,
    String? save_name,
    String? save_ext,
    void Function(double progress)? onReceiveProgress,
    void Function(String path)? onSucceed,
    void Function(String message)? onError,
  }) async {
    _onReceiveProgress = onReceiveProgress;
    _onSucceed = onSucceed;
    _onError = onError;

    String sepath = save_path ?? Res.tempPath;
    String sename;
    if (save_name != null) {
      sename = save_name;
    } else if (save_ext != null) {
      sename = '${Utils.base64Encode(url)}$save_ext';
    } else {
      int extIndex = url.lastIndexOf('.');
      if (extIndex != -1) {
        String fileExt = url.substring(extIndex);
        sename = '${Utils.base64Encode(url)}$fileExt';
      } else {
        if (_onError != null) _onError!.call('no file ext');
        return null;
      }
    }
    String path = join(sepath, sename);
    File file = File(path);
    if (file.existsSync()) {
      //文件已存在
      Debug.d('HttpDownload exists local File');
      Future.delayed(Duration(milliseconds: 5), () {
        if (_onSucceed != null) _onSucceed!.call(path);
      });

      return path;
    } else {
      //下载文件
      Debug.d('HttpDownload exists url File');
      Response response;
      try {
        Debug.d('download url : $url');
        _dio = Dio(BaseOptions(
          connectTimeout: CONNECT_TIMEOUT,

          // 响应流上前后两次接受到数据的间隔，单位为毫秒。
          receiveTimeout: RECEIVE_TIMEOUT,

          // Http请求头.
          headers: {},
        ));
        response = await _dio!.download(url, path, onReceiveProgress: (received, total) {
          double progress = received.toDouble() / total.toDouble();
          if (_onReceiveProgress != null) _onReceiveProgress!.call(progress);
        }, cancelToken: CancelToken());
        _dio?.close();
      } on DioError catch (e) {
        Debug.w('download Error: $e');
        if (_onError != null) _onError!.call(e.message);
        return null;
      } catch (e) {
        Debug.w('download Error: $e');
        if (_onError != null) _onError!.call('download error');
        return null;
      }
      Debug.d('HttpDownload Succeed File');

      if (_onSucceed != null) _onSucceed!.call(path);
      return path;
    }
  }

  void dispose() {
    _onReceiveProgress = null;
    _onSucceed = null;
    _onError = null;
    if (_dio != null) {
      _dio?.close();
      _dio = null;
    }
  }
}
