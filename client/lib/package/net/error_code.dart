import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:grpc/grpc.dart';

class ErrorCode {
  static Map<int, String> errorMessages = {
    4: '响应超时',
  };

  static void showMessageCode({required GrpcError error, Duration? duration}) {
    if (errorMessages[error.code] != null) {
      String message = errorMessages[error.code] ?? "";
      if (duration != null) {
        Future.delayed(duration, () {
          EasyLoading.showError(message);
        });
      } else {
        EasyLoading.showError(message);
      }
      // EasyLoading.showError(message);
      // showMessage(message: message, duration:duration);
    } else {
      // showMessage(message: error.message ?? '');
      if (duration != null) {
        Future.delayed(duration, () {
          EasyLoading.showError(error.message ?? '');
        });
      } else {
        EasyLoading.showError(error.message ?? '');
      }
    }
  }

  static void showMessage({required String message, Duration? duration}) {
    if (message.isEmpty) return;
    if (duration != null) {
      Future.delayed(duration, () {
        EasyLoading.showError(message);
      });
    } else {
      EasyLoading.showError(message);
    }
  }
}
