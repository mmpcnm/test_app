import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/model/user/user_model.dart' as model;
import 'package:fim_app/package/net/controller/logic.dart';
import 'package:fim_app/package/net/error_code.dart';
import 'package:fim_app/package/pb/logic.ext.pb.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:grpc/grpc.dart';

class LogicUser {
  // 获取用户信息
  static GetUser(
      {BuildContext? context,
      required Int64 userId,
      LogicCallBack<GetUserResp>? logicCallBack}) async {
    GetUserReq req = GetUserReq(userId: userId);
    Debug.d('GetUser:{${req.toString()}}');
    GetUserResp resp;
    try {
      resp = await Logic.logicClient.getUser(req, options: Logic.options);
    } catch (e) {
      Debug.w('GetUser Error:${e}');
      if (logicCallBack != null) logicCallBack.call(-1, null);
      return;
    }
    Debug.d('GetUser :{\n${resp}}');
    if (userId == model.UserModel.userId) {
      // TimeUtils.serverTime =  resp.user.updateTime;
      model.UserModel.fromUser(resp.user);
    }
    if (logicCallBack != null) logicCallBack.call(0, resp);
  }

  // 更新用户信息
  static UpdateUser(BuildContext context,
      {String? nickname,
      int? sex,
      String? avatarUrl,
      String? extra,
      LogicCallBack<UpdateUserResp>? logicCallBack}) async {
    if (sex == null && nickname == null && avatarUrl == null && extra == null) {
      return;
    }
    UpdateUserReq request = UpdateUserReq(
      nickname: nickname ?? model.UserModel.nickName,
      sex: sex ?? model.UserModel.sex,
      avatarUrl: avatarUrl ?? model.UserModel.avatarUrl,
      extra: extra ?? model.UserModel.extra,
    );

    UpdateUserResp resp;
    try {
      resp = await Logic.logicClient.updateUser(request, options: Logic.options);
    } on GrpcError catch (e) {
      Debug.w('UpdateUser Error:${e}');
      if (logicCallBack != null) logicCallBack(-1, null);
      Future.delayed(Duration(milliseconds: 100), () {
        ErrorCode.showMessage(message: e.message ?? '');
      });
      return;
    } catch (e) {
      Debug.w('UpdateUser Error:${e}');
      if (logicCallBack != null) logicCallBack(-1, null);
      Future.delayed(Duration(milliseconds: 100), () {
        //设置失败
        ErrorCode.showMessage(message: STR(10184) + '!');
      });
      return;
    }
    Debug.d('UpdateUser :{\n${resp}}');
    if (nickname != null) {
      Debug.d('设置 nickname: $nickname');
      model.UserModel.nickName = nickname;
    }
    if (sex != null) {
      Debug.d('设置 sex: $sex');
      model.UserModel.sex = sex;
    }
    if (avatarUrl != null) {
      Debug.d('设置 avatarUrl: $avatarUrl');
      model.UserModel.avatarUrl = avatarUrl;
    }

    if (extra != null) {
      Debug.d('设置 extra: $extra');
      model.UserModel.extra = extra;
    }

    model.UserModel.notify();

    if (logicCallBack != null) logicCallBack(0, resp);
  }

  ///绑定推广人
  ///
  ///int64 promoterId 推广人id
  static BindPromoter({
    BuildContext? context,
    required Int64 promoterId,
    LogicCallBack<BindPromoterResp>? logicCallBack,
  }) async {
    var request = BindPromoterReq(promoterId: promoterId);
    Debug.d('Business BindPromoter: $request');
    BindPromoterResp resp;
    try {
      resp = await Logic.logicClient.bindPromoter(request, options: Logic.options);
    } on GrpcError catch (e) {
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      if (context != null) {
        ErrorCode.showMessage(message: e.message ?? '', duration: Duration(milliseconds: 100));
      }
      Debug.w('Business BindPromoter error: $e');
      return;
    } catch (e) {
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      if (context != null) {
        ErrorCode.showMessage(message: '绑定推广人失败', duration: Duration(milliseconds: 100));
      }
      Debug.w('Business BindPromoter error: $e');
      return;
    }
    Debug.w('Business BindPromoter 成功 !');
    EasyLoading.showSuccess('绑定推广人成功');
    if (logicCallBack != null) {
      logicCallBack(0, resp);
    }
  }
}
