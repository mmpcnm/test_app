import 'package:fim_app/configs/api.dart';
import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/model/device_info.dart' as model;
import 'package:fim_app/model/user/user_model.dart' as model;
import 'package:fim_app/package/net/controller/logic.dart';
import 'package:fim_app/package/net/error_code.dart';
import 'package:fim_app/package/pb/logic.ext.pb.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:grpc/grpc.dart';

class LogicLogin {
//注册
  static Register(BuildContext context,
      {required String account,
      required String password,
      required String nickName,
      required LogicCallBack<RegisterResp> logicCallBack}) async {
    RegisterReq registerReq = RegisterReq(
      account: account,
      password: password,
      nick: nickName,
      machineId: model.DeviceInfo.machineId,
      type: model.DeviceInfo.modelType,
      brand: model.DeviceInfo.brand,
      model: model.DeviceInfo.model,
      systemVersion: model.DeviceInfo.systemVersion,
      sdkVersion: model.DeviceInfo.sdkVersion,
      channel: AppConfig.channelId,
    );

    Debug.d(registerReq.toString());

    RegisterResp resp;
    try {
      resp = await Logic.logicClient
          .register(registerReq, options: CallOptions(timeout: Logic.timeout));
    } on GrpcError catch (e) {
      Debug.w('注册错误 error:${e}');
      logicCallBack(-1, null);
      ErrorCode.showMessageCode(error: e, duration: Duration(milliseconds: 100));
      return;
    } catch (e) {
      logicCallBack(-1, null);
      Debug.w('注册错误 error:${e}');
      //注册失败
      ErrorCode.showMessage(message: STR(10182), duration: Duration(milliseconds: 100));
      return;
    }
    Debug.d('注册完成 resp:{${resp}}');
    logicCallBack(0, resp);
  }

  //登录
  static SignIn(BuildContext context,
      {required String account,
      required String password,
      required LogicCallBack<SignInResp> logicCallBack}) async {
    SignInReq req = SignInReq(
      account: account,
      password: password,
      machineId: model.DeviceInfo.machineId,
      type: model.DeviceInfo.modelType,
      brand: model.DeviceInfo.brand,
      model: model.DeviceInfo.model,
      systemVersion: model.DeviceInfo.systemVersion,
      sdkVersion: model.DeviceInfo.sdkVersion,
      channel: AppConfig.channelId,
    );

    Debug.d(req.toString());

    SignInResp resp;
    try {
      resp = await Logic.logicClient.signIn(req, options: CallOptions(timeout: Logic.timeout));
    } on GrpcError catch (e) {
      Debug.w('登录失败${e}');
      logicCallBack(-1, null);
      ErrorCode.showMessageCode(error: e, duration: const Duration(milliseconds: 100));
      // ErrorCode.showMessage(message: e.message ?? STR(10183), duration: Duration(milliseconds: 100));
      return;
    } catch (e) {
      Debug.w('登录失败${e}');
      logicCallBack(-1, null);
      //账号或密码错误
      ErrorCode.showMessage(message: STR(10183), duration: const Duration(milliseconds: 100));

      return;
    }
    Debug.d('登录成功 resp:{${resp.toString()}}');
    model.UserModel.accountId = account;
    model.UserModel.password = password;
    model.UserModel.userId = resp.userId;
    model.UserModel.token = resp.token;
    logicCallBack(0, resp);
  }

  ///修改密码
  ///
  static ModifyPwd(
      {BuildContext? context,
      required String pwd,
      required String newPwd,
      required LogicCallBack<ModifyPwdResp>? logicCallBack}) async {
    var request = ModifyPwdReq(pwd: pwd, newPwd: newPwd);
    Debug.d('Business ModifyPwd: $request');
    ModifyPwdResp resp;
    try {
      resp = await Logic.logicClient.modifyPwd(request, options: Logic.options);
    } on GrpcError catch (e) {
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      if (context != null) {
        ErrorCode.showMessage(message: e.message ?? '', duration: Duration(milliseconds: 100));
      }
      Debug.w('Business ModifyPwd error: $e');
      return;
    } catch (e) {
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      if (context != null) {
        ErrorCode.showMessage(message: '修改登录密码失败', duration: Duration(milliseconds: 100));
      }
      Debug.w('Business ModifyPwd error: $e');
      return;
    }
    Debug.w('Business ModifyPwd 成功 !');
    EasyLoading.showSuccess('密码修改完成');
    if (logicCallBack != null) {
      logicCallBack(0, resp);
    }
  }
}
