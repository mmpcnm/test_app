// ignore_for_file: non_constant_identifier_names

import 'dart:typed_data';

import 'package:fim_app/configs/api.dart';
import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/package/handler/Handler.dart';
import 'package:fim_app/package/handler/HandlerCode.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/http/http_utils.dart';
import 'package:fim_app/model/chat/list/MessageList/MessageInfo.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:fim_app/package/net/controller/logic.dart';
import 'package:fim_app/package/net/error_code.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart';
import 'package:fim_app/package/pb/logic.ext.pb.dart';
import 'package:fim_app/package/util/time_utils.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/material.dart' as flutter;
import 'package:grpc/grpc.dart';

class ChatSendData {
  ReceiverType receiverType;
  Int64 receiverId;
  List<Int64>? toUserIds;
  MessageType messageType;
  Uint8List messageContent;
  Int64 sendTime;
  Int64 localSendTime;
  LogicSendState sendState;

  ChatSendData({
    required this.receiverType,
    required this.receiverId,
    this.toUserIds,
    required this.messageType,
    required this.messageContent,
    required this.sendTime,
    required this.localSendTime,
    required this.sendState,
  });
}

class LogicChat {
  static Int64 getSendTime() {
    return Int64(TimeUtils.serverTime);
  }

  // 发送消息
  // receiver_type:接收者类型，1：user;2:group ,receiver_id:用户id或者群组id
  // to_user_ids:需要@的用户id列表
  // message_type: 消息类型
  // message_content: 消息内容
  // send_time: 消息发送时间戳，精确到毫秒(不代表服务器接收的时间)
  // is_persist: 是否将消息持久化到数据库
  static SendMessage({
    flutter.BuildContext? context,
    required ReceiverType receiverType,
    required Int64 receiverId,
    List<Int64>? toUserIds,
    required MessageType messageType,
    required Uint8List messageContent,
    required int sendTime,
    bool isPersist = true,
    LogicCallBack<SendMessageResp>? logicCallBack,
  }) async {
    if (Logic.showNextDoneMessage()) return;
    // Int64 sendTime = Int64(TimeUtils.serverTime);
    SendMessageReq request = SendMessageReq(
      receiverType: receiverType,
      receiverId: receiverId,
      toUserIds: toUserIds,
      messageType: messageType,
      messageContent: messageContent,
      sendTime: getSendTime(),
      isPersist: isPersist,
    );

    // var buffer = request.writeToBuffer();
    Debug.d('PushRoom: $request ');

    // ChatSendData chatSendData = ChatSendData(
    //   receiverType: receiverType,
    //   receiverId: receiverId,
    //   toUserIds: toUserIds,
    //   messageType: messageType,
    //   messageContent: messageContent,
    //   sendTime: request.sendTime,
    //   localSendTime: sendTime,
    //   sendState: LogicSendState.SEND,
    // );

    MessageInfo chatSendData = MessageInfo.newModel(
      senderType: SenderType.ST_USER,
      senderId: UserModel.userId,
      avatarUrl: UserModel.avatarUrl,
      nickname: UserModel.nickName,
      extra: '',
      receiverType: receiverType,
      receiverId: receiverId,
      iReceiverId: receiverId,
      toUserIds: toUserIds ?? [],
      messageType: messageType,
      messageContent: messageContent,
      seq: Int64(sendTime),
      sendTime: request.sendTime,
      senderSeq: Int64.ZERO,
      localSendTime: sendTime.toInt(),
      msg_status: MessageStatus.MS_NORMAL,
      is_reading: true,
      sendState: LogicSendState.SEND,
    );

    bool isSendHandler = false;
    if (messageType == MessageType.MT_TEXT || messageType == MessageType.MT_IMAGE) {
      isSendHandler = true;
    }
    if (isSendHandler) {
      GlobalHandler().sendMessage(
        HandlerCode.SEND_CHAT_MESSAGE,
        value: chatSendData,
      );
    }

    SendMessageResp resp;
    try {
      resp = await Logic.logicClient.sendMessage(request, options: Logic.options);
    } on GrpcError catch (e) {
      Debug.w('SendMessage Error:${e}');
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      if (context != null) {
        ErrorCode.showMessage(
            message: e.message ?? '', duration: const Duration(milliseconds: 100));
      }
      if (isSendHandler) {
        chatSendData.sendState = LogicSendState.ERROR;
        GlobalHandler().sendMessage(
          HandlerCode.SEND_CHAT_MESSAGE,
          value: chatSendData,
        );
      }
      return;
    } catch (e) {
      Debug.w('SendMessage Error:${e}');
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      if (context != null) {
        //消息发送失败
        ErrorCode.showMessage(message: STR(10175), duration: const Duration(milliseconds: 100));
      }
      if (isSendHandler) {
        chatSendData.sendState = LogicSendState.ERROR;
        GlobalHandler().sendMessage(
          HandlerCode.SEND_CHAT_MESSAGE,
          value: chatSendData,
        );
      }

      return;
    }
    Debug.d('发送消息成功 SendMessage :{\n${resp}}');
    if (logicCallBack != null) {
      logicCallBack(0, resp);
    }
    if (isSendHandler) {
      chatSendData.sendState = LogicSendState.SUCCEED;
      // chatSendData.seq = resp.seq;
      GlobalHandler().sendMessage(
        HandlerCode.SEND_CHAT_MESSAGE,
        value: chatSendData,
      );
    }
  }

// int64 room_id = 1; // 房间id
//     MessageType message_type = 2; // 消息类型
//     bytes message_content = 3; // 消息内容
//     int64 send_time = 4; // 消息发送时间戳，精确到毫秒(不代表服务器接收的时间)
//     bool is_persist = 5; // 是否将消息持久化
  // 推送消息到房间
  static PushRoom({
    flutter.BuildContext? context,
    required Int64 roomId,
    required MessageType messageType,
    required Uint8List messageContent,
    required int sendTime,
    bool isPersist = true,
    LogicCallBack<PushRoomResp>? logicCallBack,
  }) async {
    if (Logic.showNextDoneMessage()) return;
    // Int64 sendTime = Int64(TimeUtils.serverTime);
    PushRoomReq request = PushRoomReq(
      roomId: roomId, // ?? AppConfig.channelId,
      messageType: messageType,
      messageContent: messageContent,
      sendTime: getSendTime(),
      isPersist: isPersist,
    );

    MessageInfo chatSendData = MessageInfo.newModel(
      senderType: SenderType.ST_USER,
      senderId: UserModel.userId,
      avatarUrl: UserModel.avatarUrl,
      nickname: UserModel.nickName,
      extra: '',
      receiverType: ReceiverType.RT_ROOM,
      receiverId: roomId,
      iReceiverId: roomId,
      toUserIds: [],
      messageType: messageType,
      messageContent: messageContent,
      seq: Int64(sendTime),
      sendTime: request.sendTime,
      senderSeq: Int64.ZERO,
      localSendTime: sendTime.toInt(),
      msg_status: MessageStatus.MS_NORMAL,
      is_reading: true,
      sendState: LogicSendState.SEND,
    );

    bool isSendHandler = false;
    if (messageType == MessageType.MT_TEXT || messageType == MessageType.MT_IMAGE) {
      isSendHandler = true;
    }
    if (isSendHandler) {
      GlobalHandler().sendMessage(
        HandlerCode.SEND_CHAT_MESSAGE,
        value: chatSendData,
      );
    }

    // var buffer = request.writeToBuffer();
    // Debug.d('PushRoom: size: ${buffer.length}, data:$request ');
    PushRoomResp resp;
    try {
      resp = await Logic.logicClient.pushRoom(request, options: Logic.options);
    } on GrpcError catch (e) {
      Debug.w('PushRoom Error:${e}');
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      if (context != null) {
        ErrorCode.showMessage(
            message: e.message ?? '', duration: const Duration(milliseconds: 100));
      }
      if (isSendHandler) {
        chatSendData.sendState = LogicSendState.ERROR;
        GlobalHandler().sendMessage(
          HandlerCode.SEND_CHAT_MESSAGE,
          value: chatSendData,
        );
      }
      return;
    } catch (e) {
      Debug.w('PushRoom Error:${e}');
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      if (context != null) {
        //消息发送失败
        ErrorCode.showMessage(message: STR(10175), duration: const Duration(milliseconds: 100));
      }
      if (isSendHandler) {
        chatSendData.sendState = LogicSendState.ERROR;
        GlobalHandler().sendMessage(
          HandlerCode.SEND_CHAT_MESSAGE,
          value: chatSendData,
        );
      }

      return;
    }
    Debug.d('发送消息成功 PushRoom :{\n${resp}}');
    if (logicCallBack != null) {
      logicCallBack(0, resp);
    }
    if (isSendHandler) {
      chatSendData.sendState = LogicSendState.SUCCEED;
      // chatSendData.seq = resp.seq;
      GlobalHandler().sendMessage(
        HandlerCode.SEND_CHAT_MESSAGE,
        value: chatSendData,
      );
    }
  }

  static CancelMessage({
    flutter.BuildContext? context,
    required ReceiverType receiverType,
    required Int64 receiverId,
    required Int64 seq_id,
    LogicCallBack<CancelMessageResp>? logicCallBack,
  }) async {
    if (Logic.showNextDoneMessage()) return;
    CancelMessageReq request =
        CancelMessageReq(receiverType: receiverType, receiverId: receiverId, seqId: seq_id);
    Debug.d('CancelMessage request:${request}');
    CancelMessageResp resp;
    try {
      resp = await Logic.logicClient.cancelMessage(request, options: Logic.options);
    } on GrpcError catch (e) {
      Debug.w('CancelMessage Error:${e}');
      if (context != null) {
        // ErrorCode.showMessage(context, message: e.message ?? "");
        //无法撤销此消息
        ErrorCode.showMessage(message: STR(10176));
      }
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      return;
    } catch (e) {
      Debug.w('CancelMessage Error:${e}');
      if (context != null) {
        //撤销失败
        ErrorCode.showMessage(message: STR(10177));
      }
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      return;
    }
    Debug.d('CancelMessage succeed');
    if (logicCallBack != null) {
      logicCallBack(0, resp);
    }
  }

  /// 发送图片
  static SendImage({
    required ReceiverType receiverType,
    required Int64 receiverId,
    List<Int64>? toUserIds,
    required String filePath,
    required int imageWidth,
    required int imageHeight,
    required int sendTime,
  }) async {
    if (Logic.showNextDoneMessage()) return;
    MessageInfo chatSendData = MessageInfo.newModel(
      senderType: SenderType.ST_USER,
      senderId: UserModel.userId,
      avatarUrl: UserModel.avatarUrl,
      nickname: UserModel.nickName,
      extra: '''{"state":1,"progress":0}''',
      receiverType: receiverType,
      receiverId: receiverId,
      iReceiverId: receiverId,
      toUserIds: toUserIds ?? [],
      messageType: MessageType.MT_IMAGE,
      messageContent: Image(url: filePath, width: imageWidth, height: imageHeight).writeToBuffer(),
      seq: Int64(sendTime),
      sendTime: getSendTime(),
      senderSeq: Int64.ZERO,
      localSendTime: sendTime.toInt(),
      msg_status: MessageStatus.MS_NORMAL,
      is_reading: true,
      sendState: LogicSendState.SEND,
    );

    GlobalHandler().sendMessage(
      HandlerCode.SEND_CHAT_MESSAGE,
      value: chatSendData,
    );

    int _sendProgress = 0;
    int _sendProTime = DateTime.now().millisecondsSinceEpoch;

    // await Future.delayed(Duration(milliseconds: 300), () {
    //   chatSendData.extra = '''{"state":1,"progress":10}''';
    //   Handler().sendMessage(
    //     HandlerCode.SEND_CHAT_MESSAGE,
    //     value: chatSendData,
    //   );
    // });

    // await Future.delayed(Duration(milliseconds: 300), () {
    //   chatSendData.extra = '''{"state":1,"progress":20}''';
    //   Handler().sendMessage(
    //     HandlerCode.SEND_CHAT_MESSAGE,
    //     value: chatSendData,
    //   );
    // });

    // await Future.delayed(Duration(milliseconds: 300), () {
    //   chatSendData.extra = '''{"state":1,"progress":30}''';
    //   Handler().sendMessage(
    //     HandlerCode.SEND_CHAT_MESSAGE,
    //     value: chatSendData,
    //   );
    // });

    // await Future.delayed(Duration(milliseconds: 300), () {
    //   chatSendData.extra = '''{"state":1,"progress":40}''';
    //   Handler().sendMessage(
    //     HandlerCode.SEND_CHAT_MESSAGE,
    //     value: chatSendData,
    //   );
    // });
    // await Future.delayed(Duration(milliseconds: 300), () {
    //   chatSendData.extra = '''{"state":1,"progress":50}''';
    //   Handler().sendMessage(
    //     HandlerCode.SEND_CHAT_MESSAGE,
    //     value: chatSendData,
    //   );
    // });
    // await Future.delayed(Duration(milliseconds: 300), () {
    //   chatSendData.extra = '''{"state":1,"progress":60}''';
    //   Handler().sendMessage(
    //     HandlerCode.SEND_CHAT_MESSAGE,
    //     value: chatSendData,
    //   );
    // });
    // await Future.delayed(Duration(milliseconds: 300), () {
    //   chatSendData.extra = '''{"state":1,"progress":70}''';
    //   Handler().sendMessage(
    //     HandlerCode.SEND_CHAT_MESSAGE,
    //     value: chatSendData,
    //   );
    // });
    // await Future.delayed(Duration(milliseconds: 300), () {
    //   chatSendData.extra = '''{"state":1,"progress":80}''';
    //   Handler().sendMessage(
    //     HandlerCode.SEND_CHAT_MESSAGE,
    //     value: chatSendData,
    //   );
    // });
    // await Future.delayed(Duration(milliseconds: 300), () {
    //   chatSendData.extra = '''{"state":1,"progress":90}''';
    //   Handler().sendMessage(
    //     HandlerCode.SEND_CHAT_MESSAGE,
    //     value: chatSendData,
    //   );
    // });

    // await Future.delayed(Duration(milliseconds: 300), () {
    //   chatSendData.extra = '''{"state":1,"progress":100}''';
    //   Handler().sendMessage(
    //     HandlerCode.SEND_CHAT_MESSAGE,
    //     value: chatSendData,
    //   );
    // });

    /// 上传图片
    await HttpUtils.upMultipartFile(
      requestUrl: AppConfig.apiUrl,
      filePath: filePath,
      onSucceed: (response) {
        if (response['code'] == 0 && response['data'] != null) {
          /// 上传成功->发送
          String url = response['data']['url'];
          var messageContent =
              Image(url: url, width: imageWidth, height: imageHeight).writeToBuffer();
          if (!Logic.showNextDoneMessage()) {
            if (receiverType == ReceiverType.RT_ROOM) {
              PushRoom(
                roomId: receiverId,
                messageType: MessageType.MT_IMAGE,
                messageContent: messageContent,
                sendTime: chatSendData.localSendTime,
              );
            } else {
              SendMessage(
                receiverType: receiverType,
                receiverId: receiverId,
                messageType: MessageType.MT_IMAGE,
                messageContent: messageContent,
                toUserIds: toUserIds,
                sendTime: chatSendData.localSendTime,
              );
            }
          } else {
            chatSendData.messageContent = messageContent;
            chatSendData.extra = '';
            chatSendData.sendState = LogicSendState.ERROR;
            GlobalHandler().sendMessage(
              HandlerCode.SEND_CHAT_MESSAGE,
              value: chatSendData,
            );
          }
        } else {
          ///上传失败
          chatSendData.sendState = LogicSendState.ERROR;
          GlobalHandler().sendMessage(
            HandlerCode.SEND_CHAT_MESSAGE,
            value: chatSendData,
          );
        }
      },
      onError: (errorCode) {
        chatSendData.extra = '''{"state":1,"progress":0}''';
        chatSendData.sendState = LogicSendState.ERROR;
        GlobalHandler().sendMessage(
          HandlerCode.SEND_CHAT_MESSAGE,
          value: chatSendData,
        );
      },
      onSendProgress: (count, total) {
        int progress = (count / total * 100).toInt();
        if (progress == _sendProgress) return;
        _sendProgress = progress;
        int time = DateTime.now().millisecondsSinceEpoch;
        if (time - _sendProTime >= 300) {
          _sendProTime = time;
          chatSendData.extra = '''{"state":1,"progress":$_sendProgress}''';
          GlobalHandler().sendMessage(
            HandlerCode.SEND_CHAT_MESSAGE,
            value: chatSendData,
          );
        }
      },
    );
  }
}
