import 'package:fim_app/package/net/socket/connect_socket.dart';
import 'package:fim_app/package/pb/logic.ext.pbgrpc.dart';
import 'package:fim_app/configs/api.dart';
import 'package:fim_app/package/util/time_utils.dart';
import 'package:fim_app/model/device_info.dart' as model;
import 'package:fim_app/model/user/user_model.dart' as model;
import 'package:fixnum/fixnum.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:grpc/grpc.dart';
// import 'package:grpc/grpc_web.dart';

// interceptors: [MyInterceptor()]);

typedef LogicCallBack<T> = void Function(int code, T? resp);

enum LogicSendState {
  ///正常(从服务器接收的消息状态)
  NONE,

  ///发送中
  SEND,

  ///发送成功
  SUCCEED,

  ///发送失败
  ERROR,
}

class Logic {
  static final LogicExtClient _LogicClient = LogicExtClient(
    ClientChannel(
      AppConfig.logicIp,
      port: AppConfig.logicPort,
      options: const ChannelOptions(credentials: ChannelCredentials.insecure()),
    ),
  );

  static LogicExtClient get logicClient => LogicExtClient(
        ClientChannel(
          AppConfig.logicIp,
          port: AppConfig.logicPort,
          options: const ChannelOptions(credentials: ChannelCredentials.insecure()),
        ),
      ); //_LogicClient;

  static final Duration timeout = Duration(seconds: 30);
  //dynamic
  static CallOptions get options => CallOptions(
        metadata: {
          'user_id': '${model.UserModel.userId}',
          'device_id': model.DeviceInfo.machineId,
          'token': model.UserModel.token,
          'request_id': '$requestId',
        },
        timeout: timeout,
      );

  // static int _requesIndex = 1;
  static Int64 get requestId {
    return Int64(TimeUtils.serverTime); //Int64(DateTime.now().millisecondsSinceEpoch);
    // String sss = '${UserModel.userId}$_requesIndex';
    // int ret = int.parse(sss);
    // _requesIndex++;
    // if (_requesIndex >= 1000001) {
    //   _requesIndex = 1;
    // }
    // return ret;
  }

  static bool showNextDoneMessage() {
    if (!ConnectSocket().isConnect) {
      EasyLoading.showToast('离线状态，无法发送信息！');
      return true;
    }
    return false;
  }
}
