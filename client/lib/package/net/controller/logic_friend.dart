// ignore_for_file: non_constant_identifier_names

import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/model/chat/MessageModel.dart';
import 'package:fim_app/model/friend/friend_scoped.dart';
import 'package:fim_app/model/friend/new_friend_model.dart';
import 'package:fim_app/package/net/controller/logic.dart';
import 'package:fim_app/package/net/error_code.dart';
import 'package:fim_app/package/pb/logic.ext.pb.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:grpc/grpc.dart';

class LogicFriend {
  // 搜索用户(这里简单数据库实现，生产环境建议使用ES)
  static Future<SearchUserResp?> SearchUser(
      {required String key, LogicCallBack<SearchUserResp>? logicCallBack}) async {
    SearchUserReq request = SearchUserReq(key: key);
    Debug.d('SearchUser Req:{\n$request}');
    SearchUserResp resp;
    try {
      resp = await Logic.logicClient.searchUser(request, options: Logic.options);
    } catch (e) {
      Debug.w('SearchUser Error:{\n$e}');
      if (logicCallBack != null) logicCallBack(-1, null);
      return null;
    }
    Debug.d('SearchUser resp:{\n$resp}');
    if (logicCallBack != null) logicCallBack(0, resp);
    return resp;
  }

  // 添加好友
  static AddFriend(
      {required Int64 userId,
      String remarks = '',
      required String description,
      LogicCallBack<AddFriendResp>? logicCallBack}) async {
    if (Logic.showNextDoneMessage()) return;
    AddFriendReq request = AddFriendReq(
      friendId: userId,
      remarks: remarks,
      description: description,
    );
    Debug.d('AddFriend: {$request}');
    AddFriendResp resp;
    try {
      resp = await Logic.logicClient.addFriend(request, options: Logic.options);
    } on GrpcError catch (e) {
      Debug.w('AddFriend Error:$e');
      ErrorCode.showMessage(message: e.message ?? '', duration: const Duration(milliseconds: 100));
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      return;
    } catch (e) {
      Debug.w('AddFriend Error:$e');
      ErrorCode.showMessage(message: STR(10178), duration: const Duration(milliseconds: 100));
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }

      return;
    }
    Debug.d('AddFriend  resp:{\n$resp}');
    EasyLoading.showToast(STR(10221));
    if (logicCallBack != null) {
      logicCallBack(0, resp);
    }
  }

  // 同意添加好友
  static AgreeAddFriend(
      {BuildContext? context,
      required Int64 userId,
      String? remarks,
      LogicCallBack<AgreeAddFriendResp>? logicCallBack}) async {
    if (Logic.showNextDoneMessage()) return;
    Debug.d('AgreeAddFriend userid: $userId');
    Debug.d('AgreeAddFriend remarks: $remarks');
    EasyLoading.show();
    AgreeAddFriendReq request = AgreeAddFriendReq(
      userId: userId,
      remarks: remarks,
    );
    AgreeAddFriendResp resp;
    try {
      resp = await Logic.logicClient.agreeAddFriend(request, options: Logic.options);
      EasyLoading.dismiss();
    } catch (e) {
      Debug.w('AgreeAddFriend Error:$e');
      logicCallBack?.call(-1, null);
      NewFriendModel.instance.remove(userId);
      EasyLoading.dismiss();
      return;
    }
    Debug.d('AgreeAddFriend :{\n$resp}');
    NewFriendModel.instance.remove(userId);
    GetFriends();

    EasyLoading.showSuccess('好友验证通过');

    logicCallBack?.call(0, resp);
  }

  // 设置好友信息
  static SetFriend(
      {BuildContext? context,
      required Int64 friendId,
      String? remarks,
      String? extra,
      LogicCallBack<SetFriendResp>? logicCallBack}) async {
    SetFriendReq request = SetFriendReq(friendId: friendId, remarks: remarks, extra: extra);
    SetFriendResp resp;
    try {
      resp = await Logic.logicClient.setFriend(request, options: Logic.options);
    } on GrpcError catch (e) {
      Debug.w('SetFriend Error:$e');
      ErrorCode.showMessage(message: e.message ?? '', duration: const Duration(milliseconds: 100));

      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      return;
    } catch (e) {
      Debug.w('SetFriend Error:$e');
      //设置好友信息
      ErrorCode.showMessage(message: STR(10180), duration: const Duration(milliseconds: 100));

      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      return;
    }
    Debug.d('设置好友信息 SetFriend :{\n$resp}');
    FriendScoped.instance.upFriend(friendId, remarks: remarks, extra: extra);
    if (logicCallBack != null) {
      logicCallBack(0, resp);
    }
  }

  // 获取好友列表
  static GetFriends({BuildContext? context, LogicCallBack<GetFriendsResp>? logicCallBack}) async {
    GetFriendsReq request = GetFriendsReq();
    GetFriendsResp resp;
    try {
      resp = await Logic.logicClient.getFriends(request, options: Logic.options);
    } on GrpcError catch (e) {
      Debug.w('GetFriends Error:$e');
      if (context != null) {
        ErrorCode.showMessage(
            message: e.message ?? '', duration: const Duration(milliseconds: 100));
      }
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      return;
    } catch (e) {
      Debug.w('GetFriends Error:$e');
      if (context != null) {
        //获取好友列表失败
        ErrorCode.showMessage(message: STR(10179), duration: const Duration(milliseconds: 100));
      }
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      return;
    }
    Debug.d('获取好友列表成功 GetFriends :{\n$resp}');
    FriendScoped.instance.setFrinds(resp.friends);
    if (logicCallBack != null) {
      logicCallBack(0, resp);
    }
  }

  static DelFriend(
      {BuildContext? context,
      required Int64 friendId,
      LogicCallBack<DelFriendResp>? logicCallBack}) async {
    if (Logic.showNextDoneMessage()) return;
    DelFriendReq request = DelFriendReq(friendId: friendId);
    DelFriendResp resp;
    try {
      resp = await Logic.logicClient.delFriend(request, options: Logic.options);
    } on GrpcError catch (e) {
      Debug.w('DelFriend Error:$e');
      if (context != null) {
        ErrorCode.showMessage(message: e.message ?? '');
      }
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      return;
    } catch (e) {
      Debug.w('GetFriends Error:$e');
      if (context != null) {
        //删除好友成功
        ErrorCode.showMessage(message: STR(10181));
      }
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      return;
    }
    Debug.d('删除好友 GetFriends :{\n$resp}');
    FriendScoped.instance.removeFriend(friendId);
    MessageModel().remove(friendId.toString(), isSqlite: true);
    if (logicCallBack != null) {
      logicCallBack(0, resp);
    }
  }
}
