// ignore_for_file: non_constant_identifier_names

import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/model/chat/MessageModel.dart';
import 'package:fim_app/model/group/room_members_model.dart';
import 'package:fim_app/model/group/group_member_scoped.dart';
import 'package:fim_app/model/group/group_scoped.dart';
import 'package:fim_app/model/group/group_model.dart';
import 'package:fim_app/package/net/controller/logic.dart';
import 'package:fim_app/package/net/error_code.dart';
import 'package:fim_app/package/pb/logic.ext.pb.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/material.dart';
import 'package:grpc/grpc.dart';

class LogicGroup {
  // 创建群组(name:名称, avatarUrl: 头像, introduction:简介, extra:附加字段, memberIds:成员id)
  static CreateGroup(
    BuildContext context, {
    String name = '',
    String avatarUrl = '',
    String introduction = '',
    String extra = '',
    required List<Int64> memberIds,
    required LogicCallBack<CreateGroupResp> logicCallBack,
  }) async {
    if (Logic.showNextDoneMessage()) return;
    CreateGroupReq request = CreateGroupReq(
        name: name,
        avatarUrl: avatarUrl,
        introduction: introduction,
        extra: extra,
        memberIds: memberIds);
    CreateGroupResp resp;
    try {
      resp = await Logic.logicClient.createGroup(request, options: Logic.options);
    } on GrpcError catch (e) {
      Debug.w('CreateGroup Error:{\n${e}}');
      logicCallBack(-1, null);
      Future.delayed(Duration(milliseconds: 100), () {
        ErrorCode.showMessage(message: '发起群聊失败！');
      });
      return;
    } catch (e) {
      Debug.w('CreateGroup Error:{\n${e}}');
      logicCallBack(-1, null);
      Future.delayed(Duration(milliseconds: 100), () {
        ErrorCode.showMessage(message: '发起群聊失败！');
      });
      return;
    }
    Debug.d('CreateGroup resp:{\n${resp}}');
    GetGroup(
        context: context,
        groupId: resp.groupId,
        logicCallBack: (code, respe) {
          if (code == 0) {
            if (respe != null) {
              GroupScoped.instance.addGroup(GroupModel.newGroup(respe.group));
            }

            logicCallBack(0, resp);
          } else {
            logicCallBack(-1, null);
          }
        });
  }

  // 更新群组
  static UpdateGroup(
    BuildContext context, {
    required Int64 group_id,
    String? avatar_url,
    String? name,
    String? introduction,
    String? extra,
    LogicCallBack<UpdateGroupResp>? logicCallBack,
  }) async {
    if (Logic.showNextDoneMessage()) return;
    GroupModel? model = GroupScoped.instance.getGroup(group_id);
    if (model == null) {
      if (logicCallBack != null) logicCallBack.call(-1, null);
      return;
    }
    UpdateGroupReq request = UpdateGroupReq(
        groupId: group_id,
        avatarUrl: avatar_url,
        name: name,
        introduction: introduction,
        extra: extra);

    UpdateGroupResp resp;
    try {
      resp = await Logic.logicClient.updateGroup(request, options: Logic.options);
    } on GrpcError catch (e) {
      Debug.w('UpdateGroup Error:{\n${e}}');
      if (logicCallBack != null) logicCallBack.call(-1, null);
      Future.delayed(Duration(milliseconds: 100), () {
        ErrorCode.showMessage(message: e.message ?? '');
      });
      return;
    } catch (e) {
      Debug.w('UpdateGroup Error:{\n${e}}');
      if (logicCallBack != null) logicCallBack.call(-1, null);
      return;
    }
    Debug.d('UpdateGroup resp:{\n${resp}}');
    GroupScoped.instance.UPGroupDate(
        group_id: group_id,
        avatar_url: avatar_url,
        name: name,
        introduction: introduction,
        extra: extra);
    if (logicCallBack != null) logicCallBack.call(0, resp);
  }

  // 获取群组信息
  static GetGroup({
    BuildContext? context,
    required Int64 groupId,
    LogicCallBack<GetGroupResp>? logicCallBack,
  }) async {
    GetGroupReq request = GetGroupReq(groupId: groupId);
    GetGroupResp resp;
    try {
      resp = await Logic.logicClient.getGroup(request, options: Logic.options);
    } on GrpcError catch (e) {
      Debug.w('GetGroup Error:{\n${e}}');
      if (logicCallBack != null) logicCallBack(-1, null);
      return;
    } catch (e) {
      Debug.w('GetGroup Error:{\n${e}}');
      if (logicCallBack != null) logicCallBack(-1, null);
      return;
    }
    Debug.d('GetGroup resp:{\n${resp}}');

    GroupScoped.instance.UPGroup(GroupModel.newGroup(resp.group));
    if (logicCallBack != null) logicCallBack(0, resp);
  }

  // 获取用户加入的所有群组
  static GetGroups({
    BuildContext? context,
    LogicCallBack<GetGroupsResp>? logicCallBack,
  }) async {
    GetGroupsResp resp;
    try {
      resp = await Logic.logicClient.getGroups(GetGroupsReq(), options: Logic.options);
    } on GrpcError catch (e) {
      Debug.w('GetGroups Error:{\n${e}}');
      if (logicCallBack != null) logicCallBack(-1, null);
      Future.delayed(Duration(milliseconds: 100), () {
        if (context != null) ErrorCode.showMessage(message: e.message ?? '');
      });
      return;
    } catch (e) {
      Debug.w('GetGroups Error:{\n${e}}');
      if (logicCallBack != null) logicCallBack(-1, null);
      return;
    }
    Debug.d('GetGroups resp:{\n${resp}}');
    GroupScoped.instance.setGroups(resp.groups);
    resp.groups.forEach((element) {
      LogicGroup.GetGroupMembers(groupId: element.groupId);
    });

    if (logicCallBack != null) logicCallBack(0, resp);
  }

  // 添加群组成员
  static AddGroupMembers(
    BuildContext context, {
    required Int64 groupId,
    required List<Int64> userIds,
    LogicCallBack<AddGroupMembersResp>? logicCallBack,
  }) async {
    if (Logic.showNextDoneMessage()) return;
    Debug.d('添加群组成员 groupId: $groupId, userIds: [$userIds]');
    AddGroupMembersResp resp;
    try {
      resp = await Logic.logicClient.addGroupMembers(
          AddGroupMembersReq(groupId: groupId, userIds: userIds),
          options: Logic.options);
    } on GrpcError catch (e) {
      Debug.w('AddGroupMembers Error:{\n${e}}');
      if (logicCallBack != null) logicCallBack(-1, null);
      Future.delayed(Duration(milliseconds: 100), () {
        if (context != null) ErrorCode.showMessage(message: e.message ?? '');
      });
      return;
    } catch (e) {
      Debug.w('AddGroupMembers Error:{\n${e}}');
      if (logicCallBack != null) logicCallBack(-1, null);
      return;
    }
    Debug.d('AddGroupMembers resp:{\n${resp}}');
    // GetGroupMembers(groupId: groupId);
    if (logicCallBack != null) logicCallBack(0, resp);
  }

  // 更新群组成员信息
  static UpdateGroupMember({
    BuildContext? context,
    required Int64 groupId,
    required Int64 userId,
    MemberType? memberType,
    String? remarks,
    String? extra,
    LogicCallBack<UpdateGroupMemberResp>? logicCallBack,
  }) async {
    if (Logic.showNextDoneMessage()) return;
    UpdateGroupMemberReq request = UpdateGroupMemberReq(
      groupId: groupId,
      userId: userId,
      memberType: memberType,
      remarks: remarks,
      extra: extra,
    );
    UpdateGroupMemberResp resp;
    try {
      resp = await Logic.logicClient.updateGroupMember(request, options: Logic.options);
    } on GrpcError catch (e) {
      Debug.w('UpdateGroupMember Error:{\n${e}}');
      if (logicCallBack != null) logicCallBack(-1, null);
      Future.delayed(Duration(milliseconds: 100), () {
        if (context != null) ErrorCode.showMessage(message: e.message ?? '');
      });
      return;
    } catch (e) {
      Debug.w('UpdateGroupMember Error:{\n${e}}');
      if (logicCallBack != null) logicCallBack(-1, null);
      return;
    }
    Debug.d('UpdateGroupMember resp:{\n${resp}}');
    // GroupMemberScoped.instance.delMember(groupId: groupId, userId: userId);
    // GetGroup(groupId: groupId);
    if (logicCallBack != null) logicCallBack(0, resp);
  }

  // 删除群组成员
  static DeleteGroupMember({
    BuildContext? context,
    required Int64 groupId,
    required Int64 userId,
    LogicCallBack<DeleteGroupMemberResp>? logicCallBack,
  }) async {
    if (Logic.showNextDoneMessage()) return;
    DeleteGroupMemberReq req = DeleteGroupMemberReq(groupId: groupId, userId: userId);
    Debug.d('删除群组成员 groupId: $groupId, userId: $userId');
    DeleteGroupMemberResp resp;
    try {
      resp = await Logic.logicClient.deleteGroupMember(req, options: Logic.options);
    } on GrpcError catch (e) {
      Debug.w('DeleteGroupMember Error:{\n${e}}');
      if (logicCallBack != null) logicCallBack(-1, null);
      if (context != null)
        ErrorCode.showMessage(
            message: e.message ?? '', duration: const Duration(milliseconds: 100));

      return;
    } catch (e) {
      Debug.w('DeleteGroupMember Error:{\n${e}}');
      if (logicCallBack != null) logicCallBack(-1, null);
      return;
    }
    Debug.d('DeleteGroupMember resp:{\n${resp}}');
    // GroupMemberScoped.instance.delMember(groupId: groupId, userId: userId);
    // GetGroup(groupId: groupId);
    if (logicCallBack != null) logicCallBack(0, resp);
  }

  // 获取群组成员
  static GetGroupMembers({
    BuildContext? context,
    required Int64 groupId,
    LogicCallBack<GetGroupMembersResp>? logicCallBack,
  }) async {
    GetGroupMembersResp resp;
    try {
      resp = await Logic.logicClient
          .getGroupMembers(GetGroupMembersReq(groupId: groupId), options: Logic.options);
    } on GrpcError catch (e) {
      Debug.w('GetGroupMembers Error:{\n${e}}');
      if (logicCallBack != null) logicCallBack(-1, null);
      if (context != null)
        ErrorCode.showMessage(
            message: e.message ?? '', duration: const Duration(milliseconds: 100));

      return;
    } catch (e) {
      Debug.w('GetGroupMembers Error:{\n${e}}');
      if (logicCallBack != null) logicCallBack(-1, null);
      return;
    }
    Debug.d('GetGroupMembers resp:{\n${resp}}');
    GroupMemberScoped.instance.delMembers(groupId);
    GroupMemberScoped.instance.addMemberAll(groupId, resp.members);
    if (logicCallBack != null) logicCallBack(0, resp);
  }

  static ExitGroup({
    BuildContext? context,
    required Int64 groupId,
    LogicCallBack<ExitGroupResp>? logicCallBack,
  }) async {
    if (Logic.showNextDoneMessage()) return;
    ExitGroupResp resp;
    try {
      resp =
          await Logic.logicClient.exitGroup(ExitGroupReq(groupId: groupId), options: Logic.options);
    } on GrpcError catch (e) {
      Debug.w('ExitGroup Error:{\n${e}}');
      if (logicCallBack != null) logicCallBack(-1, null);
      if (context != null)
        ErrorCode.showMessage(
            message: e.message ?? '', duration: const Duration(milliseconds: 100));
      return;
    } catch (e) {
      Debug.w('ExitGroup Error:{\n${e}}');
      if (logicCallBack != null) logicCallBack(-1, null);
      return;
    }
    Debug.d('ExitGroup resp:{\n${resp}}');
    if (logicCallBack != null) logicCallBack(0, resp);
  }

  /// 查询所偶有聊天室
  static GetAllRoom({
    BuildContext? context,
    LogicCallBack<GetAllRoomResp>? logicCallBack,
  }) async {
    GetAllRoomReq request = GetAllRoomReq();
    Debug.d('GetAllRoom');
    GetAllRoomResp resp;
    try {
      resp = await Logic.logicClient.getAllRoom(request, options: Logic.options);
    } on GrpcError catch (e) {
      Debug.w('GetAllRoom: $e');
      if (logicCallBack != null) logicCallBack(-1, null);
      return;
    } catch (e) {
      Debug.w('GetAllRoom: $e');
      if (logicCallBack != null) logicCallBack(-1, null);
      return;
    }
    Debug.d('GetAllRoom resp: $resp');
    GroupScoped.instance.setRoom(
      roomIds: resp.rooms,
      name: resp.name,
      avatarUrl: resp.avatarUrl,
      introduction: resp.introduction,
      extra: resp.extra,
    );
    if (logicCallBack != null) logicCallBack(0, resp);
  }

  /// 查询聊天是玩家
  static GetRoomUserList({
    BuildContext? context,
    required Int64 roomId,
    LogicCallBack<GetRoomUserListResp>? logicCallBack,
  }) async {
    GetRoomUserListReq request = GetRoomUserListReq(roomId: roomId);
    Debug.d('GetRoomUserList roomId: $roomId');
    GetRoomUserListResp resp;
    try {
      resp = await Logic.logicClient.getRoomUserList(request, options: Logic.options);
    } on GrpcError catch (e) {
      Debug.w('GetRoomUserList: $e');
      if (logicCallBack != null) logicCallBack(-1, null);
      return;
    } catch (e) {
      Debug.w('GetRoomUserList: $e');
      if (logicCallBack != null) logicCallBack(-1, null);
      return;
    }
    Debug.d('GetRoomUserList resp: $resp');
    RoomMembersModel().setMembers(roomId: roomId, members: resp.userList);
    if (logicCallBack != null) logicCallBack(0, resp);
  }
}
