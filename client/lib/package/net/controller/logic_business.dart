import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/package/net/controller/logic.dart';
import 'package:fim_app/package/net/error_code.dart';
import 'package:fim_app/package/pb/business.int.pb.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/material.dart';
import 'package:grpc/grpc.dart';

class LogicBusiness {
  /// 查看红包
  ///
  ///String rid 红包id
  static LookUpRedPacket({
    BuildContext? context,
    required String rid,
    LogicCallBack<LookUpRedPacketResp>? logicCallBack,
  }) async {
    var request = LookUpRedPacketReq(
      rid: rid,
    );
    Debug.d('Business LookUpRedPacket: $request');
    LookUpRedPacketResp resp;
    try {
      resp = await Logic.logicClient.lookUpRedPacket(request, options: Logic.options);
    } on GrpcError catch (e) {
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      if (context != null) {
        ErrorCode.showMessage(message: '打开红包错误！', duration: Duration(milliseconds: 100));
      }
      Debug.w('Business LookUpRedPacket error: $e');
      return;
    } catch (e) {
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      if (context != null) {
        ErrorCode.showMessage(message: '打开红包错误！', duration: Duration(milliseconds: 100));
      }
      Debug.w('Business LookUpRedPacket error: $e');
      return;
    }
    Debug.d('Business LookUpRedPacket 成功 !');
    if (logicCallBack != null) {
      logicCallBack(0, resp);
    }
  }

  ///抢红包
  /// ReceiverType receiver_type 接收者类型，1：user; 2:group 3:聊天室
  /// int64 receiver_id = 2 用户id或者群组id
  /// string rid 红包唯一id
  /// int64 user_id 抢红包玩家
  /// string name 抢红包用户名
  static RobRedPacket({
    BuildContext? context,
    required ReceiverType receiver_type,
    required Int64 receiver_id,
    required String rid,
    LogicCallBack<RobRedPacketResp>? logicCallBack,
  }) async {
    var request = RobRedPacketReq(
      receiverId: receiver_id,
      receiverType: receiver_type,
      rid: rid,
      userId: UserModel.userId,
      name: UserModel.nickName,
      avatar: UserModel.avatarUrl,
    );
    Debug.d('Business RobRedPacket: $request');
    RobRedPacketResp resp;
    try {
      resp = await Logic.logicClient.robRedPacket(request, options: Logic.options);
    } on GrpcError catch (e) {
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      if (context != null) {
        ErrorCode.showMessage(
            message: e.message ?? '', duration: const Duration(milliseconds: 100));
      }
      Debug.w('Business RobRedPacket error: $e');
      return;
    } catch (e) {
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      if (context != null) {
        ErrorCode.showMessage(message: '抢红包失败！', duration: const Duration(milliseconds: 100));
      }
      Debug.w('Business RobRedPacket error: $e');
      return;
    }
    Debug.d('Business RobRedPacket 成功 !');
    if (logicCallBack != null) {
      logicCallBack(0, resp);
    }
  }
}
