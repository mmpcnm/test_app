import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/package/net/controller/logic.dart';
import 'package:fim_app/package/net/error_code.dart';
import 'package:fim_app/package/pb/logic.ext.pb.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:flutter/material.dart';
import 'package:grpc/grpc.dart';

class LogicSecurity {
  /// 查看钱包
  static OpenWallet({
    BuildContext? context,
    LogicCallBack<OpenWalletResp>? logicCallBack,
  }) async {
    Debug.d('LogicSecurity OpenWallet');
    OpenWalletResp resp;
    try {
      resp = await Logic.logicClient.openWallet(OpenWalletReq(), options: Logic.options);
    } on GrpcError catch (e) {
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      if (context != null) {
        ErrorCode.showMessage(message: e.message ?? '', duration: Duration(milliseconds: 100));
      }
      Debug.w('LogicSecurity OpenWallet error: $e');
      return;
    } catch (e) {
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      if (context != null) {
        ErrorCode.showMessage(message: '查看钱包发生错误！', duration: Duration(milliseconds: 100));
      }
      Debug.w('LogicSecurity OpenWallet error: $e');
      return;
    }
    Debug.w('LogicSecurity OpenWallet 成功 !');
    UserModel.money = resp.money;
    UserModel.notify();
    if (logicCallBack != null) {
      logicCallBack(0, resp);
    }
  }

  /// 设置钱包密码
  static SetWalletPwd({
    BuildContext? context,
    required String password,
    required String newPassword,
    LogicCallBack<SetWalletPwdResp>? logicCallBack,
  }) async {
    SetWalletPwdReq request = SetWalletPwdReq(pwd: password, newPwd: newPassword);
    SetWalletPwdResp resp;
    try {
      resp = await Logic.logicClient.setWalletPwd(request, options: Logic.options);
    } on GrpcError catch (e) {
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      if (context != null) {
        ErrorCode.showMessage(message: e.message ?? '', duration: Duration(milliseconds: 100));
      }
      Debug.w('LogicSecurity SetWalletPwd error: $e');
      return;
    } catch (e) {
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      if (context != null) {
        ErrorCode.showMessage(message: '设置密码失败！', duration: Duration(milliseconds: 100));
      }
      Debug.w('LogicSecurity SetWalletPwd error: $e');
      return;
    }
    Debug.w('LogicSecurity SetWalletPwd 成功 !');
    UserModel.isWalletPwdSet = true;
    if (logicCallBack != null) {
      logicCallBack(0, resp);
    }
  }

  /// 绑定手机号码
  static BindPhone({
    BuildContext? context,
    required String phone,
    LogicCallBack<BindPhoneResp>? logicCallBack,
  }) async {
    BindPhoneReq request = BindPhoneReq(phone: phone);
    BindPhoneResp resp;
    try {
      resp = await Logic.logicClient.bindPhone(request, options: Logic.options);
    } on GrpcError catch (e) {
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      if (context != null) {
        ErrorCode.showMessage(message: e.message ?? '', duration: Duration(milliseconds: 100));
      }
      Debug.w('LogicSecurity BindPhone error: $e');
      return;
    } catch (e) {
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      if (context != null) {
        ErrorCode.showMessage(message: '绑定失败！', duration: Duration(milliseconds: 100));
      }
      Debug.w('LogicSecurity BindPhone error: $e');
      return;
    }
    Debug.w('LogicSecurity BindPhone 成功 !');
    UserModel.phoneNumber = phone;
    UserModel.notify();
    if (logicCallBack != null) {
      logicCallBack(0, resp);
    }
  }

  /// 绑定支付宝账号
  ///
  /// String realname 实名
  ///
  /// String alipayAccount 支付宝账号
  static BindAlipay({
    BuildContext? context,
    required String realname,
    required String alipayAccount,
    LogicCallBack<BindAlipayResp>? logicCallBack,
  }) async {
    BindAlipayReq request = BindAlipayReq(realname: realname, alipayAccount: alipayAccount);
    BindAlipayResp resp;
    try {
      resp = await Logic.logicClient.bindAlipay(request, options: Logic.options);
    } on GrpcError catch (e) {
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      if (context != null) {
        ErrorCode.showMessage(message: e.message ?? '', duration: Duration(milliseconds: 100));
      }
      Debug.w('LogicSecurity BindAlipay error: $e');
      return;
    } catch (e) {
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      if (context != null) {
        ErrorCode.showMessage(message: '绑定失败！', duration: Duration(milliseconds: 100));
      }
      Debug.w('LogicSecurity BindAlipay error: $e');
      return;
    }
    Debug.w('LogicSecurity BindAlipay 成功 !');
    UserModel.alipayAccount = alipayAccount;
    UserModel.alipayRealname = realname;
    UserModel.notify();
    if (logicCallBack != null) {
      logicCallBack(0, resp);
    }
  }

  /// 查询账单
  ///
  /// String month 查询月份， 只能查询最近一年的数据
  static QueryBill({
    BuildContext? context,
    required int month,
    LogicCallBack<QueryBillResp>? logicCallBack,
  }) async {
    QueryBillReq request = QueryBillReq(month: month);
    QueryBillResp resp;
    try {
      resp = await Logic.logicClient.queryBill(request, options: Logic.options);
    } on GrpcError catch (e) {
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      if (context != null) {
        ErrorCode.showMessage(message: e.message ?? '', duration: Duration(milliseconds: 100));
      }
      Debug.w('LogicSecurity QueryBill error: $e');
      return;
    } catch (e) {
      if (logicCallBack != null) {
        logicCallBack(-1, null);
      }
      if (context != null) {
        ErrorCode.showMessage(message: '查询失败！', duration: Duration(milliseconds: 100));
      }
      Debug.w('LogicSecurity QueryBill error: $e');
      return;
    }
    Debug.d('LogicSecurity QueryBill 成功 !');
    Debug.d(resp);
    if (logicCallBack != null) {
      logicCallBack(0, resp);
    }
  }
}
