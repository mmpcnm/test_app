import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/package/net/controller/logic.dart';
import 'package:fim_app/package/pb/business.int.pb.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart';
import 'package:fim_app/package/pb/logic.ext.pb.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:grpc/grpc.dart';

import '../error_code.dart';

class LogicRedPacket {
  /// 查询可抢红包信息
  static GetRedPacketRobAuth({BuildContext? context, LogicCallBack<GetRedPacketRobAuthResp>? logicCallBack}) async {
    GetRedPacketRobAuthReq request = GetRedPacketRobAuthReq();
    GetRedPacketRobAuthResp resp;
    Debug.d('GetRedPacketRobAuth request');
    try {
      resp = await Logic.logicClient.getRedPacketRobAuth(request, options: Logic.options);
    } on GrpcError catch (e) {
      Debug.w('GetRedPacketRobAuth Error: ${e.message}');
      if (logicCallBack != null) logicCallBack(-1, null);
      if (context != null) ErrorCode.showMessage(message: e.message ?? '', duration: const Duration(milliseconds: 100));
      return;
    } catch (e) {
      Debug.w('GetRedPacketRobAuth Error: $e');
      if (logicCallBack != null) logicCallBack(-1, null);
      if (context != null) ErrorCode.showMessage(message: '获取失败！', duration: const Duration(milliseconds: 100));
      return;
    }
    Debug.d('GetRedPacketRobAuth resp: {\n$resp}');
    if (logicCallBack != null) logicCallBack(0, resp);
  }

  /// 发红包
  ///ReceiverType type 红包类型 1：发个人 2:群发 3.聊天室
  ///int64 receive_id 收红包对象
  ///int64 amount 红包金额
  ///int32 num 红包个数
  ///string blessing 红包祝词
  static SendRedPacket({
    BuildContext? context,
    required ReceiverType type,
    required Int64 receiveId,
    required Int64 amount,
    required int count,
    required String blessing,
    LogicCallBack<SendRedPacketResp>? logicCallBack,
  }) async {
    if (Logic.showNextDoneMessage()) return;
    EasyLoading.show();
    SendRedPacketReq request = SendRedPacketReq(
      type: type,
      receiveId: receiveId,
      amount: amount,
      num: count,
      blessing: blessing,
    );

    Debug.d('SendRedPacket request');
    SendRedPacketResp resp;
    try {
      resp = await Logic.logicClient.sendRedPacket(request, options: Logic.options);
      EasyLoading.dismiss();
    } on GrpcError catch (e) {
      Debug.w('SendRedPacket Error: ${e.message}');
      if (logicCallBack != null) logicCallBack(-1, null);
      ErrorCode.showMessage(message: e.message ?? '', duration: const Duration(milliseconds: 100));
      return;
    } catch (e) {
      Debug.w('SendRedPacket Error: $e');
      if (logicCallBack != null) logicCallBack(-1, null);
      ErrorCode.showMessage(message: '发送失败！', duration: const Duration(milliseconds: 100));
      return;
    }
    Debug.d('SendRedPacket resp: {\n$resp}');
    EasyLoading.showToast('红包发送成功');
    if (logicCallBack != null) logicCallBack(0, resp);
  }
}
