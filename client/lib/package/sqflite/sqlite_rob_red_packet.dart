import 'package:fim_app/model/red_packet/rob_red_packet_model.dart';

import 'sqlite_manager.dart';

///领红包记录数据库
class SqliteRobRedPacket {
  ///rob_red_packet
  static const String table = 'rob_red_packet';
  static final SqliteRobRedPacket _instatne = SqliteRobRedPacket._();
  SqliteRobRedPacket._();
  factory SqliteRobRedPacket() => _instatne;

  Future records() async {
    var values = await SqliteManager().query('SELECT * FROM $table');
    RobRedPacketModel().init(values);
  }

  Future insert(RobRedPacketInfo info) async {
    print('SqliteRobRedPacket insert');
    return await SqliteManager().execute('''INSERT INTO $table (receiver_type,
    receiver_id,
    rid,
    rob_user_id,
    amount) VALUES (${info.receiverType.value},
    ${info.receiverId.toInt()},
    "${info.rid}",
    ${info.robUserId.toInt()},
    ${info.amount.toInt()});''');
  }
}
