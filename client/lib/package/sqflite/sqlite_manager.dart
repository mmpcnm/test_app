import 'dart:async';

import 'package:fim_app/configs/api.dart';
import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:fim_app/package/sqflite/sqlite_message.dart';
import 'package:fim_app/package/sqflite/sqlite_new_friend.dart';
import 'package:fim_app/package/sqflite/sqlite_red_packet.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import 'sqlite_refund_red.dart';
import 'sqlite_rob_red_packet.dart';

class SqlitData {
  String type = '';
  String table = '';
  String? where;
  Map<String, Object?>? values;
  List<Object?>? whereArgs;
}

class SqliteManager {
  static final SqliteManager _instance = SqliteManager._create();
  SqliteManager._create();
  factory SqliteManager() => _instance;
  // Database get database => _database!;
  Database? _database;
  // List<SqlitData> _mDataList = [];
  List<String> _sqlList = [];
  Timer? _timer;
  Future<void> init({void Function()? onOpen}) async {
    String path = join(await getDatabasesPath(), UserModel.userId.toString(),
        '${AppConfig.channelId}_${UserModel.userId}_data.db');
    Debug.d('db path:$path');
    _database = await openDatabase(
      path,
      version: 8,
      onCreate: _onCreate,
      onUpgrade: _onUpgrade,
      // onOpen: (Database db) async {
      //   _database = db;

      // },
    );
    await SqliteRedPacket().records();
    await SqliteRobRedPacket().records();
    await SqliteRefundRed().records();
    await SqliteMessage().records();
    await SqliteNewFriend().records();

    // if (_timer != null) _timer?.cancel();
    // _timer = Timer.periodic(Duration(milliseconds: 20), _onExecuteHandler);

    await _onExecuteHandler();
    onOpen?.call();
  }

  Future<void> close() async {
    // _mDataList = [];
    _sqlList = [];
    if (_timer != null) {
      _timer?.cancel();
      _timer = null;
    }
    if (_database != null) {
      _database!.close();
      _database = null;
    }
  }

  Future<void> _onExecuteHandler() async {
    // String sqlStr = '';
    // var batch = _database?.batch();
    if (_sqlList.isEmpty) return;
    for (var sql in _sqlList) {
      if (sql.isEmpty) continue;
      // sqlStr = '$sqlStr$sql';
      // if (sql[sql.length - 1] != ';') {
      //   sqlStr = '$sqlStr;';
      // }
      await _database?.execute(sql).then((value) {
        // Debug.d('sqlite execute Succeed: $sqlStr');
      }, onError: (e) {
        Debug.e('sqlite execute Error: $e');
      });
    }
    // Debug.d('SqlManager execute: $sqlStr');
    // await _database?.execute(sqlStr).then((value) {
    //   // Debug.d('sqlite execute Succeed: $sqlStr');
    // }, onError: (e) {
    //   Debug.e('sqlite execute Error: $e');
    // });

    _sqlList = [];
    // await batch?.commit();
  }

  Future<void> _onCreate(Database db, int version) async {
    // 聊天列表

    await db.execute('''CREATE TABLE ${SqliteMessage.table} (id INTEGER PRIMARY KEY,
      only_key TEXT,
      sender_type INTEGER,
      sender_id INTEGER,
      avatar_url TEXT,
      nickname TEXT,
      extra TEXT,
      receiver_type INTEGER,
      receiver_id INTEGER,
      to_user_ids TEXT,
      message_type INTEGER,
      message_content BLOB,
      seq INTEGER,
      send_time INTEGER,
      status INTEGER,
      is_reading INTEGER,
      sender_seq INTEGER,
      i_receiver_id INTEGER);''').then(
      (value) {},
      onError: (e) {
        Debug.e('sqlite execute create Error: $e');
      },
    );

    // await db.execute(
    //   '''
    //   CREATE UNIQUE INDEX ${SqlChat.table}_only_key ON ${SqlChat.table} (only_key)
    //   ''',
    // );

    //好友请求列表
    await db.execute('''CREATE TABLE ${SqliteNewFriend.table} (id INTEGER PRIMARY KEY,
      friend_id INTEGER,
      nickname TEXT,
      sex INTEGER,
      avatar_url TEXT,
      description TEXT);''').then(
      (value) {},
      onError: (e) {
        Debug.e('sqlite execute create Error: $e');
      },
    );

    await db
        .execute(
            'CREATE UNIQUE INDEX ${SqliteNewFriend.table}_frend_id_key ON ${SqliteNewFriend.table} (friend_id);')
        .then(
      (value) {},
      onError: (e) {
        Debug.e('sqlite execute create Error: $e');
      },
    );

    //红包退还
    await db.execute('''CREATE TABLE ${SqliteRefundRed.table} (id INTEGER PRIMARY KEY,
      receiver_type INTEGER,
      receiver_id INTEGER,
      user_id INTEGER,
      nick TEXT,
      money INTEGER,
      time INTEGER,
      is_reading INTEGER,
      rid TEXT);''').then(
      (value) {},
      onError: (e) {
        Debug.e('sqlite execute create Error: $e');
      },
    );

    ///红包数据
    await db.execute('''CREATE TABLE ${SqliteRedPacket.table} (id INTEGER PRIMARY KEY,
      receiver_type INTEGER,
      receiver_id INTEGER,
      sender_id INTEGER,
      avatar_url TEXT,
      nickname TEXT,
      extra TEXT,
      title TEXT,
      amount INTEGER,
      count INTEGER,
      rid TEXT,
      level INTEGER,
      send_time INTEGER);''').then(
      (value) {},
      onError: (e) {
        Debug.e('sqlite execute create Error: $e');
      },
    );

    //领取红包记录
    await db.execute('''CREATE TABLE ${SqliteRobRedPacket.table} (id INTEGER PRIMARY KEY,
      receiver_type INTEGER,
      receiver_id INTEGER,
      rid TEXT,
      rob_user_id INTEGER,
      amount INTEGER);''').then(
      (value) {},
      onError: (e) {
        Debug.e('sqlite execute create Error: $e');
      },
    );
  }

  Future<void> _onUpgrade(Database db, int version, int newVersion) async {
    Debug.d('sqlite onUpgrade(version: $version, newVersion: $newVersion)');
    if (version >= newVersion) return;
    String sql = '';
    int count = newVersion - version;
    for (var i = 0; i < count; i++) {
      int v = i + version + 1;
      if (2 == v) {
        sql = 'ALTER TABLE ${SqliteMessage.table} ADD COLUMN sender_seq INTEGER;';
      } else if (3 == v) {
        sql = 'ALTER TABLE ${SqliteMessage.table} ADD COLUMN i_receiver_id INTEGER;';
      } else if (4 == v) {
        sql = '''CREATE TABLE ${SqliteRefundRed.table} (id INTEGER PRIMARY KEY, 
        receiver_type INTEGER,
        receiver_id INTEGER,
        user_id INTEGER,
        nick TEXT,
        money INTEGER,
        time INTEGER);''';
      } else if (5 == v) {
        sql = 'ALTER TABLE ${SqliteRefundRed.table} ADD COLUMN is_reading INTEGER;';
      } else if (6 == v) {
        sql = 'ALTER TABLE ${SqliteRefundRed.table} ADD COLUMN rid TEXT;';
      } else if (7 == v) {
        sql = '''CREATE TABLE ${SqliteRedPacket.table} (id INTEGER PRIMARY KEY,
        receiver_type INTEGER,
        receiver_id INTEGER,
        sender_id INTEGER,
        avatar_url TEXT,
        nickname TEXT,
        extra TEXT,
        title TEXT,
        amount INTEGER,
        count INTEGER,
        rid TEXT,
        level INTEGER,
        send_time INTEGER);''';
      } else if (8 == v) {
        sql = '''CREATE TABLE ${SqliteRobRedPacket.table} (id INTEGER PRIMARY KEY,
        receiver_type INTEGER,
        receiver_id INTEGER,
        rid TEXT,
        rob_user_id INTEGER,
        amount INTEGER);''';
      }

      await db.execute(sql).then(
        (value) {},
        onError: (e) {
          Debug.e('sqlite execute init update create Error: $e');
        },
      );
    }
  }

  Future execute(String sql) async {
    if (_database != null) {
      // Debug.d('sqlite execute: $sql');
      _database?.execute(sql).then(
        (value) {
          // Debug.d('sqlite execute Succeed: $sql');
        },
        onError: (e) {
          Debug.e('sqlite execute Error: $e');
        },
      );
    } else {
      _sqlList.add(sql);
    }
  }

  Future<List<Map<String, dynamic>>> query(String sql) async {
    if (_database != null) {
      Debug.d('sqlite query $sql');
      return await _database!.rawQuery(sql).then(
        (value) {
          return value;
        },
        onError: (e) {
          Debug.e('sqlite execute query Error: $e');
        },
      );
    } else {
      return Future.value(<Map<String, Object?>>[]);
    }
  }
}
