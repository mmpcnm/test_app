// ignore_for_file: file_names
import 'package:fim_app/package/sqflite/sqlite_manager.dart';
import 'package:fim_app/model/chat/list/RefundRedList/RefundRedList.dart';

///红包退款数据库
class SqliteRefundRed {
  ///refund_red_notify
  static const String table = 'refund_red_notify';
  static final SqliteRefundRed _instatne = SqliteRefundRed._();
  SqliteRefundRed._();
  factory SqliteRefundRed() => _instatne;

  void insert(RefundRedInfo info) {
    print('insert  RefundRedInfo');
    SqliteManager().execute('''
    INSERT INTO $table (receiver_type,
    receiver_id,
    user_id,nick,
    money,
    time,
    is_reading,
    rid) VALUES (
      ${info.receiverType.value},
      ${info.receiverId.toInt()},
      ${info.userId.toInt()},
      "${info.nick}",
      ${info.money.toInt()},
      ${info.time.toInt()},
      ${info.isReading ? 1 : 0},
      "${info.rid}");
    ''');
  }

  Future delete() async {
    await SqliteManager().execute('DELETE FROM $table');
  }

  Future records() async {
    var values = await SqliteManager().query('SELECT * FROM $table');
    RefundRedList().init(values);
  }

  // 修改已读状态
  Future<void> updateReading({required bool isReading}) async {
    int reading = isReading ? 1 : 0;
    await SqliteManager().execute('UPDATE $table SET is_reading = $reading;');
  }

  Future deleteItem({required String rid}) async {
    await SqliteManager().execute('DELETE FROM $table WHERE rid = "$rid";');
  }
}
