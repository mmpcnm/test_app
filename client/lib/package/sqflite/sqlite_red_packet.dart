import 'package:fim_app/model/red_packet/red_packet_model.dart';

import 'sqlite_manager.dart';

///发红包数据库
class SqliteRedPacket {
  ///red_packet
  static const String table = 'red_packet';
  static final SqliteRedPacket _instatne = SqliteRedPacket._();
  SqliteRedPacket._();
  factory SqliteRedPacket() => _instatne;

  Future records() async {
    var values = await SqliteManager().query('SELECT * FROM $table');
    RedPacketModel().init(values);
  }

  Future insert(RedPacketInfo info) async {
    print('SqliteRedPacket insert');
    return await SqliteManager().execute('''INSERT INTO $table (receiver_type,
    receiver_id,
    sender_id,
    avatar_url,
    nickname,
    extra,
    title,
    amount,
    count,
    rid,
    level,
    send_time) VALUES (${info.receiverType.value},
    ${info.receiverId.toInt()},
    ${info.senderId.toInt()},
    "${info.avatarUrl}",
    "${info.nickname}",
    "${info.extra}",
    "${info.title}",
    ${info.amount.toInt()},
    ${info.count},
    "${info.rid}",
    ${info.level},
    ${info.sendTime.toInt()});''');
  }
}
