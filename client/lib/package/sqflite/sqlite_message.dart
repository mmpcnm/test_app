import 'package:fim_app/configs/api.dart';
import 'package:fim_app/model/chat/MessageModel.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/controller/logic.dart';
import 'package:fim_app/package/sqflite/sqlite_manager.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/package/util/time_utils.dart';
import 'package:fixnum/fixnum.dart';

///聊天消息数据库
class SqliteMessage {
  ///chat
  static const String table = 'chat';
  static final SqliteMessage _instatne = SqliteMessage._();
  SqliteMessage._();
  factory SqliteMessage() => _instatne;

  Future<void> insert(MessageInfo chatModel) async {
    await SqliteManager().execute('INSERT INTO $table ${chatModel.toSqlString()};');
  }

  // 删除单条信息
  Future<void> delete({required Int64 receiverId, required Int64 seqId}) async {
    await SqliteManager().execute(
        'DELETE FROM $table WHERE receiver_id = ${receiverId.toInt()} and seq = ${seqId.toInt()};');
  }

  // 删除聊天记录
  Future<void> deleteMessage({required Int64 receiverId}) async {
    await SqliteManager().execute('DELETE FROM $table WHERE receiver_id = ${receiverId.toInt()};');
  }

  // 修改一条聊天状态
  Future<void> updateStatus(
      {required Int64 receiverId, required Int64 seqId, required pb.MessageStatus status}) async {
    await SqliteManager().execute(
        'UPDATE $table SET status = ${status.value} WHERE receiver_id = ${receiverId.toInt()} and seq = ${seqId.toInt()};');
  }

  // 修改已读状态
  Future<void> updateReading(
      {required Int64 receiverId, required Int64 seqId, required bool isReading}) async {
    int reading = isReading ? 1 : 0;
    await SqliteManager().execute(
        'UPDATE $table SET is_reading = $reading WHERE receiver_id = ${receiverId.toInt()} and seq = ${seqId.toInt()};');
  }

  Future<void> updateAllReading({required Int64 receiverId, required bool isReading}) async {
    int reading = isReading ? 1 : 0;
    await SqliteManager().execute(
        'UPDATE $table SET is_reading = $reading WHERE receiver_id = ${receiverId.toInt()};');
  }

  Future<MessageInfo?> findMessage({required Int64 receiverId, required Int64 seqId}) async {
    List<Map<String, dynamic>> maps = await SqliteManager()
        .query('SELECT * FROM $table WHERE receiver_id = $receiverId and seq = $seqId');
    if (maps.isNotEmpty) {
      return MessageInfo.sqlMapToChat(maps[0]);
    }
    return null;
  }

  // 读取消息记录
  Future<void> records() async {
    List<Map<String, dynamic>> maps = await SqliteManager().query('SELECT * FROM $table');
    print('SqlChat records:$maps');
    List<MessageInfo> messages = [];
    for (var item in maps) {
      messages.add(MessageInfo.sqlMapToChat(item));
    }
    MessageModel().init(messages);
  }
}
