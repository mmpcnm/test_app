import 'package:fim_app/model/friend/new_friend_info.dart';
import 'package:fim_app/model/friend/new_friend_model.dart';
import 'package:fim_app/package/sqflite/sqlite_manager.dart';
import 'package:fixnum/fixnum.dart';

///好友请求数据库
class SqliteNewFriend {
  ///new_friend
  static const String table = 'new_friend';
  static final SqliteNewFriend _instatne = SqliteNewFriend._();
  SqliteNewFriend._();
  factory SqliteNewFriend() => _instatne;

  Future<void> insert(NewFriendInfo newFriendModel) async {
    await SqliteManager().execute('INSERT INTO $table ${newFriendModel.toSqlString()};');
  }

  //friendId is null 删除全部
  Future<void> delete({required Int64? friendId}) async {
    if (friendId == null) {
      await SqliteManager().execute('DELETE FROM $table');
    } else {
      await SqliteManager().execute('DELETE FROM $table WHERE receiver_id = ${friendId.toInt()};');
    }
  }

  Future<void> records() async {
    final List<Map<String, dynamic>> maps = await SqliteManager().query('SELECT * FROM $table');
    print('SqlNewFriend records:$maps');
    NewFriendModel.instance.clear();
    final List<NewFriendInfo> infos = [];
    for (var item in maps) {
      infos.add(NewFriendInfo.fromMap(item));
    }
    NewFriendModel().init(infos);
  }
}
