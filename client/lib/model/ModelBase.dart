// ignore_for_file: file_names, non_constant_identifier_names

import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:scoped_model/scoped_model.dart';

export 'package:scoped_model/scoped_model.dart';

abstract class ModelBase<T extends Model> extends Model {
  Timer? _timer;

  @override
  void notifyListeners({bool instantly = false}) {
    if (instantly) {
      cancel();
      super.notifyListeners();
    } else {
      _timer ??= Timer.periodic(const Duration(milliseconds: 5), (timer) => notifyListeners(instantly: true));
    }
  }

  void dispose() {
    cancel();
  }

  void cancel() {
    _timer?.cancel();
    _timer = null;
  }

  Widget buidler({Widget? child, bool rebuildOnChange = true, required ScopedModelDescendantBuilder<T> builder}) {
    assert(runtimeType != T.runtimeType);
    return ScopedModel<T>(
      model: this as T,
      child: ScopedModelDescendant<T>(child: child, rebuildOnChange: rebuildOnChange, builder: builder),
    );
  }

  static of<T extends ModelBase>(BuildContext context) => ScopedModel.of<T>(context);
}
