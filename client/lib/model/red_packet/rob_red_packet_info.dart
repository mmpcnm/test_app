import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fixnum/fixnum.dart';

///抢红包记录结构
class RobRedPacketInfo {
  // 聊天室类型，1：user;2:group
  final pb.ReceiverType receiverType;

  ///聊天室id
  final Int64 receiverId;

  ///红包id
  final String rid;

  ///抢红包用户id
  final Int64 robUserId;

  ///抢红包金额
  final Int64 amount;

  RobRedPacketInfo({
    required this.receiverType,
    required this.receiverId,
    required this.rid,
    required this.robUserId,
    required this.amount,
  });

  RobRedPacketInfo.fromMap(Map<String, dynamic> values)
      : receiverType = pb.ReceiverType.values[values['receiver_type']],
        receiverId = Int64(values['receiver_id']),
        rid = values['rid'],
        robUserId = Int64(values['rob_user_id']),
        amount = Int64(values['amount']);
}
