import 'package:fim_app/model/ModelBase.dart';
import 'package:fim_app/package/sqflite/sqlite_rob_red_packet.dart';
import 'package:fixnum/fixnum.dart';

import 'rob_red_packet_info.dart';

export 'rob_red_packet_info.dart';

///抢红包数据管理
class RobRedPacketModel extends ModelBase {
  static final RobRedPacketModel _instance = RobRedPacketModel._();
  RobRedPacketModel._();
  factory RobRedPacketModel() => _instance;
  final List<RobRedPacketInfo> _values = [];
  void init(List<Map<String, dynamic>> list) {
    print('RobRedPacketModel init $list');
    _values.clear();
    for (var item in list) {
      _values.add(RobRedPacketInfo.fromMap(item));
    }
  }

  bool push(RobRedPacketInfo info) {
    if (null != find(receiverId: info.receiverId, rid: info.rid, robUserId: info.robUserId)) {
      return false;
    }
    print('RobRedPacketModel push ${info.toString()}');
    _values.add(info);
    SqliteRobRedPacket().insert(info);
    return true;
  }

  RobRedPacketInfo? find({
    required Int64 receiverId,
    required String rid,
    required Int64 robUserId,
  }) {
    print('RobRedPacketModel find $receiverId, $rid, $robUserId');
    for (var item in _values) {
      if (item.rid == rid && receiverId == item.receiverId && robUserId == item.robUserId) {
        print('RobRedPacketModel find true');
        return item;
      }
    }
    print('RobRedPacketModel find false');
    return null;
  }

  ///获取领取的人数
  int getRobCount({required Int64 receiverId, required String rid}) {
    int count = 0;
    for (var item in _values) {
      if (item.rid == rid && receiverId == item.receiverId) {
        count++;
      }
    }
    return count;
  }

  void clear() {
    _values.clear();
  }
}
