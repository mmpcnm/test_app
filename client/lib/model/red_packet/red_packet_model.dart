import 'package:fim_app/model/ModelBase.dart';
import 'package:fim_app/package/sqflite/sqlite_red_packet.dart';
import 'package:fixnum/fixnum.dart';
import 'red_packet_info.dart';

export 'red_packet_info.dart';

class RedPacketModel extends ModelBase<RedPacketModel> {
  static final RedPacketModel _instance = RedPacketModel._();
  RedPacketModel._();
  factory RedPacketModel() => _instance;

  final List<RedPacketInfo> _values = [];
  void init(List<Map<String, dynamic>> list) {
    print('RedPacketModel init $list');
    _values.clear();
    for (var item in list) {
      _values.add(RedPacketInfo.fromMap(item));
    }
  }

  RedPacketInfo? findRed({required Int64 receiverId, required String rid}) {
    for (var item in _values) {
      // if (receiverId != null) {
      //   if (receiverId == item.receiverId && rid == item.rid) {
      //     return item;
      //   }
      // } else {
      if (item.rid == rid) {
        return item;
      }
      // }
    }
    return null;
  }

  bool push(RedPacketInfo info) {
    if (null != findRed(rid: info.rid, receiverId: info.receiverId)) {
      return false;
    }

    _values.add(info);
    SqliteRedPacket().insert(info);
    return true;
  }

  void clear() {
    _values.clear();
  }
}
