import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fixnum/fixnum.dart';

///红包状态
enum RedPacketStatus {
  ///无状态
  NONE,

  ///已领取
  COLLECT,
}

class RedPacketInfo {
  ///红包所在聊天室
  ///聊天室类型
  final pb.ReceiverType receiverType; // 接收者类型，1：user;2:group

  ///聊天室id
  final Int64 receiverId; // 用户id或者群组id

  ///发送者数据
  final Int64 senderId; // 发送者id

  ///发送者头像
  final String avatarUrl; // 头像
  ///发送者昵称
  final String nickname; // 昵称
  ///发送者扩展字段
  final String extra; // 扩展字段

  ///红包数据
  ///红包内容
  final String title; // 红包内容

  ///红包金额
  final Int64 amount; // 红包金额
  ///红包个数
  final int count;

  ///红包id
  final String rid; // 红包id, 发红包不用填写，群发后由服务器填写

  /// 红包等级, 可限定具有某些等级的玩家可抢
  final int level; // 红包等级, 可限定具有某些等级的玩家可抢

  ///发送时间
  final Int64 sendTime;

  RedPacketInfo({
    required this.receiverType,
    required this.receiverId,
    required this.senderId,
    required this.avatarUrl,
    required this.nickname,
    required this.extra,
    required this.title,
    required this.amount,
    required this.count,
    required this.rid,
    required this.level,
    required this.sendTime,
  });

  RedPacketInfo.fromMap(Map<String, dynamic> values)
      : receiverType = pb.ReceiverType.values[values['receiver_type']],
        receiverId = Int64(values['receiver_id']),
        senderId = Int64(values['sender_id']),
        avatarUrl = values['avatar_url'],
        nickname = values['nickname'],
        extra = values['extra'],
        title = values['title'],
        amount = Int64(values['amount']),
        count = values['count'],
        rid = values['rid'],
        level = values['level'],
        sendTime = Int64(values['send_time']);
}
