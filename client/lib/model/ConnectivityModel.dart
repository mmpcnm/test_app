// ignore_for_file: file_names

import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:fim_app/model/ModelBase.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

///网络状态
class ConnectivityModel extends ModelBase {
  ConnectivityResult _connectType = ConnectivityResult.none;
  StreamSubscription<ConnectivityResult>? _subscription;

  ConnectivityResult get connectType => _connectType;

  static final ConnectivityModel _instacne = ConnectivityModel._();
  ConnectivityModel._() {
    init();
  }

  factory ConnectivityModel() => _instacne;

  void init() async {
    if (_subscription != null) return;
    _subscription = Connectivity().onConnectivityChanged.listen(_onChanged);
  }

  @override
  void dispose() {
    if (_subscription != null) {
      _subscription?.cancel();
    }
    super.dispose();
  }

  Future<ConnectivityResult> checkConnectivity() async {
    _connectType = await Connectivity().checkConnectivity();
    notifyListeners();
    return _connectType;
  }

  void _onChanged(ConnectivityResult result) {
    _connectType = result;
    if (_connectType == ConnectivityResult.none) {
      EasyLoading.showError('当前无网络，请检查网络链接是否正常！');
    }
    notifyListeners();
  }
}
