import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/services.dart';
import 'package:package_info/package_info.dart';

// final DeviceInfo deviceInfo = DeviceInfo();

class DeviceInfo {
  static String get machineId => _instance._machineId; // 设备号（机器码）
  static int get modelType => _instance._modelType; // 设备类型 设备类型,1:Android；2：IOS；3：Windows; 4：MacOS；5：Web
  static String get brand => _instance._brand; // 厂商(苹果，小米，vivo ,oppo, 华为)
  static String get model => _instance._model; // 机型
  static String get systemVersion => _instance._systemVersion; // 系统版本
  static String get sdkVersion => _instance._sdkVersion; // sdk版本号
  static String get appVersion => _instance._appVersion;
  static String get appName => _instance._appName;
  static String get packageName => _instance._packageName;
  static String get buildNumber => _instance._buildNumber;
  static Future<void> init() async {
    await _instance._load();
  }

  _load() async {
    if (_isLoad) return;
    _isLoad = true;
    //获取设备信息
    final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
    try {
      if (Platform.isAndroid) {
        var build = await deviceInfoPlugin.androidInfo;
        _machineId = build.androidId;
        _modelType = 1;
        _brand = build.board;
        _model = build.model.replaceAll(RegExp(r'\s+\b|\b\s'), '_');
        _systemVersion = build.version.release;
        _sdkVersion = build.version.sdkInt.toString();
        //UUID for Android
      } else if (Platform.isIOS) {
        var build = await deviceInfoPlugin.iosInfo;
        _machineId = build.identifierForVendor;
        _modelType = 2;
        _brand = 'Apple';
        _model = build.model.replaceAll(RegExp(r'\s+\b|\b\s'), '_');
        _systemVersion = build.systemVersion;
        _sdkVersion = build.utsname.release;
      }
    } on PlatformException {
      print('Failed to get platform version');
    }

    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    _appName = packageInfo.appName;
    _packageName = packageInfo.packageName;
    _appVersion = packageInfo.version;
    _buildNumber = packageInfo.buildNumber;
  }

  static DeviceInfo _instance = DeviceInfo();

  bool _isLoad = false;
  String _machineId = ''; // 设备号（机器码）
  int _modelType = 3; // 设备类型 设备类型,1:Android；2：IOS；3：Windows; 4：MacOS；5：Web
  String _brand = ''; // 厂商(苹果，小米，vivo ,oppo, 华为)
  String _model = ''; // 机型
  String _systemVersion = ''; // 系统版本
  String _sdkVersion = ''; // sdk版本号

  String _appVersion = '';
  String _appName = '';
  String _packageName = '';
  String _buildNumber = '';
}
