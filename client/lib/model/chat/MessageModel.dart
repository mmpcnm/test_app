// ignore_for_file: import_of_legacy_library_into_null_safe, non_constant_identifier_names, file_names, invalid_use_of_protected_member

import 'dart:async';
import 'dart:math';

import 'package:fim_app/configs/api.dart';
import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/model/red_packet/red_packet_model.dart';
import 'package:fim_app/model/red_packet/rob_red_packet_model.dart';
import 'package:fim_app/package/handler/Handler.dart';
import 'package:fim_app/package/handler/HandlerCode.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/controller/logic.dart';
import 'package:fim_app/model/chat/ChatCancelList.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:fim_app/model/user/user_setting.dart';
import 'package:fim_app/package/pb/push.ext.pb.dart' as pb;
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/package/util/time_utils.dart';
import 'package:fixnum/fixnum.dart';
import 'package:vibration/vibration.dart';

import '../ModelBase.dart';
import 'list/Base/MessageListBase.dart';
import 'list/MessageList/MessageInfoList.dart';
import 'list/RefundRedList/RefundRedList.dart';

export 'list/MessageList/MessageInfoList.dart';
export 'list/RefundRedList/RefundRedList.dart';

class MessageModel extends ModelBase<MessageModel> {
  static final MessageModel _instance = MessageModel._();
  factory MessageModel() => _instance;
  MessageModel._() {
    GlobalHandler().addListenable(HandlerCode.SEND_CHAT_MESSAGE, _on_SEND_CHAT_MESSAGE);
    GlobalHandler().addListenable(HandlerCode.CANCEL_MESSAGE, _on_CANCEL_MESSAGE);
    GlobalHandler().addListenable(HandlerCode.REFUND_RED_MESSAGE, _on_REFUND_RED_MESSAGE);
  }

  ///红包退款
  void _on_REFUND_RED_MESSAGE(HandlerCode key, dynamic value) {
    pb.RefundRedPacketPush push = value;

    ///红包退款
    if (RefundRedList().push(RefundRedInfo(
      receiverType: push.receiverType,
      receiverId: push.receiverId,
      userId: push.userId,
      rid: push.rid,
      money: push.money,
      nick: push.nick,
      time: Int64(TimeUtils.serverTime),
    ))) {
      _vibrate();
    }
  }

  ///撤回
  void _on_CANCEL_MESSAGE(HandlerCode key, dynamic value) {
    pb.CancelMessagePush push = value;
    if (!(MessageModel().find<MessageInfoList>(push.receiverId.toString())?.setState(
              senderId: push.optId,
              receiverType: push.receiverType,
              senderSeq: push.seqId,
              status: pb.MessageStatus.MS_RECALL,
            ) ??
        false)) {
      if (push.receiverType == pb.ReceiverType.RT_USER) {
        MessageModel().find<MessageInfoList>(push.optId.toString())?.setState(
              senderId: push.optId,
              receiverType: push.receiverType,
              senderSeq: push.seqId,
              status: pb.MessageStatus.MS_RECALL,
            );
      }
    }
    ChatCancelList().push(push);
  }

  ///消息
  void _on_SEND_CHAT_MESSAGE(HandlerCode key, dynamic value) {
    MessageInfo info = value as MessageInfo;
    Debug.d('key: $key, senderId: ${info.senderId}, sendState: ${info.sendState}');

    //发红包
    if (info.messageType == pb.MessageType.MT_SEND_REDPACKET) {
      pb.RedPacket redPacket = pb.RedPacket.fromBuffer(info.messageContent);
      RedPacketModel().push(RedPacketInfo(
        receiverType: info.receiverType,
        receiverId: info.receiverId,
        senderId: info.senderId,
        avatarUrl: info.avatarUrl,
        nickname: info.nickname,
        extra: info.extra,
        title: redPacket.title,
        amount: redPacket.amount,
        count: redPacket.num,
        rid: redPacket.rid,
        level: redPacket.level,
        sendTime: info.sendTime,
      ));
    }

    //抢红包
    if (info.messageType == pb.MessageType.MT_ROB_REDPACKET) {
      pb.RobRedPacket robRedPacket = pb.RobRedPacket.fromBuffer(info.messageContent);
      RobRedPacketModel().push(RobRedPacketInfo(
        receiverType: info.receiverType,
        receiverId: info.receiverId,
        rid: robRedPacket.rid,
        robUserId: info.senderId,
        amount: robRedPacket.amount,
      ));
    }

    MessageInfoList infoList = _findInfoList(info.receiverId);
    if (info.senderId == UserModel.userId) {
      info.isReading = true;
      if (LogicSendState.NONE == info.sendState) {
        infoList.removeTemporary(
          sendId: info.senderId,
          receiverId: info.receiverId,
          sendTime: info.sendTime,
        );
        Debug.d('添加自己的消息 ${info.toString()}');
        infoList.push(info);
      } else {
        Debug.d('发送中或发送失败的消息');
        if (infoList.pushTemporary(info)) {
          Debug.d('添加临时信息');
        }
      }
    } else {
      Debug.d('添加其他人的消息 ${info.toString()}');
      if (infoList.push(info)) {
        _vibrate();
      }
    }
  }

  Future _vibrate() async {
    if (await Vibration.hasVibrator() && UserSetting.isShake) {
      Vibration.vibrate(pattern: [0, 200, 35, 150]);
    }
  }

  void clear() {
    _mItems.clear();
    RefundRedList().clear(isSendListeners: false);
    ChatCancelList().clear();
    RedPacketModel().clear();
    RobRedPacketModel().clear();
    cancel();
  }

  final List<MessageListBase> _mItems = [];

  void init(List<MessageInfo> messages) {
    _mItems.clear();
    for (var item in messages) {
      MessageInfoList infos = _findInfoList(item.receiverId);
      infos.values.add(item);
    }
    notifyListeners();
  }

  MessageInfoList _findInfoList(Int64 id) {
    for (var list in _mItems) {
      if (list.key == id.toString()) {
        return list as MessageInfoList;
      }
    }
    var list = MessageInfoList(id: id);
    _mItems.add(list);
    return list;
  }

  ///删除子列表
  bool remove(String key, {required bool isSqlite}) {
    var value = find(key);
    if (value != null) {
      if (isSqlite) {
        value.delete();
      } else {
        value.clear();
      }

      return true;
    }
    return false;
  }

  ///获取子列表
  T? find<T extends MessageListBase>(String key) {
    MessageListBase? listBase;
    if (key == RefundRedList().key) {
      listBase = RefundRedList();
    } else {
      for (var list in _mItems) {
        if (list.key == key) {
          listBase = list;
          break;
        }
      }
    }
    try {
      T? ret = listBase as T?;
      return ret;
    } catch (e) {
      print('error' + e.toString());
    }

    return null;
  }

  ///获取全部子列表
  List<MessageListBase> getRootItems() {
    List<MessageListBase> items = [];
    if (RefundRedList().getValues.isNotEmpty) {
      print('push RefundRedList');
      items.add(RefundRedList());
    }
    for (var item in _mItems) {
      if (item.getValues.isNotEmpty) {
        items.add(item);
      }
    }
    items.sort((a, b) => b.updateTime.compareTo(a.updateTime));
    return items;
  }

  ///获取未读总数
  int getAllUnreadCount() {
    int count = 0;
    var array = getRootItems();
    for (var item in array) {
      count += item.unreadCount;
    }
    return min(count, 99);
  }

  void insertWelcomeMessage() {
    if (UserModel.firstOpenApp) {
      print('insertWelcomeMessage');
      int sendTime = TimeUtils.serverTime; //DateTime.now().millisecondsSinceEpoch;
      MessageInfo chatModel = MessageInfo.newModel(
        // onlyKey: '${AppConfig.channelId}_$sendTime',
        senderType: pb.SenderType.ST_SYSTEM,
        senderId: AppConfig.channelId,
        avatarUrl: '',
        nickname: STR(10262),
        extra: '',
        receiverType: pb.ReceiverType.RT_ROOM,
        receiverId: AppConfig.channelId,
        iReceiverId: AppConfig.channelId,
        toUserIds: [],
        messageType: pb.MessageType.MT_TEXT,
        messageContent: pb.Text(text: STR(10168)).writeToBuffer(),
        seq: Int64.ZERO,
        sendTime: Int64(sendTime),
        msg_status: pb.MessageStatus.MS_NORMAL,
        senderSeq: Int64(0),
        localSendTime: sendTime,
        sendState: LogicSendState.NONE,
      );
      GlobalHandler().sendMessage(HandlerCode.SEND_CHAT_MESSAGE, value: chatModel);
    }

    UserModel.firstOpenApp = false;
  }
}
