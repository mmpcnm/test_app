// ignore_for_file: file_names

import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/model/chat/list/Base/MessageInfoBase.dart';
import 'package:fixnum/fixnum.dart';

class RefundRedInfo extends MessageInfoBase {
  pb.ReceiverType receiverType;
  Int64 receiverId;
  Int64 userId;
  String rid; //红包id
  Int64 money;
  String nick;
  Int64 time;
  bool isReading;
  RefundRedInfo({
    required this.receiverType,
    required this.receiverId,
    required this.userId,
    required this.rid,
    required this.money,
    required this.nick,
    required this.time,
    this.isReading = false,
  }) : super(mInfoType: MessageInfoType.REFUND_RED);

  RefundRedInfo.fromJson(Map<String, dynamic> json)
      : receiverType = pb.ReceiverType.values[json['receiver_type']],
        receiverId = Int64(json['receiver_id']),
        userId = Int64(json['user_id']),
        rid = json['rid'] ?? '',
        money = Int64(json['money']),
        nick = json['nick'],
        time = Int64(json['time']),
        isReading = (json['is_reading'] ?? 1) != 0,
        super(mInfoType: MessageInfoType.REFUND_RED);

  Map<String, dynamic> toJson() => {
        'receiver_type': receiverType.value,
        'receiver_id': receiverId.toInt(),
        'user_id': userId.toInt(),
        'nick': nick,
        'money': money.toInt(),
        'time': time.toInt(),
        'is_reading': isReading ? 1 : 0,
        'rid': rid,
      };
}
