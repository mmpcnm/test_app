// ignore_for_file: file_names, non_constant_identifier_names

import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/package/sqflite/sqlite_refund_red.dart';
import 'package:fixnum/fixnum.dart';

import '../../MessageModel.dart';
import '../Base/MessageListBase.dart';

export 'RefundRedInfo.dart';

class RefundRedList extends MessageListBase<RefundRedInfo> {
  static final RefundRedList _instance = RefundRedList._();
  RefundRedList._() : super(key: 'RefundRedList', infoType: MessageInfoType.REFUND_RED, values: []);
  factory RefundRedList() => _instance;

  void init(List<Map<String, dynamic>> datas) {
    clear();
    for (var item in datas) {
      try {
        var push = RefundRedInfo.fromJson(item);
        values.add(push);
      } catch (e) {
        Debug.e('ChatRefundRedNotifyList setList: $e');
      }
    }
  }

  @override
  bool push(RefundRedInfo value) {
    if (!isItem(
        receiverType: value.receiverType,
        receiverId: value.receiverId,
        userId: value.userId,
        rid: value.rid)) {
      values.add(value);
      print('RefundRedInfo push value');
      SqliteRefundRed().insert(value);
      MessageModel().notifyListeners();
      return true;
    }
    return false;
  }

  /// ReceiverType receiver_type 聊天室类型
  /// int64 receiver_id 用户id或者群组id或者聊天室id
  /// int64 user_id  发红包玩家id
  /// string rid 红包id
  bool isItem({
    required pb.ReceiverType receiverType,
    required Int64 receiverId,
    required Int64 userId,
    required String rid,
  }) {
    for (var item in values) {
      if (item.receiverType == receiverType &&
          item.receiverId == receiverId &&
          item.userId == userId &&
          item.rid.isNotEmpty &&
          item.rid == rid) {
        return true;
      }
    }
    return false;
  }

  @override
  void clear({bool isSendListeners = true}) {
    values.clear();
    if (isSendListeners) {
      MessageModel().notifyListeners();
    }
  }

  @override
  Int64 get updateTime {
    // if (values.isEmpty) return Int64(0);
    var items = getValues;
    if (items.isEmpty) return Int64(0);
    items.sort((a, b) => a.time.compareTo(b.time));
    return items.last.time;
  }

  @override
  int get unreadCount {
    int count = 0;
    var items = getValues;
    for (var item in items) {
      if (item.userId == UserModel.userId && !item.isReading) count++;
    }
    return count;
  }

  @override
  bool setReadings() {
    for (var item in values) {
      if (!item.isReading) {
        item.isReading = true;
      }
    }
    SqliteRefundRed().updateReading(isReading: true);
    MessageModel().notifyListeners();
    return false;
  }

  @override
  void delete() {
    values.clear();
    SqliteRefundRed().delete();
    MessageModel().notifyListeners();
  }

  @override
  List<RefundRedInfo> get getValues {
    List<RefundRedInfo> ret = []; // = List.from(values);
    for (var item in values) {
      if (item.userId == UserModel.userId) {
        ret.add(item);
      }
    }
    ret.sort((a, b) => a.time.compareTo(b.time));
    return ret;
  }
}
