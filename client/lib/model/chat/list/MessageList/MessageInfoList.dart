// ignore_for_file: file_names

import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/package/net/controller/logic.dart';
import 'package:fim_app/package/sqflite/sqlite_message.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/model/user/user_model.dart';
import 'package:fixnum/fixnum.dart';

import '../../MessageModel.dart';
import '../Base/MessageListBase.dart';

export 'MessageInfo.dart';

class MessageInfoList extends MessageListBase<MessageInfo> {
  final Int64 id;
  // final List<MessageInfo> _tmpValues = [];
  MessageInfoList({required this.id})
      : super(key: id.toString(), infoType: MessageInfoType.MESSAGE, values: []);
  @override
  bool push(MessageInfo value) {
    if (find(seq: value.seq) == null) {
      values.add(value);
      SqliteMessage().insert(value);
      MessageModel().notifyListeners();
      return true;
    }
    return false;
  }

  MessageInfo? find({required Int64 seq}) {
    int index = findIndex(seq: seq);
    if (index != -1) {
      return values[index];
    }
    return null;
  }

  int findIndex({required Int64 seq}) {
    for (int index = 0; index < values.length; ++index) {
      if (values[index].seq == seq) {
        return index;
      }
    }
    return -1;
  }

  bool remove({required Int64 seq}) {
    int index = findIndex(seq: seq);
    print('MessageInfoList remove $index');
    if (index != -1) {
      values.removeAt(index);
      SqliteMessage().delete(receiverId: id, seqId: seq);
      MessageModel().notifyListeners();
      return true;
    }
    return false;
  }

  @override
  Int64 get updateTime {
    if (values.isEmpty) return Int64(0);
    values.sort((a, b) => a.sendTime.compareTo(b.sendTime));
    return values.last.sendTime;
  }

  @override
  bool setReadings() {
    for (var value in values) {
      if (!value.isReading) {
        value.isReading = true;
      }
    }
    SqliteMessage().updateAllReading(receiverId: id, isReading: true);
    MessageModel().notifyListeners();
    return true;
  }

  bool setReading({required Int64 seq, bool isSql = true}) {
    var value = find(seq: seq);
    if (value != null && !value.isReading) {
      value.isReading = true;
      if (isSql) {
        SqliteMessage().updateReading(receiverId: id, seqId: seq, isReading: true);
      }
      return true;
    }
    return false;
  }

  bool setState({
    required Int64 senderId,
    required Int64 senderSeq,
    required pb.ReceiverType receiverType,
    required pb.MessageStatus status,
  }) {
    for (var item in values) {
      if (item.senderId == senderId &&
          item.senderSeq == senderSeq &&
          receiverType == item.receiverType) {
        item.status = status;
        SqliteMessage().updateStatus(receiverId: item.receiverId, seqId: item.seq, status: status);

        MessageModel().notifyListeners();
        return true;
      }
    }
    return false;
  }

  @override
  int get unreadCount {
    int count = 0;
    for (var item in values) {
      if (!item.isReading && item.senderId != UserModel.userId) {
        count++;
      }
    }
    return count;
  }

  @override
  void clear({bool isSendListeners = true}) {
    values.clear();
    // SqlChat.instatne.deleteChat(receiverId: id);
    if (isSendListeners) {
      MessageModel().notifyListeners();
    }
  }

  ///删除临时信息
  bool removeTemporary(
      {required Int64 sendId, required Int64 receiverId, Int64? sendTime, int? localSendTime}) {
    for (int i = 0; i < values.length; ++i) {
      var item = values[i];
      if (item.receiverId == receiverId &&
          item.senderId == sendId &&
          item.sendState != LogicSendState.NONE) {
        if ((sendTime != null && sendTime == item.sendTime) ||
            (localSendTime != null && localSendTime == item.localSendTime)) {
          print('removeTemporary message ');
          values.removeAt(i);
          MessageModel().notifyListeners();
          return true;
        }
      }
    }
    return false;
  }

  ///查找临时信息
  MessageInfo? findTemporary(
      {required Int64 sendId, required Int64 receiverId, Int64? sendTime, int? localSendTime}) {
    for (int i = 0; i < values.length; ++i) {
      var item = values[i];
      if (item.receiverId == receiverId && item.senderId == sendId) {
        if ((sendTime != null && sendTime == item.sendTime) ||
            (localSendTime != null && localSendTime == item.localSendTime)) {
          return item;
        }
      }
    }
    return null;
  }

  ///压入临时信息
  bool pushTemporary(MessageInfo tmpInfo) {
    if (tmpInfo.messageType != pb.MessageType.MT_TEXT &&
        tmpInfo.messageType != pb.MessageType.MT_IMAGE) {
      return false;
    }
    if (removeTemporary(
        sendId: tmpInfo.senderId,
        receiverId: tmpInfo.receiverId,
        localSendTime: tmpInfo.localSendTime)) {
      values.add(tmpInfo);
      MessageModel().notifyListeners();
      return true;
    } else {
      return false;
    }
  }

  ///获取消息列表图片
  List<pb.Image> getImageUrls() {
    List<pb.Image> urls = [];
    for (var item in values) {
      if (item.messageType == pb.MessageType.MT_IMAGE) {
        pb.Image image = pb.Image.fromBuffer(item.messageContent);
        urls.add(image);
      }
    }
    return urls;
  }

  ///获取音频消息文件路劲列表
  ///isUnread 是否只获取未读消息
  List<String> getVoiceUrls({bool isUnread = true}) {
    List<String> urls = [];
    for (var item in values) {
      if (item.messageType == pb.MessageType.MT_VOICE) {
        if (isUnread) {
          if (!item.isReading) {
            pb.Voice voice = pb.Voice.fromBuffer(item.messageContent);
            urls.add(voice.url);
          }
        } else {
          pb.Voice voice = pb.Voice.fromBuffer(item.messageContent);
          urls.add(voice.url);
        }
      }
    }
    return urls;
  }

  // ///是否已经枪过的红包
  // bool isSelfRedPacket(String redId) {
  //   for (var item in values) {
  //     if (item.messageType == pb.MessageType.MT_ROB_REDPACKET &&
  //         item.senderId == UserModel.userId) {
  //       pb.RobRedPacket redPacketItem = pb.RobRedPacket.fromBuffer(item.messageContent);
  //       if (redPacketItem.rid == redId) {
  //         return true;
  //       }
  //     }
  //   }
  //   return false;
  // }

  // int getRobRedPacketCount(String redId) {
  //   int count = 0;
  //   for (var item in values) {
  //     if (item.messageType == pb.MessageType.MT_ROB_REDPACKET) {
  //       pb.RobRedPacket redPacketItem = pb.RobRedPacket.fromBuffer(item.messageContent);
  //       if (redPacketItem.rid == redId) {
  //         count++;
  //       }
  //     }
  //   }
  //   return count;
  // }

  // //获取红包
  // MessageInfo? getRedPacke(String redId) {
  //   for (var item in values) {
  //     if (item.messageType == pb.MessageType.MT_SEND_REDPACKET) {
  //       pb.RedPacket redPacketItem = pb.RedPacket.fromBuffer(item.messageContent);
  //       if (redPacketItem.rid == redId) {
  //         return item;
  //       }
  //     }
  //   }

  //   return null;
  // }

  @override
  void delete() {
    values.clear();
    SqliteMessage().deleteMessage(receiverId: id);
    MessageModel().notifyListeners();
  }

  @override
  List<MessageInfo> get getValues {
    List<MessageInfo> ret = List.from(values);
    ret.sort((a, b) => a.sendTime.compareTo(b.sendTime));
    return ret;
  }
}
