// ignore_for_file: file_names

import 'dart:convert';
import 'dart:typed_data';

import 'package:fim_app/package/net/controller/logic.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/package/sqflite/sqlite_message.dart';
import 'package:fim_app/model/chat/list/Base/MessageInfoBase.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:fim_app/package/util/time_utils.dart';
import 'package:fixnum/fixnum.dart';

class MessageInfo extends MessageInfoBase {
  //发送者
  pb.SenderType senderType; // 发送者类型，1:系统,2:用户,3:第三方业务系统
  Int64 senderId; // 发送者id
  String avatarUrl; // 头像
  String nickname; // 昵称
  String extra; // 扩展字段
  Int64 senderSeq; // 发送者消息序列号，用于撤销消息

  pb.ReceiverType receiverType; // 接收者类型，1：user;2:group
  Int64 receiverId; // 用户id或者群组id
  Int64 iReceiverId; // 用户id或者群组id(实际的id 用于好友私聊)
  List<Int64> toUserIds; // 需要@的用户id列表
  pb.MessageType messageType; // 消息类型
  Uint8List messageContent; // 消息内容
  Int64 seq; // 用户消息发送序列号
  Int64 sendTime; // 消息发送时间戳，精确到毫秒
  // String onlyKey;
  int localSendTime;
  LogicSendState sendState;

  // 消息状态
  pb.MessageStatus _status = pb.MessageStatus.MS_NORMAL;
  pb.MessageStatus get status => _status;
  set status(value) {
    if (value != _status) {
      _status = value;
      // SqlChat().updateStatus(receiverId: receiverId, seqId: seq, status: _status);
    }
  }

  //是否已阅读
  bool _isReading = false;
  bool get isReading => _isReading;
  set isReading(value) {
    if (value != _isReading) {
      _isReading = value;
      // SqlChat.instatne.updateReading(receiverId: receiverId, onlyKey: onlyKey, isReading: _isReading);
    }
  }

  // String set sendState()
  String getOnlyKey() {
    return '${senderId}_$sendTime';
  }

  MessageInfo.newModel(
      {
      // required this.onlyKey,
      required this.senderType,
      required this.senderId,
      required this.avatarUrl,
      required this.nickname,
      required this.extra,
      required this.receiverType,
      required this.receiverId,
      required this.iReceiverId,
      required this.toUserIds,
      required this.messageType,
      required this.messageContent,
      required this.seq,
      required this.sendTime,
      required this.senderSeq,
      required this.localSendTime,
      required pb.MessageStatus msg_status,
      bool is_reading = false,
      this.sendState = LogicSendState.NONE})
      : super(mInfoType: MessageInfoType.MESSAGE) {
    _isReading = is_reading;
    _status = msg_status;
  }

  static MessageInfo fromMessage(pb.Message message) {
    Int64 receiverId = message.receiverId;
    if (message.receiverType == pb.ReceiverType.RT_USER) {
      if (receiverId == UserModel.userId) {
        receiverId = message.sender.senderId;
      }
    }

    return MessageInfo.newModel(
      // onlyKey: '${message.sender.senderId}_${message.sendTime}',
      senderType: message.sender.senderType,
      senderId: message.sender.senderId,
      avatarUrl: message.sender.avatarUrl,
      nickname: message.sender.nickname,
      extra: message.sender.extra,
      receiverType: message.receiverType,
      receiverId: receiverId, //message.receiverId,
      iReceiverId: message.receiverId,
      toUserIds: List.from(message.toUserIds),
      messageType: message.messageType,
      messageContent: Uint8List.fromList(message.messageContent),
      seq: message.seq,
      sendTime: message.sendTime,
      msg_status: message.status,
      is_reading: false,
      localSendTime: message.sendTime.toInt(),
      senderSeq: message.senderSeq,
    );
  }

  static MessageInfo formModel(MessageInfo model) {
    return MessageInfo.newModel(
      // onlyKey: model.onlyKey,
      senderType: model.senderType,
      senderId: model.senderId,
      avatarUrl: model.avatarUrl,
      nickname: model.nickname,
      extra: model.extra,
      receiverType: model.receiverType,
      receiverId: model.receiverId,
      iReceiverId: model.iReceiverId,
      toUserIds: List.from(model.toUserIds),
      messageType: model.messageType,
      messageContent: Uint8List.fromList(model.messageContent),
      seq: model.seq,
      sendTime: model.sendTime,
      msg_status: model.status,
      is_reading: model._isReading,
      senderSeq: model.senderSeq,
      localSendTime: model.localSendTime,
      sendState: model.sendState,
    );
  }

  // String get sendTimeStr {
  //   DateTime updateTime = DateTime.fromMillisecondsSinceEpoch(sendTime.toInt());
  //   // print('spch:${milliseconds}, updateTime: ${updateTime.toString()}');
  //   String suptime = updateTime.toString().substring(6, 16);
  //   return suptime;
  // }

  String getToUserIdsStr() {
    String userIds = '';
    for (var element in toUserIds) {
      if (userIds.isEmpty) {
        userIds = element.toString();
      } else {
        userIds = '$userIds,$element';
      }
    }
    return userIds;
  }

  String toSqlString() {
    String userIds = getToUserIdsStr();
    String retStr = '''(only_key,
        sender_type,
        sender_id,
        avatar_url,
        nickname,
        extra,
        receiver_type,
        receiver_id,
        to_user_ids,
        message_type,
        message_content,
        seq,send_time,
        status,
        is_reading,
        sender_seq,
        i_receiver_id)
         VALUES ("${getOnlyKey()}",
    ${senderType.value},
    ${senderId.toInt()},
    "$avatarUrl",
    "$nickname",
    "$extra",
    ${receiverType.value},
    ${receiverId.toInt()},
    "$userIds",
    ${messageType.value},
    "$messageContent",
    ${seq.toInt()},
    ${sendTime.toInt()},
    ${status.value},
    ${isReading ? 1 : 0},
    ${senderSeq.toInt()},
    ${iReceiverId.toInt()}
    )''';
    return retStr;
  }

  // static Uint8List _stringEncode(String s) {
  //   var encodedString = utf8.encode(s);
  //   var encodedLength = encodedString.length;
  //   var data  = ByteData(encodedLength + 4);
  //   data.setUint32(0, encodedLength, Endian.big);
  //   var bytes = data.buffer.asUint8List();
  //   bytes.setRange(4, encodedLength, encodedString);
  //   return bytes;
  // }

  static MessageInfo sqlMapToChat(Map<String, dynamic> sqlMap) {
    String only_key = sqlMap['only_key'];
    int sender_type = sqlMap['sender_type'];
    int sender_id = sqlMap['sender_id'];
    String avatar_url = sqlMap['avatar_url'];
    String nickname = sqlMap['nickname'];
    String extra = sqlMap['extra'];
    int receiver_type = sqlMap['receiver_type'];
    int receiver_id = sqlMap['receiver_id'];
    String to_user_ids = sqlMap['to_user_ids'];
    int message_type = sqlMap['message_type'];

    Uint8List message_content;
    try {
      List<dynamic> data = jsonDecode(sqlMap['message_content']);
      // Debug.w(data);
      message_content = Uint8List.fromList(data.cast<int>());
    } catch (e) {
      message_content = sqlMap['message_content'];
    }

    // Uint8List message_content = _stringEncode(msgCentent);
    int seq = sqlMap['seq'];
    int send_time = sqlMap['send_time'];
    int status = sqlMap['status'];
    int is_reading = sqlMap['is_reading'];
    int sender_seq = sqlMap['sender_seq'] ?? seq;
    int i_receiver_id = sqlMap['i_receiver_id'] ?? receiver_id;

    List<Int64> userIds = [];
    List<String> strlist = to_user_ids.split(',');
    print('ChatModel to_user_ids:$to_user_ids, $strlist');
    for (var element in strlist) {
      int? value = int.tryParse(element);
      if (value != null) userIds.add(Int64(value));
    }

    int local_send_time = send_time;
    // int send_state = sqlMap['send_state'] ?? 0;

    return MessageInfo.newModel(
      // onlyKey: only_key,
      senderType: pb.SenderType.valueOf(sender_type) ?? pb.SenderType.ST_UNKNOWN,
      senderId: Int64(sender_id),
      avatarUrl: avatar_url,
      nickname: nickname,
      extra: extra,
      receiverType: pb.ReceiverType.valueOf(receiver_type) ?? pb.ReceiverType.RT_UNKNOWN,
      receiverId: Int64(receiver_id),
      iReceiverId: Int64(i_receiver_id),
      toUserIds: userIds,
      messageType: pb.MessageType.valueOf(message_type) ?? pb.MessageType.MT_UNKNOWN,
      messageContent: message_content,
      seq: Int64(seq),
      sendTime: Int64(send_time),
      msg_status: pb.MessageStatus.valueOf(status) ?? pb.MessageStatus.MS_UNKNOWN,
      senderSeq: Int64(sender_seq),
      is_reading: is_reading != 0,
      localSendTime: local_send_time,
      sendState: LogicSendState.NONE,
    );
  }

  @override
  String toString() {
    return '''MessageInfo:{
      only_key: ${getOnlyKey()},
      sender_type: $senderType,
      sender_id: ${senderId.toInt()},
      avatar_url: $avatarUrl,
      nickname: $nickname,
      extra: $extra,
      receiver_type: $receiverType,
      receiver_id: ${receiverId.toInt()},
      to_user_ids: ${getToUserIdsStr()},
      message_type: $messageType,
      message_content: $messageContent,
      seq: ${seq.toInt()},
      send_time: ${sendTime.toInt()},
      status: $status,
      is_reading: $isReading,
      sender_seq: ${senderSeq.toInt()},
      i_receiver_id: ${iReceiverId.toInt()},
    }''';
  }

  ///是否可以撤回
  bool isPermittedCancel() {
    if (isTemporary()) {
      return false;
    }

    if (messageType != pb.MessageType.MT_TEXT &&
        messageType != pb.MessageType.MT_VOICE &&
        messageType != pb.MessageType.MT_IMAGE) {
      return false;
    }

    if (((TimeUtils.serverTime - sendTime.toInt()).abs() >
        const Duration(minutes: 2).inMilliseconds)) {
      return false;
    }

    return true;
  }

  ///是否是临时消息
  bool isTemporary() => sendState != LogicSendState.NONE;

  ///是否容许删除记录
  bool isPermittedDelete() {
    if (sendState == LogicSendState.NONE || sendState == LogicSendState.ERROR) {
      if (messageType == pb.MessageType.MT_TEXT ||
          messageType == pb.MessageType.MT_VOICE ||
          messageType == pb.MessageType.MT_IMAGE) {
        return true;
      }
    }
    return false;
  }
}
