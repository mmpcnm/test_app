// ignore_for_file: file_names

import 'package:fixnum/fixnum.dart';
import 'package:flutter/cupertino.dart';

import 'MessageInfoType.dart';

export 'MessageInfoType.dart';

abstract class MessageListBase<T> {
  final String key;
  final MessageInfoType infoType;
  @protected
  final List<T> values;
  MessageListBase({required this.key, required this.infoType, required this.values});

  bool push(T value);

  List<T> get getValues;

  Int64 get updateTime;

  int get unreadCount;

  ///clear 不会清除数据库记录
  void clear({bool isSendListeners = true});

  bool setReadings();

  ///清除数据库记录
  void delete();
}
