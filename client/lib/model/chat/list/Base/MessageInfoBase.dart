// ignore_for_file: file_names, constant_identifier_names
import 'MessageInfoType.dart';

export 'MessageInfoType.dart';

abstract class MessageInfoBase {
  final MessageInfoType mInfoType;
  MessageInfoBase({required this.mInfoType});
}
