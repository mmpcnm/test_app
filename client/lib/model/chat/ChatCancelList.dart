// ignore_for_file: file_names, non_constant_identifier_names

import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/package/pb/push.ext.pb.dart' as pb;
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fixnum/fixnum.dart';

class ChatCancelList {
  static final ChatCancelList _instatnce = ChatCancelList._();
  ChatCancelList._();
  factory ChatCancelList() => _instatnce;

  final List<pb.CancelMessagePush> _mCancelMessages = [];
  void push(pb.CancelMessagePush push) {
    Debug.d('ChatCancelList push $push');
    if (find(sender_id: push.optId, sender_seq: push.seqId, receiverType: push.receiverType, receiverId: push.receiverId) == null) {
      _mCancelMessages.add(push);
    }
  }

  pb.CancelMessagePush? find({
    required Int64 sender_id,
    required Int64 sender_seq,
    required pb.ReceiverType receiverType,
    Int64? receiverId,
  }) {
    for (var item in _mCancelMessages) {
      if (item.optId == sender_id && item.seqId == sender_seq && item.receiverType == receiverType) {
        if (receiverId != null) {
          if (receiverId == item.receiverId) {
            return item;
          }
        } else {
          return item;
        }
      }
    }
    return null;
  }

  void removeItem(pb.CancelMessagePush item) {
    _mCancelMessages.remove(item);
  }

  void remove({
    required Int64 sender_id,
    required Int64 sender_seq,
    required pb.ReceiverType receiverType,
    Int64? receiverId,
  }) {
    for (var i = 0; i < _mCancelMessages.length; i++) {
      var item = _mCancelMessages[i];
      if (item.optId == sender_id && item.seqId == sender_seq && item.receiverType == receiverType) {
        if (receiverId != null) {
          if (receiverId == item.receiverId) {
            _mCancelMessages.removeAt(i);
            return;
          }
        } else {
          _mCancelMessages.removeAt(i);
          return;
        }
      }
    }
  }

  void clear() {
    _mCancelMessages.clear();
  }
}
