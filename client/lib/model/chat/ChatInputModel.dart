// ignore_for_file: constant_identifier_names, file_names

import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/pages/chat/chat_page.dart';
import 'package:fim_app/model/ModelBase.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

enum ChatEditorState {
  UNKNOWN, // 空
  EMOJI, // 表情
  KEYBOARD, // 键盘
  // HIDE_KEYBOARD, // 键盘
  VOICE, // 语音
  ADDITION,
}

class InputText {
  ///当前文本
  String text;

  ///插入的文本
  String insertText;

  ///当前偏移
  InputText({
    this.text = '',
    this.insertText = '',
  });

  void clear() {
    text = '';
    insertText = '';
  }
}

class ChatInputModel extends ModelBase<ChatInputModel> with WidgetsBindingObserver {
  // static ChatInputModel? _instance;
  // static ChatInputModel get instance => ChatInputModel();
  // factory ChatInputModel() => _instance ??= ChatInputModel._();
  ChatInputModel() {
    WidgetsBinding.instance?.addObserver(this);
    _key = Key(DateTime.now().millisecondsSinceEpoch.toString());
  }

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance?.removeObserver(this);
  }

  ChatEditorState _mEditorState = ChatEditorState.UNKNOWN;

  final InputText _mInputText = InputText();
  late Key _key;
  Key get key => _key;

  bool get isSelfView {
    ChatPage? widget = CustomRoute().getCurrentWidget<ChatPage>();
    if (widget != null) {
      return widget.mChatInputModel._key == _key;
    }
    return false;
  }

  bool _mIsKeyboardVisible = false;
  double _mCurretneKeyboardHeight = 0;
  static const inputHeight = 85;
  double _mKeyboardHeight = 0;
  double getKeyboardHeight() {
    if (_mKeyboardHeight > 0 && _mKeyboardHeight < SUT_S_HEIGHT) {
      return _mKeyboardHeight;
    } else {
      return 300;
    }
  }

  bool get isTextEmpty => _mInputText.text.isEmpty && _mInputText.insertText.isEmpty;

  ///输入框类容只容许输入框操作
  String getText() => _mInputText.text;
  // set text(value) {
  //   _inputText.text = value;
  //   _inputText.insertText = '';
  //   _sendNotify();
  // }

  void setText(String value, {bool isSend = false}) {
    _mInputText.text = value;
    _mInputText.insertText = '';
    print('setText ${_mInputText.text}');
    if (isSend) notifyListeners();
  }

  void clearText() {
    _mInputText.text = '';
    _mInputText.insertText = '';
    print('clearText ${_mInputText.text}');
    notifyListeners();
  }

  ///输入框外部插入
  String getInsertText() => _mInputText.insertText;
  void setInsertText(String value) {
    _mInputText.insertText = value;
    if (value.isNotEmpty) {
      notifyListeners();
    }
  }

  ChatEditorState getState() => _mEditorState;
  void setState(ChatEditorState state) {
    if (_mEditorState != state) {
      Debug.d('set ChatEditorState:${state.toString()} $_mIsKeyboardVisible');
      _mEditorState = state;
      bool keyVisible = _mIsKeyboardVisible;
      if (state == ChatEditorState.KEYBOARD) {
        if (!keyVisible) {
          _mIsKeyboardVisible = true;
          SystemChannels.textInput.invokeMethod('TextInput.show');
        }
      } else {
        _mIsKeyboardVisible = false;
        if (keyVisible) SystemChannels.textInput.invokeMethod('TextInput.hide');
      }
    }
    notifyListeners();
  }

  @override
  void didChangeMetrics() {
    super.didChangeMetrics();

    WidgetsBinding.instance?.addPostFrameCallback((_) {
      BuildContext? context = OneContext().context;
      if (context != null) {
        double bottom = MediaQuery.of(context).viewInsets.bottom;
        if (bottom > 0) {
          //显示键盘
          if (bottom > _mCurretneKeyboardHeight) {
            _mIsKeyboardVisible = true;
            _mKeyboardHeight = bottom;
            if (_mEditorState != ChatEditorState.KEYBOARD && isSelfView) {
              _mEditorState = ChatEditorState.KEYBOARD;
            }
          } else if (bottom < _mCurretneKeyboardHeight) {
            _mIsKeyboardVisible = false;
          }
        } else {
          //关闭键盘
          _mIsKeyboardVisible = false;
          if (_mEditorState == ChatEditorState.KEYBOARD && isSelfView) {
            _mEditorState = ChatEditorState.UNKNOWN;
          }
        }
        if (_mCurretneKeyboardHeight != bottom) {
          _mCurretneKeyboardHeight = bottom;
          Debug.d(
              "-------------------- $bottom  ${_mEditorState.toString()} $_mIsKeyboardVisible $_mKeyboardHeight");
          if (isSelfView) {
            notifyListeners();
          }
        }
      }
    });
  }
}
