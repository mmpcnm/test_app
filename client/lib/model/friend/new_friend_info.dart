import 'package:fim_app/package/pb/push.ext.pb.dart';
import 'package:fixnum/fixnum.dart';

class NewFriendInfo {
  final Int64 friendId; // 好友id
  final String nickname; // 昵称
  final int sex; // 性别
  final String avatarUrl; // 头像地址
  final String description; // 描述

  const NewFriendInfo(
      {required this.friendId,
      required this.nickname,
      this.sex = 0,
      required this.avatarUrl,
      required this.description});

  static NewFriendInfo formFriend(NewFriendInfo friendModel) {
    return NewFriendInfo(
        friendId: friendModel.friendId,
        nickname: friendModel.nickname,
        sex: friendModel.sex,
        avatarUrl: friendModel.avatarUrl,
        description: friendModel.description);
  }

  static NewFriendInfo formNewFriend(AddFriendPush addFriendPush) {
    return NewFriendInfo(
        friendId: addFriendPush.friendId,
        nickname: addFriendPush.nickname,
        avatarUrl: addFriendPush.avatarUrl,
        description: addFriendPush.description);
  }

  Map<String, dynamic> toMap() {
    return {
      'friend_id': friendId.toInt(),
      'nickname': nickname,
      'sex': sex,
      'avatar_url': avatarUrl,
      'description': description
    };
  }

  String toSqlString() {
    String retStr = '(friend_id,nickname,sex,avatar_url,description)';
    retStr =
        '$retStr VALUES (${friendId.toInt()}, "$nickname", $sex, "$avatarUrl", "$description")';
    return retStr;
  }

  static NewFriendInfo fromMap(Map<String, dynamic> map) {
    return NewFriendInfo(
      friendId: Int64(map['friend_id']),
      nickname: map['nickname'],
      sex: map['sex'],
      avatarUrl: map['avatar_url'],
      description: map['description'],
    );
  }
}
