import 'dart:async';

import 'package:fim_app/model/friend/friend_info.dart';
import 'package:fim_app/package/pb/logic.ext.pbgrpc.dart';
import 'package:fixnum/fixnum.dart';

import '../ModelBase.dart';

class FriendScoped extends ModelBase {
  static FriendScoped instance = FriendScoped();

  final List<FriendInfo> _friends = [];

  int get count => _friends.length;
  List<FriendInfo> get friends => List.from(_friends);

  FriendInfo at(int index) => _friends[index];

  Timer? _notifyTimer;

  void _sendNotify() {
    if (_notifyTimer != null) {
      return;
    }
    _notifyTimer = Timer.periodic(const Duration(milliseconds: 10), (timer) => notify());
  }

  void setFrinds(List<Friend> friends) {
    _friends.clear();
    for (var item in friends) {
      FriendInfo newFriend = FriendInfo.copyFriend(item);
      _friends.add(newFriend);
    }
    _sendNotify();
  }

  void addFriend(FriendInfo friend) {
    if (isFriend(friend.userId)) return;
    _friends.add(friend);
    _sendNotify();
  }

  void upFriend(Int64 friendId, {String? remarks, String? extra}) {
    for (var item in _friends) {
      if (item.userId == friendId) {
        item.remarks = remarks ?? item.remarks;
        item.extra = extra ?? item.extra;
        _sendNotify();
        return;
      }
    }
  }

  void removeFriend(Int64 userId) {
    for (var item in _friends) {
      if (item.userId == userId) {
        _friends.remove(item);
        _sendNotify();
        return;
      }
    }
  }

  void clear() {
    _friends.clear();
  }

  FriendInfo? findFriend(Int64 userId) {
    for (var item in _friends) {
      if (item.userId == userId) {
        return item;
      }
    }
    return null;
  }

  bool isFriend(Int64 userId) {
    for (var item in _friends) {
      if (item.userId == userId) {
        return true;
      }
    }
    return false;
  }

  void notify() {
    if (_notifyTimer != null) _notifyTimer?.cancel();
    _notifyTimer = null;
    notifyListeners();
  }
}
