import 'package:fim_app/model/ModelBase.dart';
import 'package:fim_app/model/friend/new_friend_info.dart';
import 'package:fim_app/package/sqflite/sqlite_new_friend.dart';
import 'package:fixnum/fixnum.dart';

class NewFriendModel extends ModelBase {
  static NewFriendModel get instance => _instance;

  static final NewFriendModel _instance = NewFriendModel();

  final List<NewFriendInfo> _values = [];

  int get count => _values.length;
  List<NewFriendInfo> get newFriends => List.from(_values);

  void init(List<NewFriendInfo> list) {
    _values.clear();
    _values.addAll(list);
    notifyListeners();
  }

  bool push(NewFriendInfo newFriend) {
    if (!isItem(newFriend.friendId)) {
      _values.add(newFriend);
      SqliteNewFriend().insert(newFriend);
      notifyListeners();
      return true;
    }
    return false;
  }

  bool remove(Int64 friendId) {
    for (var item in _values) {
      if (item.friendId == friendId) {
        _values.remove(item);
        SqliteNewFriend().delete(friendId: friendId);
        notifyListeners();
        return true;
      }
    }
    return false;
  }

  bool isItem(Int64 friendId) {
    for (var item in _values) {
      if (item.friendId == friendId) return true;
    }
    return false;
  }

  void clear() {
    _values.clear();
  }
}
