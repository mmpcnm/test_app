import 'package:fim_app/package/pb/logic.ext.pb.dart';
import 'package:fixnum/fixnum.dart';

class FriendInfo {
  String get userNickname => remarks.isNotEmpty ? remarks : nickname;

  Int64 userId; // 用户id
  String phoneNumber; // 电话号码
  String nickname; // 昵称
  int sex; // 性别
  String avatarUrl; // 头像地址
  String userExtra; // 用户附加字段
  String remarks; // 备注
  String extra; // 附加字段

  FriendInfo(
      {required this.userId,
      required this.phoneNumber,
      required this.nickname,
      required this.sex,
      required this.avatarUrl,
      required this.userExtra,
      required this.remarks,
      required this.extra});
  static FriendInfo copyFriend(Friend friend) {
    return FriendInfo(
        userId: friend.userId,
        phoneNumber: friend.phoneNumber,
        nickname: friend.nickname,
        sex: friend.sex,
        avatarUrl: friend.avatarUrl,
        userExtra: friend.userExtra,
        remarks: friend.remarks,
        extra: friend.extra);
  }

  static FriendInfo newFriendModel(FriendInfo friend) {
    return FriendInfo(
        userId: friend.userId,
        phoneNumber: friend.phoneNumber,
        nickname: friend.nickname,
        sex: friend.sex,
        avatarUrl: friend.avatarUrl,
        userExtra: friend.userExtra,
        remarks: friend.remarks,
        extra: friend.extra);
  }
}
