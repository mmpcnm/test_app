import 'package:fim_app/package/net/http/http_utils.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:flutter_plugin_record/flutter_plugin_record.dart';
import 'package:scoped_model/scoped_model.dart';

enum VoicePlayerState {
  UNKNOWN,
  ERROR,
  START,
  STOP,
}

class VoicePlayerScoped extends Model {
  VoicePlayerState _mState = VoicePlayerState.UNKNOWN;
  bool isInit = false;
  FlutterPluginRecord? _mPluginRecordPaly;
  HttpDownload? _mDownload;

  VoicePlayerState get state => _mState;

  void _init(void Function(bool succeed) onInit) {
    _mPluginRecordPaly = FlutterPluginRecord();
    _mPluginRecordPaly!.init();

    _mPluginRecordPaly!.responseFromInit.listen((event) {
      if (event) {
        print('FlutterPluginRecord init succeed.');
        onInit(true);
      } else {
        print('FlutterPluginRecord init failed !');
        onInit(false);
      }
    });

    _mPluginRecordPaly!.response.listen((event) {
      print('VoicePlayerScoped Record Response success:${event.success}');
      print('VoicePlayerScoped Record Response key:${event.key}');
      print('VoicePlayerScoped Record Response msg:${event.msg}');
      print('VoicePlayerScoped Record Response path:${event.path}');
      print('VoicePlayerScoped Record Response audioTimeLength:${event.audioTimeLength}');
    });

    _mPluginRecordPaly!.responsePlayStateController.listen((event) {
      print('VoicePlayerScoped Play State :${event.playState}');
      print('VoicePlayerScoped Play Path :${event.playPath}');
      if (event.playState == 'complete') {
        _mState = VoicePlayerState.STOP;
        _dispose();
        notifyListeners();
      }
    });
  }

  void _dispose() {
    if (_mPluginRecordPaly != null) {
      _mPluginRecordPaly!.dispose();
      _mPluginRecordPaly = null;
    }

    if (_mDownload != null) {
      _mDownload!.dispose();
      _mDownload = null;
    }
  }

  playUrl(String url) async {
    if (_mDownload != null) _mDownload!.dispose();
    _mDownload = HttpDownload();
    String? path = await _mDownload!.download(url, save_path: Res.tempAudio, save_ext: '.wav');
    if (_mDownload != null) _mDownload!.dispose();
    _mDownload = null;

    if (path != null) {
      play(path);
    } else {
      _mState = VoicePlayerState.ERROR;
      notifyListeners();
    }
  }

  play(String path) {
    _mState = VoicePlayerState.START;
    if (_mPluginRecordPaly == null) {
      _init((succeed) {
        if (succeed) {
          _mPluginRecordPaly!.playByPath(path, 'file');
        } else {
          _dispose();
          _mState = VoicePlayerState.ERROR;
        }
        notifyListeners();
      });
    }
  }

  stop() {
    if (_mPluginRecordPaly != null) {
      _mState = VoicePlayerState.STOP;
      _mPluginRecordPaly!.stop();
      notifyListeners();
      _dispose();
    }
  }
}
