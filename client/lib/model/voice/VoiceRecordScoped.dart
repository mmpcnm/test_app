import 'dart:io' as io;
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/preferences/cache_manager.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:flutter_plugin_record/flutter_plugin_record.dart';
import 'package:scoped_model/scoped_model.dart';

enum VoiceRecordState { UNKNOWN, ERROR, START, STOP, CANCEL }

class VoiceRecordScoped extends Model {
  FlutterPluginRecord? _mPluginRecord;
  VoiceRecordState _mState = VoiceRecordState.UNKNOWN;

  ///振幅大小
  void Function(double value)? onColumeCallFunc;

  ///开始录音
  void Function()? onStartCallFunc;

  ///停止录音
  void Function(String filePath, double timeLength)? onStopCallFunc;

  ///取消录音
  void Function()? onCancelCallFunc;

  ///异常
  void Function(String error)? onErrorCallFunc;

  void start() async {
    init((succeed) {
      if (succeed) {
        _mState = VoiceRecordState.START;
        if (_mPluginRecord != null) _mPluginRecord!.start();
      } else {
        _mState = VoiceRecordState.ERROR;
        if (onErrorCallFunc != null) onErrorCallFunc!.call(STR(10253)); //'初始化失败');
        _dispose();
      }
    });
  }

  void stop() async {
    _mState = VoiceRecordState.STOP;
    if (_mPluginRecord != null) _mPluginRecord!.stop();
  }

  void cancel() async {
    _mState = VoiceRecordState.CANCEL;
    if (_mPluginRecord != null) _mPluginRecord!.stop();
  }

  void dispose() {
    _mState = VoiceRecordState.UNKNOWN;
    if (_mPluginRecord != null) {
      _mPluginRecord!.stop();
      _mPluginRecord!.dispose();
      _mPluginRecord = null;
    }
  }

  void init(void Function(bool succeed) onInit) {
    _mPluginRecord = FlutterPluginRecord();
    _mPluginRecord!.init();

    _mPluginRecord!.responseFromInit.listen((event) {
      if (event) {
        print('FlutterPluginRecord init succeed.');
        onInit(true);
      } else {
        print('FlutterPluginRecord init failed !');
        onInit(false);
      }
    });

    _mPluginRecord!.response.listen((event) {
      print('VoicePlayerScoped Record Response success:${event.success}');
      print('VoicePlayerScoped Record Response key:${event.key}');
      print('VoicePlayerScoped Record Response msg:${event.msg}');
      print('VoicePlayerScoped Record Response path:${event.path}');
      print('VoicePlayerScoped Record Response audioTimeLength:${event.audioTimeLength}');
      if (event.msg == 'onStop') {
        if (onCancelCallFunc != null && _mState == VoiceRecordState.CANCEL) onCancelCallFunc!.call();
        String filePath = event.path ?? '';
        if (filePath.isEmpty) {
          if (onStopCallFunc != null && _mState == VoiceRecordState.STOP) onStopCallFunc!.call('', 0);
          return;
        }
        try {
          io.File file = io.File(filePath);
          var newFile = file;
          // io.File newFile = Res.copyFileNameSync(file, ResAudio('temRecorder.jpg'));
          filePath = newFile.path;

          if (onStopCallFunc != null && _mState == VoiceRecordState.STOP) onStopCallFunc!.call(filePath, event.audioTimeLength ?? 0);

          newFile.length().then((value) {
            double filesize = double.parse(value.toString());
            String sizeStr = CCacheManager.formatSize(filesize);
            print('record file size:$sizeStr');
          });
          // file.deleteSync();
        } catch (e) {
          // if (onStopCallFunc != null && _mState == VoiceRecordState.STOP) onStopCallFunc!.call('', 0);
          //录音失败
          if (onErrorCallFunc != null) onErrorCallFunc!.call('${STR(10254)}！');
        }

        _dispose();
      } else if (event.msg == 'onStart') {
        // io.File file = io.File(ResAudio('temRecorder.jpg'));
        // if (file.existsSync()) {
        //   file.deleteSync();
        // }
        if (onStartCallFunc != null && _mState == VoiceRecordState.START) onStartCallFunc!.call();
      }
    });

    //振幅大小
    _mPluginRecord!.responseFromAmplitude.listen((data) {
      var value = double.parse(data.msg ?? '0');
      if (onColumeCallFunc != null) onColumeCallFunc!.call(value);
    });
  }

  void _dispose() {
    if (_mPluginRecord != null) {
      _mPluginRecord!.dispose();
      _mPluginRecord = null;
    }
  }
}
