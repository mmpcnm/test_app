import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/model/voice/VoicePlayerScoped.dart';
import 'package:scoped_model/scoped_model.dart';

class VoicePlayerListScoped extends Model {
  VoicePlayerScoped? _mPlayerScoped;
  static VoicePlayerListScoped instance = VoicePlayerListScoped();
  bool _mIsNetwork = true;
  int _mCurrentIndex = 0;
  VoicePlayerState _mState = VoicePlayerState.UNKNOWN;
  List<String> _mSounds = [];

  VoicePlayerListScoped() {}

  int get currentIndex => _mCurrentIndex;
  VoicePlayerState get state => _mState;
  int get count => _mSounds.length;
  String get currentUrl =>
      _mSounds.isEmpty || _mCurrentIndex >= count ? '' : _mSounds[_mCurrentIndex];

  void _onPlayerListener() {
    Debug.d('_onPlayerListener: ${_mState.toString()}');
    // if (_mState == VoicePlayerState.UNKNOWN) return;
    switch (_mPlayerScoped?.state) {
      case VoicePlayerState.START:
        _mState = VoicePlayerState.START;
        notifyListeners();
        break;
      case VoicePlayerState.STOP:
        // if (_mState == VoicePlayerState.STOP) return;
        if (!_play(index: _mCurrentIndex + 1)) {
          stop();
        }
        break;
      case VoicePlayerState.ERROR:
        _mState = VoicePlayerState.ERROR;
        notifyListeners();
        break;
      default:
    }
  }

  void playSounds({bool isNetwork = true, int index = 0, required List<String> urls}) {
    this._mIsNetwork = isNetwork;
    this._mSounds = urls;
    _mCurrentIndex = index;
    if (_mPlayerScoped != null) {
      _mPlayerScoped?.removeListener(_onPlayerListener);
      _mPlayerScoped?.stop();
      _mPlayerScoped = null;
    }

    if (!_play(index: _mCurrentIndex)) {
      _mState = VoicePlayerState.UNKNOWN;
      notifyListeners();
    }
  }

  bool _play({int index = 0}) {
    if (_mSounds.isEmpty || _mSounds.length <= index) {
      return false;
    }
    _mCurrentIndex = index;
    if (_mPlayerScoped != null) {
      _mPlayerScoped?.removeListener(_onPlayerListener);
      _mPlayerScoped?.stop();
      _mPlayerScoped = null;
    }
    if (_mPlayerScoped == null) {
      _mPlayerScoped = VoicePlayerScoped();
      _mPlayerScoped?.addListener(_onPlayerListener);
    }

    _mState = VoicePlayerState.UNKNOWN;
    if (_mIsNetwork) {
      _mPlayerScoped?.playUrl(_mSounds[_mCurrentIndex]);
    } else {
      _mPlayerScoped?.play(_mSounds[_mCurrentIndex]);
    }
    return true;
  }

  void stop() {
    // _mSounds.clear();
    _mState = VoicePlayerState.STOP;
    if (_mPlayerScoped != null) {
      _mPlayerScoped?.removeListener(_onPlayerListener);
      _mPlayerScoped?.stop();
      _mPlayerScoped = null;
    }
    notifyListeners();
  }
}
