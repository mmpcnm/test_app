import 'dart:async';

import 'package:fim_app/configs/api.dart';
import 'package:fim_app/model/device_info.dart';
import 'package:fim_app/model/group/group_model.dart';
import 'package:fim_app/package/pb/logic.ext.pb.dart' as pb;
import 'package:fixnum/fixnum.dart';
import 'package:scoped_model/scoped_model.dart';

enum GroupScopedState {
  NONE,
  NEW_GROUP, //新增群组
  DEL_GROUP, //删除群组
  UP_GROUP, //更新群组
  GROUPS, //获取列表
}

class RoomData {
  Int64 roomId = Int64.ZERO;
  String name;
  String avatar_url;
  String introduction;
  String extra;
  RoomData({
    this.roomId = Int64.ZERO,
    this.name = '',
    this.avatar_url = '',
    this.introduction = '',
    this.extra = '',
  });
}

class GroupScoped extends Model {
  static final GroupScoped instance = GroupScoped();
  Int64 currentGroupId = Int64.ZERO;
  GroupScopedState state = GroupScopedState.NONE;
  Timer? _notifyTimer;
  List<GroupModel> get groups => List.from(_groups);
  int get count => _groups.length;

  List<GroupModel> _groups = [];

  final List<RoomData> _rooms = [];

  int get roomCount => _rooms.isEmpty ? 1 : _rooms.length;
  RoomData atRoomData(int index) {
    if (_rooms.isEmpty) {
      return RoomData(
        roomId: AppConfig.channelId,
        name: DeviceInfo.appName,
        avatar_url: '',
        introduction: '',
        extra: '',
      );
    } else {
      return _rooms[index];
    }
  }

  void setRoom({
    required List<Int64> roomIds,
    String name = '',
    String avatarUrl = '',
    String introduction = '',
    String extra = '',
  }) {
    _rooms.clear();
    for (var i = 0; i < roomIds.length; i++) {
      var id = roomIds[i];
      _rooms.add(RoomData(
        roomId: id,
        name: '$name $i',
        avatar_url: avatarUrl,
        introduction: introduction,
        extra: extra,
      ));
    }
    _sendNotify();
  }

  RoomData getRoomData(Int64 roomId) {
    for (var item in _rooms) {
      if (roomId == item.roomId) {
        return item;
      }
    }
    return RoomData(
      roomId: roomId,
      name: DeviceInfo.appName,
      avatar_url: '',
      introduction: '',
      extra: '',
    );
  }

  void _sendNotify() {
    if (_notifyTimer != null) {
      return;
    }
    _notifyTimer = Timer.periodic(Duration(milliseconds: 10), (timer) => notify());
  }

  void addGroup(GroupModel group) {
    if (!isGroup(group.groupId)) {
      _groups.add(group);
      currentGroupId = group.groupId;
      state = GroupScopedState.NEW_GROUP;
      _sendNotify();
    }
  }

  void delGroup(Int64 groupId) {
    for (var item in _groups) {
      if (item.groupId == groupId) {
        currentGroupId = groupId;
        state = GroupScopedState.DEL_GROUP;
        _groups.remove(item);
        _sendNotify();
        return;
      }
    }
  }

  void UPGroup(GroupModel group) {
    for (int i = 0; i < _groups.length; ++i) {
      if (_groups[i].groupId == group.groupId) {
        currentGroupId = group.groupId;
        state = GroupScopedState.UP_GROUP;
        _groups[i] = group;
        _sendNotify();
        return;
      }
    }

    addGroup(group);
  }

  void UPGroupDate(
      {required Int64 group_id,
      String? avatar_url,
      String? name,
      String? introduction,
      String? extra}) {
    for (int i = 0; i < _groups.length; ++i) {
      if (_groups[i].groupId == group_id) {
        currentGroupId = group_id;
        state = GroupScopedState.UP_GROUP;
        var item = _groups[i];
        item.avatarUrl = avatar_url ?? item.avatarUrl;
        item.name = name ?? item.name;
        item.introduction = introduction ?? item.introduction;
        item.extra = extra ?? item.introduction;
        _sendNotify();
        return;
      }
    }
  }

  GroupModel atGroup(int index) {
    return _groups[index];
  }

  bool isGroup(Int64 groupId) {
    for (var item in _groups) {
      if (item.groupId == groupId) return true;
    }
    return false;
  }

  int findGroup(Int64 groupId) {
    for (int i = 0; i < _groups.length; ++i) {
      if (_groups[i].groupId == groupId) {
        return i;
      }
    }
    return -1;
  }

  GroupModel? getGroup(Int64 groupId) {
    for (var item in _groups) {
      if (item.groupId == groupId) return item;
    }
    return null;
  }

  void setGroups(List<pb.Group> groups) {
    _groups.clear();
    // _groups.add(GroupModel(
    //   groupId: AppConfig.channelId,
    //   name: DeviceInfo.appName,
    //   introduction: '',
    //   user_mum: 1,
    //   extra: '',
    //   createTime: Int64.ZERO,
    //   updateTime: Int64.ZERO,
    // ));

    for (var item in groups) {
      _groups.add(GroupModel.newGroup(item));
    }

    currentGroupId = Int64.ZERO;
    state = GroupScopedState.GROUPS;
    _sendNotify();
  }

  void clear() {
    currentGroupId = Int64.ZERO;
    state = GroupScopedState.NONE;
    _groups.clear();
    _rooms.clear();
  }

  void notify() {
    if (_notifyTimer != null) _notifyTimer?.cancel();
    _notifyTimer = null;
    notifyListeners();
  }
}
