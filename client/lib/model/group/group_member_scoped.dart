import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/package/pb/logic.ext.pb.dart' as pb;
import 'package:fixnum/fixnum.dart';
import 'package:scoped_model/scoped_model.dart';

import 'group_member_model.dart';

class GroupMemberScoped extends Model {
  static GroupMemberScoped instance = GroupMemberScoped();

  List<GroupMemberModel> _members = [];

  void addMember(GroupMemberModel member, {bool isNotify = true}) {
    if (isMember(groupId: member.groupId, userId: member.userId)) return;
    Debug.d('addMember groupId:${member.groupId}, memberId:${member.userId}, nickName:${member.userNickname}');
    _members.add(member);
    if (isNotify) notifyListeners();
  }

  void addMemberAll(Int64 groupId, List<pb.GroupMember> members) {
    for (var item in members) {
      addMember(GroupMemberModel.fromMember(groupId, item), isNotify: false);
    }
    notifyListeners();
  }

  void delMember({required Int64 groupId, required Int64 userId, bool isNotify = true}) {
    Debug.d('delMember groupid:$groupId, userid:$userId');
    for (int idx = 0; idx < _members.length; ++idx) {
      var item = _members[idx];
      if (item.groupId == groupId && userId == item.userId) {
        Debug.d('delMember ${item.userNickname}');
        _members.removeAt(idx);
        if (isNotify) notifyListeners();
        return;
      }
    }
  }

  void delMembers(Int64 groupId) {
    List<GroupMemberModel> list = [];
    for (var item in _members) {
      if (item.groupId != groupId) {
        list.add(item);
      }
    }
    _members = list;
  }

  List<GroupMemberModel> getMembers({required Int64 groupId}) {
    List<GroupMemberModel> ret = [];
    for (var item in _members) {
      if (item.groupId == groupId) {
        ret.add(item);
      }
    }

    return ret;
  }

  GroupMemberModel? getMember({required Int64 groupId, required Int64 memberId}) {
    for (var item in _members) {
      if (item.groupId == groupId && item.userId == memberId) {
        return item;
      }
    }
    return null;
  }

  int getMemberCount(Int64 groupId) {
    int count = 0;
    for (var item in _members) {
      if (item.groupId == groupId) {
        ++count;
      }
    }
    return count;
  }

  pb.MemberType getMemberType({required Int64 groupId, required Int64 userId}) {
    for (var item in _members) {
      if (item.groupId == groupId && item.userId == userId) {
        return item.memberType;
      }
    }
    return pb.MemberType.GMT_MEMBER;
  }

  bool isMember({required Int64 groupId, required Int64 userId}) {
    for (var item in _members) {
      if (item.groupId == groupId && item.userId == userId) {
        return true;
      }
    }
    return false;
  }
}
