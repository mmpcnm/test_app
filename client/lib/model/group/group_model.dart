import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/pb/logic.ext.pb.dart' as pb;
import 'package:fixnum/fixnum.dart';

class GroupModel {
  Int64 groupId; // 群组id
  String _name = ''; // 名称
  String avatarUrl; // 头像
  String introduction; // 简介
  int user_mum; // 用户数
  String extra; // 附加字段
  Int64 createTime; // 创建时间
  Int64 updateTime; // 更新时间

  String get name => _name.isEmpty ? STR(10222) : _name;
  void set name(String value) => _name = value;

  GroupModel({
    required this.groupId,
    String name = '',
    this.avatarUrl = '',
    required this.introduction,
    required this.user_mum,
    required this.extra,
    required this.createTime,
    required this.updateTime,
  }) {
    _name = name;
  }

  static newGroup(pb.Group group) {
    return GroupModel(
      name: group.name,
      avatarUrl: group.avatarUrl,
      groupId: group.groupId,
      introduction: group.introduction,
      user_mum: group.userMum,
      extra: group.extra,
      createTime: group.createTime,
      updateTime: group.updateTime,
    );
  }
}
