import 'package:fim_app/package/pb/logic.ext.pb.dart' as pb;
import 'package:fixnum/fixnum.dart';

class GroupMemberModel {
  Int64 groupId;
  Int64 userId;
  String nickname; // 昵称
  int sex; // 性别
  String avatarUrl; // 头像地址
  String userExtra; // 用户附加字段
  pb.MemberType memberType; // 成员类型
  String remarks; // 备注
  String extra; // 群组成员附加字段

  String get userNickname {
    return remarks.isEmpty ? nickname : remarks;
  }

  GroupMemberModel({
    required this.groupId,
    required this.userId,
    required this.nickname,
    required this.sex,
    required this.avatarUrl,
    required this.userExtra,
    required this.memberType,
    required this.remarks,
    required this.extra,
  });

  static GroupMemberModel fromMember(Int64 groupId, pb.GroupMember member) {
    return GroupMemberModel(
        groupId: groupId,
        userId: member.userId,
        nickname: member.nickname,
        sex: member.sex,
        avatarUrl: member.avatarUrl,
        userExtra: member.userExtra,
        memberType: member.memberType,
        remarks: member.remarks,
        extra: member.extra);
  }

  static GroupMemberModel fromGroupMember(GroupMemberModel member) {
    return GroupMemberModel(
        groupId: member.groupId,
        userId: member.userId,
        nickname: member.nickname,
        sex: member.sex,
        avatarUrl: member.avatarUrl,
        userExtra: member.userExtra,
        memberType: member.memberType,
        remarks: member.remarks,
        extra: member.extra);
  }

  Map<String, dynamic> toMap() {
    return {
      'group_id': groupId.toInt(),
      'user_id': userId.toInt(),
      'nickname': nickname,
      'sex': sex,
      'avatar_url': avatarUrl,
      'user_extra': userExtra,
      'member_type': memberType.value,
      'remarks': remarks,
      'extra': extra
    };
  }

  static GroupMemberModel sqlMapToMember(Map<String, dynamic> sqlMap) {
    int group_id = sqlMap['group_id'];
    int user_id = sqlMap['user_id'];
    String nickname = sqlMap['nickname'];
    int sex = sqlMap['sex'];
    String avatar_url = sqlMap['avatar_url'];
    String user_extra = sqlMap['user_extra'];
    int member_type = sqlMap['member_type'];
    String remarks = sqlMap['remarks'];
    String extra = sqlMap['extra'];
    return GroupMemberModel(
        groupId: Int64(group_id),
        userId: Int64(user_id),
        nickname: nickname,
        sex: sex,
        avatarUrl: avatar_url,
        userExtra: user_extra,
        memberType: pb.MemberType.valueOf(member_type) ?? pb.MemberType.GMT_MEMBER,
        remarks: remarks,
        extra: extra);
  }
}
