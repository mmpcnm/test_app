// ignore_for_file: file_names

import 'package:fim_app/package/pb/logic.ext.pb.dart';
import 'package:fixnum/fixnum.dart';

import '../ModelBase.dart';

class RoomMembersModel extends ModelBase {
  static final RoomMembersModel _instance = RoomMembersModel._create();
  RoomMembersModel._create();
  factory RoomMembersModel() => _instance;

  final Map<Int64, List<RoomMember>> _memberMap = {};
  void setMembers({required Int64 roomId, required List<RoomMember> members}) {
    _memberMap[roomId] = members;
    notifyListeners();
  }

  List<RoomMember> getMembers(Int64 roomId) {
    return _memberMap[roomId] ?? [];
  }

  void clear() {
    _memberMap.clear();
  }
}
