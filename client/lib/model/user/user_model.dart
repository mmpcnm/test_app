import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/pb/logic.ext.pb.dart' as pb;
import 'package:fixnum/fixnum.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:fim_app/package/preferences/prefers.dart';

class UserModel extends Model {
  static UserModel instance = UserModel();

//账号
  static String get accountId => Prefers.getString('account_id');
  static set accountId(String value) => Prefers.setString('account_id', value);
//密码
  static String get password => instance._password;
  static set password(String value) => instance._password = value;
//用户id
  static set userId(Int64 value) => instance._userId = value;
  static Int64 get userId => instance._userId;

  static set maxSYN(Int64 value) => Prefers.setInt('${userId}_maxSYN', value.toInt());
  static Int64 get maxSYN => Int64(Prefers.getInt('${userId}_maxSYN', defaultValue: 0));

  static set maxRoomSYN(Int64 value) => Prefers.setInt('${userId}_maxRoomSYN', value.toInt());
  static Int64 get maxRoomSYN => Int64(Prefers.getInt('${userId}_maxRoomSYN', defaultValue: 0));

  ///token
  static String get token => instance._token;
  static set token(String value) => instance._token = value;

  ///用户昵称
  static String get nickName => instance._nickName;
  static set nickName(String value) => instance._nickName = value;

  ///用户头像
  static String get avatarUrl => instance._avatarUrl;
  static set avatarUrl(String value) => instance._avatarUrl = value;

  ///手机号
  static String get phoneNumber => instance._phoneNumber;
  static set phoneNumber(String value) => instance._phoneNumber = value;

  ///性别
  static int get sex => instance._sex;
  static void set sex(int value) => instance._sex = value;

  ///
  static String get salt => instance._salt;
  static void set salt(String value) => instance._salt = value;

  ///用户签名
  static String get extra => instance._extra.isNotEmpty ? instance._extra : STR(10010);
  static void set extra(String value) => instance._extra = value;

  ///创建时间
  static Int64 get createTime => instance._createTime;
  static void set createTime(Int64 value) => instance._createTime = value;

  ///上次在线更细时间
  // static Int64 get updateTime => instance._updateTime;
  // static void set updateTime(Int64 value) => instance._updateTime = value;

  ///是否设置了钱包密码
  static bool get isWalletPwdSet => instance._isWalletPwdSet;
  static void set isWalletPwdSet(bool value) => instance._isWalletPwdSet = value;

  ///钱包金额
  static Int64 get money => instance._money;
  static void set money(Int64 value) => instance._money = value;
  static double get dmoney => (instance._money.toDouble() / 100);

  ///Alipay实名
  static String get alipayRealname => instance._alipayRealname;
  static void set alipayRealname(String value) => instance._alipayRealname = value;

  ///Alipay账号
  static String get alipayAccount => instance._alipayAccount;
  static void set alipayAccount(String value) => instance._alipayAccount = value;

  ///是否首次打开App
  static bool get firstOpenApp => Prefers.getBool('${userId}_firstOpenApp', defaultValue: true);
  static set firstOpenApp(bool value) => Prefers.setBool('${userId}_firstOpenApp', value);

  static void clear() {
    instance._avatarUrl = '';
    instance._salt = '';
    instance._nickName = '';
    instance._sex = 0;
    instance._extra = '';
    instance._createTime = Int64.ZERO;
    // instance._updateTime = Int64.ZERO;
    instance._token = '';

    instance._phoneNumber = '';
    instance._money = Int64.ZERO;
    instance._isWalletPwdSet = false;
  }

//发送数据改变监听
  static void notify() => instance.notifyListeners();
//copy接收服务器数据
  static void fromUser(pb.User user) {
    instance._avatarUrl = user.avatarUrl;
    // instance._salt = user.salt;
    instance._nickName = user.nickname;
    instance._sex = user.sex;
    instance._extra = user.extra;
    instance._createTime = user.createTime;
    // instance._updateTime = user.updateTime;
    instance._isWalletPwdSet = user.isWalletPwdSet;
    instance._phoneNumber = user.phone;
  }

  Int64 _userId = Int64(0); // 用户id
  String _password = ''; //密码
  String _token = '';
  String _nickName = ''; // 昵称
  String _avatarUrl = ''; // 头像地址
  String _phoneNumber = '';
  bool _isWalletPwdSet = false;

  String _salt = ''; //盐
  int _sex = 0; // 性别
  String _extra = ''; // 附加字段
  Int64 _createTime = Int64(0); // 创建时间
  // Int64 _updateTime = Int64(0); // 更新时间

  Int64 _money = Int64.ZERO;

  String _alipayRealname = '';
  String _alipayAccount = '';
}
