import 'package:fim_app/model/user/user_model.dart';
import 'package:fim_app/package/preferences/prefers.dart';

class UserSetting {
  static get isNewNotice => Prefers.getBool('${UserModel.userId}_isNewNotice', defaultValue: true);
  static set isNewNotice(value) => Prefers.setBool('${UserModel.userId}_isNewNotice', value);

  static get isVoice => Prefers.getBool('${UserModel.userId}_isVoice', defaultValue: true);
  static set isVoice(value) => Prefers.setBool('${UserModel.userId}_isVoice', value);

  static get isShake => Prefers.getBool('${UserModel.userId}_isShake', defaultValue: true);
  static set isShake(value) => Prefers.setBool('${UserModel.userId}_isShake', value);
}
