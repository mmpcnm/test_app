import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/configs/im_config.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/controller/logic_user.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:flutter/material.dart';
import 'package:one_context/one_context.dart';

class SexSettingPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SexSettingPage(UserModel.sex);
}

class _SexSettingPage extends State<SexSettingPage> {
  int currentSex;
  _SexSettingPage(this.currentSex);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: defBackgroundColor,
      appBar: CustomAppbar(
        isBackIcon: true,
        //设置性别
        title: Text(STR(10235), style: defTitleTextStyle),
        actions: [
          Button(
            margin: EdgeInsets.only(right: 15.sp),
            padding: EdgeInsets.symmetric(horizontal: 25.sp, vertical: 8.sp),
            //完成
            child: Text(STR(10215), style: defTextStyle),
            color: Colors.blue,
            onPressed: () {
              if (UserModel.sex != currentSex) {
                LogicUser.UpdateUser(context, sex: currentSex);
              }
              OneContext().pop();
            },
          )
        ],
      ),
      body: Container(
        child: ListView(
          physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
          children: [
            _buildTable(1, () {
              if (currentSex == 1) return;
              currentSex = 1;
              setState(() {});
            }),
            Divider(height: 0, indent: 20.sp, color: Colors.black38),
            _buildTable(2, () {
              if (currentSex == 2) return;
              currentSex = 2;
              setState(() {});
            }),
          ],
        ),
      ),
    );
  }

  Widget _buildTable(int sex, VoidCallback onPressed) {
    return Button(
      color: whiteColor,
      onPressed: onPressed,
      borderCircular: 0,
      padding: EdgeInsets.symmetric(horizontal: defPaddingSize, vertical: 20.sp),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(sex.sex_text, style: defTextStyle),
          sex == currentSex
              ? Icon(Icons.check, size: defIconSize, color: Colors.green)
              : SizedBox(height: defIconSize)
          //  Visibility(visible: sex == currentSex, child: ),
        ],
      ),
    );
  }
}
