import 'package:fim_app/custom/dialog/CustomDialog.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/custom/wdiget/custom_list_table.dart';
import 'package:fim_app/custom/wdiget/custom_switch.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/update/update.dart';
import 'package:fim_app/pages/user/BindPromoterPage.dart';
import 'package:fim_app/model/device_info.dart';
import 'package:fim_app/package/preferences/cache_manager.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/model/user/user_setting.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:one_context/one_context.dart';

class UserSettingPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _UserSettingPage();
}

class _UserSettingPage extends State<UserSettingPage> {
  double _cacheSize = 0;
  @override
  void initState() {
    super.initState();
    Future<double> size = CCacheManager.loadApplicationCache();
    size.then((value) {
      _cacheSize = value;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: defBackgroundColor,
      appBar: CustomAppbar(
        isBackIcon: true,
        //设置
        title: Text(STR(10015), style: defTitleTextStyle),
      ),
      body: Container(
        // padding: EdgeInsets.only(left: paddingHorizontal),
        child: LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: ListView(
                  physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                  children: [
                    // _buildSwitch(
                    //     name: STR(10236), //'新的通知',
                    //     value: UserSetting.isNewNotice,
                    //     onChanged: (value) {
                    //       UserSetting.isNewNotice = value;
                    //     }),
                    // _buildSwitch(
                    //   name: STR(10237), //'声音',
                    //   value: UserSetting.isVoice,
                    //   onChanged: (value) {
                    //     UserSetting.isVoice = value;
                    //   },
                    // ),
                    _buildSwitch(
                      name: STR(10238), //'震动',
                      value: UserSetting.isShake,
                      onChanged: (value) {
                        UserSetting.isShake = value;
                      },
                    ),
                    Container(height: defTabelSplitHeight, color: lineColor),
                    //关于我们
                    _buildTable(
                      name: STR(10239),
                      explain: null,
                      onPressed: () {},
                    ),
                    Divider(height: 1, color: lineColor, indent: defPaddingSize),
                    _buildTable(
                      name: '推广人',
                      onPressed: () {
                        OneContext().push(pageRoute(BindPromoterPage()));
                      },
                    ),
                    Divider(height: 1, color: lineColor, indent: defPaddingSize),
                    //检查更新 当前版本
                    _buildTable(
                      name: STR(10240),
                      explain: '${STR(10241)}:v${DeviceInfo.appVersion}',
                      onPressed: () {
                        Update.instace.checkUpdate(context);
                      },
                    ),
                    Divider(height: 1, color: lineColor, indent: defPaddingSize),
                    _buildTable(
                      name: STR(10242), //'清理缓存',
                      explain: CCacheManager.formatSize(_cacheSize),
                      onPressed: () {
                        if (_cacheSize > 0) {
                          //提示 是否要清理缓存
                          showMessageDialogEx(
                            context,
                            title: STR(10185),
                            content: '${STR(10243)}？',
                            confirmCallback: () async {
                              await DefaultCacheManager().emptyCache();
                              await CCacheManager.emptyCache();
                              _cacheSize = 0;
                              setState(() {});
                            },
                          );
                        }
                      },
                    ),
                    Divider(height: 1, color: lineColor, indent: defPaddingSize),
                  ],
                ),
              ),
              Container(
                constraints: BoxConstraints(minWidth: constraints.maxWidth),
                padding: EdgeInsets.only(
                    left: defPaddingSize,
                    right: defPaddingSize,
                    bottom: ScreenUtil.bottomBarHeight + 50.h),
                child: TextButton(
                  onPressed: () {
                    //提示 是否退出当前账号
                    showMessageDialogEx(context, title: STR(10185), content: '${STR(10244)}？',
                        confirmCallback: () {
                      gotoLogin(context);
                    });
                  },
                  //切换账号
                  child: Text(STR(10245),
                      style: TextStyle(fontSize: defFontSize, color: Colors.red[400])),
                  style: ButtonStyle(
                    padding: MaterialStateProperty.all(EdgeInsets.symmetric(vertical: 15.sp)),
                    shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(borderRadius: BorderRadius.circular(45))),
                    side: MaterialStateProperty.all(BorderSide(color: lineColor, width: 1)),
                  ),
                ),
              ),
            ],
          );
        }),
      ),
    );
  }

  Widget _buildSwitch(
      {required String name, required bool value, required void Function(bool value) onChanged}) {
    return CustomListTileSwith(
      padding: EdgeInsets.only(left: defPaddingSize, right: 0, top: 10.sp, bottom: 10.sp),
      title: Text(name, style: defTextStyle, overflow: TextOverflow.ellipsis),
      value: value,
      onChanged: onChanged,
    );
  }

  Widget _buildTable({required String name, String? explain, required void Function() onPressed}) {
    return CustomListTile(
      padding: EdgeInsets.symmetric(horizontal: defPaddingSize, vertical: 25.sp),
      title: Text(name, style: defTextStyle, overflow: TextOverflow.ellipsis),
      trailing: explain != null
          ? Text(explain, style: defTextStyle, overflow: TextOverflow.ellipsis)
          : null,
      isArrowIcon: true,
      onPressed: onPressed,
    );
  }
}
