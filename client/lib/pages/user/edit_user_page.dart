import 'package:fim_app/configs/api.dart';
import 'package:fim_app/configs/im_config.dart';
import 'package:fim_app/custom/image/image_utils.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/text/CustomTextFieldFormatter.dart';
import 'package:fim_app/custom/wdiget/custom_list_table.dart';
import 'package:fim_app/package/net/http/http_utils.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/controller/logic_user.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/custom/wdiget/user_icon_widget.dart';
import 'package:fim_app/pages/editor/editor_input_page.dart';
import 'package:fim_app/pages/user/sex_setting_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:one_context/one_context.dart';
import 'package:scoped_model/scoped_model.dart';

class EditUserPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _EditUserPage();
  }
}

class _EditUserPage extends State<EditUserPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: CustomAppbar(
        isBackIcon: true,
        // isLineshade: true,
        title: Text(STR(10039), style: defTitleTextStyle),
      ),
      body: Container(
        alignment: Alignment.topCenter,
        child: ScopedModel<UserModel>(
          model: UserModel.instance,
          child: ScopedModelDescendant<UserModel>(
            builder: (context, child, model) {
              return ListView(
                physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                children: [
                  _buildItem(
                    title: STR(10043), //头像
                    value: UserIconWidget(UserModel.avatarUrl),
                    padding: EdgeInsets.only(
                        left: defPaddingSize, right: defPaddingSize, top: 5.sp, bottom: 5.sp),
                    onPressed: () {
                      ImageUtils.pickAndCropImageUpService(
                        onSucceed: (String url, double width, double height) {
                          HttpUtils.upMultipartFile(
                            requestUrl: AppConfig.apiUrl,
                            filePath: url,
                            onSucceed: (response) {
                              if (response['code'] == 0 && response['data'] != null) {
                                LogicUser.UpdateUser(context, avatarUrl: response['data']['url']);
                              } else {
                                EasyLoading.showError('图像上传失败');
                              }
                            },
                          );
                        },
                        onError: (msg) {},
                      );
                    },
                  ),
                  _buildItem(
                    title: STR(10044), //昵称
                    value: Text(UserModel.nickName,
                        style: TextStyle(fontSize: defFontSize, color: grayColor)),
                    onPressed: () {
                      OneContext().push(
                        pageRoute(
                          EditorInputPage(
                            title: STR(10047),
                            // content: STR(10010),
                            hitText: STR(10048),
                            inputMaxLength: constUserNameMaxLength,
                            inputLeading: UserIconWidget(UserModel.avatarUrl, size: 64),
                            bottomWidget:
                                Text('${STR(10044)}: ${UserModel.nickName}', style: defTextStyle),
                            inputFormatters: [
                              FilteringTextInputFormatter.allow(
                                  RegExp(r'[\u4E00-\u9FA5A-Za-z0-9_.]')),
                              CustomTextFieldSpaceFormatter(),
                            ],
                            onPressed: (value) {
                              if (value.isEmpty) return;
                              LogicUser.UpdateUser(context, nickname: value);
                            },
                          ),
                        ),
                      );
                    },
                  ),
                  _buildItem(
                    title: STR(10046), //性别
                    value: Text(UserModel.sex.sex_text,
                        style: TextStyle(fontSize: defFontSize, color: grayColor)),
                    onPressed: () {
                      OneContext().push(pageRoute(SexSettingPage()));
                    },
                  ),
                  _buildItem(
                    title: STR(10045), //签名
                    onPressed: () {
                      OneContext().push(
                        pageRoute(
                          EditorInputPage(
                            title: STR(10228), //'设置个性签名',
                            content: STR(10010),
                            hitText: STR(10010),
                            inputMaxLength: constSignatureMaxLength,
                            inputLeading: UserIconWidget(UserModel.avatarUrl, size: 64.sp),
                            //个性签名
                            bottomWidget:
                                Text('${STR(10009)}: ${UserModel.extra}', style: defTextStyle),
                            onPressed: (value) {
                              if (value.isEmpty) return;
                              LogicUser.UpdateUser(context, extra: value);
                            },
                          ),
                        ),
                      );
                    },
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  Widget _buildItem(
      {required String title,
      Widget? value,
      EdgeInsets? padding,
      required VoidCallback onPressed}) {
    return CustomListTable.buildTable(
      leftText: title,
      rightWidget: value,
      isLine: true,
      lineIndent: defPaddingSize,
      padding: padding ?? EdgeInsets.symmetric(horizontal: defPaddingSize, vertical: 25.sp),
      onPressed: onPressed,
    );
  }
}
