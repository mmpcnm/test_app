import 'dart:ui' as ui;
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/dialog/CustomDialog.dart';
import 'package:fim_app/custom/wdiget/user_icon_widget.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:qr_flutter/qr_flutter.dart';

class MyQRCodePage extends StatelessWidget {
  final GlobalKey _mQRGlobalKey;

  MyQRCodePage({Key? key})
      : _mQRGlobalKey = GlobalKey(debugLabel: 'QR'),
        super(key: key);
  @override
  Widget build(BuildContext context) {
    double bgWidth = 646.sp * 0.8;
    double bgHeight = 844.sp * 0.8;
    return Scaffold(
      backgroundColor: defBackgroundColor,
      appBar: CustomAppbar(
        isBackIcon: true,
        isLineshade: false,
      ),
      body: ListView(
        children: [
          // SizedBox(height: paddingHorizontal),
          Center(
            child: Text(STR(10011), style: defTitleTextStyle),
          ),
          SizedBox(height: 35.sp),
          Center(
            child: Container(
              width: bgWidth,
              height: bgHeight,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(ResImgs('icon_my_qr_bg')),
                  scale: 1.0.sp,
                  alignment: Alignment.center,
                ),
              ),
              child: Stack(
                alignment: AlignmentDirectional.topCenter,
                children: [
                  Positioned(
                    top: bgHeight * 0.13,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        _buildCodeImage(),
                        Text('长按保存二维码', style: defTextStyle),
                      ],
                    ),
                  ),
                  Positioned(
                    bottom: bgHeight * 0.1,
                    left: 30.sp,
                    child: Row(
                      children: [
                        UserIconWidget(UserModel.avatarUrl),
                        SizedBox(width: 10.sp),
                        Text(UserModel.nickName, style: defTextStyle),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 15.sp),
          Center(
            child: Text(
              STR(10266),
              style: TextStyle(fontSize: defMinFontSize, color: grayColor),
            ),
          ),
        ],
      ),
    );
  }

  void _saveQRCode() async {
    BuildContext? buildContext = _mQRGlobalKey.currentContext;
    if (null != buildContext) {
      try {
        RenderRepaintBoundary? boundary = buildContext.findRenderObject() as RenderRepaintBoundary;
        ui.Image image = await boundary.toImage();
        ByteData? byteData = await image.toByteData(format: ui.ImageByteFormat.png);
        final result = await ImageGallerySaver.saveImage(byteData!.buffer.asUint8List());
        //二维码已保存到相册
        EasyLoading.showToast(STR(10234), toastPosition: EasyLoadingToastPosition.center);
      } catch (e) {
        Debug.e('保存二维码失败：$e');
      }
    }
  }

  Widget _buildCodeImage() {
    Object image = UserModel.avatarUrl.isEmpty
        ? AssetImage(ResImgs('default_head_icon'))
        : NetworkImage(UserModel.avatarUrl);
    return GestureDetector(
      onLongPress: () {
        showMessageBox(content: '保存二维码到相册', buttons: [
          MessageBoxButton('取消'),
          MessageBoxButton('保存', onPressed: () {
            _saveQRCode();
          }),
        ]);
      },
      child: Container(
        padding: EdgeInsets.all(10.sp),
        // color: whiteColor,
        width: 300.sp,
        height: 300.sp,
        decoration: BoxDecoration(
          color: whiteColor,
          borderRadius: BorderRadius.all(Radius.circular(15.sp)),
          border: const Border(),
        ),
        child: RepaintBoundary(
          key: _mQRGlobalKey,
          child: QrImage(
            size: 400.sp,
            version: QrVersions.auto,
            backgroundColor: whiteColor,
            data: 'http://userid_${UserModel.userId}',
            embeddedImage: image as ImageProvider<Object>,
          ),
        ),
      ),
    );
  }
}
