import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/button/custom_button.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/custom/text/CusotmTextField.dart';
import 'package:fim_app/package/net/controller/logic_user.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class BindPromoterPage extends StatelessWidget {
  String _userId = '';
  CustomButtonConttroller _buttonConttroller = CustomButtonConttroller();
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: (TapDownDetails details) {
        SystemChannels.textInput.invokeMethod('TextInput.hide');
      },
      child: Scaffold(
        backgroundColor: defBackgroundColor,
        appBar: CustomAppbar(
          isBackIcon: true,
          isLineshade: true,
          title: Text('绑定推广人', style: defTitleTextStyle),
        ),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 32.sp, vertical: defPaddingSize),
          child: ListView(
            physics: const NeverScrollableScrollPhysics(),
            children: [
              CustomTextField(
                hintText: '填写推广人ID',
                keyboardType: TextInputType.number,
                inputFormatters: [FilteringTextInputFormatter.allow(RegExp(r'\d+'))],
                onChanged: (String value) {
                  _userId = value;
                  _buttonConttroller.isEnable = _userId.isNotEmpty;
                },
                onSaved: (String? value) {},
              ),
              SizedBox(height: 32.sp),
              CustomButton(
                child: Text('确定', style: defTextStyle),
                color: Colors.blue,
                disableColor: defBtnColorL,
                isEnable: _userId.isNotEmpty,
                conttroller: _buttonConttroller,
                onPressed: () {
                  Int64 promoterId = Int64.parseInt(_userId);
                  LogicUser.BindPromoter(context: context, promoterId: promoterId);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
