import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/custom/text/CustomTextFieldFormatter.dart';
import 'package:fim_app/package/net/controller/logic_business.dart';
import 'package:fim_app/package/net/controller/logic_chat.dart';
import 'package:fim_app/package/net/controller/logic_red_packet.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/model/chat/ChatInputModel.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:one_context/one_context.dart';

class RedPacketPage extends StatefulWidget {
  ///redType 1.普通红包 2.普通和手气红包
  int mRedType;
  Int64 receiverId;
  pb.ReceiverType receiverType;
  int minCount;
  RedPacketPage(
      {required this.receiverId, required this.receiverType, this.minCount = 1, this.mRedType = 2});
  @override
  State<StatefulWidget> createState() => StateRedPackPage();
}

class StateRedPackPage extends State<RedPacketPage> {
  int _redType = 1;
  final TextEditingController _moneyController = TextEditingController();
  final TextEditingController _numberController = TextEditingController();
  final TextEditingController _textController = TextEditingController();

  double getMoney() {
    String value = _moneyController.text;
    // int length = value.length;
    if (value.isEmpty || value == '.') {
      return 0;
    } else {
      // if (value[length - 1] == '.') {
      //   value = value.substring(0, length - 1);
      // }
      return double.parse(value);
    }
  }

  int get redCount {
    if (_numberController.text.isEmpty) return 1;
    return int.parse(_numberController.text);
  }

  String get redTitle {
    if (_textController.text.isEmpty) return '恭喜发财,大吉大利!';
    return _textController.text;
  }

  bool get isCountFieldEnabled {
    if (widget.receiverType != pb.ReceiverType.RT_USER) {
      return true;
    } else {
      return _redType != 1;
    }
  }

  @override
  void initState() {
    super.initState();
    if (widget.receiverType != pb.ReceiverType.RT_USER) {
      if (widget.mRedType == 2) {
        _redType = 2;
      }
    }
  }

  @override
  void dispose() {
    // SystemChannels.textInput.invokeMethod('TextInput.hide');
    // FocusScope.of(OneContext().context!).requestFocus(FocusNode());
    // ChatInputScoped.isLock = false;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        SystemChannels.textInput.invokeMethod('TextInput.hide');
      },
      child: Scaffold(
        backgroundColor: defBackgroundColor,
        appBar: CustomAppbar(
          isBackIcon: true,
          isLineshade: false,
          title: _buildTitle(),
        ),
        body: ListView(
          // mainAxisAlignment: MainAxisAlignment.start,
          // crossAxisAlignment: CrossAxisAlignment.center,

          children: [
            SizedBox(height: 15.sp),
            Container(
              height: 78.sp,
              margin: EdgeInsets.symmetric(horizontal: 30.sp, vertical: defPaddingSize),
              padding: EdgeInsets.symmetric(horizontal: defPaddingSize, vertical: defPaddingSize),
              decoration: BoxDecoration(
                color: whiteColor,
                borderRadius: BorderRadius.circular(10.sp),
              ),
              child: Row(
                children: [
                  Text('金额', style: defTextStyle),
                  Expanded(
                    child: TextField(
                      textAlign: TextAlign.end,
                      keyboardType: TextInputType.number,
                      style: defTextStyle,
                      textAlignVertical: TextAlignVertical.center,
                      maxLines: 1,
                      controller: _moneyController,
                      inputFormatters: [
                        CustomTextFieldNumFormatter(digit: 2),
                        CustomTextFieldSpaceFormatter()
                      ],
                      // inputFormatters: [FilteringTextInputFormatter.allow(RegExp(r'^(([1-9]{1}\d*)|(0{1}\d*))(\.\d{0,2})?$'))],
                      maxLength: 8,
                      decoration: InputDecoration(
                        border: const OutlineInputBorder(borderSide: BorderSide.none),
                        contentPadding:
                            EdgeInsets.symmetric(horizontal: defPaddingSize, vertical: 0),
                        hintText: '0.0',
                        hintStyle: TextStyle(fontSize: defFontSize, color: grayColor),
                        counterText: '',
                      ),
                      onChanged: (String value) {
                        setState(() {
                          // if (value.isNotEmpty) {
                          //   String text = double.parse(value).toString();
                          //   if (text != value) {
                          //     _moneyController.value = TextEditingValue(
                          //       text: text,
                          //       selection: TextSelection.fromPosition(
                          //         TextPosition(offset: text.length),
                          //       ),
                          //     );
                          //   }
                          // }

                          // if (getMoney() > UserModel.dmoney) {
                          //   String text = UserModel.dmoney.toString();
                          //   _moneyController.value = TextEditingValue(
                          //     text: text,
                          //     selection: TextSelection.fromPosition(
                          //       TextPosition(offset: text.length),
                          //     ),
                          //   );
                          // }
                        });
                      },
                    ),
                  ),
                  Text('元', style: defTextStyle),
                ],
              ),
            ),
            Container(
              height: 78.sp,
              margin: EdgeInsets.symmetric(horizontal: 30.sp, vertical: defPaddingSize),
              padding: EdgeInsets.symmetric(horizontal: defPaddingSize, vertical: defPaddingSize),
              decoration: BoxDecoration(
                color: whiteColor,
                borderRadius: BorderRadius.circular(10.sp),
              ),
              child: Row(
                children: [
                  Text('红包个数', style: defTextStyle),
                  Expanded(
                    child: TextField(
                      textAlign: TextAlign.end,
                      keyboardType: TextInputType.number,
                      style: defTextStyle,
                      textAlignVertical: TextAlignVertical.center,
                      maxLines: 1,
                      controller: _numberController,
                      inputFormatters: [
                        FilteringTextInputFormatter.allow(RegExp(r'\d+')),
                        CustomTextFieldSpaceFormatter()
                      ],
                      decoration: InputDecoration(
                        border: const OutlineInputBorder(borderSide: BorderSide.none),
                        contentPadding:
                            EdgeInsets.symmetric(horizontal: defPaddingSize, vertical: 0),
                        hintText: '1',
                        hintStyle: TextStyle(fontSize: defFontSize, color: grayColor),
                        enabled: isCountFieldEnabled,
                        // suffixIcon: Text('个', style: defTextStyle, textAlign: TextAlign.center),
                        // suffixIconConstraints: BoxConstraints(),
                      ),
                      onChanged: (String value) {
                        if (value.isNotEmpty && int.parse(value) == 0) {
                          _numberController.value = TextEditingValue(
                              text: '1',
                              selection: TextSelection.fromPosition(const TextPosition(offset: 1)));
                        }
                      },
                    ),
                  ),
                  Text('个', style: defTextStyle),
                ],
              ),
            ),
            Container(
              height: 78.sp,
              margin: EdgeInsets.symmetric(horizontal: 30.sp, vertical: defPaddingSize),
              padding: EdgeInsets.symmetric(horizontal: defPaddingSize, vertical: defPaddingSize),
              decoration: BoxDecoration(
                color: whiteColor,
                borderRadius: BorderRadius.circular(10.sp),
              ),
              child: Row(
                children: [
                  Text('留言', style: defTextStyle),
                  Expanded(
                    child: TextField(
                      // textAlign: TextAlign.end,
                      keyboardType: TextInputType.text,
                      style: defTextStyle,
                      textAlignVertical: TextAlignVertical.center,
                      maxLines: 1,
                      maxLength: 20,
                      controller: _textController,
                      decoration: InputDecoration(
                        // icon: Text('留言', style: defTextStyle),
                        border: const OutlineInputBorder(borderSide: BorderSide.none),
                        contentPadding:
                            EdgeInsets.symmetric(horizontal: defPaddingSize, vertical: 0),
                        hintText: '恭喜发财,大吉大利!',
                        hintStyle: TextStyle(fontSize: defFontSize, color: grayColor),
                        counterText: '',
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 30.sp),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    '余额: ',
                    style: defTextStyle,
                  ),
                  Text(
                    '${UserModel.dmoney} 元',
                    style: TextStyle(
                      fontSize: defMinFontSize,
                      color: Colors.grey[600],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 50.sp),
              child: Center(
                child: Text('${getMoney()}￥',
                    style: TextStyle(
                        fontSize: 64.sp, color: defFontColor, fontWeight: FontWeight.bold)),
              ),
            ),
            Button(
              color: Colors.orange[800],
              borderCircular: 10.sp,
              disableColor: grayColor,
              isEnable: getMoney() > 0,
              margin: EdgeInsets.symmetric(horizontal: 80.sp),
              padding: EdgeInsets.symmetric(horizontal: 80.sp, vertical: 15.sp),
              child: Text('塞钱进钱包', style: TextStyle(fontSize: defFontSize, color: whiteColor)),
              onPressed: () {
                if (redCount < widget.minCount) {
                  EasyLoading.showError('红包最少${widget.minCount}个');
                  return;
                }
                Int64 money = Int64((getMoney() * 100).toInt());
                LogicRedPacket.SendRedPacket(
                  context: OneContext().context,
                  type: widget.receiverType,
                  receiveId: widget.receiverId,
                  amount: money,
                  count: redCount,
                  blessing: redTitle,
                  logicCallBack: (code, resp) {
                    CustomRoute().pop();
                  },
                );
                // pb.RedPacket redPacket = pb.RedPacket(num: redCount, amount: moeny, title: redTitle, level: 0);
                // print('RedPacket: ${redPacket.toString()}');
                // if (widget.receiverType == pb.ReceiverType.RT_ROOM) {
                //   LogicChat.PushRoom(
                //     roomId: widget.receiverId,
                //     messageType: pb.MessageType.MT_SEND_REDPACKET,
                //     messageContent: redPacket.writeToBuffer(),
                //     sendTime: LogicChat.getSendTime().toInt(),
                //     logicCallBack: (code, resp) {
                //       OneContext().pop();
                //     },
                //   );
                // } else {
                //   LogicChat.SendMessage(
                //     context: context,
                //     receiverType: widget.receiverType,
                //     receiverId: widget.receiverId,
                //     messageType: pb.MessageType.MT_SEND_REDPACKET,
                //     messageContent: redPacket.writeToBuffer(),
                //     sendTime: LogicChat.getSendTime().toInt(),
                //     logicCallBack: (code, resp) {
                //       OneContext().pop();
                //     },
                //   );
                // }
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildTitle() {
    if (widget.mRedType == 1) {
      return Text('发红包', style: defTitleTextStyle);
    } else {
      double buttonWidth = 140.sp;
      double circular = 5.sp;
      return Container(
        padding: EdgeInsets.all(3.sp),
        decoration: BoxDecoration(
          color: grayColor,
          borderRadius: BorderRadius.circular(circular),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              constraints: BoxConstraints(minWidth: buttonWidth),
              child: Button(
                // color: grayColor,
                borderCircular: circular,
                padding: EdgeInsets.symmetric(horizontal: 0.sp, vertical: 6.sp),
                disableColor: whiteColor,
                isEnable: _redType != 1,
                child:
                    Text('普通红包', style: TextStyle(fontSize: defMinFontSize, color: defFontColor)),
                onPressed: () {
                  setState(() {
                    _redType = 1;
                    if (widget.receiverType == pb.ReceiverType.RT_USER) {
                      _numberController.text = '';
                    }
                  });
                },
              ),
            ),
            Container(
              constraints: BoxConstraints(minWidth: buttonWidth),
              child: Button(
                // color: whiteColor,
                disableColor: whiteColor,
                isEnable: _redType != 2,
                borderCircular: circular,
                padding: EdgeInsets.symmetric(horizontal: 0.sp, vertical: 6.sp),
                child:
                    Text('拼手气红包', style: TextStyle(fontSize: defMinFontSize, color: defFontColor)),
                onPressed: () {
                  setState(() {
                    _redType = 2;
                    if (widget.receiverType == pb.ReceiverType.RT_USER) {
                      _numberController.text = '';
                    }
                  });
                },
              ),
            ),
          ],
        ),
      );
    }
  }
}
