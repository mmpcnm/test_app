import 'dart:math';

import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/wdiget/user_icon_widget.dart';
import 'package:fim_app/custom/wdiget/custom_list_table.dart';
import 'package:fim_app/package/pb/business.int.pb.dart' as pb;
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/package/util/time_utils.dart';
import 'package:fim_app/package/util/utils.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flukit/flukit.dart';
import 'package:flutter/material.dart';

class LookRedPacketDialog extends StatefulWidget {
  final pb.LookUpRedPacketResp resp;
  final pb.RedPacket redPacket;
  final Int64 userId;
  final String userIcon;
  final String userName;
  final Int64 money;
  final int number;
  const LookRedPacketDialog(
    this.resp, {
    Key? key,
    required this.redPacket,
    required this.userIcon,
    required this.userId,
    required this.userName,
    required this.money,
    required this.number,
  }) : super(key: key);

  static show(
    pb.LookUpRedPacketResp resp, {
    required pb.RedPacket redPacket,
    required Int64 userId,
    required String userIcon,
    required String userName,
  }) {
    OneContext().push(
      fadeRoute(
        LookRedPacketDialog(
          resp,
          redPacket: redPacket,
          userIcon: userIcon,
          userId: userId,
          userName: userName,
          money: redPacket.amount,
          number: redPacket.num,
        ),
      ),
    );

    // OneContext().showDialog(builder: (BuildContext context) {
    //   return LookRedPacketDialog(resp, redPacket: redPacket, userIcon: userIcon, userId: userId, userName: userName, money: redPacket.amount, number: redPacket.num);
    // });
  }

  @override
  State<StatefulWidget> createState() => StateLookRedPacketDialog();
}

class StateLookRedPacketDialog extends State<LookRedPacketDialog> with TickerProviderStateMixin {
  // double extraPicHeight = 0;
  // double moveDy = 0;
  //加在这里
  // late AnimationController animationController;
  // late Animation<double> anim;

  // bool _isScrollMove = false;
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //加在这里
    // animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    // anim = Tween(begin: 0.0, end: 0.0).animate(animationController);
    // _scrollController.addListener(() {
    //   if (!_isScrollMove) return;
    //   var offset = _scrollController.offset;
    //   // print('offset $offset');
    //   setState(() {
    //     //更新数据
    //     extraPicHeight = min(max(-offset, 0), 200);
    //   });
    //   // _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
    // });
  }

  @override
  void dispose() {
    // animationController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double toolbarHeight = defPaddingSizeV + defIconSize;
    double bottombarHeight = SUT_SP(84);

    return Scaffold(
      backgroundColor: defBackgroundColor,
      body: CustomScrollView(
        physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
        controller: _scrollController,
        slivers: <Widget>[
          SliverFlexibleHeader(
            visibleExtent: toolbarHeight + ScreenUtil.statusBarHeight + bottombarHeight,
            builder: (context, availableHeight, direction) {
              if (availableHeight < 1) return const Center();
              return SizedBox(
                height: availableHeight,
                child: Center(
                  child: ListView(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    reverse: true,
                    children: [
                      SizedBox(
                        height: max(availableHeight,
                            toolbarHeight + ScreenUtil.statusBarHeight + bottombarHeight),
                        child: Stack(
                          children: [
                            Positioned(
                              left: 0,
                              right: 0,
                              top: 0,
                              bottom: bottombarHeight - 1,
                              child: Container(
                                color: const Color.fromARGB(255, 237, 101, 66),
                                padding: EdgeInsets.only(top: ScreenUtil.statusBarHeight),
                                margin: EdgeInsets.zero,
                                child: CustomListTile(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: defPaddingSizeH, vertical: defPaddingSizeV),
                                  leading: InkWell(
                                    child: Icon(Icons.arrow_back_ios,
                                        size: defIconSize, color: blackColor),
                                    onTap: () => OneContext().pop(),
                                  ),
                                ),
                              ),
                            ),
                            Positioned(
                              left: 0,
                              right: 0,
                              // top: toolbarHeight + ScreenUtil.statusBarHeight,
                              bottom: 0,
                              child: Image.asset(
                                ResImgs('red/red_envelop_detail_top.9'),
                                // width: dialogWidth,
                                height: bottombarHeight,
                                fit: BoxFit.fill,
                              ),
                            ),
                          ],
                        ),
                      ),
                      // Column(
                      //   mainAxisSize: MainAxisSize.min,
                      //   children: [
                      //     Container(
                      //       color: const Color.fromARGB(255, 237, 101, 66),
                      //       padding: EdgeInsets.only(top: ScreenUtil.statusBarHeight),
                      //       margin: EdgeInsets.zero,
                      //       height: max(availableHeight - bottombarHeight, toolbarHeight + ScreenUtil.statusBarHeight),
                      //       child: CustomListTile(
                      //         padding: EdgeInsets.symmetric(horizontal: defPaddingSizeH, vertical: defPaddingSizeV),
                      //         leading: InkWell(
                      //           child: Icon(Icons.arrow_back_ios, size: defIconSize, color: blackColor),
                      //           onTap: () => OneContext().pop(),
                      //         ),
                      //       ),
                      //     ),
                      //     Image.asset(
                      //       ResImgs('red/red_envelop_detail_top.9'),
                      //       // width: dialogWidth,
                      //       height: bottombarHeight,
                      //       fit: BoxFit.fill,
                      //     ),
                      //   ],
                      // ),
                    ],
                  ),
                ),
              );
            },
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              <Widget>[
                SizedBox(height: defPaddingSize),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    UserIconWidget(widget.userIcon, circular: 64.sp),
                    SizedBox(width: 5.sp),
                    Text(widget.userName, style: TextStyle(fontSize: 32.sp, color: defFontColor)),
                  ],
                ),
                SizedBox(height: 25.sp),
                Center(
                  child: Text(widget.redPacket.title, style: defTextStyle),
                ),
                SizedBox(height: 25.sp),
                Center(
                  child: Text(
                    '${Utils.moenyToDouble(widget.money).toStringAsFixed(2)} 元',
                    style: TextStyle(
                        fontSize: 64.sp, color: defFontColor, fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(height: 25.sp),
              ],
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (context, index) {
                pb.RedPacketItem item = widget.resp.messages[index];
                return CustomListTable.buildTable(
                  leading: UserIconWidget(item.avatar, size: 64.sp),
                  leftWidget: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Expanded(
                            child: Text(
                              item.name,
                              style: TextStyle(
                                fontSize: defFontSize,
                                color: defFontColor,
                              ),
                            ),
                          ),
                          Text(
                            Utils.moenyToDouble(item.amount).toStringAsFixed(2) + ' 元',
                            style: TextStyle(
                              fontSize: defFontSize,
                              color: defFontColor,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 5.sp),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            TimeUtils.timeToString(item.time.toInt()),
                            style: TextStyle(
                              fontSize: defFontSize,
                              color: lightGrayColor,
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                  // rightWidget: Text(
                  //   TimeUtils.timeToString(item.time.toInt()),
                  //   style: TextStyle(
                  //     fontSize: defFontSize,
                  //     color: defFontColor,
                  //   ),
                  // ),
                  trailing: Center(),
                );
              },
              childCount: widget.resp.messages.length,
            ),
          ),
        ],
      ),
    );
  }
}

class SliverTopBar extends StatelessWidget {
  const SliverTopBar({Key? key, required this.extraPicHeight}) : super(key: key);
  final double extraPicHeight; //传入的加载到图片上的高度

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    double dialogWidth = SUT_SP(750);
    double dialogHeight = SUT_SP(84);
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
          //缩放的图片
          height: extraPicHeight,
          color: Color.fromARGB(255, 237, 101, 66),
        ),
        Image.asset(
          ResImgs('red/red_envelop_detail_top.9'),
          width: dialogWidth,
          height: dialogHeight,
        ),
      ],
    );
  }
}
