import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/wdiget/user_icon_widget.dart';
import 'package:fim_app/package/pb/business.int.pb.dart' as pb;
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:one_context/one_context.dart';
import 'package:path/path.dart';

class RobRedPacketDialog extends StatelessWidget {
  static show(Int64 userId, String userIcon, String userName, pb.RobRedPacketResp resp) {
    // OneContext().showDialog(builder: (BuildContext context) {
    //   return RobRedPacketDialog(amount: resp.amount);
    // });
    String money = (resp.amount.toDouble() / 100).toStringAsFixed(2);
    EasyLoading.showSuccess('你抢到了${money}元');
  }

  Int64 userId;
  String userIcon;
  String userName;
  Int64 amount;
  RobRedPacketDialog(
      {required this.userIcon, required this.userId, required this.userName, required this.amount});

  @override
  Widget build(BuildContext context) {
    String money = (amount.toDouble() / 100).toStringAsFixed(2);
    double dialogWidth = SUT_SP(574) * 0.9;
    double dialogHeight = SUT_SP(720) * 0.9;
    return Center(
      child: Container(
        constraints: BoxConstraints(
            minWidth: dialogWidth,
            maxWidth: dialogWidth,
            minHeight: dialogHeight,
            maxHeight: dialogHeight),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(ResImgs('red/sign_red')),
          ),
        ),
        child: Column(
          children: [
            SizedBox(height: defPaddingSize),
            Row(
              children: [
                UserIconWidget(userIcon, circular: 45.sp),
                Text(userName, style: defTextStyle),
              ],
            ),

            // Text('${Utils.moenyToDouble(money).toStringAsFixed(2)} 元', style: defTextStyle),
            // // Expanded(child: SizedBox()),
            // ListView.builder(
            //   padding: EdgeInsets.symmetric(horizontal: paddingHorizontal),
            //   physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
            //   itemBuilder: (context, index) {
            //     pb.RedPacketItem item = resp.messages[index];
            //     return CustomListTable.buildTable(
            //       leftWidget: Text(item.name, style: TextStyle(fontSize: defMinFontSize, color: defFontColor)),
            //       middleWidget: Text(Utils.moenyToDouble(item.amount).toStringAsFixed(2), style: TextStyle(fontSize: defMinFontSize, color: defFontColor)),
            //       rightWidget: Text(TimeUtils.timeToString(item.time.toInt()), style: TextStyle(fontSize: defMinFontSize, color: defFontColor)),
            //       arrowForwardIcon: Center(),
            //     );
            //   },
            //   itemCount: resp.messages.length,
            // ),
          ],
        ),
      ),
    );
  }
}
