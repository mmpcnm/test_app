import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/custom/wdiget/CustomRefreshHandler.dart';
import 'package:fim_app/custom/wdiget/EmptyBackWidger.dart';
import 'package:fim_app/custom/wdiget/custom_list_table.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/controller/logic_red_packet.dart';
import 'package:fim_app/package/pb/business.int.pb.dart' as pb;
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:flutter/material.dart';
import 'package:one_context/one_context.dart';

class RedPacketInfoPage extends StatefulWidget {
  const RedPacketInfoPage({Key? key}) : super(key: key);

  // final VoidCallback onCloseClick;
  // RedPacketInfoPage({required this.onCloseClick});
  @override
  State<StatefulWidget> createState() => _StateRedPacketInfoPage();

  static show() {
    CustomRoute().showModalBottomSheet(
      backgroundColor: const Color(0x00FFFFFF),
      isScrollControlled: true,
      isDismissible: true,
      builder: (BuildContext context) {
        return const RedPacketInfoPage(
            // onCloseClick: () {
            //   OneContext().popDialog();
            // },
            );
      },
    );
  }
}

class _StateRedPacketInfoPage extends State<RedPacketInfoPage> {
  final List<pb.RobAuthItem> _items = [];
  late CustomRefreshHanler mCustomRefreshHanler;

  int get _itemCount {
    if (_items.isEmpty) return 0;
    return _items.length + 1;
  }

  @override
  void initState() {
    super.initState();
    mCustomRefreshHanler = CustomRefreshHanler(onRefreshHandlerStart: onRefreshHandlerStart, onRefreshHandlerEnd: onRefreshHandlerEnd);
    mCustomRefreshHanler.refreshHandler();
    // mCustomRefreshHanler.dispose()
  }

  @override
  void dispose() {
    mCustomRefreshHanler.dispose();
    super.dispose();
  }

  Future onRefreshHandlerEnd() async {
    print('count: ${_itemCount}');
    setState(() {});
  }

  Future onRefreshHandlerStart() async {
    await LogicRedPacket.GetRedPacketRobAuth(
      logicCallBack: (code, resp) {
        _items.clear();
        if (code == 0) {
          _items.addAll(resp!.items);
          _items.sort((a, b) {
            return a.level.compareTo(b.level);
          });
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: SUT_S_HEIGHT * 0.7,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(SUT_SP(35)),
          topRight: Radius.circular(SUT_SP(35)),
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(height: 5.sp),
          CustomListTile(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(SUT_SP(35)),
              topRight: Radius.circular(SUT_SP(35)),
            ),
            padding: EdgeInsets.symmetric(horizontal: defPaddingSizeH, vertical: defPaddingSizeH),
            middletitle: Text('我的红包', style: defTitleTextStyle),
            trailing: Button(
              padding: EdgeInsets.zero,
              child: const Icon(Icons.close_outlined),
              onPressed: () {
                OneContext().popDialog();
              },
            ),
          ),
          Divider(height: 1, color: lineColor),
          Expanded(
            child: Container(
              child: (_itemCount == 0)
                  ? Center(child: EmptyBackWidget(image: ResImgs('no_data_empty'), text: STR(10268)))
                  : mCustomRefreshHanler.buildWidget(
                      context,
                      builder: () {
                        return ListView.separated(
                          physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                          itemBuilder: (BuildContext context, int index) {
                            if (index == _items.length) {
                              return const Center();
                            }
                            var item = _items[index];
                            return CustomListTile(
                              color: Colors.grey,
                              title: Text('${item.level}级红包', style: defTextStyle),
                              middletitle: Text('${item.num}', style: defTextStyle),
                            );
                          },
                          separatorBuilder: (BuildContext context, int index) {
                            return Divider(height: 1, color: lineColor);
                          },
                          itemCount: _itemCount,
                        );
                        // return Center();
                      },
                    ),
            ),
          ),
          SizedBox(height: ScreenUtil.bottomBarHeight),
        ],
      ),
    );
  }
}
