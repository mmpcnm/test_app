// import 'package:azlistview/azlistview.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/custom/azlistview/azlistview.dart';
import 'package:fim_app/custom/dialog/CustomDialog.dart';
import 'package:fim_app/custom/dialog/CustomPicker.dart';
import 'package:fim_app/custom/wdiget/EmptyBackWidger.dart';
import 'package:fim_app/custom/wdiget/custom_list_table.dart';
import 'package:fim_app/custom/wdiget/user_icon_widget.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/controller/logic_security.dart';
import 'package:fim_app/package/pb/logic.ext.pb.dart' as pb;
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/package/util/time_utils.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

class QueryModel extends ISuspensionBean {
  pb.BillItem item;
  // late String icon; //图标
  // late String productName; //产品名
  // late String param; //产品名， 和产品名拼接为title
  // late int type; //类型 1：入账 2：出账 3:内部转移，如钱包到支付宝(目前忽略）',
  // late int status; //当前状态 0-失败，1-成功（支付成功，或者已收钱）
  // late int way; //支付方式 （0：无 1：钱包）  收款方式 （0：无 1:红包 2. 转账 3：二维码扫码 ）
  // late Int64 money; //金额
  // late Int64 opId; //操作类型
  // late String innerOrderId; //内部单号 （收款， 就只有内部单号，无外部单号）
  // late String outerOrderId; //外部单号 （目前没用）
  // late Int64 time; //操作时间
  QueryModel({
    required this.item,
    // this.icon = '',
    // this.productName = '',
    // this.param = '',
    // this.type = 0,
    // this.status = 0,
    // this.way = 0,
    // this.money = Int64.ZERO,
    // this.opId = Int64.ZERO,
    // this.innerOrderId = '',
    // this.outerOrderId = '',
    // this.time = Int64.ZERO,
  });
  // QueryModel.form(pb.BillItem item) {
  //   icon = item.icon;
  //   productName = item.productName;
  //   param = item.param;
  //   type = item.type;
  //   status = item.status;
  //   way = item.way;
  //   money = item.money;
  //   opId = item.opId;
  //   innerOrderId = item.innerOrderId;
  //   outerOrderId = item.outerOrderId;
  //   time = item.time;
  // }

  @override
  String getSuspensionTag() {
    DateTime dateTime = localDate;
    String suspensionTag = '${dateTime.year}年${dateTime.month}月';
    return suspensionTag;
  }

  DateTime get localDate {
    return TimeUtils.sToLocalDate(item.time.toInt());
  }

  // @override
  // String toString() => json.encode(this);

  String getStrMoney() {
    double ret = item.money.toInt() / 100;
    if (item.money > 0) {
      return '+${ret.toStringAsFixed(2)}';
    } else {
      return ret.toStringAsFixed(2);
    }
  }

  String getAfterMoney() {
    double ret = item.afterMoney.toInt() / 100;
    return ret.toStringAsFixed(2);
    // if (item.afterMoney > 0) {
    //   return '+${ret.toStringAsFixed(2)}';
    // } else {
    //   return '${ret.toStringAsFixed(2)}';
    // }
  }
}

class QueryBillPage extends StatefulWidget {
  const QueryBillPage({Key? key}) : super(key: key);

  @override
  StateQueryBillPage createState() => StateQueryBillPage();
}

class StateQueryBillPage extends State<QueryBillPage> {
  List<QueryModel> queryList = [];
  // int _currentMonth = 12;
  late DateTime _currentDate;

  // List<dynamic> _paickList = [];
  final List<DateTime> _dateList = [];

  @override
  void initState() {
    super.initState();

    DateTime dateTime = TimeUtils.sToLocalDate(TimeUtils.serverTime);
    // _currentMonth = dateTime.month;
    _currentDate = TimeUtils.serverDate;
    loadData(_currentDate);
    // _listYears.add(dateTime.year);

    // Map<int, List<String>> pickMap = {};

    for (int index = 0; index < 12; ++index) {
      int year = dateTime.year;
      int month = dateTime.month - index;
      if (month <= 0) {
        year = year - 1;
        month = 12 - month;
      }
      // _paickPDurations.add(PDuration(year: year, month: month));
      // if (!pickMap.containsKey(year)) {
      //   pickMap[year] = [];
      // }
      // pickMap[year]?.add('${month}月');

      _dateList.add(DateTime(year, month, dateTime.day, dateTime.minute, dateTime.second, dateTime.millisecond));
    }
    // var keys = pickMap.keys;
    // // _paickData = '[';
    // for (var year in keys) {
    //   var months = pickMap[year]!;
    //   var yearKey = '$year年';
    //   Map<String, List<String>> monthMap = {yearKey: months};
    //   // _paickList.add(monthMap);
    // }
  }

  void loadData(DateTime date) {
    print('loadData:${date.toString()}');
    showLoding('', duration: const Duration(seconds: 30), maskType: EasyLoadingMaskType.black);
    LogicSecurity.QueryBill(
      context: context,
      month: date.month,
      logicCallBack: (code, resp) {
        EasyLoading.dismiss();
        if (code == 0 && resp != null) {
          queryList.clear();
          for (var item in resp.items) {
            if (item.money != 0) queryList.add(QueryModel(item: item));
          }

          queryList.sort((itema, itemb) => itemb.item.time.compareTo(itema.item.time));
          print(queryList.length);
          SuspensionUtil.setShowSuspensionStatus(queryList);
          // for (int index = 0; index < 3; ++index) {
          // queryList.addAll(List.from(queryList));
          // }
          setState(() {});
        } else {
          queryList.clear();
          SuspensionUtil.setShowSuspensionStatus(queryList);
          setState(() {});
        }
      },
    );
  }

  //分组标题
  Widget getSusItem(BuildContext context, QueryModel model) {
    return Container(
      // height: 78.sp,
      width: SUT_S_WIDTH,
      color: Colors.white,
      padding: EdgeInsets.symmetric(horizontal: defPaddingSizeH, vertical: 15.sp),
      child: Row(
        children: [
          Text(
            model.getSuspensionTag(),
            style: TextStyle(fontSize: defFontSize, color: defFontColor),
          ),
          Icon(Icons.keyboard_arrow_down, size: defIconSize),
        ],
      ),

      // CustomListTable.buildTable(
      //   backColor: defBackgroundColor,
      //   mainAxisSize: MainAxisSize.max,
      //   leftWidget: Row(
      //     children: [
      //       Text(
      //         model.getSuspensionTag(),
      //         style: TextStyle(fontSize: defFontSize, color: defFontColor),
      //       ),
      //       Icon(Icons.keyboard_arrow_down, size: defIconSize),
      //     ],
      //   ),
      //   trailing: const Center(),
      //   onPressed: () {
      //     _showPaick(selectDate: model.localDate);
      //   },
      // ),
    );
  }

  ///数据
  Widget getListItem(BuildContext context, QueryModel model) {
    DateTime time = model.localDate;
    return CustomListTile(
      leading: UserIconWidget(model.item.icon),
      title: Row(
        children: [
          Expanded(
            child: Text(
              '${model.item.productName}${model.item.param}',
              style: defTextStyle,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          SizedBox(width: 5.sp),
          Text(
            model.getStrMoney(),
            style: TextStyle(
              fontSize: defFontSize,
              color: model.item.money.toInt() >= 0 ? Colors.yellow[800] : defFontColor,
            ),
          ),
        ],
      ),
      subtitle: Row(
        children: [
          Expanded(
            child: Text(
              '${time.month}月${time.day}日 ${time.hour}:${time.minute}',
              style: TextStyle(
                fontSize: defMinFontSize,
                color: grayColor,
              ),
              overflow: TextOverflow.ellipsis,
            ),
          ),
          SizedBox(width: 5.sp),
          Text(
            '零钱余额 ${model.getAfterMoney()}',
            style: TextStyle(
              fontSize: defMinFontSize,
              color: grayColor,
            ),
          ),
        ],
      ),
      isArrowIcon: true,
      bottomLine: Divider(height: 1, color: lineColor, indent: 108.sp),
      padding: EdgeInsets.symmetric(horizontal: defPaddingSizeH, vertical: 15.sp),
      // color: Colors.white,
      onPressed: () {},
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: defBackgroundColor,
      appBar: CustomAppbar(
        isBackIcon: true,
        isLineshade: false,
        title: Text('零钱明细', style: defTitleTextStyle),
      ),
      // body: EasyRefresh(
      //   emptyWidget: Visibility(
      //     visible: queryList.isEmpty,
      //     child: EmptyBackWidget(image: ResImgs('no_data_empty'), text: STR(10268)),
      //   ),
      //   header: BallPulseHeader(),
      //   onRefresh: () async {
      //     await Future.delayed(const Duration(seconds: 2), () {
      //       if (mounted) {
      //         print('onRefresh');
      //         loadData(_currentMonth);
      //       }
      //     });
      //   },
      //   child:
      body: Stack(
        children: [
          Visibility(
            visible: queryList.isEmpty,
            child: EmptyBackWidget(image: ResImgs('no_data_empty'), text: STR(10268)),
          ),
          Visibility(
            visible: queryList.isEmpty,
            child: getSusItem(
                context,
                // QueryModel(time: Int64(TimeUtils.serverTime)),
                QueryModel(item: pb.BillItem(time: Int64(_currentDate.millisecondsSinceEpoch)))),
          ),
          Visibility(
            visible: queryList.isNotEmpty,
            child: SuspensionView(
              physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
              data: queryList,
              itemCount: queryList.length,
              itemBuilder: (BuildContext context, int index) {
                QueryModel model = queryList[index];
                return getListItem(context, model);
              },
              // padding: EdgeInsets.zero,
              susItemBuilder: (BuildContext context, int index) {
                QueryModel model = queryList[index];
                // String tag = model.getSuspensionTag();
                // print(tag);
                return getSusItem(context, model);
              },
            ),
          ),
        ],
      ),
      // ),
    );
  }

  void _showPaick({DateTime? selectDate}) {
    // var localDate = model.localDate;
    // print(_paickData);
    CustomPicker.showDatePicker(
      context,
      // topTime: _dateList.last,
      bottomTime: _dateList.first,
      selectDate: selectDate,
      suffix: PickerSuffix(year: '年', month: '月', day: '日'),
      dateType: PickerDateType.YM,
      onConfirm: (List<int> values) {
        if (values.length != 2) return;

        int iyear = values[0];
        int iMonth = values[1];
        DateTime dateTime = TimeUtils.sToLocalDate(TimeUtils.serverTime);
        int millisecond =
            DateTime(iyear, iMonth, dateTime.day, dateTime.hour, dateTime.minute, dateTime.second, dateTime.millisecond).millisecondsSinceEpoch;
        dateTime = TimeUtils.lToSeverDate(millisecond);
        _currentDate = dateTime;
        // _currentMonth = dateTime.month;
        loadData(_currentDate);
      },
    );
  }
}
