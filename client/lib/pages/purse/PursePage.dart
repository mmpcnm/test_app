import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/dialog/CustomDialog.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/custom/wdiget/custom_list_table.dart';
import 'package:fim_app/package/net/controller/logic_security.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/pages/purse/PocketMoneyPage.dart';
import 'package:fim_app/pages/security/BindAlipayPage.dart';
import 'package:fim_app/pages/security/SettingWalletPwdPage.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:one_context/one_context.dart';
import 'package:scoped_model/scoped_model.dart';
// import 'package:flutter_icons/flutter_icons.dart';

class PursePage extends StatelessWidget {
  PursePage() {
    if (!UserModel.isWalletPwdSet) {
      Future.delayed(Duration(milliseconds: 100), () {
        showMessageDialogEx(
          OneContext().context!,
          content: '还未设置支付密码，是否设置?',
          confirmCallback: () {
            OneContext().push(pageRoute(SettingWalletPwdPage(title: '设置支付密码')));
          },
        );
      });
    }

    LogicSecurity.OpenWallet();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: defBackgroundColor,
      appBar: CustomAppbar(
        isBackIcon: true,
        title: Text('钱包', style: defTitleTextStyle),
      ),
      body: ListView(
        physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
        children: [
          ScopedModel<UserModel>(
            model: UserModel.instance,
            child: ScopedModelDescendant<UserModel>(
              builder: (BuildContext context, Widget? child, UserModel model) {
                return _buildTable(
                  icon: Icon(Icons.attach_money, size: defIconSize, color: Colors.yellow[800]),
                  // icon: Text(
                  //   '￥',
                  //   style: TextStyle(fontSize: defIconSize, color: defFontColor, fontWeight: FontWeight.bold),
                  // ),
                  leftText: '零钱',
                  rightText: UserModel.dmoney.toStringAsFixed(2),
                  onPressed: () {
                    OneContext().push(pageRoute(PocketMoneyPage()));
                  },
                );
              },
            ),
          ),
          _buildTable(
            icon: Image.asset(ResImgs('icon_my_bank_card'),
                width: defIconSize, height: defIconSize, color: Colors.blue),
            // icon: Icon(Icons.credit_card_outlined, size: defIconSize, color: Colors.blue),
            leftText: '银行卡',
            onPressed: () {
              EasyLoading.showToast('开发中。。。');
            },
          ),
          _buildTable(
            icon: Image.asset(ResImgs('ali_pay'), width: defIconSize, height: defIconSize),
            // icon: Icon(Icons.credit_card_outlined, size: defIconSize, color: Colors.blue),
            leftText: '支付宝',
            isLine: false,
            onPressed: () {
              // EasyLoading.showToast('开发中。。。');
              OneContext().push(pageRoute(BindAlipayPage()));
            },
          ),
          Container(height: defTabelSplitHeight, color: lineColor),
          _buildTable(
            icon: Image.asset(ResImgs('icon_account'),
                width: defIconSize, height: defIconSize, color: Colors.blue),
            leftText: '身份信息',
            onPressed: () {
              EasyLoading.showToast('开发中。。。');
            },
          ),
          _buildTable(
            icon: Image.asset(ResImgs('icon_account_security'),
                width: defIconSize, height: defIconSize),
            leftText: '安全保障',
            onPressed: () {
              EasyLoading.showToast('开发中。。。');
            },
          ),
        ],
      ),
    );
  }

  Widget _buildTable(
      {required Widget icon,
      required String leftText,
      String? rightText,
      bool isLine = true,
      VoidCallback? onPressed}) {
    return CustomListTile(
      leading: icon,
      title: Row(
        children: [
          Text(leftText, style: defTextStyle),
          if (rightText != null) ...[
            const Expanded(child: SizedBox()),
            Text(rightText, style: defTextStyle),
          ]
        ],
      ),
      bottomLine: isLine ? Divider(height: 1, color: lineColor) : null,
      padding: EdgeInsets.symmetric(horizontal: defPaddingSizeH, vertical: 25.sp),
      isArrowIcon: true,
      onPressed: onPressed,
    );
    // CustomListTable.buildTable(
    //   padding: EdgeInsets.symmetric(horizontal: defPaddingSize, vertical: 25.sp),
    //   leftText: leftText,
    //   leftWidget: Row(
    //     children: [
    //       icon,
    //       SizedBox(width: defPaddingSize),
    //       Text(leftText, style: defTextStyle),
    //     ],
    //   ),
    //   rightText: rightText,
    //   isLine: isLine,
    //   onPressed: onPressed,
    // );
  }
}
