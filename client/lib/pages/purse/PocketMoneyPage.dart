import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:one_context/one_context.dart';

import 'QueryBillPage.dart';

class PocketMoneyPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: defBackgroundColor,
      resizeToAvoidBottomInset: false,
      appBar: CustomAppbar(
        isBackIcon: true,
        isLineshade: false,
        actions: [
          InkWell(
            child: Text('零钱明细', style: defTextStyle),
            onTap: () {
              OneContext().push(pageRoute(QueryBillPage()));
            },
          ),
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(defPaddingSize),
        child: Column(
          children: [
            // SizedBox(height: paddingHorizontal),
            Center(child: Image.asset(ResImgs('other_pay'), width: SUT_SP(78), height: SUT_SP(78))),
            SizedBox(height: defPaddingSize * 2),
            Text('我的零钱', style: defTextStyle),
            // SizedBox(height: 5.sp),
            Text(
              '￥${UserModel.dmoney.toStringAsFixed(2)}',
              style: TextStyle(fontSize: 64.sp, color: defFontColor, fontWeight: FontWeight.bold),
            ),
            Expanded(child: SizedBox()),
            Button(
              // margin: EdgeInsets.symmetric(horizontal: 100.sp),
              padding: EdgeInsets.symmetric(horizontal: 130.sp, vertical: 20.sp),
              color: Colors.greenAccent[700],
              child: Text(
                '充值',
                style: TextStyle(fontSize: defFontSize, color: whiteColor),
              ),
              onPressed: () {
                EasyLoading.showToast('开发中。。。');
              },
            ),
            SizedBox(height: 25.sp),
            Button(
              // margin: EdgeInsets.symmetric(horizontal: 100.sp),
              padding: EdgeInsets.symmetric(horizontal: 130.sp, vertical: 20.sp),
              color: Colors.grey[350],
              disableColor: grayColor,
              isEnable: UserModel.money > 0,
              child: Text(
                '提现',
                style: TextStyle(
                    fontSize: defFontSize,
                    color: UserModel.money > 0 ? Colors.green[600] : Colors.white70),
              ),
              onPressed: () {
                EasyLoading.showToast('开发中。。。');
              },
            ),
            SizedBox(height: 100.sp),
          ],
        ),
      ),
    );
  }
}
