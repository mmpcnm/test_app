// ignore_for_file: no_logic_in_create_state

import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/wdiget/dot_tips.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/pages/home/view/friend_view.dart';
import 'package:fim_app/pages/home/view/message_view.dart';
import 'package:fim_app/pages/home/view/my_view.dart';
import 'package:fim_app/pages/home/view/small_program.dart';
import 'package:fim_app/pages/webview/webview.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/model/chat/MessageModel.dart';
import 'package:fim_app/model/friend/new_friend_model.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  final int currentIndex;
  const HomePage({Key? key, this.currentIndex = 0}) : super(key: key);
  @override
  State<StatefulWidget> createState() =>
      StateHomePage(pageController: PageController(initialPage: currentIndex));
}

class StateHomePage extends State<HomePage> {
  final PageController pageController;
  final GlobalKey<_BottomBar> _bottomBarkey = GlobalKey();

  StateHomePage({required this.pageController});

  @override
  void initState() {
    super.initState();
    // LogicFriend.GetFriends(logicCallBack: (code, resp) {
    //   LogicGroup.GetGroups(logicCallBack: (code, resp) {
    //     setState(() {});
    //   });
    // });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        // OneContext().pushReplacementNamed(context, '/sign_in');
        return false;
      },
      child: Scaffold(
        backgroundColor: defBackgroundColor,
        bottomNavigationBar: BottomBar(
          currentIndex: widget.currentIndex,
          key: _bottomBarkey,
          onTap: (index) {
            if (pageController.hasClients) {
              pageController.jumpToPage(index);
              // _pageController.animateToPage(index, duration: const Duration(milliseconds: 300), curve: Curves.decelerate);
            }
          },
        ),
        body: PageView(
          children: [
            const MessageView(),
            const FriendView(),
            WebView(title: STR(10003), isWantKeepAlive: true),
            SmallProgram(), //发现
            MyView(),
          ],
          onPageChanged: (int index) {
            _bottomBarkey.currentState!.setCurrentIndex(index);
          },
          controller: pageController,
          pageSnapping: false,
          physics: const NeverScrollableScrollPhysics(),
        ),
      ),
    );
  }
}

class BottomBar extends StatefulWidget {
  final void Function(int) onTap;
  final int currentIndex;

  const BottomBar({Key? key, required this.currentIndex, required this.onTap}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _BottomBar();
  }
}

class _BottomBar extends State<BottomBar> {
  int _currentIndex = 0;
  @override
  void initState() {
    super.initState();
    _currentIndex = widget.currentIndex;
    MessageModel().addListener(onChatListener);
    NewFriendModel.instance.addListener(onNewFriendListener);
  }

  @override
  void dispose() {
    MessageModel().removeListener(onChatListener);
    NewFriendModel.instance.removeListener(onNewFriendListener);
    super.dispose();
  }

  void setCurrentIndex(int index) {
    _currentIndex = index;
    setState(() {});
  }

  void onChatListener() {
    setState(() {});
  }

  void onNewFriendListener() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      onTap: (index) {
        setState(() {
          _currentIndex = index;
        });
        widget.onTap.call(index);
      },
      backgroundColor: defBackgroundColor,
      type: BottomNavigationBarType.fixed,
      fixedColor: Colors.black,
      currentIndex: _currentIndex,
      enableFeedback: false,
      unselectedItemColor: Colors.black,
      // selectedFontSize: SUT_SP(18),
      // unselectedFontSize: SUT_SP(18),
      selectedLabelStyle: TextStyle(fontSize: defMinFontSize),
      unselectedLabelStyle: TextStyle(fontSize: defMinFontSize),
      items: <BottomNavigationBarItem>[
        _buildBarItem(ResImgs('check_msg'), ResImgs('uncheck_msg'), STR(10001),
            MessageModel().getAllUnreadCount() > 0),
        _buildBarItem(ResImgs('check_contact'), ResImgs('uncheck_contact'), STR(10002),
            NewFriendModel.instance.count > 0),
        _buildBarItem(ResImgs('check_webpage'), ResImgs('uncheck_webpage'), STR(10003), false),
        _buildBarItem(ResImgs('check_find_page'), ResImgs('uncheck_find_page'), STR(10004), false),
        _buildBarItem(ResImgs('check_mine'), ResImgs('uncheck_mine'), STR(10005), false),
      ],
    );
  }

  Widget _buildIcon(String imagePath, double iconSize, bool isDotTips) {
    EdgeInsets imagePadding = EdgeInsets.only(left: 15.sp, right: 15.sp, top: 5.sp);
    if (!isDotTips) {
      return Padding(
        padding: imagePadding,
        child: Image(
          image: AssetImage(imagePath),
          width: iconSize,
          height: iconSize,
        ),
      );
    } else {
      return Stack(
        alignment: Alignment.center,
        children: [
          Padding(
            padding: imagePadding,
            child: Image(
              image: AssetImage(imagePath),
              width: iconSize,
              height: iconSize,
            ),
          ),
          Positioned(
            top: 0,
            right: 0,
            child: DotTips(),
          ),
        ],
      );
    }
  }

  BottomNavigationBarItem _buildBarItem(
      String imagePath, String unImagePath, String textTitle, bool isDotTips) {
    double iconSize = defIconSize;
    return BottomNavigationBarItem(
        icon: _buildIcon(unImagePath, iconSize, isDotTips),
        activeIcon: _buildIcon(imagePath, iconSize, isDotTips),
        // Image(
        //   image: AssetImage(unImagePath),
        //   width: iconSize,
        //   height: iconSize,
        // ),
        // activeIcon: Image(
        //   image: AssetImage(imagePath),
        //   width: iconSize,
        //   height: iconSize,
        // ),
        label: textTitle);
  }
}
