import 'package:fim_app/custom/app_bar/LodingTitle.dart';
import 'package:fim_app/custom/custom_popup_menu/home_title_pop.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/wdiget/CustomRefreshHandler.dart';
import 'package:fim_app/custom/wdiget/EmptyBackWidger.dart';
import 'package:fim_app/package/net/controller/logic_friend.dart';
import 'package:fim_app/package/net/controller/logic_group.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/model/chat/list/Base/MessageListBase.dart';
import 'package:fim_app/model/chat/MessageModel.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/pages/search/search_page.dart';
import './message/message_item.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

import 'package:scoped_model/scoped_model.dart';

class MessageView extends StatefulWidget {
  const MessageView({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MessageView();
}

class _MessageView extends State<MessageView> with AutomaticKeepAliveClientMixin {
  late CustomRefreshHanler mCustomRefreshHanler;
  List<MessageListBase> _messages = [];

  @override
  void initState() {
    super.initState();
    _messages = MessageModel().getRootItems();
    mCustomRefreshHanler = CustomRefreshHanler(
        onRefreshHandlerStart: onRefreshHandlerStart, onRefreshHandlerEnd: onRefreshHandlerEnd);
    mCustomRefreshHanler.refreshHandler();
  }

  @override
  void dispose() {
    mCustomRefreshHanler.dispose();
    super.dispose();
    print('chatPage dispose');
  }

  @override
  bool get wantKeepAlive => true;

  Future onRefreshHandlerStart() async {
    await LogicFriend.GetFriends();
    await Future.delayed(const Duration(milliseconds: 10));
    await LogicGroup.GetGroups();
    await Future.delayed(const Duration(milliseconds: 10));
    await LogicGroup.GetAllRoom();
  }

  Future onRefreshHandlerEnd() async {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: defBackgroundColor,
      appBar: CustomAppbar(
        isLineshade: true,
        title: LodingTitle(
          noneChild: Text(STR(10001), style: defTitleTextStyle),
          waitingChild: Text('${STR(10001)}(${STR(10227)})', style: defTitleTextStyle),
        ),
        actions: [
          Button(
            child: Icon(Icons.search, color: blackColor, size: defIconSize),
            padding: const EdgeInsets.all(0),
            onPressed: () =>
                SearchPage.showSearchPage(), //OneContext().push(pageRoute(SearchPage())),
          ),
          SizedBox(width: 5.sp),
          HomeTitlePop(),
        ],
        // bottomHeight: 64.sp,
        // bottom: ,
        // bottom: Container(
        //   color: Colors.red,
        //   height: 100.sp,
        // ),
      ),
      body: MessageModel().buidler(
        builder: (context, child, model) {
          _messages = MessageModel().getRootItems();
          return mCustomRefreshHanler.buildWidget(
            context,
            loadingSize: 100.sp,
            ballSize: 10.sp,
            builder: () {
              return Stack(
                children: [
                  Visibility(
                    visible: _messages.isEmpty,
                    child: EmptyBackWidget(image: ResImgs('message_empty'), text: STR(10263)),
                  ),
                  RefreshIndicator(
                    onRefresh: () async {
                      await mCustomRefreshHanler.refreshHandler(
                          isSetStatus: false, duration: const Duration(seconds: 1));
                    },
                    child: ListView.builder(
                      physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                      itemBuilder: (context, index) {
                        if (index == _messages.length) return const Center();
                        var item = _messages[index];
                        return MessageItem(infoList: item);
                      },
                      itemCount: _messages.length + 1,
                      // separatorBuilder: (BuildContext context, int index) {
                      //   return Divider(height: 1, indent: 118.sp, color: lineColor);
                      // },
                    ),
                  ),
                ],
              );
            },
          );
        },
      ),
    );
  }
}
