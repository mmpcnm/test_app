import 'package:fim_app/custom/app_bar/LodingTitle.dart';
import 'package:fim_app/custom/azlistview/azlistview.dart';
import 'package:fim_app/custom/custom_popup_menu/home_title_pop.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/wdiget/CustomRefreshHandler.dart';
import 'package:fim_app/custom/wdiget/EmptyBackWidger.dart';
import 'package:fim_app/custom/wdiget/custom_list_table.dart';
import 'package:fim_app/custom/wdiget/dot_tips.dart';
import 'package:fim_app/package/net/controller/logic_group.dart';
import 'package:fim_app/pages/friend/new_friend_page.dart';
import 'package:fim_app/pages/search/search_page.dart';
import 'package:fim_app/model/friend/friend_scoped.dart';
import 'package:fim_app/model/friend/friend_info.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/controller/logic_friend.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/custom/wdiget/user_icon_widget.dart';
import 'package:fim_app/pages/Group/group_page.dart';
import 'package:fim_app/pages/friend/friend_profile_page.dart';
import 'package:fim_app/model/friend/new_friend_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lpinyin/lpinyin.dart';
import 'package:one_context/one_context.dart';
import 'package:scoped_model/scoped_model.dart';

double get indexBarWidth => 42.sp;

class _AZFreind extends ISuspensionBean {
  int itemType;
  FriendInfo? freindModel;
  _AZFreind({required this.itemType, this.freindModel});

  @override
  String getSuspensionTag() {
    if (itemType != 0 || freindModel == null) return '@';
    String pinyins = PinyinHelper.getFirstWordPinyin(freindModel!.userNickname);
    pinyins = pinyins.toUpperCase();
    String tag = pinyins.isEmpty ? '#' : pinyins[0];
    if (int.tryParse(tag) != null) {
      return '#';
    } else {
      return tag;
    }
  }
}

class FriendView extends StatefulWidget {
  const FriendView({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _FriendView();
}

class _FriendView extends State<FriendView> with AutomaticKeepAliveClientMixin {
  int _count = 0;
  // List<FriendModel> _friends = [];
  final List<_AZFreind> _azFreinds = [];
  late CustomRefreshHanler mCustomRefreshHanler;
  List<String> _mIndexBarData = [];
  @override
  void initState() {
    super.initState();
    mCustomRefreshHanler = CustomRefreshHanler(
        onRefreshHandlerStart: onRefreshHandlerStart, onRefreshHandlerEnd: onRefreshHandlerEnd);
    mCustomRefreshHanler.refreshHandler();
  }

  @override
  void dispose() {
    mCustomRefreshHanler.dispose();
    super.dispose();
    print('FriendPage dispose');
  }

  @override
  bool get wantKeepAlive => true;

  Future onRefreshHandlerStart() async {
    await LogicFriend.GetFriends();
    await Future.delayed(const Duration(milliseconds: 10));
    await LogicGroup.GetGroups();
  }

  Future onRefreshHandlerEnd() async {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: defBackgroundColor,
      appBar: CustomAppbar(
        isLineshade: true,
        title: LodingTitle(
          noneChild: Text(STR(10002), style: defTitleTextStyle),
          waitingChild: Text('${STR(10002)}(${STR(10227)})', style: defTitleTextStyle),
        ),
        actions: [
          Button(
            child: Icon(Icons.search, color: blackColor, size: defIconSize),
            padding: const EdgeInsets.all(0),
            onPressed: () =>
                SearchPage.showSearchPage(), //OneContext().push(pageRoute(SearchPage())),
          ),
          SizedBox(width: 5.sp),
          const HomeTitlePop(),
        ],
      ),
      body: ScopedModel<FriendScoped>(
        model: FriendScoped.instance,
        child: ScopedModelDescendant<FriendScoped>(
          builder: (context, child, model) {
            // _friends = FriendScoped.instance.friends;
            // _count = _friends.length;
            _count = FriendScoped.instance.count;
            var newFriends = FriendScoped.instance.friends;
            _azFreinds.clear();
            _azFreinds.add(_AZFreind(itemType: 1));
            _azFreinds.add(_AZFreind(itemType: 2));
            for (var item in newFriends) {
              _azFreinds.add(_AZFreind(freindModel: item, itemType: 0));
            }
            SuspensionUtil.sortListBySuspensionTag(_azFreinds);
            SuspensionUtil.setShowSuspensionStatus(_azFreinds);
            _mIndexBarData = [];
            for (var item in _azFreinds) {
              if (item.isShowSuspension) {
                String tag = item.getSuspensionTag();
                if (tag != '@' && !_mIndexBarData.contains(tag)) {
                  _mIndexBarData.add(tag);
                }
              }
            }
            if (_mIndexBarData.length == 1) {
              _mIndexBarData = [];
            }

            print('---------$_mIndexBarData---------');

            return mCustomRefreshHanler.buildWidget(context, loadingSize: 100.sp, ballSize: 10.sp,
                builder: () {
              return _buildList();
            });
          },
        ),
      ),
    );
  }

  Widget _buildList() {
    return Stack(
      alignment: Alignment.topCenter,
      children: [
        Visibility(
          visible: 0 == _count,
          child: EmptyBackWidget(image: ResImgs('friend_empty'), text: STR(10264)),
        ),
        RefreshIndicator(
          onRefresh: () async {
            await mCustomRefreshHanler.refreshHandler(
                isSetStatus: false, duration: const Duration(seconds: 2));
          },
          child: AzListView(
            physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
            data: _azFreinds,
            itemCount: _azFreinds.length,
            susItemBuilder: (context, index) {
              var freind = _azFreinds[index];
              if (freind.getSuspensionTag() == '@') {
                return const Center();
              } else {
                return Container(
                  color: defBackgroundColor,
                  width: SUT_S_WIDTH,
                  padding: EdgeInsets.all(defPaddingSize),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      freind.getSuspensionTag(),
                      style: defTextStyle,
                    ),
                  ),
                );
              }
            },
            itemBuilder: (context, index) {
              var freind = _azFreinds[index];
              return FriendListItem(freind.itemType, freindModel: freind.freindModel);
            },
            indexBarWidth: indexBarWidth,
            indexBarData: _mIndexBarData,
            indexBarOptions: IndexBarOptions(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: const BorderRadius.all(Radius.circular(45)),
                border: Border.all(color: Colors.grey.shade300, width: 1),
              ),
              downDecoration: BoxDecoration(
                color: Colors.white,
                borderRadius: const BorderRadius.all(Radius.circular(45)),
                border: Border.all(color: Colors.grey.shade300, width: 1),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///好友item
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

class FriendListItem extends StatelessWidget {
  final int itemType;
  final FriendInfo? freindModel;

  const FriendListItem(this.itemType, {Key? key, this.freindModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // print('icson size:${ScreenUtil.toWidth(108)}');
    Widget? dot = _getDot();
    Widget table = CustomListTile(
      color: Colors.white,
      leading: _buildIcon(),
      title: Row(
        children: [
          Expanded(
            child: Text(
              _getTitle(context),
              style: defTextStyle,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          if (dot != null) dot,
        ],
      ),
      bottomLine: Divider(height: 1, color: lineColor, indent: 108.sp),
      onPressed: () => _onTap(context),
    );

    if (1 == itemType) {
      return ScopedModel<NewFriendModel>(
        model: NewFriendModel.instance,
        child: ScopedModelDescendant<NewFriendModel>(
          builder: (context, child, model) {
            return table;
          },
        ),
      );
    } else {
      return table;
    }
  }

  Widget _buildIcon() {
    if (itemType == 1) {
      return UserIconWidget(ResImgs('new_friend'), isNetworkIamge: false);
    } else if (itemType == 2) {
      return UserIconWidget(ResImgs('groupchat'), isNetworkIamge: false);
    } else {
      return UserIconWidget(freindModel!.avatarUrl);
    }
  }

  String _getTitle(BuildContext context) {
    if (itemType == 1) {
      return STR(10028);
    } else if (itemType == 2) {
      return STR(10029); //'群聊';
    } else {
      return freindModel?.userNickname ?? '';
    }
  }

  Widget? _getDot() {
    if (itemType == 1 && NewFriendModel.instance.count > 0) return DotTips();
    return null;
  }

  void _onTap(BuildContext context) {
    if (1 == itemType) {
      OneContext().push(pageRoute(const NewFriendPage()));
    } else if (2 == itemType) {
      OneContext().push(pageRoute(GroupPage()));
    } else {
      OneContext().push(pageRoute(FriendProfilePage(freindModel!.userId)));
    }
  }
}
