import 'dart:math' as math;

import 'package:fim_app/custom/dialog/CustomDialog.dart';
import 'package:fim_app/custom/text/src/text_span_builder.dart';
import 'package:fim_app/custom/wdiget/user_icon_widget.dart';
import 'package:fim_app/custom/wdiget/chat_title_wdiget.dart';
import 'package:fim_app/custom/wdiget/custom_list_table.dart';
import 'package:fim_app/custom/wdiget/dot_tips.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/controller/logic.dart';
import 'package:fim_app/pages/chat/chat_page.dart';
import 'package:fim_app/pages/chat/items/chat_item_rob_red_packet.dart';
import 'package:fim_app/pages/chat/payment/MessagePaymentPage.dart';
import 'package:fim_app/pages/group/widget/group_icon.dart';
import 'package:fim_app/model/chat/list/Base/MessageListBase.dart';
import 'package:fim_app/model/chat/MessageModel.dart';
import 'package:fim_app/model/device_info.dart';
import 'package:fim_app/model/friend/friend_scoped.dart';
import 'package:fim_app/model/group/group_scoped.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/package/util/time_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:scoped_model/scoped_model.dart';

class MessageItem extends StatelessWidget {
  final MessageListBase infoList;

  const MessageItem({Key? key, required this.infoList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // return Text('');
    Widget iconWidget;

    Widget titleWidget;
    Widget messageWiget;

    Widget timeWidget;
    Widget dottipWidget;

    int count = math.min(infoList.unreadCount, 99);
    if (count == 1) {
      dottipWidget = DotTips();
    } else if (count > 1) {
      dottipWidget = DotTips(
        size: 32.sp,
        child: Text('$count', style: TextStyle(fontSize: 18.sp, color: whiteColor)),
      );
    } else {
      dottipWidget = SizedBox(width: 32.sp);
    }

    if (MessageInfoType.REFUND_RED == infoList.infoType) {
      // RefundRedList list = infoList as RefundRedList;
      iconWidget = UserIconWidget(ResImgs('app_icon'), isNetworkIamge: false);
      titleWidget = Text(
        DeviceInfo.appName + '支付',
        style: defTextStyle,
        overflow: TextOverflow.ellipsis,
      );
      timeWidget = Text(
        TimeUtils.chatTime(infoList.updateTime.toInt(), isSecond: true),
        style: TextStyle(fontSize: defMinFontSize, color: Colors.grey),
      );
      messageWiget = Text(
        '红包退款到账通知',
        style: TextStyle(fontSize: defFontSize, color: Colors.grey),
        overflow: TextOverflow.ellipsis,
      );
    } else {
      MessageInfo info = infoList.getValues.last;

      if (info.receiverType == pb.ReceiverType.RT_ROOM) {
        iconWidget = UserIconWidget(ResImgs('app_icon'), isNetworkIamge: false);
      } else if (info.receiverType == pb.ReceiverType.RT_GROUP) {
        iconWidget = GroupIcon(info.receiverId);
      } else if (info.receiverType == pb.ReceiverType.RT_USER) {
        var friend = FriendScoped.instance.findFriend(info.receiverId);
        iconWidget = UserIconWidget(friend != null ? friend.avatarUrl : '');
      } else {
        iconWidget = UserIconWidget('', isNetworkIamge: false);
      }

      if (info.receiverType == pb.ReceiverType.RT_USER) {
        titleWidget = ScopedModel<FriendScoped>(
          model: FriendScoped.instance,
          child: ScopedModelDescendant<FriendScoped>(
            builder: (BuildContext context, Widget? child, FriendScoped model) {
              return Text(
                ChatTitle.getTitleText(info.receiverId, info.receiverType),
                style: defTextStyle,
                overflow: TextOverflow.ellipsis,
              );
            },
          ),
        );
      } else if (info.receiverType == pb.ReceiverType.RT_GROUP) {
        titleWidget = ScopedModel<GroupScoped>(
          model: GroupScoped.instance,
          child: ScopedModelDescendant<GroupScoped>(
            builder: (BuildContext context, Widget? child, GroupScoped model) {
              return Text(
                ChatTitle.getTitleText(info.receiverId, info.receiverType),
                style: defTextStyle,
                overflow: TextOverflow.ellipsis,
              );
            },
          ),
        );
      } else {
        titleWidget = Text(
          ChatTitle.getTitleText(info.receiverId, info.receiverType),
          style: defTextStyle,
          overflow: TextOverflow.ellipsis,
        );
      }
      if (info.sendState == LogicSendState.SEND) {
        timeWidget = Text(
          '发送中...',
          style: TextStyle(fontSize: defMinFontSize, color: lightGrayColor),
        );
      } else if (info.sendState == LogicSendState.ERROR) {
        timeWidget = Text(
          '发送失败！',
          style: TextStyle(fontSize: defMinFontSize, color: lightGrayColor),
        );
      } else {
        timeWidget = Text(
          TimeUtils.chatTime(infoList.updateTime.toInt(), isSecond: true),
          style: TextStyle(fontSize: defMinFontSize, color: lightGrayColor),
        );
      }
      messageWiget = _messageToText(info, overflow: TextOverflow.ellipsis);
    }

    return Slidable(
      child: CustomListTile(
        leading: iconWidget,
        title: Row(
          mainAxisSize: MainAxisSize.min,
          children: [Expanded(child: titleWidget), timeWidget],
        ),
        subtitle: Row(
          // mainAxisSize: MainAxisSize.min,
          children: <Widget>[Expanded(child: messageWiget), dottipWidget],
        ),
        bottomLine: Divider(height: 1, color: lineColor, indent: 108.sp),
        onPressed: () {
          if (MessageInfoType.REFUND_RED == infoList.infoType) {
            CustomRoute().pushPage(const MessagePaymentPage());
          } else {
            MessageInfo info = infoList.getValues.last;
            CustomRoute().pushPage(ChatPage(
              key: Key(DateTime.now().millisecondsSinceEpoch.toString()),
              mReceiverType: info.receiverType,
              mReceiverId: info.receiverId,
            ));
          }
        },
      ),
      endActionPane: ActionPane(motion: const DrawerMotion(), children: [
        SlidableAction(
          //删除
          label: STR(10193),
          icon: Icons.delete,
          backgroundColor: Colors.red,
          foregroundColor: whiteColor,
          onPressed: (BuildContext context) {
            showMessageBox(
              content: '删除后，将清空该聊天的信息记录',
              alignment: MessageBoxAlignment.Bottom,
              buttons: <MessageBoxButton>[
                MessageBoxButton('删除', textColor: Colors.red, onPressed: () {
                  MessageModel().remove(infoList.key, isSqlite: true);
                }),
                MessageBoxButton('取消'),
              ],
            );
          },
        )
      ]),
    );
  }

  Widget _messageToText(MessageInfo message, {TextOverflow? overflow}) {
    TextStyle textStyle = TextStyle(fontSize: defFontSize, color: Colors.grey);

    Widget? leding;
    Widget textWidget;

    if (message.sendState == LogicSendState.SEND) {
      leding = Icon(Icons.send, size: 32.sp, color: Colors.blue);
      // return Text('正在发送...', style: textStyle, overflow: overflow);
    } else if (message.sendState == LogicSendState.ERROR) {
      leding = Icon(Icons.warning_rounded, size: 32.sp, color: Colors.red);
      //   // return Row(
      //   //   mainAxisSize: MainAxisSize.min,
      //   //   children: [
      //   //     ,
      //   //     Text('消息发送失败！', style: textStyle, overflow: overflow),
      //   //   ],
      //   // );
    }
    if (message.status == pb.MessageStatus.MS_RECALL) {
      textWidget = Text(STR(10246), style: textStyle, overflow: overflow); //'撤回消息';
    } else {
      switch (message.messageType) {
        case pb.MessageType.MT_UNKNOWN:
          // 未知内容
          textWidget = Text(STR(10195), style: textStyle, overflow: overflow); //'未知';
          break;
        case pb.MessageType.MT_TEXT: //
          // 文本
          pb.Text text = pb.Text.fromBuffer(message.messageContent);
          textWidget = _chatText(text.text, color: Colors.grey, overflow: overflow, isEnter: false);
          break;
        case pb.MessageType.MT_FACE:
          // 表情
          textWidget = Text(STR(10247), style: textStyle, overflow: overflow);
          break;
        case pb.MessageType.MT_VOICE:
          // 语音消息
          textWidget = Text(STR(10248), style: textStyle, overflow: overflow);
          break;
        case pb.MessageType.MT_IMAGE:
          // 图片
          textWidget = Text(STR(10249), style: textStyle, overflow: overflow);
          break;
        case pb.MessageType.MT_FILE:
          // 文件
          textWidget = Text(STR(10250), style: textStyle, overflow: overflow);
          break;
        case pb.MessageType.MT_LOCATION:
          // 地理位置
          textWidget = Text(STR(10251), style: textStyle, overflow: overflow);
          break;
        case pb.MessageType.MT_COMMAND:
          // 指令推送
          textWidget = Text(STR(10252), style: textStyle, overflow: overflow);
          break;
        case pb.MessageType.MT_SEND_REDPACKET:
          textWidget = Text('[红包]', style: textStyle, overflow: overflow);
          break;
        case pb.MessageType.MT_ROB_REDPACKET:
          textWidget = ChatItemRobRedPacket.toWidgetText(message,
              alignment: MainAxisAlignment.start, overflow: overflow);
          break;
        // case pb.MessageType.MT_CUSTOM:
        //   // 自定义
        //   return '...';
        default:
          textWidget = Text('...', style: textStyle, overflow: overflow);
          break;
      }
    }

    if (leding != null) {
      return Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          leding,
          textWidget,
        ],
      );
    } else {
      return textWidget;
    }
  }

  Text _chatText(String text,
      {required Color color, double? fontSize, TextOverflow? overflow, bool isEnter = true}) {
    List<InlineSpan> stack = [];
    final TextStyle style = TextStyle(fontSize: fontSize ?? defFontSize, color: color);
    var datas = TextSpanBuilder.decodeText(text, isEnter: isEnter);
    for (var item in datas) {
      stack.add(item.getSpan(style: style, iconSize: 26));
    }
    return Text.rich(TextSpan(children: stack), overflow: overflow, textAlign: TextAlign.start);
  }
}
