import 'package:fim_app/configs/im_config.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/wdiget/custom_list_table.dart';
import 'package:fim_app/pages/purse/PursePage.dart';
import 'package:fim_app/pages/red_packets/RedPacketInfoPage.dart';
import 'package:fim_app/pages/security/SecurityPage.dart';
import 'package:fim_app/pages/user/my_qr_code_page.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/controller/logic_user.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/custom/wdiget/user_icon_widget.dart';
import 'package:fim_app/pages/editor/editor_input_page.dart';
import 'package:fim_app/pages/user/edit_user_page.dart';
import 'package:fim_app/pages/user/user_setting_page.dart';
import 'package:flukit/flukit.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class MyView extends StatefulWidget {
  const MyView({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _MyView();
  }
}

class _MyView extends State<MyView> with AutomaticKeepAliveClientMixin {
  @override
  void initState() {
    super.initState();
    LogicUser.GetUser(userId: UserModel.userId);
  }

  @override
  void dispose() {
    OneContext().popDialog();
    super.dispose();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.topCenter,
      children: [
        Container(
          constraints: BoxConstraints(maxHeight: 370.sp),
          decoration: BoxDecoration(
            image: DecorationImage(
              // centerSlice: Rect.fromLTWH(80, 0, 550, 380),
              alignment: Alignment.topCenter,
              image: AssetImage(ResImgs('default_bg')),
              fit: BoxFit.fill,
            ),
          ),
        ),
        Container(
          // constraints: BoxConstraints(minHeight: 320.h),
          padding: EdgeInsets.only(top: ScreenUtil.statusBarHeight, left: 30.sp, right: 30.sp),
          width: ScreenUtil.screenWidth,
          child: ScopedModel<UserModel>(
            model: UserModel.instance,
            child: ScopedModelDescendant<UserModel>(
              builder: (context, child, model) {
                return CustomScrollView(
                  physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                  slivers: [
                    SliverFlexibleHeader(
                      visibleExtent: 260.sp,
                      builder: (context, availableHeight, direction) {
                        if (availableHeight < 1) return const Center();
                        return SizedBox(
                          height: availableHeight,
                          child: Center(
                            child: ListView(
                              physics: const NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              reverse: true,
                              children: [_buildUser()],
                            ),
                          ),
                        );
                      },
                    ),
                    SliverList(
                      delegate: SliverChildListDelegate(
                        <Widget>[
                          //用户签名
                          _buildUserSign(
                            callback: () {
                              CustomRoute().pushPage(
                                EditorInputPage(
                                  title: STR(10228), //'设置个性签名',
                                  content: STR(10010),
                                  hitText: STR(10010),
                                  inputMaxLength: constSignatureMaxLength,
                                  inputLeading: UserIconWidget(UserModel.avatarUrl, size: 64.sp),
                                  //个性签名
                                  bottomWidget: Text('${STR(10009)}: ${UserModel.extra}',
                                      style: defTextStyle),
                                  onPressed: (value) {
                                    if (value.isEmpty) return;
                                    LogicUser.UpdateUser(context, extra: value);
                                  },
                                ),
                              );
                            },
                          ),
                          SizedBox(height: 10.sp),
                          //我的二维码
                          _buildListBox(
                            iconPath: ResImgs('icon_wallet'),
                            text: STR(10011), //'我的二维码',
                            callback: () {
                              // showMyQRCode(context);
                              // EasyLoading.showToast('开发中...');
                              // QRCodeDialog.show(context);
                              CustomRoute().pushPage(MyQRCodePage());
                            },
                          ),
                          SizedBox(height: 10.sp),
                          //我的钱包
                          _buildListBox(
                            iconPath: ResImgs('icon_commission'),
                            text: STR(10012), //'我的钱包',
                            callback: () {
                              CustomRoute().pushPage(PursePage());
                            },
                          ),
                          SizedBox(height: 10.sp),
                          //我的钱包
                          _buildListBox(
                            iconPath: ResImgs('red/red_alarm_logo'),
                            text: '我的红包', //'我的红包',
                            callback: () {
                              // CustomRoute().pushPage(PursePage());
                              // EasyLoading.showToast('开发中...');
                              RedPacketInfoPage.show();
                            },
                          ),
                          SizedBox(height: 10.sp),
                          //修改资料
                          _buildListBox(
                            iconPath: ResImgs('icon_info'),
                            text: STR(10013), //'修改资料',
                            callback: () {
                              CustomRoute().pushPage(EditUserPage());
                            },
                          ),
                          SizedBox(height: 10.sp),
                          //账户安全
                          _buildListBox(
                            iconPath: ResImgs('icon_account_security'),
                            text: STR(10014), //'账户安全',
                            callback: () {
                              CustomRoute().pushPage(const SecurityPage());
                            },
                          ),
                          SizedBox(height: 10.sp),
                          //设置
                          _buildListBox(
                            iconPath: ResImgs('icon_my_big_emoji_setting'),
                            text: STR(10015), //'设置',
                            callback: () {
                              CustomRoute().pushPage(UserSettingPage());
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                );
              },
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildUser() {
    List<InlineSpan> spans = [
      TextSpan(
          text: '${UserModel.nickName} ',
          style: TextStyle(color: whiteColor, fontSize: defFontSize)),
    ];
    if (UserModel.sex.sex != USER_SEX.UNKNOWN) {
      spans.add(
        WidgetSpan(
          alignment: PlaceholderAlignment.bottom,
          child: Image(width: 32.sp, height: 32.sp, image: AssetImage(UserModel.sex.sex_image)),
        ),
      );
    }

    return Row(
      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text.rich(TextSpan(children: spans)),
            const SizedBox(height: 5),
            Text(
              //账号
              '${STR(10008)}: ${UserModel.accountId}',
              style: TextStyle(color: grayColor, fontSize: defFontSize),
            ),
            const SizedBox(height: 5),
            Text(
              //账号
              'ID: ${UserModel.userId}',
              style: TextStyle(color: grayColor, fontSize: defFontSize),
            ),
          ],
        ),
        const Expanded(child: SizedBox()),
        UserIconWidget(UserModel.avatarUrl, size: 100.sp, circular: 50.sp, frame: true)
      ],
    );
  }

  Widget _buildUserSign({required VoidCallback callback}) {
    return CustomListTile(
      color: Colors.white,
      borderRadius: BorderRadius.circular(10.sp),
      padding: EdgeInsets.symmetric(horizontal: SUT_W(20), vertical: SUT_H(30)),
      title: InkWell(
        child: Text.rich(
          TextSpan(
            children: [
              TextSpan(
                text: STR(10009),
                style: TextStyle(color: defFontColor, fontSize: defMinFontSize),
              ),
              WidgetSpan(
                alignment: PlaceholderAlignment.bottom,
                child: Icon(Icons.edit_outlined, size: 28.sp),
                //  Image(
                //   width: 32.sp,
                //   height: 32.sp,
                //   image: AssetImage(ResImgs('edit_icon')),
                // ),
              ),
            ],
          ),
          overflow: TextOverflow.ellipsis,
        ),
        onTap: callback,
      ),
      subtitle: Padding(
        padding: const EdgeInsets.only(top: 5),
        child: Text(
          UserModel.extra,
          softWrap: true,
          style: TextStyle(
            color: Colors.black87,
            fontWeight: FontWeight.bold,
            fontSize: SUT_SP(22),
          ),
        ),
      ),
    );
  }

  Widget _buildListBox(
      {required String iconPath, required String text, required VoidCallback callback}) {
    return CustomListTile(
      padding: EdgeInsets.only(left: 25.sp, right: 10.sp, top: 20.sp, bottom: 20.sp),
      color: Colors.white,
      borderRadius: BorderRadius.circular(10.sp),
      leading: Image(width: 32.sp, height: 32.sp, image: AssetImage(iconPath)),
      title: Text(text, style: defTextStyle),
      isArrowIcon: true,
      onPressed: callback,
    );
  }
}
