import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/wdiget/CustomRefreshHandler.dart';
import 'package:fim_app/custom/wdiget/EmptyBackWidger.dart';
import 'package:fim_app/custom/wdiget/user_icon_widget.dart';
import 'package:fim_app/custom/app_bar/CustiomSliverPersistentHeaderDelegate.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/custom/wdiget/custom_list_table.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/pages/webview/webview.dart';
import 'package:flutter/material.dart';
import 'package:one_context/one_context.dart';

///小程序
class SmallProgram extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SmallProgram();
}

class _SmallProgram extends State<SmallProgram> with AutomaticKeepAliveClientMixin {
  int _count = 0;
  late CustomRefreshHanler mCustomRefreshHanler;
  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    mCustomRefreshHanler = CustomRefreshHanler(onRefreshHandlerStart: onRefreshHandlerStart, onRefreshHandlerEnd: onRefreshHandlerEnd);
    mCustomRefreshHanler.refreshHandler(isSetStatus: true, duration: Duration(seconds: 2));
  }

  @override
  void dispose() {
    mCustomRefreshHanler.dispose();
    super.dispose();
  }

  Future onRefreshHandlerEnd() async {
    setState(() {});
  }

  Future onRefreshHandlerStart() async {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: defBackgroundColor,
      appBar: CustomAppbar(
        title: Text(STR(10004), style: defTitleTextStyle),
      ),
      body: mCustomRefreshHanler.buildWidget(
        context,
        loadingSize: 100.sp,
        ballSize: 10.sp,
        builder: () {
          return Stack(
            alignment: Alignment.topCenter,
            children: [
              Visibility(
                visible: 0 == _count,
                child: EmptyBackWidget(image: ResImgs('no_data_empty'), text: STR(10268)),
              ),
              RefreshIndicator(
                onRefresh: () async {
                  await mCustomRefreshHanler.refreshHandler(isSetStatus: false, duration: Duration(seconds: 2));
                },
                child: CustomScrollView(
                  physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                  slivers: <Widget>[
                    SliverToBoxAdapter(
                      child: CustomListTile(
                        leading: UserIconWidget(ResImgs('app_icon'), isNetworkIamge: false),
                        padding: EdgeInsets.symmetric(horizontal: defPaddingSizeH, vertical: defPaddingSizeV),
                        isArrowIcon: true,
                        onPressed: () {
                          CustomRoute().pushPage(WebView(isCloseIcon: true));
                          // OneContext().push(fadeRoute(WebView(isCloseIcon: true)));
                        },
                      ),
                    ),
                    SliverToBoxAdapter(
                      child: CustomListTile(
                        color: lineColor,
                        title: Text(STR(10004), style: defTextStyle),
                      ),
                    ),
                    // SliverPersistentHeader(
                    //   pinned: true,
                    //   floating: false,
                    //   delegate: CustomSliverPersistentHeaderDelegate(
                    //     mMinExtent: 78.sp,
                    //     mMaxExtent: 78.sp,
                    //     color: lineColor,
                    //     child: Align(
                    //       alignment: Alignment.centerLeft,
                    //       child: Padding(
                    //         padding: EdgeInsets.only(left: defPaddingSize),
                    //         child: Text(STR(10004), style: defTextStyle),
                    //       ),
                    //     ),
                    //   ),
                    // ),
                    // SliverGrid(
                    //   gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    //     crossAxisCount: 4,
                    //     mainAxisSpacing: 20.sp,
                    //     crossAxisSpacing: 20.sp,
                    //   ),
                    //   delegate: SliverChildBuilderDelegate(
                    //     (BuildContext context, int index) {
                    //       if (index == 0) {
                    //         return Button(
                    //           padding: EdgeInsets.all(3.sp),
                    //           borderCircular: 15.sp,
                    //           child: Column(
                    //             children: [
                    //               Expanded(
                    //                 child: Image.asset(ResImgs('qr_code_icon'), fit: BoxFit.fitHeight),
                    //               ),
                    //               SizedBox(height: 3.sp),
                    //               Text(
                    //                 '扫一扫',
                    //                 style: TextStyle(
                    //                   fontSize: defMinFontSize,
                    //                   color: defFontColor,
                    //                 ),
                    //               ),
                    //             ],
                    //           ),
                    //           onPressed: () {
                    //             OneContext().push(fadeRoute(ScanCodePage()));
                    //           },
                    //         );
                    //       }
                    //       return Center();
                    //     },
                    //     childCount: _count,
                    //   ),
                    // ),
                  ],
                ),
              ),

              // EasyRefresh.custom(
              //   topBouncing: true,
              //   bottomBouncing: true,
              //   header: BallPulseHeader(),
              //   onRefresh: () async {
              //     await mCustomRefreshHanler.refreshHandler(isSetStatus: false, duration: Duration(seconds: 2));
              //   },
              //   slivers: <Widget>[
              //     SliverPersistentHeader(
              //       pinned: false,
              //       floating: false,
              //       delegate: CustomSliverPersistentHeaderDelegate(
              //         mMinExtent: 100.sp,
              //         mMaxExtent: 100.sp,
              //         // color: backgroundColor,
              //         child: Align(
              //           alignment: Alignment.centerLeft,
              //           child: CustomListTile(
              //             leading: UserIconWidget(ResImgs('app_icon'), isNetworkIamge: false),
              //             padding: EdgeInsets.symmetric(horizontal: defPaddingSize, vertical: 10.sp),
              //             isArrowIcon: true,
              //             onPressed: () {
              //               OneContext().push(fadeRoute(WebView(isCloseIcon: true)));
              //             },
              //           ),
              //         ),
              //       ),
              //     ),
              //     SliverPersistentHeader(
              //       pinned: true,
              //       floating: false,
              //       delegate: CustomSliverPersistentHeaderDelegate(
              //         mMinExtent: 78.sp,
              //         mMaxExtent: 78.sp,
              //         color: lineColor,
              //         child: Align(
              //           alignment: Alignment.centerLeft,
              //           child: Padding(
              //             padding: EdgeInsets.only(left: defPaddingSize),
              //             child: Text(STR(10004), style: defTextStyle),
              //           ),
              //         ),
              //       ),
              //     ),
              //     // SliverGrid(
              //     //   gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              //     //     crossAxisCount: 4,
              //     //     mainAxisSpacing: 20.sp,
              //     //     crossAxisSpacing: 20.sp,
              //     //   ),
              //     //   delegate: SliverChildBuilderDelegate(
              //     //     (BuildContext context, int index) {
              //     //       if (index == 0) {
              //     //         return Button(
              //     //           padding: EdgeInsets.all(3.sp),
              //     //           borderCircular: 15.sp,
              //     //           child: Column(
              //     //             children: [
              //     //               Expanded(
              //     //                 child: Image.asset(ResImgs('qr_code_icon'), fit: BoxFit.fitHeight),
              //     //               ),
              //     //               SizedBox(height: 3.sp),
              //     //               Text(
              //     //                 '扫一扫',
              //     //                 style: TextStyle(
              //     //                   fontSize: defMinFontSize,
              //     //                   color: defFontColor,
              //     //                 ),
              //     //               ),
              //     //             ],
              //     //           ),
              //     //           onPressed: () {
              //     //             OneContext().push(fadeRoute(ScanCodePage()));
              //     //           },
              //     //         );
              //     //       }
              //     //       return Center();
              //     //     },
              //     //     childCount: _count,
              //     //   ),
              //     // ),
              //   ],
              // ),
            ],
          );
        },
      ),
    );
  }
}
