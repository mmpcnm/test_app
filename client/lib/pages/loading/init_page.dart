import 'package:fim_app/configs/api.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/package/net/controller/logic_security.dart';
import 'package:fim_app/pages/login/sign_in_page.dart';
import 'package:fim_app/model/chat/MessageModel.dart';
import 'package:fim_app/model/friend/friend_scoped.dart';
import 'package:fim_app/model/friend/new_friend_model.dart';
import 'package:fim_app/model/group/group_scoped.dart';
import 'package:fim_app/model/user/user_model.dart' as model;
import 'package:fim_app/package/net/controller/logic_user.dart';
import 'package:fim_app/package/net/socket/connect_socket.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/sqflite/sqlite_manager.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

class InitPage extends StatelessWidget {
  bool _isInit = false;
  InitPage({Key? key}) : super(key: key) {
    // ..maskType = EasyLoadingMaskType.clear;
    AppConfig.clear();
    if (model.UserModel.token.isEmpty) {
      Future.delayed(const Duration(milliseconds: 5), () {
        // Update.instace.checkUpdate(OneContext().context!);
        CustomRoute().pushReplacement(const SignInPage());
        // OneContext().pushReplacement(pageRoute(const SignInPage()));
      });
      // return SignInPage();
    } else {
      _initData();
    }
  }

  @override
  Widget build(BuildContext context) {
    //初始化适配信息
    // ScreenUtil.init(context, width: 750 * 0.8);

    return Scaffold(
      backgroundColor: defBackgroundColor,
      body: ScreenUtil.build(
        context,
        width: 750 * 0.8,
        build: () {
          EasyLoading.instance
            ..toastPosition = EasyLoadingToastPosition.top
            ..contentPadding = EdgeInsets.all(25.sp)
            ..indicatorSize = 64.sp
            ..fontSize = defFontSize
            ..radius = 15.sp
            ..backgroundColor = blackColor
            // ..dismissOnTap = true
            ..textStyle = TextStyle(fontSize: defFontSize, color: whiteColor)
            ..textPadding = EdgeInsets.all(25.sp);
          return Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(ResImgs('login_bg')), alignment: Alignment.topCenter)),
            alignment: Alignment.topCenter,
          );
        },
      ),
    );
  }

  Future<void> _initData() async {
    if (_isInit) return;
    _isInit = true;

    // if (model.UserModel.token.isEmpty) {
    //   Update.instace.checkUpdate(context);
    //   Future.delayed(const Duration(milliseconds: 5), () {
    //     OneContext().push(pageRoute(SignInPage()));
    //   });
    //   return;
    // }

    MessageModel().clear();
    GroupScoped.instance.clear();
    FriendScoped.instance.clear();
    NewFriendModel.instance.clear();

    //初始化本地存储信息
    await SqliteManager().init(onOpen: () {
      //获取用户信息
      LogicUser.GetUser(userId: model.UserModel.userId);

      // //获取好友列表
      // LogicFriend.GetFriends();

      // //获取群组列表
      // LogicGroup.GetGroups();

      //获取钱包
      LogicSecurity.OpenWallet();

      //链接到推送服
      ConnectSocket().connect();

      //跳转到home界面
      Future.delayed(const Duration(milliseconds: 5), () {
        gotpHome(OneContext().context!, index: HomeViewIndex.MESSAGE);
      });
    });
  }
}
