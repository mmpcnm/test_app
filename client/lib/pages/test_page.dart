import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class TestPage extends StatefulWidget {
  String title;
  TestPage({required this.title});
  @override
  State<StatefulWidget> createState() => _TestPage();
}

class _TestPage extends State<TestPage> {
  @override
  Widget build(BuildContext context) {
    // String text = ']][[[fendou]]]][[[[fendou]福气[fendou]aseeg[fendou]a[fendou]v[fendou]w';
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
        backgroundColor: defBackgroundColor,
        appBar: CustomAppbar(
          title: Text(
            widget.title,
            style: defTitleTextStyle,
          ),
        ),
      ),
    );
  }
}
