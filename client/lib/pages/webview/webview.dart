import 'dart:io';

import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:one_context/one_context.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:webview_flutter/platform_interface.dart';
import 'package:webview_flutter/webview_flutter.dart' as web;

class WebView extends StatefulWidget {
  String url;
  String title;
  bool isCloseIcon;
  bool isWantKeepAlive;
  WebView({this.title = '', this.isCloseIcon = false, this.url = 'https://baidu.com', this.isWantKeepAlive = false});

  @override
  State<StatefulWidget> createState() => _WebView();
}

class _WebView extends State<WebView> with AutomaticKeepAliveClientMixin {
  //with AutomaticKeepAliveClientMixin {
  web.WebViewController? _webController;
  String _currentUrl = '';
  WebErrorViewController _errorController = WebErrorViewController();

  @override
  bool get wantKeepAlive => widget.isWantKeepAlive;

  @override
  void initState() {
    super.initState();
    // _pageUrl = [widget.url];
    _currentUrl = widget.url;
    if (Platform.isAndroid) web.WebView.platform = web.SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: defBackgroundColor,
      appBar: CustomAppbar(
        title: Text(widget.title, style: defTitleTextStyle),
        leading: [
          if (widget.isCloseIcon)
            InkWell(
              child: Icon(
                Icons.close,
                size: defIconSize,
                color: Colors.black,
              ),
              onTap: () {
                OneContext().pop();
              },
            ),
          if (widget.isCloseIcon) SizedBox(width: defPaddingSize),
          //主页
          InkWell(
            child: Icon(
              Icons.home_outlined,
              size: defIconSize,
              color: Colors.black,
            ),
            onTap: () {
              _currentUrl = widget.url;
              if (_webController != null) {
                _webController?.loadUrl(_currentUrl);
              }
            },
          ),
        ],
        actions: [
          // Button(
          //   color: Colors.blue,
          //   child: Text('send to js', style: defTextStyle),
          //   onPressed: () {
          //     if (_webController != null) {
          //       print('flutter evaluateJavascript');

          //       _webController?.evaluateJavascript('flutterCallJsMethod("sssss")').then(
          //             (value) => print('发送成功 flutterCallJsMethod("sssss")'),
          //           );
          //     }
          //   },
          // ),
          //刷新页面
          InkWell(
            child: Icon(
              Icons.restart_alt_outlined,
              size: defIconSize,
              color: Colors.black,
            ),
            onTap: () {
              if (_webController != null) {
                _webController?.reload();
              }
              _errorController.isVisible = false;
            },
          ),
        ],
      ),
      body: Stack(
        alignment: AlignmentDirectional.center,
        children: [
          _buildWebView(),
          WebErrorView(controller: _errorController),
        ],
      ),
    );
  }

  

  Widget _buildWebView() {
    return web.WebView(
      initialUrl: widget.url,
      javascriptMode: web.JavascriptMode.unrestricted,
      gestureNavigationEnabled: true,
      allowsInlineMediaPlayback: true,
      javascriptChannels: <web.JavascriptChannel>[
        web.JavascriptChannel(
          name: 'onJsFlutterHandler',
          onMessageReceived: (web.JavascriptMessage message) {
            print('js call onJsFlutterHandler(${message.message})');
            EasyLoading.showToast(message.message);
          },
        )
      ].toSet(),
      onWebViewCreated: (web.WebViewController controller) {
        //WebView创建完成
        _webController = controller;
        _webController?.loadUrl(widget.url);
      },
      onPageFinished: (String value) {
        //页面加载完成
        _currentUrl = value;
      },
      onWebResourceError: (WebResourceError error) {
        //页面加载失败
        _errorController.isVisible = true;
      },
    );
  }
}

class WebErrorViewController extends Model {
  bool _mIsVisible = false;
  WebErrorViewController({bool isVisible = false}) {
    _mIsVisible = isVisible;
  }
  void set isVisible(bool visible) {
    _mIsVisible = visible;
    notifyListeners();
  }

  bool get isVisible => _mIsVisible;
}

class WebErrorView extends StatelessWidget {
  WebErrorViewController controller;
  WebErrorView({required this.controller});
  @override
  Widget build(BuildContext context) {
    return ScopedModel<WebErrorViewController>(
      model: controller,
      child: ScopedModelDescendant<WebErrorViewController>(
        builder: (BuildContext context, Widget? child, Model model) {
          return Visibility(
            visible: controller.isVisible,
            child: Container(
              color: defBackgroundColor,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Image(
                      width: 660.sp * 0.5,
                      height: 356.sp * 0.5,
                      image: AssetImage(ResImgs('message_empty')),
                    ),
                    SizedBox(height: 15.sp),
                    Text(STR(10265), style: defTextStyle),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
