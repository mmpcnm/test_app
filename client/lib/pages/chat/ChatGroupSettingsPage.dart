import 'package:fim_app/custom/dialog/CustomDialog.dart';
import 'package:fim_app/custom/wdiget/CustomRefreshHandler.dart';
import 'package:fim_app/custom/wdiget/EmptyBackWidger.dart';
import 'package:fim_app/custom/wdiget/user_icon_widget.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/custom/wdiget/chat_title_wdiget.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/wdiget/custom_list_table.dart';
import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/controller/logic_group.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/package/pb/logic.ext.pb.dart' as pb;
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/model/chat/ChatInputModel.dart';
import 'package:fim_app/model/chat/MessageModel.dart';
import 'package:fim_app/model/group/group_member_model.dart';
import 'package:fim_app/model/group/group_member_scoped.dart';
import 'package:fim_app/model/group/group_model.dart';
import 'package:fim_app/model/group/group_scoped.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/pages/editor/editor_filed_page.dart';
import 'package:fim_app/pages/editor/editor_input_page.dart';
import 'package:fim_app/pages/chat/view/ChatSettingUsersView.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/material.dart';
import 'package:m_loading/m_loading.dart';
import 'package:one_context/one_context.dart';
import 'package:scoped_model/scoped_model.dart';

class GroupChatSettingsPage extends StatefulWidget {
  Int64 groupId;
  GroupChatSettingsPage(this.groupId);

  @override
  State<StatefulWidget> createState() => _GroupSettingsPage();
}

class _GroupSettingsPage extends State<GroupChatSettingsPage> {
  late GroupModel? _groupModel;
  late CustomRefreshHanler mCustomRefreshHanler;
  @override
  void initState() {
    super.initState();
    mCustomRefreshHanler = CustomRefreshHanler(
        onRefreshHandlerStart: onRefreshHandlerStart, onRefreshHandlerEnd: onRefreshHandlerEnd);
    if (!GroupScoped.instance.isGroup(widget.groupId)) mCustomRefreshHanler.refreshHandler();
  }

  @override
  void dispose() {
    mCustomRefreshHanler.dispose();
    super.dispose();
  }

  Future onRefreshHandlerStart() async {
    await LogicGroup.GetGroup(groupId: widget.groupId);
    Future.delayed(Duration(milliseconds: 10));
    await LogicGroup.GetGroupMembers(groupId: widget.groupId);
  }

  Future onRefreshHandlerEnd() async {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModel<GroupScoped>(
      model: GroupScoped.instance,
      child: ScopedModelDescendant<GroupScoped>(
        builder: (context, child, model) {
          Widget title = Center();
          Widget listView = RefreshIndicator(
            onRefresh: () async {
              await mCustomRefreshHanler.refreshHandler(
                  isSetStatus: false, duration: Duration(seconds: 2));
            },
            child: ListView(
              physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
              children: [
                EmptyBackWidget(image: ResImgs('no_data_empty'), text: '无群信息'),
              ],
            ),
          );
          if (mCustomRefreshHanler.mRefreshStatus == 1) {
            title = SizedBox(
              width: 100.sp,
              height: 64.sp,
              child: Center(
                child: BallPulseLoading(
                  // duration: Duration(minutes: 1),
                  ballStyle: BallStyle(
                    // size: 5.sp,
                    color: Colors.blue,
                    ballType: BallType.solid,
                  ),
                ),
              ),
            );
            listView = Center(
              child: SizedBox(
                width: 100.sp,
                height: 100.sp,
                child: BallCircleOpacityLoading(
                  // duration: Duration(minutes: 1),
                  ballStyle: BallStyle(
                    size: 10.sp,
                    color: Colors.blue,
                    ballType: BallType.solid,
                  ),
                ),
              ),
            );
          } else if (GroupScoped.instance.isGroup(widget.groupId)) {
            title = ChatTitle(
              receiverId: widget.groupId,
              receiverType: pb.ReceiverType.RT_GROUP,
            );
            listView = RefreshIndicator(
              onRefresh: () async {
                await mCustomRefreshHanler.refreshHandler(
                    isSetStatus: false, duration: Duration(seconds: 2));
              },
              child: _buileList(),
            );
          }

          return Scaffold(
              backgroundColor: defBackgroundColor,
              appBar: CustomAppbar(
                isBackIcon: true,
                title: title,
              ),
              body: listView);
        },
      ),
    );
  }

  Widget _buileList() {
    List<Widget> children = [];
    children.add(
      ScopedModel<GroupMemberScoped>(
        model: GroupMemberScoped.instance,
        child: ScopedModelDescendant<GroupMemberScoped>(
          builder: (context, model, child) {
            return ChatSettingUsersView(
                mReceiverId: widget.groupId, mMemberMaxCount: 18, mIsUser: false);
          },
        ),
      ),
    );
    GroupMemberModel? selfMemberModel =
        GroupMemberScoped.instance.getMember(groupId: widget.groupId, memberId: UserModel.userId);
    int memberCount = GroupMemberScoped.instance.getMemberCount(widget.groupId);
    _groupModel = GroupScoped.instance.getGroup(widget.groupId);
    if (_groupModel != null) {
      if (memberCount > 18) {
        children.addAll([
          _buildTable(
            middle: STR(10206), //'查看更多群成员',
            onPressed: () {},
          ),
          Divider(height: 1, color: lineColor),
        ]);
      }

      children.addAll([
        SizedBox(height: SUT_H(25)),
        _buildTable(
          left: STR(10207), //'群聊名称',
          right: _groupModel?.name,
          onPressed: () {
            if (GroupMemberScoped.instance
                    .getMemberType(groupId: _groupModel!.groupId, userId: UserModel.userId) !=
                pb.MemberType.GMT_ADMIN) {
              showMessageDialog(context, content: '你不是管理员，没有权限修改群名称！');
            } else {
              OneContext().push(
                pageRoute(
                  EditorInputPage(
                    title: STR(10208), //'修改群聊名称',
                    content: STR(10209), //'修改群聊名称后，将在群内通知其他成员。',
                    hitText: _groupModel!.name,
                    inputIsLine: false,
                    inputMaxLength: constNoticeMaxLength,
                    inputLeading: UserIconWidget(UserModel.avatarUrl, size: 64.sp),
                    onPressed: (value) {
                      if (value.isEmpty) return;
                      LogicGroup.UpdateGroup(context,
                          group_id: _groupModel!.groupId,
                          name: value,
                          introduction: _groupModel!.introduction);
                    },
                  ),
                ),
              );
            }
          },
        ),
        Divider(height: 1, color: lineColor),
        _buildTable(
          leftWidget: Text.rich(
            TextSpan(
              children: [
                //群公告
                TextSpan(
                    text: STR(10210) + (_groupModel!.introduction.isEmpty ? '' : ': '),
                    style: defTextStyle),
                TextSpan(
                    text: _groupModel!.introduction,
                    style: TextStyle(fontSize: defFontSize, color: grayColor)),
              ],
            ),
            softWrap: true,
          ),
          // left: '群公告' + (_groupModel!.introduction.isEmpty ? '' : ': ${_groupModel!.introduction}'),
          onPressed: () {
            if (GroupMemberScoped.instance
                    .getMemberType(groupId: _groupModel!.groupId, userId: UserModel.userId) !=
                pb.MemberType.GMT_ADMIN) {
              showMessageDialog(context, content: '你不是管理员，没有权限发布群公告！');
            } else {
              OneContext().push(
                pageRoute(
                  EditorFiledPage(
                    //群公告
                    title: Text(STR(10210), style: defTitleTextStyle),
                    hitText: STR(10211), //'请编辑群公告',
                    inputMaxLength: 30,
                    onPassed: (value) {
                      LogicGroup.UpdateGroup(context,
                          group_id: _groupModel!.groupId,
                          name: _groupModel!.name,
                          introduction: value);
                    },
                  ),
                ),
              );
            }
          },
        ),
        Divider(height: 1, color: lineColor),
        _buildTable(
          left: STR(10212), //'我在群里的昵称',
          right: selfMemberModel?.userNickname,
          // onPressed: () {},
        ),
        SizedBox(height: SUT_H(25)),
        _buildClaerMessage(),
        Divider(height: 1, color: lineColor),
        _buildExitGroup(),
      ]);
    } else {
      children.addAll([
        SizedBox(height: SUT_H(25)),
        _buildClaerMessage(),
        Divider(height: 1, color: lineColor),
        _buildExitGroup(),
      ]);
    }

    return ListView(
      physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
      children: children,
    );
  }

  Widget _buildTable(
      {Widget? leftWidget, String? left, String? middle, String? right, VoidCallback? onPressed}) {
    return CustomListTile(
      color: Colors.white,
      padding: EdgeInsets.symmetric(horizontal: defPaddingSize, vertical: 25.sp),
      title: leftWidget ??
          (left != null ? Text(left, style: defTextStyle, overflow: TextOverflow.ellipsis) : null),
      middletitle: middle != null
          ? Text(middle, style: defTextStyle, overflow: TextOverflow.ellipsis)
          : null,
      trailing:
          right != null ? Text(right, style: defTextStyle, overflow: TextOverflow.ellipsis) : null,
      isArrowIcon: true,
      onPressed: onPressed,
    );
  }

  Widget _buildClaerMessage() {
    return Button(
      //清空聊天记录
      child: Text(STR(10204), style: TextStyle(fontSize: defFontSize, color: Colors.red)),
      padding: EdgeInsets.all(SUT_W(25)),
      borderCircular: 0,
      color: Colors.white,
      onPressed: () {
        showMessageDialogEx(
          context,
          title: STR(10185), //'提示',
          content: '${STR(10205)}?', //'是否要删除聊天记录？',
          confirmCallback: () {
            MessageModel().remove(widget.groupId.toString(), isSqlite: true);
          },
        );
      },
    );
  }

  Widget _buildExitGroup() {
    return Button(
      //退出群
      child: Text(STR(10213), style: TextStyle(fontSize: defFontSize, color: Colors.red)),
      padding: EdgeInsets.all(SUT_W(25)),
      borderCircular: 0,
      color: Colors.white,
      onPressed: () {
        showMessageDialogEx(
          context,
          title: STR(10185), //'提示',
          content: '${STR(10214)}?', //'是否退出群聊 ？',
          confirmCallback: () {
            LogicGroup.ExitGroup(
              groupId: widget.groupId,
              logicCallBack: (code, resp) {
                if (code == 0) {
                  GroupScoped.instance.delGroup(widget.groupId);
                  MessageModel().remove(widget.groupId.toString(), isSqlite: true);
                  gotpHome(context, index: HomeViewIndex.FRIEND);
                }
              },
            );
          },
        );
      },
    );
  }
}
