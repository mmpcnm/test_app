// ignore_for_file: must_be_immutable

import 'dart:convert';

import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/pages/chat/items/chat_item.dart';
import 'package:fim_app/model/chat/list/MessageList/MessageInfo.dart';
import 'package:fim_app/model/chat/list/RefundRedList/RefundRedInfo.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChatItemCustom extends StatelessWidget with ChatItem {
  late pb.Custom _custom;
  ChatItemCustom(MessageInfo message, {Key? key, bool isTopTable = false}) : super(key: key) {
    itemInit(message, isTopTable: isTopTable);
    _custom = pb.Custom.fromBuffer(mMessage.messageContent);
  }
  @override
  Widget build(BuildContext context) {
    var json = jsonDecode(_custom.data);
    if (json['type'] == 'RefundRedNotifyData') {
      return _buildRefundRedNotify(json['data']);
    }
    return const Center();
  }

  Widget _buildRefundRedNotify(Map<String, dynamic> json) {
    var data = RefundRedInfo.fromJson(json);
    var money = (data.money.toDouble() / 100).toStringAsFixed(2);
    return Center(
      child: Text('红包退还 $money 元', style: TextStyle(fontSize: defFontSize, color: Colors.grey)),
    );
  }
}
