// ignore_for_file: must_be_immutable, import_of_legacy_library_into_null_safe

import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/custom/image/CustomImage.dart';
import 'package:fim_app/custom/image/GalleryPhotoViewWrapper.dart';
import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/package/net/controller/logic.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/model/chat/MessageModel.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/pages/chat/items/chat_item.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:flutter/material.dart';
import 'package:m_loading/m_loading.dart';

class ChatItemImage extends StatelessWidget with ChatItem {
  ChatItemImage(MessageInfo message, {Key? key, bool isTopTable = false}) : super(key: key) {
    itemInit(message, isTopTable: isTopTable);
  }

  @override
  Widget build(BuildContext context) {
    double imageMaxWidth = SUT_S_WIDTH / 2.0 - 100.w;

    pb.Image pbImage = pb.Image.fromBuffer(mMessage.messageContent);
    double imageWidth = pbImage.width == 0 ? 126.sp : pbImage.width.toDouble();
    double imageHeight = pbImage.height == 0 ? 126.sp : pbImage.height.toDouble();
    double scaleFx = min(imageMaxWidth / imageWidth, 1);
    if (scaleFx < 1) {
      imageWidth = scaleFx * imageWidth;
      imageHeight = scaleFx * imageHeight;
    }

    Debug.d(
        'imageSize:{${pbImage.width}x${pbImage.height}}, scaleSize:{${imageWidth}x$imageHeight}');

    /// image
    Widget msgContent;
    if (mMessage.sendState == LogicSendState.NONE || mMessage.sendState == LogicSendState.SUCCEED) {
      msgContent = Button(
        padding: EdgeInsets.zero,
        child: CustomImage.network(
          pbImage.url,
          width: imageWidth,
          height: imageHeight,
          loadingBuilder: CustomImage.defaultImageLoadingBuilder,
          errorBuilder: (BuildContext context, Object exception, StackTrace? stackTrace) {
            return const Icon(
              Icons.broken_image_outlined,
            );
          },
        ),
        shape: RoundedRectangleBorder(
            //无背景 圆角边框
            side: const BorderSide(color: Colors.grey, width: 1, style: BorderStyle.solid),
            borderRadius: BorderRadius.all(Radius.circular(15.sp))),
        onPressed: () {
          List<pb.Image> imageUrls = MessageModel()
                  .find<MessageInfoList>(mMessage.receiverId.toString())
                  ?.getImageUrls() ??
              [];
          pb.Image image = pb.Image.fromBuffer(mMessage.messageContent);
          int index = 0;
          for (int i = 0; i < imageUrls.length; ++i) {
            if (imageUrls[i].url == image.url) {
              index = i;
              break;
            }
          }

          CustomRoute().pushPage(
            GalleryPhotoViewWrapper(mInitialIndex: index, mImageUrls: imageUrls),
          );
        },
      );
    } else if (mMessage.sendState == LogicSendState.SEND) {
      int _sendProgress = 0;
      Widget _progressChild;
      ImageProvider imageProvider;
      // try {
      //   if (pbImage.url.indexOf('http') == 0) {
      //     imageProvider = NetImage.netImageProvider(pbImage.url);
      //   } else {
      //     imageProvider = NativeImageProvider(pbImage.url);
      //   }
      // } catch (e) {
      imageProvider = AssetImage(ResImgs('picture_icon_placeholder'));
      // }

      if (mMessage.extra.isEmpty) {
        _sendProgress = 100;
        _progressChild = Center(
          child: SizedBox(
            width: min(imageWidth, imageHeight) / 2,
            height: min(imageWidth, imageHeight) / 2,
            child: Ring2InsideLoading(
              color: Colors.blue,
              strokeWidth: 5.sp,
            ),
          ),
        );
      } else {
        try {
          var jsonData = jsonDecode(mMessage.extra);
          int progress = jsonData['progress'];
          _sendProgress = progress;
        } catch (e) {
          _sendProgress = 0;
        }

        // _progressChild = Center(
        //   child: SizedBox(
        //     width: min(imageWidth, imageHeight) / 2,
        //     height: min(imageWidth, imageHeight) / 2,
        //     child: CircularProgressIndicator(
        //       color: Colors.green,
        //       value: _sendProgress / 100,
        //     ),
        //   ),
        // );
        _progressChild = Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Icon(Icons.upload, color: Colors.blue, size: defIconSize),
              Text(
                _sendProgress.toStringAsFixed(2) + '%',
                style: TextStyle(color: Colors.white, fontSize: defFontSize),
              ),
            ],
          ),
        );
      }
      print('Image LogicSendState.SEND progress: $_sendProgress');
      msgContent = Container(
        width: imageWidth,
        height: imageHeight,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(15.sp)),
          border: Border.all(width: 1, color: Colors.grey),
          image: DecorationImage(
            image: imageProvider,
            fit: BoxFit.fill,
          ),
        ),
        child: _progressChild,
      );
    } else {
      msgContent = Container(
        width: imageWidth,
        height: imageHeight,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(15.sp)),
          border: Border.all(width: 1, color: Colors.grey),
          image: DecorationImage(
            image: FileImage(File(pbImage.url)),
            fit: BoxFit.contain,
          ),
        ),
        child: Container(
          color: const Color(0x66000000),
          child: Center(
            child: Icon(Icons.error_rounded, size: 32.sp, color: Colors.red),
          ),
        ),
      );
    }

    if (isSelf) {
      msgContent = buildPopUpMenu(child: msgContent);
    }
    //内容
    List<Widget> columnChildren = [];
    if (mMessage.receiverType != pb.ReceiverType.RT_USER) {
      columnChildren.add(nickName);
    }

    Widget? sendIcon = buildSendStateIcon();
    if (sendIcon != null) {
      msgContent = Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          sendIcon,
          msgContent,
        ],
      );
    }
    columnChildren.addAll([
      msgContent,
      sendTime,
    ]);
    Widget childColumn = Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: isSelf ? CrossAxisAlignment.end : CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: columnChildren,
    );

    List<Widget> children = [];
    if (isSelf) {
      //右对齐列表
      children = [
        const Expanded(child: SizedBox()),
        childColumn,
        SizedBox(width: SUT_W(10)),
        userIcon,
      ];
    } else {
      //左对齐列表
      children = [
        userIcon,
        SizedBox(width: SUT_W(10)),
        childColumn,
        const Expanded(child: SizedBox()),
      ];
    }

    return Row(
      mainAxisAlignment: isSelf ? MainAxisAlignment.end : MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      // mainAxisSize: MainAxisSize.min,
      children: children,
    );
  }
}
