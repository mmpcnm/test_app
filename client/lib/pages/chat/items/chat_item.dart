// ignore_for_file: curly_braces_in_flow_control_structures, import_of_legacy_library_into_null_safe

import 'package:custom_pop_up_menu/custom_pop_up_menu.dart';
import 'package:fim_app/configs/api.dart';
import 'package:fim_app/custom/custom_popup_menu/im_popup_menu.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/custom/text/src/emoji_icon.dart';
import 'package:fim_app/custom/text/src/text_span_builder.dart';
import 'package:fim_app/custom/wdiget/user_icon_widget.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/controller/logic.dart';
import 'package:fim_app/package/net/controller/logic_chat.dart';
import 'package:fim_app/package/util/time_utils.dart';
import 'package:fim_app/pages/chat/items/chat_item_cancel.dart';
import 'package:fim_app/pages/chat/items/chat_item_custom.dart';
import 'package:fim_app/pages/chat/items/chat_item_red_packet.dart';
import 'package:fim_app/pages/chat/items/chat_item_rob_red_packet.dart';
import 'package:fim_app/pages/chat/items/chat_item_voice.dart';
import 'package:fim_app/pages/chat/view/ChatATDialog.dart';
import 'package:fim_app/model/chat/list/Base/MessageListBase.dart';
import 'package:fim_app/model/chat/list/MessageList/MessageInfo.dart';
import 'package:fim_app/model/chat/MessageModel.dart';
import 'package:fim_app/model/chat/list/MessageList/MessageInfoList.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/pages/chat/items/chat_item_image.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:m_loading/m_loading.dart';
// import 'package:flutter_slidable/flutter_slidable.dart';
import 'chat_item_text.dart';

import 'package:flutter/material.dart';

import 'package:intl/intl.dart';

abstract class ChatItem {
  late MessageInfo mMessage;
  // const ChatItem(this.mMessage, {bool isTopTable = false}):_isTopTable = isTopTable;
  bool get isSelf => (mMessage.senderId == UserModel.userId);

  double get circular => 10.sp;

  Color get bubbleColor => isSelf ? Colors.lightGreenAccent.shade700 : Colors.white;

  Widget get userIcon {
    if (mMessage.senderType == pb.SenderType.ST_SYSTEM) {
      return UserIconWidget(ResImgs('app_icon'), isNetworkIamge: false);
    } else {
      if (!isTopTable()) {
        return SizedBox(
          width: defAvatarSize + 15.sp,
          height: defAvatarSize,
        );
      }
      String avatarUrl = isSelf ? UserModel.avatarUrl : mMessage.avatarUrl;
      // if (mMessage.receiverType != pb.ReceiverType.RT_USER) {
      //   return Padding(padding: EdgeInsets.only(top: 30.sp), child: UserIconWidget(avatarUrl));
      // } else {
      return UserIconWidget(avatarUrl);
      // }
    }
  }

  Widget get sendTime {
    //发送时间
    // String time = TimeUtils.chatTime(mMessage.sendTime.toInt());
    // List<InlineSpan> children = [TextSpan(text: time, style: TextStyle(fontSize: defMinFontSize, color: lightGrayColor))];
    // if (isSelf) {
    //   children.add(WidgetSpan(child: Icon(Icons.done_all, color: Colors.blue, size: SUT_SP(28)), alignment: PlaceholderAlignment.middle));
    // }

    // EdgeInsets padding = isSelf ? EdgeInsets.only(right: 15.sp, top: 5.sp) : EdgeInsets.only(left: 15.sp, top: 5.sp);

    // return Padding(padding: padding, child: Text.rich(TextSpan(children: children)));
    if (AppConfig.isDebug) {
      var fm = DateFormat('yyyy年MM月dd日 HH:mm:ss');
      DateTime time = DateTime.fromMillisecondsSinceEpoch(mMessage.sendTime.toInt());
      return Column(
        crossAxisAlignment: isSelf ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text('${mMessage.sendState}', style: defTextStyle),
          Text('server ${DateFormat('yyyy年MM月dd日 HH:mm:ss').format(time)}', style: defTextStyle),
        ],
      );
    } else {
      return const Center();
    }
  }

  Widget get nickName {
    if (mMessage.receiverType != pb.ReceiverType.RT_USER && isTopTable()) {
      EdgeInsets padding = isSelf
          ? EdgeInsets.only(right: 15.sp, bottom: 5.sp)
          : EdgeInsets.only(left: 15.sp, bottom: 5.sp);
      return Padding(padding: padding, child: Text(mMessage.nickname, style: defTextStyle));
    } else {
      return const Center();
    }
  }

  Widget buildPopUpMenu({required Widget child}) {
    if (mMessage.isPermittedDelete()) {
      return ImPopUpMenu(
        icon: child,
        axis: Axis.horizontal,
        axisSpacing: 30.w,
        pressType: PressType.longPress,
        color: whiteColor,
        items: [
          //重新发送
          if (mMessage.sendState == LogicSendState.ERROR)
            ImPopupItemModel(
              text: '重新发送',
              onPressed: () {
                reSendMessage();
              },
            ),
          //撤回
          if (!mMessage.isPermittedCancel())
            ImPopupItemModel(
              text: STR(10192),
              onPressed: () => cancelMessage(),
            ),
          //删除
          ImPopupItemModel(
            text: STR(10193),
            onPressed: () {
              deleteMessage();
            },
          ),
        ],
      );
    } else {
      return child;
    }
  }

  Widget? buildErrorIcon() {
    if (mMessage.status == pb.MessageStatus.MS_UNKNOWN) {}
  }

  bool _isTopTable = false;
  bool isTopTable() => _isTopTable;

  void itemInit(MessageInfo message, {bool isTopTable = false}) {
    mMessage = message;
    _isTopTable = isTopTable;
  }

  Widget? buildSendStateIcon() {
    if (!isSelf) {
      return null;
    }
    if (mMessage.sendState == LogicSendState.SEND) {
      return SizedBox(
        width: 32.sp,
        height: 32.sp,
        child: Ring2InsideLoading(
          color: Colors.blue,
          strokeWidth: 5.sp,
        ),
      );
    } else if (mMessage.sendState == LogicSendState.ERROR) {
      return Icon(Icons.error_sharp, size: 32.sp, color: Colors.red);
    } else {
      return null;
    }
  }

  static Widget createItem(MessageInfo message, {bool isTopTable = false}) {
    Widget item;
    if (message.status == pb.MessageStatus.MS_RECALL) {
      item = ChatItemCancel(message, isTopTable: isTopTable);
    } else {
      switch (message.messageType) {
        case pb.MessageType.MT_TEXT:
          item = ChatItemText(message, isTopTable: isTopTable);
          break;
        case pb.MessageType.MT_IMAGE:
          item = ChatItemImage(message, isTopTable: isTopTable);
          break;
        case pb.MessageType.MT_VOICE:
          item = ChatItemVoice(message, isTopTable: isTopTable);
          break;
        case pb.MessageType.MT_SEND_REDPACKET:
          item = ChatItemRedPacket(message, isTopTable: isTopTable);
          break;
        case pb.MessageType.MT_ROB_REDPACKET:
          item = ChatItemRobRedPacket(message, isTopTable: isTopTable);
          break;
        case pb.MessageType.MT_CUSTOM:
          item = ChatItemCustom(message);
          break;
        default:
          item = const Center();
      }
    }

    return item;
  }

  ///撤回(MessageType = MT_TEXT、MT_VOICE、MT_IMAGE)
  void cancelMessage() {
    print('ChatItem cancelMessage ${mMessage.sendState}');
    if (!mMessage.isPermittedCancel()) {
      EasyLoading.showToast('发送超过两分钟无法撤回！');
      return;
    }
    if (mMessage.sendState == LogicSendState.NONE &&
        (mMessage.messageType == pb.MessageType.MT_TEXT ||
            mMessage.messageType == pb.MessageType.MT_VOICE ||
            mMessage.messageType == pb.MessageType.MT_IMAGE)) {
      print('ChatItem cancelMessage ${mMessage.sendState} ${mMessage.messageType}');
      LogicChat.CancelMessage(
          context: CustomRoute().context,
          receiverType: mMessage.receiverType,
          receiverId: mMessage.receiverId,
          seq_id: mMessage.seq);
    }
  }

  ///删除
  void deleteMessage() {
    var item = MessageModel().find<MessageInfoList>(mMessage.receiverId.toString());
    if (item == null || item.infoType != MessageInfoType.MESSAGE) return;
    print('ChatItem deleteMessage ${mMessage.sendState}');
    if (mMessage.sendState == LogicSendState.NONE) {
      print('ChatItem deleteMessage');
      item.remove(seq: mMessage.seq);
    } else if (mMessage.sendState != LogicSendState.SEND) {
      item.removeTemporary(
          sendId: mMessage.senderId, receiverId: mMessage.receiverId, sendTime: mMessage.sendTime);
    }
  }

  ///重新发送(发送失败的消息才可以重新发送)
  void reSendMessage() {
    if (mMessage.sendState != LogicSendState.ERROR) return;

    if (mMessage.messageType == pb.MessageType.MT_IMAGE) {
      pb.Image pbimage = pb.Image.fromBuffer(mMessage.messageContent);
      if (mMessage.extra.isNotEmpty) {
        LogicChat.SendImage(
          receiverType: mMessage.receiverType,
          receiverId: mMessage.receiverId,
          filePath: pbimage.url,
          imageWidth: pbimage.width,
          imageHeight: pbimage.height,
          sendTime: mMessage.localSendTime,
        );
        return;
      }
    }

    if (mMessage.receiverType == pb.ReceiverType.RT_ROOM) {
      LogicChat.PushRoom(
        roomId: mMessage.receiverId,
        messageType: mMessage.messageType,
        messageContent: mMessage.messageContent,
        sendTime: mMessage.localSendTime,
      );
    } else {
      LogicChat.SendMessage(
        receiverType: mMessage.receiverType,
        receiverId: mMessage.receiverId,
        messageType: mMessage.messageType,
        messageContent: mMessage.messageContent,
        sendTime: mMessage.localSendTime,
      );
    }
  }
}
