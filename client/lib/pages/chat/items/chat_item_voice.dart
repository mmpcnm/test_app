// ignore_for_file: must_be_immutable

import 'package:bubble/bubble.dart';
import 'package:fim_app/custom/animation/voice_animation_image.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/wdiget/ClockText.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/pages/chat/items/chat_item.dart';
import 'package:fim_app/model/chat/list/MessageList/MessageInfo.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/model/chat/MessageModel.dart';
import 'package:fim_app/model/voice/VoicePlayerScoped.dart';
import 'package:flutter/material.dart';

class ChatItemVoice extends StatefulWidget with ChatItem {
  ChatItemVoice(MessageInfo message, {Key? key, bool isTopTable = false}) : super(key: key) {
    itemInit(message, isTopTable: isTopTable);
  }
  @override
  State<StatefulWidget> createState() => StateChatItemVoice();
}

class StateChatItemVoice extends State<ChatItemVoice> {
  late pb.Voice _mVoiceData;
  final VoicePlayerItemControll _playerItemControll = VoicePlayerItemControll();
  final VoicePlayerScoped _mVoicePlayerScoped = VoicePlayerScoped();

  @override
  void initState() {
    super.initState();
    _mVoicePlayerScoped.addListener(onPlayerScopedState);
    // VoicePlayerListScoped.instance.addListener(onPlayerScopedState);
  }

  @override
  void dispose() {
    _mVoicePlayerScoped.removeListener(onPlayerScopedState);
    _mVoicePlayerScoped.stop();
    // VoicePlayerListScoped.instance.removeListener(onPlayerScopedState);
    super.dispose();
  }

  void onPlayerScopedState() {
    if (_mVoicePlayerScoped.state == VoicePlayerState.START) {
      _playerItemControll.play();
    } else {
      _playerItemControll.stop();
    }
    // Debug.d('state ${VoicePlayerListScoped.instance.state.toString()}: ${VoicePlayerListScoped.instance.currentUrl}');
    // if (VoicePlayerListScoped.instance.state == VoicePlayerState.START) {
    //   if (_mVoiceData.url == VoicePlayerListScoped.instance.currentUrl) {
    //     _playerItemControll.play();
    //     return;
    //   }
    // }
    // _playerItemControll.stop();
  }

  @override
  Widget build(BuildContext context) {
    //聊天类容
    _mVoiceData = pb.Voice.fromBuffer(widget.mMessage.messageContent);

    //气泡内容
    List<Widget> columnChildren = [];
    if (widget.mMessage.receiverType != pb.ReceiverType.RT_USER && widget.isTopTable()) {
      columnChildren.add(widget.nickName);
    }
    columnChildren.addAll([
      InkWell(
        child: Bubble(
          nip: !widget.isTopTable()
              ? null
              : widget.isSelf
                  ? BubbleNip.rightTop
                  : BubbleNip.leftTop,
          alignment: widget.isSelf ? Alignment.topRight : Alignment.topLeft,
          color: widget.bubbleColor,
          nipRadius: 1.sp,
          nipOffset: 15.h,
          nipHeight: 20.h,
          padding: BubbleEdges.symmetric(vertical: SUT_W(8), horizontal: SUT_W(15)),
          radius: Radius.circular(widget.circular),
          child: Container(
            constraints: BoxConstraints(
                minWidth: 100.sp,
                maxWidth: SUT_S_WIDTH - 230.w,
                minHeight: 64.sp,
                maxHeight: 78.sp),
            child: VoicePlayerItem(
              isSelf: widget.isSelf,
              voice: _mVoiceData,
              controll: _playerItemControll,
              onPalyEnd: () {},
            ),
          ),
        ),
        onTap: () {
          //播放声音
          if (_playerItemControll.isRunting) {
            _playerItemControll.stop();
          } else {
            _playerItemControll.play();
          }
          if (_mVoicePlayerScoped.state == VoicePlayerState.START) {
            _mVoicePlayerScoped.stop();
          } else {
            _mVoicePlayerScoped.playUrl(_mVoiceData.url);
          }
          // if (VoicePlayerListScoped.instance.state == VoicePlayerState.START) {
          //   if (VoicePlayerListScoped.instance.currentUrl == _mVoiceData.url) {
          //     VoicePlayerListScoped.instance.stop();
          //     return;
          //   }
          // }
          // List<String> urls = ChatScoped.instance.getVoiceUrls(widget.mMessage.receiverId, isUnread: false);
          // if (urls.isNotEmpty) {
          //   int index = urls.indexOf(_mVoiceData.url);
          //   if (index >= 0) {
          //     VoicePlayerListScoped.instance.playSounds(urls: urls, index: index);
          //     return;
          //   }
          // }
          // VoicePlayerListScoped.instance.playSounds(urls: [_mVoiceData.url], index: 0);
        },
      ),
      widget.sendTime
    ]);

    Widget childColumn = Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: widget.isSelf ? CrossAxisAlignment.end : CrossAxisAlignment.start,
      children: columnChildren,
    );

    List<Widget> children = [];
    if (widget.isSelf) {
      //右对齐列表
      children = [
        widget.buildPopUpMenu(child: childColumn),
        SizedBox(width: 10.w),
        widget.userIcon,
      ];
    } else {
      //左对齐列表
      children = [
        widget.userIcon,
        SizedBox(width: 10.w),
        childColumn,
      ];
    }

    return Row(
      mainAxisAlignment: widget.isSelf ? MainAxisAlignment.end : MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: children,
    );
  }
}

class VoicePlayerItemControll {
  final VoiceAnimationImageControll _voiceAnimationImageControll = VoiceAnimationImageControll();
  final ClockTextControll _clockTextControll = ClockTextControll();

  bool get isRunting => _clockTextControll.isRunting;

  void play() {
    _voiceAnimationImageControll.start();
    _clockTextControll.start();
  }

  void stop() {
    _voiceAnimationImageControll.stop(isReset: true);
    _clockTextControll.stop(isReset: true);
  }
}

class VoicePlayerItem extends StatelessWidget {
  VoicePlayerItemControll controll;
  bool isSelf;
  pb.Voice voice;
  VoidCallback onPalyEnd;
  VoicePlayerItem(
      {required this.isSelf, required this.voice, required this.controll, required this.onPalyEnd});

  @override
  Widget build(BuildContext context) {
    List<String> images = [
      ResImgs(isSelf ? 'right_voice_3' : 'left_voice_3'),
      ResImgs(isSelf ? 'right_voice_1' : 'left_voice_1'),
      ResImgs(isSelf ? 'right_voice_2' : 'left_voice_2'),
    ];

    ClockText clockText = ClockText(
      millisecond: voice.duration,
      isReverse: true,
      controll: controll._clockTextControll,
      style: TextStyle(fontSize: defFontSize, color: isSelf ? whiteColor : defFontColor),
      onAnimationEnd: () {
        controll._voiceAnimationImageControll.stop(isReset: true);
        onPalyEnd.call();
      },
    );

    VoiceAnimationImage voiceAnimationImage = VoiceAnimationImage(
      images,
      width: 46.sp,
      height: 46.sp,
      isLoop: true,
      isFlipX: isSelf,
      interval: 500,
      controll: controll._voiceAnimationImageControll,
    );
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: isSelf ? MainAxisAlignment.end : MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: isSelf ? [clockText, voiceAnimationImage] : [voiceAnimationImage, clockText],
    );
  }
}
