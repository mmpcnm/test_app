// ignore_for_file: must_be_immutable

import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/model/red_packet/red_packet_model.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/pages/chat/items/chat_item.dart';
import 'package:fim_app/model/chat/MessageModel.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:flutter/material.dart';

class ChatItemRobRedPacket extends StatelessWidget with ChatItem {
  // late pb.RobRedPacket _mRobRedPacket;
  ChatItemRobRedPacket(MessageInfo message, {Key? key, bool isTopTable = false}) : super(key: key) {
    itemInit(message, isTopTable: isTopTable);
    // _mRobRedPacket = pb.RobRedPacket.fromBuffer(mMessage.messageContent);
  }

  static Widget toWidgetText(MessageInfo message,
      {MainAxisAlignment alignment = MainAxisAlignment.center, TextOverflow? overflow}) {
    var robRedPacket = pb.RobRedPacket.fromBuffer(message.messageContent);
    String name = message.senderId == UserModel.userId ? STR(10189) : message.nickname;
    String other = '***';
    RedPacketInfo? info =
        RedPacketModel().findRed(receiverId: message.receiverId, rid: robRedPacket.rid);
    if (info != null) {
      ///是否自己发的红包
      if (info.senderId == UserModel.userId) {
        //是否领取自己的红包
        if (message.senderId == UserModel.userId) {
          other = '自己';
        } else {
          other = STR(10189);
        }
      } else {
        other = info.nickname;
      }
    }

    String text = STR(10270, [other]);

    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: alignment,
      children: <Widget>[
        Image.asset(ResImgs('red/red_alarm_logo'), width: 32.sp, height: 32.sp),
        Text('$name$text', style: defTextStyle),
        Text(
          STR(10271),
          style: TextStyle(fontSize: defFontSize, color: Colors.orange),
          overflow: overflow,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return toWidgetText(mMessage);
  }
}
