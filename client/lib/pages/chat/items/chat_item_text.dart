// import 'package:bubble/bubble.dart';
// ignore_for_file: use_key_in_widget_constructors, must_be_immutable, non_constant_identifier_names

import 'package:bubble/bubble.dart';
import 'package:extended_text/extended_text.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/text/src/extended_text.dart';
import 'package:fim_app/custom/text/src/extended_text_selection_controls.dart';
import 'package:fim_app/custom/text/src/text_span_builder.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/controller/logic.dart';
import 'package:fim_app/model/chat/MessageModel.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/package/util/time_utils.dart';
import 'package:fim_app/pages/chat/items/chat_item.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
// import 'package:flui/flui.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
// import 'package:flutter_easyloading/flutter_easyloading.dart';

class ChatItemText extends StatelessWidget with ChatItem {
  late pb.Text _mPBText;
  ChatItemText(MessageInfo message, {bool isTopTable = false}) {
    itemInit(message, isTopTable: isTopTable);
    _mPBText = pb.Text.fromBuffer(message.messageContent);
  }

  // static Widget toWidgetText(MessageInfo message, {TextOverflow? overflow}) {
  //   pb.Text text = pb.Text.fromBuffer(message.messageContent);
  //   return ChatItem.chatText(text.text, color: defFontColor, overflow: overflow);
  // }

  Widget _buildText() {
    bool isAt = mMessage.receiverType != pb.ReceiverType.RT_USER;

    return CustomExtendedText(
      _mPBText.text,
      key: Key(mMessage.seq.toString()),
      style: TextStyle(fontSize: defFontSize, color: defFontColor),
      textAlign: TextAlign.left,
      specialTextSpanBuilder: TextSpanBuilder(showAtBackground: isAt, isAt: isAt),
      // selectionControls: ExtendedTextSelectionControls(
      //   selectionColor: Colors.indigoAccent.shade700,
      //   toolbarItems: _toolbarItems,
      // ),
      selectToolbarColor: Colors.indigoAccent.shade700,
      toolbarItems: _toolbarItems,
      selectionEnabled: true,
      selectionColor: Colors.indigoAccent.shade700.withOpacity(0.5),
      // selectionHeightStyle: ui.BoxHeightStyle.tight,
    );
  }

  List<TextSelectionToolbarItemData> get _toolbarItems {
    if (!isSelf) return [];
    // if (!mMessage.isPermittedDelete()) {
    //   return [];
    // }
    return [
      //重新发送
      if (mMessage.sendState == LogicSendState.ERROR)
        TextSelectionToolbarItemData(label: '重新发送', onPressed: reSendMessage),
      //撤回
      if (mMessage.isPermittedCancel())
        TextSelectionToolbarItemData(label: STR(10192), onPressed: cancelMessage),
      //删除
      if (mMessage.isPermittedDelete())
        TextSelectionToolbarItemData(label: STR(10193), onPressed: deleteMessage),
    ];
  }

  @override
  Widget build(BuildContext context) {
    //聊天类容
    // pb.Text text = pb.Text.fromBuffer(mMessage.messageContent);

    if (mMessage.senderType == pb.SenderType.ST_SYSTEM) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          userIcon,
          SizedBox(width: 10.w),
          Bubble(
            nip: BubbleNip.leftTop,
            alignment: Alignment.topLeft,
            color: bubbleColor,
            nipRadius: 1.sp,
            nipOffset: 20.sp,
            nipHeight: 15.sp,
            padding: BubbleEdges.symmetric(vertical: 15.sp, horizontal: 15.sp),
            radius: Radius.circular(circular),
            child: Container(
              constraints: BoxConstraints(
                maxWidth: SUT_S_WIDTH - 230.sp,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(mMessage.nickname, style: defTextStyle),
                  Divider(height: 5, color: lineColor),
                  SizedBox(height: 10.sp),
                  // ChatItem.chatText(_mPBText.text, color: isSelf ? whiteColor : defFontColor),
                  _buildText(),
                  SizedBox(height: 10.sp),
                  Text(
                    TimeUtils.chatTime(mMessage.sendTime.toInt()),
                    style: TextStyle(fontSize: defMinFontSize, color: lightGrayColor),
                  ),
                ],
              ),
            ),
          )
        ],
      );

      // return Row(
      //   mainAxisAlignment: MainAxisAlignment.start,
      //   crossAxisAlignment: CrossAxisAlignment.start,
      //   mainAxisSize: MainAxisSize.max,
      //   children: [
      //     userIcon,
      //     Container(
      //       constraints: BoxConstraints(maxWidth: SUT_S_WIDTH - 120.w),
      //       padding: EdgeInsets.only(left: paddingHorizontal),
      //       child: Column(
      //         mainAxisSize: MainAxisSize.min,
      //         crossAxisAlignment: CrossAxisAlignment.start,
      //         children: [
      //           ChatText(text.text, color: lightGrayColor, leading: TextSpan(text: '${mMessage.nickname}: ', style: TextStyle(fontSize: defFontSize))),
      //           Divider(height: 5, color: lineColor),
      //           Text(TimeUtils.chatTime(mMessage.sendTime.toInt()), style: TextStyle(fontSize: defMinFontSize, color: lightGrayColor)),
      //         ],
      //       ),
      //     ),
      //   ],
      // );
    } else {
      //消息内容
      List<Widget> columnChildren = [];
      if (mMessage.receiverType != pb.ReceiverType.RT_USER) {
        columnChildren.add(nickName);
      }
      Widget bubble = Bubble(
        nip: !isTopTable()
            ? null
            : isSelf
                ? BubbleNip.rightTop
                : BubbleNip.leftTop,
        alignment: isSelf ? Alignment.topRight : Alignment.topLeft,
        color: bubbleColor, //isSelf ? Colors.blue : whiteColor,
        nipRadius: 1.sp,
        nipOffset: 20.sp,
        nipHeight: 15.sp,
        padding: BubbleEdges.all(defPaddingSize),
        radius: Radius.circular(10.sp),
        child: Container(
          constraints: BoxConstraints(
            maxWidth: SUT_S_WIDTH - 230.sp,
            minHeight: defAvatarSize - defPaddingSize * 2,
          ), // minWidth: 100.sp),
          // child: Center(
          //   child: ChatText(
          //     text.text,
          //     color: isSelf ? whiteColor : defFontColor,
          //   ),
          // ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            // crossAxisAlignment: isSelf ? CrossAxisAlignment.end : CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              // ChatItem.chatText(_mPBText.text, color: isSelf ? whiteColor : defFontColor),
              _buildText(),
            ],
          ),
        ),
      );

      Widget? sendStateIcon = buildSendStateIcon();
      if (sendStateIcon != null) {
        bubble = Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            sendStateIcon,
            bubble,
          ],
        );
      }

      columnChildren.addAll([bubble, sendTime]);
      Widget childColumn = Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: isSelf ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: columnChildren,
      );

      List<Widget> children = [];
      if (isSelf) {
        //右对齐列表
        children = [
          childColumn,
          SizedBox(width: 10.w),
          userIcon,
        ];
      } else {
        //左对齐列表
        children = [
          userIcon,
          SizedBox(width: 10.w),
          childColumn,
        ];
      }

      return Container(
        constraints: BoxConstraints(maxWidth: SUT_S_WIDTH),
        child: Row(
          mainAxisAlignment: isSelf ? MainAxisAlignment.end : MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: children,
        ),
      );
    }
  }
}
