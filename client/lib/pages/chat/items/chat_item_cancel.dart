// ignore_for_file: must_be_immutable

import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/controller/logic_user.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/pages/chat/items/chat_item.dart';
import 'package:fim_app/pages/friend/user_profile_page.dart';
import 'package:fim_app/pages/group/group_user_profile_page.dart';
import 'package:fim_app/model/ModelBase.dart';
import 'package:fim_app/model/chat/ChatInputModel.dart';
import 'package:fim_app/model/chat/list/MessageList/MessageInfo.dart';

import 'package:flutter/material.dart';

class ChatItemCancel extends StatelessWidget with ChatItem {
  ChatItemCancel(MessageInfo message, {Key? key, bool isTopTable = false}) : super(key: key) {
    itemInit(message, isTopTable: isTopTable);
  }
  @override
  Widget build(BuildContext context) {
    //你
    String nickName = isSelf ? STR(10189) : mMessage.nickname;
    List<Widget> children = [Text('"', style: TextStyle(fontSize: defFontSize, color: grayColor))];

    if (isSelf) {
      children.add(Text(nickName, style: TextStyle(fontSize: defFontSize, color: defFontColor)));
    } else {
      children.add(
        Button(
          padding: EdgeInsets.zero,
          child: Text(
            nickName,
            style: TextStyle(fontSize: defFontSize, color: defFontColor),
          ),
          onPressed: () {
            if (mMessage.receiverType == pb.ReceiverType.RT_GROUP) {
              CustomRoute().pushPage(
                GroupUserProfilePage(groupId: mMessage.receiverId, memberId: mMessage.senderId),
              );
            } else if (mMessage.receiverType == pb.ReceiverType.RT_ROOM) {
              LogicUser.GetUser(
                userId: mMessage.senderId,
                logicCallBack: (code, resp) {
                  if (code == 0) {
                    CustomRoute().pushPage(UserProfilePage(resp!.user));
                  }
                },
              );
            }
          },
        ),
      );
    }

    //撤回了一条信息
    children
        .add(Text(' ${STR(10190)} ', style: TextStyle(fontSize: defFontSize, color: grayColor)));

    if (isSelf && mMessage.messageType == pb.MessageType.MT_TEXT) {
      children.add(
        Button(
          padding: EdgeInsets.zero,
          //编辑
          child: Text(STR(10191), style: defTextStyle),
          onPressed: () {
            pb.Text text = pb.Text.fromBuffer(mMessage.messageContent);
            ChatInputModel model = ScopedModel.of<ChatInputModel>(context);

            model.setState(ChatEditorState.KEYBOARD);
            model.setText(text.text, isSend: true);
          },
        ),
      );
    }

    children.add(Text('"', style: TextStyle(fontSize: defFontSize, color: grayColor)));

    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: children,
    );
  }
}
