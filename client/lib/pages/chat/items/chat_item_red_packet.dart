// ignore_for_file: must_be_immutable

import 'package:bubble/bubble.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/dialog/CustomDialog.dart';
import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/model/red_packet/rob_red_packet_model.dart';
import 'package:fim_app/package/net/controller/logic_business.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/pages/chat/items/chat_item.dart';
import 'package:fim_app/pages/red_packets/LookRedPacketDialog.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/model/chat/MessageModel.dart';
import 'package:fim_app/model/device_info.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:one_context/one_context.dart';

class ChatItemRedPacket extends StatelessWidget with ChatItem {
  late pb.RedPacket _mRedPacket;

  ChatItemRedPacket(MessageInfo message, {Key? key, bool isTopTable = false}) : super(key: key) {
    itemInit(message, isTopTable: isTopTable);
  }

  int _getRedStatus() {
    int redStatus = 0;
    if (_mRedPacket.num <=
        RobRedPacketModel().getRobCount(receiverId: mMessage.receiverId, rid: _mRedPacket.rid)) {
      redStatus = 2;
    }

    if (null !=
        RobRedPacketModel().find(
          receiverId: mMessage.receiverId,
          rid: _mRedPacket.rid,
          robUserId: UserModel.userId,
        )) {
      redStatus = 1;
    }

    if (RefundRedList().isItem(
      receiverType: mMessage.receiverType,
      receiverId: mMessage.receiverId,
      userId: mMessage.senderId,
      rid: _mRedPacket.rid,
    )) {
      redStatus = 3;
    }
    return redStatus;
  }

  void _lookRed() {
    //打开红包
    Debug.d('查看红包');
    EasyLoading.show();
    LogicBusiness.LookUpRedPacket(
      context: OneContext().context,
      rid: _mRedPacket.rid,
      logicCallBack: (code, resp) {
        EasyLoading.dismiss();
        if (code == 0) {
          LookRedPacketDialog.show(
            resp!,
            redPacket: _mRedPacket,
            userIcon: mMessage.avatarUrl,
            userId: mMessage.senderId,
            userName: mMessage.nickname,
          );
        }
      },
    );
  }

  void _robRedPacket() {
    EasyLoading.show();
    LogicBusiness.RobRedPacket(
      context: OneContext().context,
      receiver_type: mMessage.receiverType,
      receiver_id: mMessage.receiverId,
      rid: _mRedPacket.rid,
      logicCallBack: (code, resp) {
        EasyLoading.dismiss();
        if (code == 0) {
          ///领取成功
          showMessageBox(content: '成功领取${mMessage.nickname}的红包', buttons: <MessageBoxButton>[
            MessageBoxButton('关闭'),
            MessageBoxButton('打开查看', onPressed: _lookRed),
          ]);
        } else {
          if (mMessage.receiverType == pb.ReceiverType.RT_ROOM) {
            showMessageBox(content: '红包领取失败', buttons: <MessageBoxButton>[
              MessageBoxButton('关闭'),
              MessageBoxButton('打开查看', onPressed: _lookRed),
            ]);
          }
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    _mRedPacket = pb.RedPacket.fromBuffer(mMessage.messageContent);
    String redName = '${DeviceInfo.appName}红包';
    // bool isRobRed = false;
    // var infoList = MessageModel().find<MessageInfoList>(mMessage.receiverId.toString());
    // if (infoList != null) {
    //   isRobRed = infoList.isSelfRedPacket(_mRedPacket.rid);
    //   if (mMessage.receiverType != pb.ReceiverType.RT_USER ||
    //       mMessage.senderId != UserModel.userId) {
    //     int robCount = infoList.getRobRedPacketCount(_mRedPacket.rid);
    //     if (robCount == _mRedPacket.num) {
    //       isRobRed = true;
    //     }
    //   }
    // }

    if (_mRedPacket.num > 1) {
      redName = '拼手气红包';
    }

    int redPacketStatus = _getRedStatus();

    //消息内容
    List<Widget> columnChildren = [];
    if (mMessage.receiverType != pb.ReceiverType.RT_USER) {
      columnChildren.add(nickName);
    }
    columnChildren.addAll([
      InkWell(
        child: Bubble(
          nip: !isTopTable()
              ? null
              : isSelf
                  ? BubbleNip.rightTop
                  : BubbleNip.leftTop,
          alignment: isSelf ? Alignment.topRight : Alignment.topLeft,
          color: redPacketStatus == 0 || mMessage.receiverType == pb.ReceiverType.RT_USER
              ? Colors.orange
              : grayColor,
          nipRadius: 1.sp,
          nipOffset: 20.sp,
          nipHeight: 15.sp,
          padding: BubbleEdges.symmetric(vertical: 15.sp, horizontal: 15.sp),
          radius: Radius.circular(circular),
          child: Container(
            width: 350.sp,
            constraints:
                BoxConstraints(maxWidth: SUT_S_WIDTH - 155.w, minHeight: 45.sp, minWidth: 100.sp),
            foregroundDecoration:
                redPacketStatus == 0 || mMessage.receiverType == pb.ReceiverType.RT_USER
                    ? null
                    : BoxDecoration(
                        color: grayColor,
                        backgroundBlendMode: BlendMode.saturation,
                      ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              // children: [ChatText(text.text, color: isSelf ? whiteColor : defFontColor)],
              children: [
                Row(
                  children: [
                    Image.asset(
                      ResImgs('red/red_enevlop_no_open'),
                      width: 46.sp,
                      height: 46.sp,
                    ),
                    Expanded(
                        child: Text(_mRedPacket.title,
                            style: defTextStyle, overflow: TextOverflow.ellipsis)),
                  ],
                ),
                SizedBox(height: 15.sp),
                Divider(height: 1, color: whiteColor),
                SizedBox(height: 5.sp),
                Text(redName, style: TextStyle(fontSize: defMinFontSize, color: defFontColor)),
              ],
            ),
          ),
        ),
        onTap: () {
          if (mMessage.receiverType == pb.ReceiverType.RT_USER) {
            if (mMessage.senderId == UserModel.userId) {
              _lookRed();
              return;
            }
          }

          if (_getRedStatus() == 0) {
            _robRedPacket();
          } else {
            _lookRed();
          }
        },
      ),
      sendTime
    ]);
    Widget childColumn = Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: isSelf ? CrossAxisAlignment.end : CrossAxisAlignment.start,
      children: columnChildren,
    );

    List<Widget> children = [];
    if (isSelf) {
      //右对齐列表
      children = [childColumn, SizedBox(width: 10.w), userIcon];
    } else {
      //左对齐列表
      children = [userIcon, SizedBox(width: 10.w), childColumn];
    }
    return Row(
      mainAxisAlignment: isSelf ? MainAxisAlignment.end : MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: children,
    );
  }
}
