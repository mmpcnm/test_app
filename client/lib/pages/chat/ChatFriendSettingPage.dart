// ignore_for_file: file_names

import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/custom/dialog/CustomDialog.dart';
import 'package:fim_app/custom/wdiget/chat_title_wdiget.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/pages/chat/view/ChatSettingUsersView.dart';
import 'package:fim_app/model/chat/MessageModel.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/material.dart';

class ChatFriendSettingPage extends StatefulWidget {
  final Int64 mReceiverId;
  const ChatFriendSettingPage({Key? key, required this.mReceiverId}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _ChatFriendSettingPage();
}

class _ChatFriendSettingPage extends State<ChatFriendSettingPage> {
  @override
  void dispose() {
    // ChatInputScoped.isLock = false;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppbar(
        isBackIcon: true,
        title: ChatTitle(receiverId: widget.mReceiverId, receiverType: pb.ReceiverType.RT_USER),
      ),
      body: ListView(
        physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
        children: [
          ChatSettingUsersView(mReceiverId: widget.mReceiverId, mIsUser: true),
          Divider(height: 1, color: lineColor),
          _buildClaerMessage(context),
        ],
      ),
    );
  }

  Widget _buildClaerMessage(BuildContext context) {
    return Button(
      //清空聊天记录
      child: Text(STR(10204), style: TextStyle(fontSize: defFontSize, color: Colors.red)),
      padding: EdgeInsets.all(SUT_W(25)),
      borderCircular: 0,
      color: Colors.white,
      onPressed: () {
        showMessageDialogEx(
          context,
          title: STR(10185), //'提示',
          content: '${STR(10205)}?', //'是否要删除聊天记录？',
          confirmCallback: () {
            MessageModel().remove(widget.mReceiverId.toString(), isSqlite: true);
          },
        );
      },
    );
  }
}
