// ignore_for_file: non_constant_identifier_names

import 'package:fim_app/custom/app_bar/LodingTitle.dart';
import 'package:fim_app/custom/base/StateBase.dart';
import 'package:fim_app/custom/text/src/global_extended_text_model.dart';
import 'package:fim_app/custom/wdiget/CustomRefreshHandler.dart';
import 'package:fim_app/package/LocalNotifications/LocalNotifications.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/controller/logic_friend.dart';
import 'package:fim_app/package/net/controller/logic_group.dart';
import 'package:fim_app/pages/chat/ChatFriendSettingPage.dart';
import 'package:fim_app/pages/chat/ChatRoomSettingPage.dart';
import 'package:fim_app/pages/chat/view/BottomView/ChatBottomView.dart';
import 'package:fim_app/pages/chat/view/input_row_box/InputRowBox.dart';
import 'package:fim_app/model/chat/ChatInputModel.dart';
import 'package:fim_app/model/device_info.dart';
import 'package:fim_app/model/friend/friend_scoped.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/custom/wdiget/chat_title_wdiget.dart';
import 'package:fim_app/pages/chat/ChatGroupSettingsPage.dart';
import 'package:fim_app/model/group/group_scoped.dart';
import 'package:fim_app/model/voice/VoicePlayerListScoped.dart';
import 'package:fixnum/fixnum.dart';
import 'view/ChatDetailsBody.dart';

import 'package:flutter/material.dart';

class ChatPage extends StatefulWidget {
  final Int64 mReceiverId;
  final pb.ReceiverType mReceiverType;
  final ChatInputModel mChatInputModel = ChatInputModel();

  ChatPage({Key? key, required this.mReceiverId, required this.mReceiverType}) : super(key: key);

  @override
  State<StatefulWidget> createState() => StateChatPage();
}

class StateChatPage extends StateBase<ChatPage> {
  late CustomRefreshHanler mCustomRefreshHanler;
  Offset _monseOffset = Offset.zero;

  // MessageInfoList? _mInfoList;
  @override
  void initState() {
    super.initState();
    // ChatInputScoped.isLock = false;

    mCustomRefreshHanler = CustomRefreshHanler(
        onRefreshHandlerStart: onRefreshHandlerStart, onRefreshHandlerEnd: onRefreshHandlerEnd);
    LocalNotifications.instance.cancel(widget.mReceiverId.toInt());
    mCustomRefreshHanler.refreshHandler(duration: const Duration(milliseconds: 500));
    // GlobalHandler().addListenable(HandlerCode.CHAT_AT_USER_START, on_CHAT_AT_USER_START);
    CustomRoute().addListener(_onReplacePage);
    print('debug widget runtimeType ${widget.runtimeType}');
  }

  @override
  void dispose() {
    widget.mChatInputModel.dispose();
    VoicePlayerListScoped.instance.stop();
    // _mInfoList?.setReadings();
    FocusScope.of(OneContext().context!).requestFocus(FocusNode());
    // ChatScoped.instance.notify();
    mCustomRefreshHanler.dispose();
    // GlobalHandler().removeListenable(HandlerCode.CHAT_AT_USER_START, on_CHAT_AT_USER_START);
    CustomRoute().removeListener(_onReplacePage);
    super.dispose();
  }

  void _onReplacePage() {
    if (!widget.mChatInputModel.isSelfView) {
      widget.mChatInputModel.setState(ChatEditorState.UNKNOWN);
    }
  }

  Future onRefreshHandlerStart() async {
    if (widget.mReceiverType == pb.ReceiverType.RT_GROUP) {
      await LogicGroup.GetGroup(groupId: widget.mReceiverId);
      await Future.delayed(const Duration(milliseconds: 10));
      await LogicGroup.GetGroupMembers(groupId: widget.mReceiverId);
    } else if (widget.mReceiverType == pb.ReceiverType.RT_USER) {
      await LogicFriend.GetFriends();
    } else if (widget.mReceiverType == pb.ReceiverType.RT_ROOM) {
      await LogicGroup.GetRoomUserList(roomId: widget.mReceiverId);
    }
  }

  Future onRefreshHandlerEnd() async {
    setState(() {});
  }

  // void on_CHAT_AT_USER_START(HandlerCode key, dynamic value) {
  //   ChatATDialog.showDialog(receiverId: widget.mReceiverId, receiverType: widget.mReceiverType);
  // }

  @override
  Widget build(BuildContext context) {
    print('StateBase build');
    List<Widget> actions = [];
    if (widget.mReceiverType != pb.ReceiverType.RT_UNKNOWN) {
      //添加导航栏按钮
      actions.add(
        Button(
          padding: EdgeInsets.all(SUT_W(10)),
          child: Icon(Icons.more_horiz, size: 28.sp, color: Colors.black),
          onPressed: () {
            if (widget.mReceiverType == pb.ReceiverType.RT_GROUP) {
              // if (GroupScoped.instance.isGroup(widget.mReceiverId))
              // ChatInputScoped.isLock = true;
              OneContext().push(pageRoute(GroupChatSettingsPage(widget.mReceiverId)));
            } else if (widget.mReceiverType == pb.ReceiverType.RT_USER) {
              if (FriendScoped.instance.isFriend(widget.mReceiverId)) {
                // ChatInputScoped.isLock = true;
                OneContext()
                    .push(pageRoute(ChatFriendSettingPage(mReceiverId: widget.mReceiverId)));
              }
            } else if (widget.mReceiverType == pb.ReceiverType.RT_ROOM) {
              OneContext().push(pageRoute(
                ChatRoomSettingPage(
                  RoomData(
                    roomId: widget.mReceiverId,
                    name: DeviceInfo.appName,
                  ),
                ),
              ));
            }
          },
        ),
      );
    }

    CustomAppbar appBar = CustomAppbar(
      isLineshade: true,
      isBackIcon: true,
      title: LodingTitle(
        noneChild: Text(ChatTitle.getTitleText(widget.mReceiverId, widget.mReceiverType),
            style: defTitleTextStyle),
        waitingChild: Text(
            '${ChatTitle.getTitleText(widget.mReceiverId, widget.mReceiverType)}(${STR(10227)})',
            style: defTitleTextStyle),
      ),
      // ChatTitle(receiverId: widget.mReceiverId, receiverType: widget.mReceiverType),
      actions: actions,
    );

    return GestureDetector(
      onTapDown: (TapDownDetails details) {},
      onTap: () {
        widget.mChatInputModel.setState(ChatEditorState.UNKNOWN);
        GlobalExtendedTextModel().currentCentrols = null;
      },
      child: Scaffold(
        // resizeToAvoidBottomInset: ChatInputScoped.instance.state == ChatEditorState.KEYBOARD,
        resizeToAvoidBottomInset: false,
        backgroundColor: defBackgroundColor,
        // backgroundColor: Colors.blue,
        appBar: appBar,
        body: Column(
          // mainAxisAlignment: MainAxisAlignment.end,
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              child: Listener(
                onPointerDown: (PointerDownEvent event) {
                  _monseOffset = event.localPosition;
                },
                onPointerMove: (PointerMoveEvent event) {
                  // print('onPointerMove ${event}');
                  if (event.localPosition.dy - _monseOffset.dy > 100) {
                    final ChatInputModel model = widget.mChatInputModel;
                    if (model.getState() != ChatEditorState.VOICE &&
                        model.getState() != ChatEditorState.UNKNOWN) {
                      model.setState(ChatEditorState.UNKNOWN);
                    }
                  }
                },
                child: ChatDetailsBody(widget.mReceiverId, widget.mReceiverType),
              ),
            ),
            //输入框
            widget.mChatInputModel.buidler(
              builder: (BuildContext context, Widget? child, ChatInputModel model) {
                return SizedBox(
                  width: SUT_S_WIDTH,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Divider(height: 1, color: lineColor),
                      InputRowBox(
                          mReceiverId: widget.mReceiverId, mReceiverType: widget.mReceiverType),
                      Divider(height: 1, color: lineColor),
                      ChatBottomView(
                          mReceiverId: widget.mReceiverId, mReceiverType: widget.mReceiverType),
                      SizedBox(height: ScreenUtil.bottomBarHeight),
                    ],
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

class ChatAtDialog {}
