// ignore_for_file: file_names

import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/custom/wdiget/custom_list_table.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/package/util/time_utils.dart';
import 'package:fim_app/model/chat/MessageModel.dart';
import 'package:fim_app/model/device_info.dart';
import 'package:flutter/material.dart';

const _tablePadding = 10;

class MessagePaymentPage extends StatefulWidget {
  const MessagePaymentPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MessagePaymentPage();
}

class _MessagePaymentPage extends State<MessagePaymentPage> {
  late List<RefundRedInfo> _items = [];
  final ScrollController _controller = ScrollController();
  @override
  void initState() {
    super.initState();
    _items = RefundRedList().getValues;
    Future.delayed(const Duration(milliseconds: 200), () {
      if (_controller.hasClients) {
        _controller.jumpTo(_controller.position.maxScrollExtent);
      }
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    RefundRedList().setReadings();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: defBackgroundColor,
      appBar: CustomAppbar(
        isBackIcon: true,
        title: Text(DeviceInfo.appName + '支付', style: defTitleTextStyle),
      ),
      body: Container(
        padding: EdgeInsets.only(
          left: 20.sp,
          right: 20.sp,
          bottom: ScreenUtil.bottomBarHeight,
        ),
        child: MessageModel().buidler(
          builder: (BuildContext context, Widget? child, MessageModel model) {
            return ListView.builder(
              physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
              controller: _controller,
              itemBuilder: (BuildContext context, int index) {
                return _buildCard(_items[index]);
              },
              itemCount: _items.length,
            );
          },
        ),
      ),
    );
  }

  Widget _buildCard(RefundRedInfo info) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(top: 20.sp, bottom: 10.sp),
          child: Text(
            TimeUtils.chatTime(info.time.toInt()),
            style: TextStyle(fontSize: defFontSize, color: grayColor),
          ),
        ),
        Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.sp)),
          ),
          color: Colors.white,
          elevation: 0,
          child: Padding(
            padding: EdgeInsets.only(left: 15.sp, right: 25.sp, top: 15.sp, bottom: 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  '红包退还到账通知',
                  style: defTitleTextStyle,
                  softWrap: true,
                  overflow: TextOverflow.ellipsis,
                ),
                SizedBox(height: 25.sp),
                Center(
                  child: Text(
                    '退还金额',
                    style: TextStyle(fontSize: defFontSize, color: grayColor),
                  ),
                ),
                SizedBox(height: 10.sp),
                Center(
                  child: Text(
                    '￥${(info.money.toInt() / 100).toStringAsFixed(2)}',
                    style: TextStyle(
                        fontSize: 46.sp, color: defFontColor, fontWeight: FontWeight.bold),
                    softWrap: true,
                  ),
                ),
                SizedBox(height: 15.sp),
                _buildTextTable('退还方式', '退还到零钱'),
                SizedBox(height: _tablePadding.sp),
                _buildTextTable('退款原因', '好友未在24小时内领取的你发的红包'),
                SizedBox(height: _tablePadding.sp),
                _buildTextTable('到账时间', TimeUtils.timeToString(info.time.toInt())),
                SizedBox(height: _tablePadding.sp),
                _buildTextTable('备注', '退款资金已到账'),
                SizedBox(height: 20.sp),
                CustomListTile(
                  padding: EdgeInsets.symmetric(vertical: 10.sp),
                  title: Text('详情', style: defTextStyle),
                  isArrowIcon: true,
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildTextTable(String title, String subtitle) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          width: 130.sp,
          child: Text(
            title,
            style: TextStyle(fontSize: defFontSize, color: grayColor),
          ),
        ),
        Expanded(
          child: Text(
            subtitle,
            style: TextStyle(fontSize: defFontSize, color: defFontColor),
          ),
        ),
      ],
    );
  }
}
