// ignore_for_file: file_names

import 'dart:math';

import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/custom/dialog/CustomDialog.dart';
import 'package:fim_app/custom/wdiget/CustomRefreshHandler.dart';
import 'package:fim_app/custom/wdiget/chat_title_wdiget.dart';
import 'package:fim_app/custom/wdiget/custom_list_table.dart';
import 'package:fim_app/custom/wdiget/user_icon_widget.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/controller/logic_group.dart';
import 'package:fim_app/package/pb/connect.ext.pbenum.dart' as pb;
import 'package:fim_app/package/pb/logic.ext.pb.dart' as pb;
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/pages/friend/user_profile_page.dart';
import 'package:fim_app/model/chat/MessageModel.dart';
import 'package:fim_app/model/group/group_scoped.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:flutter/material.dart';

class ChatRoomSettingPage extends StatefulWidget {
  final RoomData roomData;
  const ChatRoomSettingPage(this.roomData, {Key? key}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _StateChatRoomSettingPage();
}

class _StateChatRoomSettingPage extends State<ChatRoomSettingPage> {
  List<pb.RoomMember> _members = [];
  late CustomRefreshHanler mCustomRefreshHanler;
  @override
  void initState() {
    super.initState();
    mCustomRefreshHanler = CustomRefreshHanler(
        onRefreshHandlerStart: onRefreshHandlerStart, onRefreshHandlerEnd: onRefreshHandlerEnd);
    mCustomRefreshHanler.refreshHandler();
  }

  @override
  void dispose() {
    mCustomRefreshHanler.dispose();
    super.dispose();
  }

  Future onRefreshHandlerEnd() async {
    setState(() {});
  }

  Future onRefreshHandlerStart() async {
    await LogicGroup.GetRoomUserList(
        roomId: widget.roomData.roomId,
        logicCallBack: (code, resp) {
          if (code == 0 && resp != null) {
            _members = resp.userList;
            _members.sort((a, b) => a.userId.compareTo(b.userId));
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: defBackgroundColor,
      appBar: CustomAppbar(
        isLineshade: true,
        isBackIcon: true,
        title: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            UserIconWidget(ResImgs('app_icon'), isNetworkIamge: false, size: defIconSize),
            ChatTitle(receiverId: widget.roomData.roomId, receiverType: pb.ReceiverType.RT_ROOM)
            // Text('${widget.roomData.name}(${_members.length})', style: defTitleTextStyle),
          ],
        ),
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          await mCustomRefreshHanler.refreshHandler(isSetStatus: false);
        },
        child: CustomScrollView(
          physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
          slivers: [
            // SliverToBoxAdapter(child: SizedBox(height: 15.sp)),
            SliverToBoxAdapter(
              child: Container(
                color: Colors.white,
                child: GridView.builder(
                  padding: EdgeInsets.all(15.sp),
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 5, //每行三个
                    mainAxisSpacing: SUT_SP(15),
                    crossAxisSpacing: SUT_SP(15),
                    childAspectRatio: 1, //显示区域宽高相等
                    // mainAxisExtent: 15.sp,
                  ),
                  itemBuilder: (BuildContext context, int index) {
                    var item = _members[index];
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Expanded(
                          child: UserIconWidget(
                            item.avatarUrl,
                            userId: item.userId,
                            onTap: (id) {
                              if (id == UserModel.userId) {
                                gotpHome(context, index: HomeViewIndex.MY);
                              } else {
                                OneContext().push(
                                  pageRoute(
                                    UserProfilePage(pb.User(
                                        userId: id,
                                        avatarUrl: item.avatarUrl,
                                        nickname: item.nickname,
                                        account: '',
                                        extra: item.extra,
                                        sex: item.sex)),
                                  ),
                                );
                              }
                            },
                          ),
                        ),
                        SizedBox(height: SUT_H(3)),
                        Text(
                          item.nickname,
                          style: TextStyle(fontSize: defMinFontSize, color: defFontColor),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    );
                  },
                  itemCount: min(_members.length, 20),
                ),
              ),
            ),
            // SliverToBoxAdapter(child: SizedBox(height: 15.sp)),
            if (_members.length > 20)
              SliverToBoxAdapter(
                child: CustomListTile(
                  padding: EdgeInsets.all(defPaddingSize),
                  middletitle:
                      Text(STR(10206), style: defTextStyle, overflow: TextOverflow.ellipsis),
                  onPressed: () {},
                ),
              ),
            SliverList(
              delegate: SliverChildListDelegate(<Widget>[
                // Divider(height: 1, color: lineColor),
                SizedBox(height: SUT_H(25)),

                ///聊天室名称
                CustomListTile(
                  color: Colors.white,
                  padding: EdgeInsets.symmetric(horizontal: defPaddingSize, vertical: 25.sp),
                  title: Text(STR(10207), style: defTextStyle, overflow: TextOverflow.ellipsis),
                  trailing: Text(widget.roomData.name,
                      style: defTextStyle, overflow: TextOverflow.ellipsis),
                ),
                Divider(height: 1, color: lineColor),

                ///聊天室公告
                CustomListTile(
                  color: Colors.white,
                  padding: EdgeInsets.symmetric(horizontal: defPaddingSize, vertical: 25.sp),
                  title: Text(STR(10210) + (widget.roomData.introduction.isEmpty ? '' : ': '),
                      style: defTextStyle),
                  subtitle: widget.roomData.introduction.isEmpty
                      ? null
                      : Text(
                          widget.roomData.introduction,
                          style: TextStyle(fontSize: defFontSize, color: grayColor),
                          softWrap: true,
                        ),
                ),
                Divider(height: 1, color: lineColor),

                ///我在群里的名称
                CustomListTile(
                  color: Colors.white,
                  padding: EdgeInsets.symmetric(horizontal: defPaddingSize, vertical: 25.sp),
                  title: Text(STR(10212), style: defTextStyle),
                  trailing: Text(UserModel.nickName, style: defTextStyle),
                ),
                // Divider(height: 1, color: lineColor),
                SizedBox(height: SUT_H(25)),
                Button(
                  //清空聊天记录
                  child:
                      Text(STR(10204), style: TextStyle(fontSize: defFontSize, color: Colors.red)),
                  padding: EdgeInsets.all(SUT_W(25)),
                  borderCircular: 0,
                  color: Colors.white,
                  onPressed: () {
                    showMessageDialogEx(
                      context,
                      title: STR(10185), //'提示',
                      content: '${STR(10205)}?', //'是否要删除聊天记录？',
                      confirmCallback: () {
                        MessageModel().remove(widget.roomData.roomId.toString(), isSqlite: true);
                      },
                    );
                  },
                ),
              ]),
            ),
          ],
        ),
      ),
    );
  }
}
