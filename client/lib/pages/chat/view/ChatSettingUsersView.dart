// ignore_for_file: file_names, must_be_immutable

import 'dart:math';

import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/wdiget/user_icon_widget.dart';
import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/pb/logic.ext.pb.dart' as pb;
import 'package:fim_app/pages/friend/friend_profile_page.dart';
import 'package:fim_app/model/friend/friend_info.dart';
import 'package:fim_app/model/friend/friend_scoped.dart';
import 'package:fim_app/model/group/group_member_model.dart';
import 'package:fim_app/model/group/group_member_scoped.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/pages/Group/new_group_page.dart';
import 'package:fim_app/pages/group/group_user_profile_page.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/material.dart';
import 'package:one_context/one_context.dart';

class ChatSettingUsersView extends StatelessWidget {
  Int64 mReceiverId;
  int mMemberMaxCount;
  bool mIsUser;

  int _minCount = 0;
  int _addCount = 0;
  pb.MemberType _memberType = pb.MemberType.GMT_MEMBER;
  List<GroupMemberModel> _members = [];
  final List<FriendInfo> _users = [];

  ChatSettingUsersView(
      {Key? key, required this.mReceiverId, this.mIsUser = false, this.mMemberMaxCount = 0})
      : super(key: key) {
    if (mIsUser) {
      FriendInfo? model = FriendScoped.instance.findFriend(mReceiverId);
      if (model != null) {
        _users.add(model);
        _minCount = 1;
      }
      _addCount = 1;
    } else {
      _members = GroupMemberScoped.instance.getMembers(groupId: mReceiverId);
      Debug.d('ChatSettingUsersView memebers count:${_members.length}');
      if (mMemberMaxCount <= 0) {
        _minCount = _members.length;
      } else {
        _minCount = min(_members.length, mMemberMaxCount);
      }
      _members.sort((a, b) => a.userId.compareTo(b.userId));
      _memberType =
          GroupMemberScoped.instance.getMemberType(groupId: mReceiverId, userId: UserModel.userId);
      if (_memberType == pb.MemberType.GMT_ADMIN) {
        _addCount = 1;
      }
    }
  }

  List<Int64> getUserIds() {
    List<Int64> ret = [];
    if (mIsUser) {
      ret.add(mReceiverId);
    } else {
      for (var item in _members) {
        if (FriendScoped.instance.isFriend(item.userId)) ret.add(item.userId);
      }
    }

    return ret;
  }

  @override
  Widget build(BuildContext context) {
    double spacing = 15.sp;
    return Container(
      color: Colors.white,
      child: GridView.builder(
        shrinkWrap: true,
        // scrollDirection: Axis.horizontal,
        padding: EdgeInsets.all(spacing),
        physics: const NeverScrollableScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 5, //每行三个
          mainAxisSpacing: spacing,
          crossAxisSpacing: spacing,
          childAspectRatio: 1, //显示区域宽高相等
        ),
        itemBuilder: (context, index) {
          if (index < _minCount) {
            String avatarUrl = '';
            String nickName = '';
            if (mIsUser) {
              FriendInfo friend = _users[index];
              avatarUrl = friend.avatarUrl;
              nickName = friend.nickname;
            } else {
              GroupMemberModel member = _members[index];
              nickName = member.userNickname;
              avatarUrl = member.avatarUrl;
            }
            //好友群友
            Widget userIcon = UserIconWidget(
              avatarUrl,
              userId: Int64(index),
              size: 78.sp,
              onTap: (idx) {
                if (!mIsUser) {
                  GroupMemberModel member = _members[idx.toInt()];
                  if (member.userId == UserModel.userId) {
                    gotpHome(context, index: HomeViewIndex.MY);
                  } else {
                    OneContext().push(
                      pageRoute(
                          GroupUserProfilePage(groupId: member.groupId, memberId: member.userId)),
                    );
                  }
                } else {
                  OneContext().push(
                    pageRoute(FriendProfilePage(mReceiverId)),
                  );
                }
              },
            );

            Text nameText = Text(
              nickName,
              style: TextStyle(fontSize: defMinFontSize, color: defFontColor),
              overflow: TextOverflow.ellipsis,
            );

            return Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(child: userIcon),
                SizedBox(height: 3.sp),
                nameText,
              ],
            );
          } else {
            //添加好友
            return Column(
              children: [
                UserIconWidget(
                  ResImgs('icon_add_emoji'),
                  isNetworkIamge: false,
                  size: 78.sp,
                  onTap: (idx) {
                    if (!mIsUser) {
                      OneContext().push(
                        pageRoute(NewGroupPage(
                            groupId: mReceiverId, tilteText: STR(10164), select: getUserIds())),
                      );
                    } else {
                      OneContext().push(
                        pageRoute(NewGroupPage(
                          groupId: Int64.ZERO,
                          tilteText: STR(10164),
                          select: getUserIds(),
                        )),
                      );
                    }
                  },
                ),
              ],
            );
          }
        },
        itemCount: _minCount + _addCount,
      ),
    );
  }
}
