// ignore_for_file: must_be_immutable, file_names

import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/custom/image/image_utils.dart';
import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/package/net/controller/logic_chat.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/pages/red_packets/RedPacketPage.dart';
import 'package:fim_app/model/chat/ChatInputModel.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:image_picker/image_picker.dart';
import 'package:scoped_model/scoped_model.dart';

class ChatAdditionalView extends StatelessWidget {
  final pb.ReceiverType mReceiverType;
  final Int64 mReceiverId;
  final double height;
  const ChatAdditionalView(
      {Key? key, required this.mReceiverId, required this.mReceiverType, required this.height})
      : super(key: key);

  void sendImage(String imagePath, double width, double height) {
    Debug.d('sendImage: $imagePath, width:$width x height:$height');
    LogicChat.SendImage(
      receiverType: mReceiverType,
      receiverId: mReceiverId,
      filePath: imagePath,
      imageWidth: width.toInt(),
      imageHeight: height.toInt(),
      sendTime: LogicChat.getSendTime().toInt(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: defBackgroundColor,
      height: height,
      child: PageView(
        physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
        onPageChanged: (int index) {},
        children: [
          GridView(
            padding: EdgeInsets.only(top: 40.sp),
            physics: const NeverScrollableScrollPhysics(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 4, //每列三个
              mainAxisSpacing: 40.sp,
              crossAxisSpacing: 0,
              childAspectRatio: 1, //显示区域宽高相等
            ),
            children: [
              _buildIcon(
                iconName: 'icon_image',
                text: '相册',
                onPressed: () {
                  ChatInputModel model = ScopedModel.of<ChatInputModel>(context);
                  model.setState(ChatEditorState.UNKNOWN);
                  Future.delayed(
                    const Duration(milliseconds: 5),
                    () => ImageUtils.pickAndCropImageUpService(
                      onSucceed: sendImage,
                      onError: (msg) {
                        // EasyLoading.showError('图片上传失败！');
                        if (msg.isEmpty) {
                          EasyLoading.showError('图片上传失败！');
                        } else {
                          EasyLoading.showError(msg);
                        }
                      },
                    ),
                  );
                },
              ),
              _buildIcon(
                iconName: 'icon_camrea',
                text: '拍照',
                onPressed: () {
                  ChatInputModel model = ScopedModel.of<ChatInputModel>(context);
                  model.setState(ChatEditorState.UNKNOWN);
                  Future.delayed(
                    const Duration(milliseconds: 5),
                    () => ImageUtils.pickAndCropImageUpService(
                      source: ImageSource.camera,
                      onSucceed: sendImage,
                      onError: (msg) {},
                    ),
                  );
                },
              ),
              _buildIcon(
                iconName: 'icon_red_packets',
                text: '红包',
                onPressed: () {
                  CustomRoute().pushPage(
                    RedPacketPage(
                      receiverId: mReceiverId,
                      receiverType: mReceiverType,
                      mRedType: 2,
                    ),
                  );
                },
              ),
              _buildIcon(
                iconName: 'icon_file',
                text: '文件',
                onPressed: () {
                  EasyLoading.showToast('开发中。。。');
                },
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildIcon({required String iconName, required String text, VoidCallback? onPressed}) {
    return Button(
      padding: EdgeInsets.zero,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(ResImgs(iconName), width: 90.sp, height: 90.sp),
          SizedBox(height: 10.sp),
          Text(text, style: TextStyle(fontSize: defMinFontSize, color: defFontColor)),
        ],
      ),
      onPressed: onPressed,
    );
  }
}
