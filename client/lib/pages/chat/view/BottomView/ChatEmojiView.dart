// ignore_for_file: file_names

import 'package:fim_app/custom/text/src/emoji_icon.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/model/chat/ChatInputModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:scoped_model/scoped_model.dart';

class ChatEmojiView extends StatelessWidget {
  final double height;
  const ChatEmojiView({Key? key, required this.height}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    double spacing = 0;
    return Container(
      height: height,
      color: Colors.white,
      child: GridView.builder(
        scrollDirection: Axis.vertical,
        padding: EdgeInsets.all(spacing),
        physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 7, //每列三个
          mainAxisSpacing: spacing,
          crossAxisSpacing: spacing,
          childAspectRatio: 1, //显示区域宽高相等
        ),
        itemBuilder: (context, index) {
          Emoji emoji = EmojiData.atEmoji(index);
          return EmojiIcon(
            emoji.key,
            // size: 100.sp,
            padinng: EdgeInsets.all(5.sp),
            onLongPress: (value) {},
            onPressed: (String value) {
              ChatInputModel model = ScopedModel.of<ChatInputModel>(context);
              model.setInsertText('[$value]');
            },
          );
        },
        itemCount: EmojiData.count,
      ),
    );
  }
}
