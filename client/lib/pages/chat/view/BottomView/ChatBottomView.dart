// ignore_for_file: file_names

import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/pages/chat/view/BottomView/ChatAdditionalView.dart';
import 'package:fim_app/pages/chat/view/BottomView/ChatEmojiView.dart';
import 'package:fim_app/model/chat/ChatInputModel.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/material.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:scoped_model/scoped_model.dart';

class ChatBottomView extends StatefulWidget {
  final pb.ReceiverType mReceiverType;
  final Int64 mReceiverId;
  const ChatBottomView({Key? key, required this.mReceiverId, required this.mReceiverType})
      : super(key: key);
  @override
  State<StatefulWidget> createState() => _StateChatBottomView();
}

class _StateChatBottomView extends State<ChatBottomView> with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    final ChatInputModel model = ScopedModel.of<ChatInputModel>(context);
    // Debug.w('ChatBottomView build ${model.state}');
    if (model.getState() == ChatEditorState.ADDITION) {
      return _buildAnimation(
        child: ChatAdditionalView(
          mReceiverId: widget.mReceiverId,
          mReceiverType: widget.mReceiverType,
          height: model.getKeyboardHeight(),
        ),
      );
    } else if (model.getState() == ChatEditorState.EMOJI) {
      return _buildAnimation(
        child: ChatEmojiView(height: model.getKeyboardHeight()),
      );
    } else if (model.getState() == ChatEditorState.KEYBOARD) {
      return Container(
        height: model.getKeyboardHeight(),
        color: Colors.white,
      );
    } else {
      return SizedBox(height: ScreenUtil.bottomBarHeight);
    }
  }

  Widget _buildAnimation({required Widget child}) {
    // return SlideTransitionX(
    //   position: Tween<double>(begin: 0, end: ChatInputScoped.keyboardHeight).animate(
    //     CurvedAnimation(
    //       parent: AnimationController(
    //         vsync: this,
    //         duration: const Duration(milliseconds: 300),
    //       ),
    //       curve: Curves.easeInOut,
    //     ),
    //   ),
    //   child: child,
    // );
    return child;
  }
}
