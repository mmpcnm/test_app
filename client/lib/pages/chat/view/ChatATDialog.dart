// ignore_for_file: file_names

import 'dart:convert';

import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/custom/text/src/text_span_builder.dart';
import 'package:fim_app/custom/wdiget/CustomRefreshHandler.dart';
import 'package:fim_app/custom/wdiget/custom_list_table.dart';
import 'package:fim_app/custom/wdiget/user_icon_widget.dart';
import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/package/net/controller/logic_group.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/model/chat/ChatInputModel.dart';
import 'package:fim_app/model/group/room_members_model.dart';
import 'package:fim_app/model/group/group_member_scoped.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class ChatATDialog extends StatefulWidget {
  final Int64 mReceiverId;
  final pb.ReceiverType mReceiverType;
  // final List<ATDialogUserData> users;
  final ChatInputModel mChatInputModel;
  const ChatATDialog(
      {Key? key,
      required this.mChatInputModel,
      required this.mReceiverId,
      required this.mReceiverType})
      : super(key: key);

  static Future showDialog(
      {required ChatInputModel chatInputModel,
      required Int64 receiverId,
      required pb.ReceiverType receiverType}) {
    return CustomRoute().showModalBottomSheet(builder: (context) {
      return ChatATDialog(
          mChatInputModel: chatInputModel, mReceiverId: receiverId, mReceiverType: receiverType);
    });
  }

  @override
  State<StatefulWidget> createState() => _StateChatATDialog();
}

class _StateChatATDialog extends State<ChatATDialog> {
  late CustomRefreshHanler _refreshHanler;
  final List<ATData> users = [];

  @override
  void initState() {
    super.initState();
    _refreshHanler = CustomRefreshHanler(
        onRefreshHandlerStart: onRefreshHandlerStart, onRefreshHandlerEnd: onRefreshHandlerEnd);
    _refreshHanler.refreshHandler(isSetStatus: true);
  }

  @override
  void dispose() {
    _refreshHanler.dispose();
    super.dispose();
  }

  Future onRefreshHandlerStart() async {
    if (widget.mReceiverType == pb.ReceiverType.RT_ROOM) {
      await LogicGroup.GetRoomUserList(roomId: widget.mReceiverId);
    } else if (widget.mReceiverType == pb.ReceiverType.RT_GROUP) {
      await LogicGroup.GetGroupMembers(groupId: widget.mReceiverId);
    }
  }

  Future onRefreshHandlerEnd() async {
    users.clear();
    if (widget.mReceiverType == pb.ReceiverType.RT_ROOM) {
      var list = RoomMembersModel().getMembers(widget.mReceiverId);
      Debug.d('ChatATDialog Room: ${list.length}');
      for (var item in list) {
        if (item.userId != UserModel.userId)
          users.add(ATData(userId: item.userId, nick: item.nickname, avatarUrl: item.avatarUrl));
      }
    } else if (widget.mReceiverType == pb.ReceiverType.RT_GROUP) {
      var list = GroupMemberScoped.instance.getMembers(groupId: widget.mReceiverId);
      Debug.d('ChatATDialog Group: ${list.length}');
      for (var item in list) {
        if (item.userId != UserModel.userId)
          users.add(ATData(userId: item.userId, nick: item.nickname, avatarUrl: item.avatarUrl));
      }
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: SUT_S_HEIGHT * 0.7,
      constraints: BoxConstraints(maxHeight: SUT_S_HEIGHT * 0.7),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(SUT_SP(35)),
          topRight: Radius.circular(SUT_SP(35)),
        ),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(height: 5.sp),
          CustomListTile(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(SUT_SP(35)),
              topRight: Radius.circular(SUT_SP(35)),
            ),
            padding: EdgeInsets.symmetric(horizontal: defPaddingSizeH, vertical: defPaddingSizeH),
            middletitle: Text('选择群友', style: defTitleTextStyle),
            trailing: Button(
              padding: EdgeInsets.zero,
              child: const Icon(Icons.close_outlined),
              onPressed: () {
                CustomRoute().popDialog();
              },
            ),
          ),
          Divider(height: 1, color: lineColor),
          Expanded(
            child: SizedBox(
              // height: SUT_S_HEIGHT * 0.5,
              child: _refreshHanler.buildWidget(context, builder: () {
                return ListView.builder(
                  physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                  itemBuilder: (BuildContext context, int index) {
                    var item = users[index];
                    return CustomListTile(
                      leading: UserIconWidget(item.avatarUrl, size: 64.sp),
                      title: Text(item.nick, style: defTextStyle),
                      sender: jsonEncode(item.toJson()),
                      onValuePressed: (String? value) {
                        CustomRoute().popDialog();
                        if (value != null) {
                          final ChatInputModel model = widget.mChatInputModel;
                          model.setInsertText('$value ');
                        }
                      },
                      bottomLine: Divider(height: 1, color: lineColor),
                    );
                  },
                  itemCount: users.length,
                );
              }),
            ),
          ),
        ],
      ),
    );
  }
}
