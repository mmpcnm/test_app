// ignore_for_file: must_call_super, must_be_immutable, file_names

import 'dart:async';
import 'dart:math' as math;

import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/azlistview/azlistview.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/custom/scrollable_positioned_list/src/item_positions_listener.dart';
import 'package:fim_app/custom/scrollable_positioned_list/src/scrollable_positioned_list.dart';
import 'package:fim_app/custom/wdiget/EmptyBackWidger.dart';
import 'package:fim_app/package/LocalNotifications/LocalNotifications.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/package/util/time_utils.dart';
import 'package:fim_app/model/chat/MessageModel.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:fixnum/fixnum.dart';
import '../items/chat_item.dart';

import 'package:flutter/material.dart';

class _MsgListData extends ISuspensionBean {
  MessageInfo chatModel;
  String _suspensionTag = '';
  bool isShowTable = false;
  _MsgListData({required this.chatModel}) {
    _suspensionTag = TimeUtils.chatTime(chatModel.sendTime.toInt());
  }

  @override
  String getSuspensionTag() {
    return _suspensionTag;
  }
}

class ChatDetailsBody extends StatefulWidget {
  final Int64 mReceiverId;
  final pb.ReceiverType mReceiverType;

  const ChatDetailsBody(this.mReceiverId, this.mReceiverType, {Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _ChatDetailsBody();
  }
}

class _ChatDetailsBody extends State<ChatDetailsBody> with AutomaticKeepAliveClientMixin {
  final ItemScrollController _mItemScrollController = ItemScrollController();
  final ItemPositionsListener _mItemPositionsListener = ItemPositionsListener.create();
  // MessageInfoList? _mInfoList;
  final List<_MsgListData> _mMessages = [];
  int _mCurrentCount = 0;

  int _mAtSelfIndex = -1;

  // Size _bodySize = Size.zero;

  double getAlignment(int index) {
    double alignment = _mItemPositionsListener.getAlignment(index);
    // double viewHeight = SUT_S_HEIGHT;

    // if (alignment > 0 && context.size != null) {
    //   Size bodySize = context.size!;
    //   double startHeight = SUT_H(74) + ScreenUtil.statusBarHeight;
    //   double startAlignment = startHeight / viewHeight;

    //   alignment = math.min(alignment, bodySize.height / viewHeight + startAlignment);

    //   print('debug getAlignment ${context.size} $alignment (${bodySize.height}, $viewHeight)');
    // }

    return alignment;
  }

  @override
  bool get wantKeepAlive => false;
  @override
  void initState() {
    super.initState();
    var list = MessageModel().find(widget.mReceiverId.toString())?.getValues ?? [];
    for (int index = 0; index < list.length; index++) {
      _mMessages.add(_MsgListData(chatModel: list[index]));
    }
    _mCurrentCount = _mMessages.length;
    _setShowSuspensionStatus();

    _mAtSelfIndex = _getAtSelfIndex();

    MessageModel().addListener(_onChatHandler);
    if (_mMessages.isNotEmpty) {
      Future.delayed(const Duration(milliseconds: 10), () {
        if (_mItemScrollController.isAttached) {
          _mItemScrollController.jumpToEx(
              index: _mCurrentCount - 1, itemPositionsListener: _mItemPositionsListener);
          // double alignment = getAlignment(_mCurrentCount - 1);
          // if (alignment > 0) {
          //   _mItemScrollController.jumpTo(index: _mCurrentCount - 1, alignment: alignment);
          // }
        }
      });
    }
  }

  @override
  void dispose() {
    MessageModel().find(widget.mReceiverId.toString())?.setReadings();
    MessageModel().removeListener(_onChatHandler);
    super.dispose();
  }

  void _setShowSuspensionStatus() {
    // _mMessages.sort((a, b) => a.chatModel.sendTime.compareTo(b.chatModel.sendTime));

    SuspensionUtil.setShowSuspensionStatus<_MsgListData>(
      _mMessages,
      onStatus: (_MsgListData? dataA, _MsgListData dataB, _MsgListData? lastTagItem) {
        if (dataA == null) return true;
        if (lastTagItem == null) return true;
        // DateTime aTime = DateTime.fromMillisecondsSinceEpoch(dataA.chatModel.sendTime.toInt());
        // DateTime bTime = DateTime.fromMillisecondsSinceEpoch(dataB.chatModel.sendTime.toInt());
        int dvalue = (dataB.chatModel.sendTime - lastTagItem.chatModel.sendTime).toInt();
        if (Duration(milliseconds: dvalue).inMinutes >= 5) {
          return true;
        }

        dvalue = (dataB.chatModel.sendTime - dataA.chatModel.sendTime).toInt();
        if (Duration(milliseconds: dvalue).inMinutes > 3) {
          return true;
        }
        // if (dvalue > 1000 * 60 * 3) {
        //   return true;
        // }

        if (dataA.chatModel.senderId != dataB.chatModel.senderId) {
          dataB.isShowTable = true;
        } else {
          dataB.isShowTable = false;
        }

        return false;
      },
    );
  }

  void _onChatHandler() {
    setState(() {
      _mCurrentCount = _mMessages.length;
      _mMessages.clear();

      var list = MessageModel().find(widget.mReceiverId.toString())?.getValues ?? [];
      for (var item in list) {
        _mMessages.add(_MsgListData(chatModel: item));
      }

      _setShowSuspensionStatus();
      LocalNotifications.instance.cancel(widget.mReceiverId.toInt());
      int count = _mMessages.length;

      _mAtSelfIndex = _getAtSelfIndex();
      if (count != _mCurrentCount) {
        Future.delayed(const Duration(milliseconds: 200), () async {
          if (_mItemScrollController.isAttached) {
            _mItemScrollController.scrollToEx(
              index: _mCurrentCount - 1,
              duration: const Duration(milliseconds: 200),
              curve: Curves.easeInSine,
              itemPositionsListener: _mItemPositionsListener,
            );
            // double alignment = getAlignment(_mCurrentCount - 1);
            // if (alignment > 0) {
            //   _mItemScrollController.scrollTo(
            //     index: count,
            //     duration: const Duration(milliseconds: 200),
            //     curve: Curves.easeInSine,
            //     alignment: alignment,
            //   );
            // }
          }
        });
      }
      _mCurrentCount = count;
    });
  }

  int _getAtSelfIndex() {
    for (var i = _mMessages.length - 1; i >= 0; i--) {
      var item = _mMessages[i];
      if (!item.chatModel.isReading && item.chatModel.toUserIds.contains(UserModel.userId)) {
        return i;
      }
    }
    return -1;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned(
          top: 0,
          bottom: 0,
          left: 0,
          right: 0,
          child: _mMessages.isEmpty
              ? EmptyBackWidget(image: ResImgs('message_empty'), text: STR(10263))
              : _buildListView(),
        ),
        if (widget.mReceiverType != pb.ReceiverType.RT_USER)
          Positioned(
            bottom: 10.sp,
            right: 0,
            child: AnimatedSlide(
              offset: Offset(_mAtSelfIndex == -1 ? 1 : 0, 0),
              duration: const Duration(milliseconds: 200),
              child: Button(
                color: Colors.white,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(45), bottomLeft: Radius.circular(45)),
                padding: EdgeInsets.only(left: 15.sp, right: 25.sp, top: 15.sp, bottom: 15.sp),
                child: Text('有人@了你', style: defTextStyle),
                onPressed: () {
                  _mItemScrollController.scrollToEx(
                    index: _mAtSelfIndex,
                    duration: const Duration(milliseconds: 200),
                    curve: Curves.easeInSine,
                    itemPositionsListener: _mItemPositionsListener,
                  );
                  _mAtSelfIndex = -1;
                  setState(() {});
                },
              ),
            ),
          ),
      ],
    );
  }

  Widget _buildListView() {
    return ScrollablePositionedList.separated(
      padding: EdgeInsets.symmetric(horizontal: defPaddingSize, vertical: defPaddingSize),
      physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
      itemScrollController: _mItemScrollController,
      itemPositionsListener: _mItemPositionsListener,
      // shrinkWrap: true,
      // initialAlignment: 1,
      itemCount: _mCurrentCount,
      itemBuilder: (BuildContext context, int idx) {
        int index = idx;
        var msg = _mMessages[index];
        MessageInfo item = _mMessages[index].chatModel;
        if (!item.isReading) {
          item.isReading = true;
          MessageInfoList? infoList =
              MessageModel().find<MessageInfoList>(item.receiverId.toString());
          if (infoList != null) {
            infoList.setReading(seq: item.seq, isSql: false);
          }
        }
        if (msg.isShowSuspension) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(height: SUT_H(15)),
              Center(
                child: Text(msg.getSuspensionTag(), style: defTextStyle),
              ),
              SizedBox(height: SUT_H(30)),
              ChatItem.createItem(item, isTopTable: true),
            ],
          );
        }
        return ChatItem.createItem(item, isTopTable: true);
      },
      separatorBuilder: (BuildContext context, int idx) {
        return SizedBox(height: SUT_H(20));
      },
    );
  }
}
