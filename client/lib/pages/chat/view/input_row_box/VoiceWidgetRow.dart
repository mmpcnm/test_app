import 'dart:async';
import 'dart:math';

import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/model/voice/VoiceRecordScoped.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_plugin_record/utils/common_toast.dart';
import 'package:flutter_plugin_record/widgets/custom_overlay.dart';

typedef startRecord = Future Function();
typedef stopRecord = Future Function();

class VoiceWidgetRow extends StatefulWidget {
  final Function? startRecord;
  final void Function(String filePath, double timeLength)? stopRecord;
  final Function? cancelRecord;
  final EdgeInsets? padding;
  final Decoration? decoration;

  /// 总时长(秒)
  final int countTotal;

  /// startRecord 开始录制回调  stopRecord回调
  const VoiceWidgetRow(
      {Key? key,
      this.startRecord,
      this.stopRecord,
      this.decoration,
      this.cancelRecord,
      this.padding,
      this.countTotal = 12})
      : super(key: key);

  @override
  _VoiceWidgetRow createState() => _VoiceWidgetRow();
}

class _VoiceWidgetRow extends State<VoiceWidgetRow> {
  // 倒计时总时长
  // double starty = 0.0;
  // double offset = 0.0;
  bool _mIsCancel = false;
  String _mTextShow = STR(10198); //按住说话
  String _mToastShow = STR(10199); //手指上滑,取消发送
  String _mVoiceIco = 'images/voice_volume_1.png';

  ///默认隐藏状态
  bool _mVoiceState = true;
  Timer? _mTimer;
  int _mCount = 0;
  OverlayEntry? _mOverlayEntry;

  VoiceRecordScoped _mVoiceRecordScoped = VoiceRecordScoped();

  @override
  void initState() {
    super.initState();

    _mVoiceRecordScoped.init((succeed) {
      if (succeed) {}
      _mVoiceRecordScoped.dispose();
    });

    _mVoiceRecordScoped.onStartCallFunc = () {
      print('VoiceRecord start');
      _mTimer = Timer.periodic(Duration(milliseconds: 1000), (t) {
        _mCount++;
        // print('_count is 👉 $_count');
        if (_mCount == widget.countTotal) {
          _mVoiceRecordScoped.stop();
        }
      });

      setState(() {
        _mTextShow = STR(10200); //松开结束
        _mVoiceState = false;
      });

      ///显示录音悬浮布局
      buildOverLayView(context);
    };

    _mVoiceRecordScoped.onStopCallFunc = (String filePath, double timeLength) {
      print('VoiceRecord stop');
      if (_mOverlayEntry != null) {
        _mOverlayEntry?.remove();
        _mOverlayEntry = null;
      }
      if (_mTimer!.isActive) {
        if (_mCount < 1) {
          // CommonToast.showView(context: context, msg: '说话时间太短', icon: Text('!', style: TextStyle(fontSize: 80, color: Colors.white)));
          EasyLoading.showToast('${STR(10201)}!', toastPosition: EasyLoadingToastPosition.center);
          _mIsCancel = true;
        }
        _mTimer?.cancel();
        _mCount = 0;
      }

      setState(() {
        _mTextShow = STR(10198); //按住说话
        _mVoiceState = true;
      });

      if (!_mIsCancel && widget.stopRecord != null) widget.stopRecord!.call(filePath, timeLength);
    };
    _mVoiceRecordScoped.onCancelCallFunc = () {
      print('VoiceRecord cancel');
      _mTimer?.cancel();
      _mCount = 0;
      if (_mOverlayEntry != null) {
        _mOverlayEntry?.remove();
        _mOverlayEntry = null;
      }

      setState(() {
        _mTextShow = STR(10198); //按住说话
        _mVoiceState = true;
      });
      print('取消发送');
      if (widget.cancelRecord != null) widget.cancelRecord!.call();
    };
    _mVoiceRecordScoped.onColumeCallFunc = (double value) {
      setState(() {
        if (value > 0 && value < 0.1) {
          _mVoiceIco = 'images/voice_volume_2.png';
        } else if (value > 0.2 && value < 0.3) {
          _mVoiceIco = 'images/voice_volume_3.png';
        } else if (value > 0.3 && value < 0.4) {
          _mVoiceIco = 'images/voice_volume_4.png';
        } else if (value > 0.4 && value < 0.5) {
          _mVoiceIco = 'images/voice_volume_5.png';
        } else if (value > 0.5 && value < 0.6) {
          _mVoiceIco = 'images/voice_volume_6.png';
        } else if (value > 0.6 && value < 0.7) {
          _mVoiceIco = 'images/voice_volume_7.png';
        } else if (value > 0.7 && value < 1) {
          _mVoiceIco = 'images/voice_volume_7.png';
        } else {
          _mVoiceIco = 'images/voice_volume_1.png';
        }
        if (_mOverlayEntry != null) {
          _mOverlayEntry!.markNeedsBuild();
        }
      });

      // print('振幅大小   ' + value.toString() + '  ' + voiceIco);
    };

    _mVoiceRecordScoped.onErrorCallFunc = (String error) {
      print('VoiceRecord error');
      print(error);
      _mTimer?.cancel();
      _mCount = 0;
      if (_mOverlayEntry != null) {
        _mOverlayEntry?.remove();
        _mOverlayEntry = null;
      }

      setState(() {
        _mTextShow = STR(10198); //按住说话
        _mVoiceState = true;
      });

      //录音发生内部错误
      EasyLoading.showToast('${STR(10202)}！', toastPosition: EasyLoadingToastPosition.center);
    };
  }

  @override
  void dispose() {
    print('_VoiceWidgetRow dispose');
    // recordPlugin?.dispose();
    _mVoiceRecordScoped.onStartCallFunc = null;
    _mVoiceRecordScoped.onStopCallFunc = null;
    _mVoiceRecordScoped.onCancelCallFunc = null;
    _mVoiceRecordScoped.onColumeCallFunc = null;

    _mVoiceRecordScoped.dispose();
    _mTimer?.cancel();
    super.dispose();
  }

  ///显示录音悬浮布局
  buildOverLayView(BuildContext context) {
    if (_mOverlayEntry == null) {
      _mOverlayEntry = new OverlayEntry(builder: (content) {
        return CustomOverlay(
          decoration: BoxDecoration(
            color: Color(4286019962),
            borderRadius: BorderRadius.all(
              Radius.circular(20.sp),
            ),
          ),
          width: 300.sp,
          height: 300.sp,
          icon: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 5.sp),
                height: 230.sp,
                width: 230.sp,
                child: widget.countTotal - _mCount < 11
                    ? Center(
                        child: Padding(
                          padding: EdgeInsets.only(bottom: 15.sp),
                          child: Text(
                            max(widget.countTotal - _mCount, 0).toString(),
                            style: TextStyle(fontSize: 60.sp, color: whiteColor),
                          ),
                        ),
                      )
                    : Image(
                        image: AssetImage(_mVoiceIco, package: 'flutter_plugin_record'),
                        fit: BoxFit.contain,
                      ), // new Image.asset(_mVoiceIco, package: 'flutter_plugin_record'),
              ),
              Center(
                child: Text(
                  _mToastShow,
                  style: TextStyle(
                    // fontStyle: FontStyle.normal,
                    color: whiteColor,
                    fontSize: defFontSize,
                  ),
                ),
              ),
              SizedBox(height: 5.sp),
            ],
          ),
        );
      });
      Overlay.of(context)!.insert(_mOverlayEntry!);
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onLongPressStart: (details) {
        // starty = details.globalPosition.dy;
        _mIsCancel = false;
        _mCount = 0;
        _mVoiceRecordScoped.start();
      },
      onLongPressEnd: (details) {
        if (_mVoiceState) return;
        if (_mIsCancel) {
          _mVoiceRecordScoped.cancel();
        } else {
          _mVoiceRecordScoped.stop();
        }
      },
      onLongPressMoveUpdate: (details) {
        if (_mVoiceState) return;

        // offset = details.globalPosition.dy;
        Offset localOffset = details.localPosition;
        bool cancel = (localOffset.dy < 0 || localOffset.dx < 0);
        print('cancel: $cancel, ${details.localPosition.toString()}');
        if (cancel == _mIsCancel) return;
        setState(() {
          _mIsCancel = cancel;
          // isUp = starty - offset > 100 ? true : false;
          if (_mIsCancel) {
            _mTextShow = STR(10203); //松开手指,取消发送
            _mToastShow = _mTextShow;
          } else {
            _mTextShow = STR(10200); //松开结束
            _mToastShow = STR(10199); //手指上滑,取消发送
          }
        });
      },
      child: Container(
        // height: widget.height ?? 60,
        // color: Colors.blue,
        decoration: widget.decoration ??
            BoxDecoration(
              borderRadius: new BorderRadius.circular(15.sp),
              border: Border.all(width: 1.0, color: Colors.grey.shade200),
            ),
        child: Center(child: Text(_mTextShow, style: defTextStyle)),
      ),
    );
  }
}
