// ignore_for_file: must_be_immutable, file_names

import 'dart:math';

import 'package:extended_text_field/extended_text_field.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/custom/text/src/text_span_builder.dart';
import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/package/handler/Handler.dart';
import 'package:fim_app/package/handler/HandlerCode.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/model/ModelBase.dart';
import 'package:fim_app/model/chat/ChatInputModel.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/material.dart';

import '../ChatATDialog.dart';

///输入框
class TextFieldRow extends StatefulWidget {
  pb.ReceiverType mReceiverType;
  Int64 mReceiverId;
  TextFieldRow({Key? key, required this.mReceiverId, required this.mReceiverType})
      : super(key: key);
  @override
  State<StatefulWidget> createState() => _TextFieldRow();
}

class _TextFieldRow extends State<TextFieldRow> {
  final TextEditingController _textController = TextEditingController();
  final FocusNode _mFocusNode = FocusNode();

  String _currentInputText = '';
  @override
  void initState() {
    super.initState();
    // ChatInputScoped.instance.onInsetTextHandler = onInsetText;
  }

  @override
  void dispose() {
    // ChatInputScoped.instance.onInsetTextHandler = null;
    // FocusScope.of(context).requestFocus(FocusNode());
    super.dispose();
  }

  String insetText(String insetStr, [bool isClear = false]) {
    String text = _textController.text;
    print('insetText: $insetStr text:$text');
    if (isClear) {
      // setState(() {
      _textController.clear();
      // });
      return '';
    }

    if (!_mFocusNode.hasFocus) {
      text = '$text$insetStr';
      // setState(() {
      Future.delayed(const Duration(milliseconds: 10), () {
        _textController.text = text;
      });
      // });
    } else {
      int length = insetStr.length;
      int curIndex = _textController.selection.end;
      if (_textController.text.isEmpty || curIndex < 0) {
        text = '$text$insetStr';
      } else if (curIndex == text.length) {
        text = '$text$insetStr';
      } else if (curIndex == 0) {
        text = '$insetStr$text';
      } else if (curIndex < text.length) {
        String startText = text.substring(0, curIndex);
        String endText = text.substring(curIndex);
        text = '$startText$insetStr$endText';
      }

      int index = min(max(curIndex + length, 0), text.length);
      // setState(() {
      Future.delayed(const Duration(milliseconds: 10), () {
        _textController.value = TextEditingValue(
          text: text,
          selection: TextSelection.fromPosition(
            TextPosition(
              offset: index,
            ),
          ),
          composing: TextRange.empty,
        );
      });
      // });
    }
    return text;
  }

  @override
  Widget build(BuildContext context) {
    ChatInputModel model = ScopedModel.of<ChatInputModel>(context);
    if (!_mFocusNode.hasFocus && model.getState() == ChatEditorState.KEYBOARD && model.isSelfView) {
      Debug.d('input requestFocus');
      FocusScope.of(context).requestFocus(_mFocusNode);
    }
    // else if (ChatInputScoped.isLock && _mFocusNode.hasFocus) {
    //   Debug.w('input unfocus');
    //   _mFocusNode.unfocus();
    // }
    //print('---------------------------------------------------111111111111111111111111111111111');
    // Debug.w('----------------------------------------------');
    // Debug.w('${model.text}::::${_textController.text}');
    // Debug.w('----------------------------------------------');
    if (model.getInsertText().isNotEmpty) {
      model.setText(insetText(model.getInsertText()));
      _currentInputText = model.getInsertText();
    } else if (_textController.text != model.getText()) {
      // Debug.w('--------------------------------------------');
      _textController.clear();
      insetText(model.getText());
    }

    if (model.getText().isEmpty) {
      _currentInputText = '';
    }

    if (!model.isSelfView) {
      return const Center();
    }

    return ExtendedTextField(
      focusNode: _mFocusNode,
      // keyboardType: TextInputType.multiline,
      specialTextSpanBuilder: TextSpanBuilder(
        showAtBackground: true,
        isAt: widget.mReceiverType != pb.ReceiverType.RT_USER,
      ),
      controller: _textController,
      style: defTextStyle,
      textAlignVertical: TextAlignVertical.center,
      decoration: InputDecoration(
        fillColor: whiteColor,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.sp),
          borderSide: BorderSide.none,
        ),
        contentPadding: EdgeInsets.symmetric(horizontal: 15.sp, vertical: 5.sp),
        hintText: '',
        filled: true,
        counterText: '',
      ),
      maxLines: null,
      maxLength: constChatMaxLength,
      onChanged: (value) {
        ChatInputModel model = ScopedModel.of<ChatInputModel>(context);
        String text = model.getText();
        print('_currentInputText: $_currentInputText  text:$text');
        String inputText = '';
        if (value.length > text.length) {
          inputText = value.substring(text.length);
        } else {
          inputText = '';
        }
        if (inputText != _currentInputText) {
          if (inputText == '@') {
            if (widget.mReceiverType != pb.ReceiverType.RT_USER) {
              model.setState(ChatEditorState.UNKNOWN);
              // GlobalHandler().sendMessage(HandlerCode.CHAT_AT_USER_START);
              ChatATDialog.showDialog(
                chatInputModel: model,
                receiverId: widget.mReceiverId,
                receiverType: widget.mReceiverType,
              );
            }
          }
          _currentInputText = inputText;
        }
        model.setText(value, isSend: true);
      },
      onAppPrivateCommand: (String value, Map<String, dynamic> data) {
        Debug.d('onAppPrivateCommand: value:$value, data:$data');
      },
    );
  }
}
