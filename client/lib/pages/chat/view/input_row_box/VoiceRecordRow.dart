// ignore_for_file: file_names

import 'dart:typed_data';

import 'package:fim_app/configs/api.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/dialog/CustomDialog.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/controller/logic_chat.dart';
import 'package:fim_app/package/net/http/http_utils.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fixnum/fixnum.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

import 'VoiceWidgetRow.dart';

//录音按钮
class VoiceRecordRow extends StatefulWidget {
  final pb.ReceiverType mReceiverType;
  final Int64 mReceiverId;
  const VoiceRecordRow({Key? key, required this.mReceiverId, required this.mReceiverType}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _VoiceRecordRow();
}

class _VoiceRecordRow extends State<VoiceRecordRow> {
  void sendMessage(BuildContext context, Uint8List messageContent) {
    if (widget.mReceiverType == pb.ReceiverType.RT_ROOM) {
      LogicChat.PushRoom(
        roomId: widget.mReceiverId,
        context: context,
        messageType: pb.MessageType.MT_VOICE,
        messageContent: messageContent,
        sendTime: LogicChat.getSendTime().toInt(),
      );
    } else {
      LogicChat.SendMessage(
        context: context,
        receiverType: widget.mReceiverType,
        receiverId: widget.mReceiverId,
        messageType: pb.MessageType.MT_VOICE,
        messageContent: messageContent,
        sendTime: LogicChat.getSendTime().toInt(),
      );
    }
  }

  void startRecord() {
    print('开始录制');
  }

  void stopRecord(BuildContext context, String path, double duration) {
    print('结束束录制');
    print('音频文件位置' + path);
    print('音频录制时长' + duration.toString());
    if (path.isEmpty) return;
    //语音发送中
    showLoding('${STR(10196)}。。。', duration: const Duration(seconds: 30));
    // EasyLoading.show(status: '${STR(10196)}。。。', maskType: EasyLoadingMaskType.clear);
    HttpUtils.upMultipartFile(
      requestUrl: AppConfig.apiUrl,
      filePath: path,
      onSucceed: (dynamic response) {
        EasyLoading.dismiss();
        if (response['code'] == 0 && response['data'] != null) {
          pb.Voice voice = pb.Voice(url: response['data']['url'], duration: (duration * 1000).toInt());
          sendMessage(context, voice.writeToBuffer());
        } else {
          EasyLoading.showToast('${STR(10197)}：${response['code']}');
        }
      },
      onError: (String code) {
        EasyLoading.dismiss();
        //上传失败
        EasyLoading.showToast('${STR(10197)}：$code');
      },
    );
  }

  void cancelRecord() {
    print('取消发送');
  }

  @override
  Widget build(BuildContext context) {
    return VoiceWidgetRow(
      startRecord: startRecord,
      stopRecord: (String path, double duration) {
        stopRecord(context, path, duration);
      },
      cancelRecord: cancelRecord,
      // 加入定制化Container的相关属性
      countTotal: 15,
      // padding: EdgeInsets.only(left: 10.sp, top: 5.sp, bottom: 5.sp),

      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(15.sp)),
        color: whiteColor,
      ),
    );
  }
}
