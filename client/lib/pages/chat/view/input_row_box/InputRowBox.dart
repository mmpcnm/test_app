// ignore_for_file: file_names, must_be_immutable, non_constant_identifier_names

import 'dart:typed_data';

import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/custom/text/src/text_span_builder.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/controller/logic_chat.dart';
import 'package:fim_app/model/chat/ChatInputModel.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fixnum/fixnum.dart';
import 'package:flutter/material.dart';
import 'package:one_context/one_context.dart';
import 'package:scoped_model/scoped_model.dart';

import 'TextFieldRow.dart';
import 'VoiceRecordRow.dart';

// const double _inputHeight = 85;

class InputRowBox extends StatefulWidget {
  final pb.ReceiverType mReceiverType;
  final Int64 mReceiverId;
  const InputRowBox({Key? key, required this.mReceiverId, required this.mReceiverType})
      : super(key: key);
  @override
  State<StatefulWidget> createState() => _StateInputRowBox();
}

class _StateInputRowBox extends State<InputRowBox> {
  double iconSize = 56.sp;

  @override
  Widget build(BuildContext context) {
    final ChatInputModel model = ScopedModel.of<ChatInputModel>(context);
    // var editorState = model.getState();
    return SizedBox(
      height: ChatInputModel.inputHeight.sp,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          //语音切换按钮
          Button(
            // margin: EdgeInsets.symmetric(horizontal: 15.sp),
            padding: EdgeInsets.all(10.sp),
            borderRadius: BorderRadius.circular(45.sp),
            child: Icon(
              model.getState() == ChatEditorState.VOICE
                  ? Icons.keyboard_alt_outlined
                  : Icons.keyboard_voice_outlined,
              size: iconSize,
              color: blackColor,
            ),
            onPressed: () {
              if (model.getState() == ChatEditorState.VOICE) {
                model.setState(ChatEditorState.KEYBOARD);
              } else {
                model.setState(ChatEditorState.VOICE);
              }
            },
          ),
          //语音输入与文字输入
          model.getState() == ChatEditorState.VOICE
              ? Expanded(
                  child: Container(
                    constraints: BoxConstraints(maxHeight: ChatInputModel.inputHeight.sp),
                    padding: EdgeInsets.only(top: 10.sp, bottom: 10.sp),
                    child: VoiceRecordRow(
                      mReceiverId: widget.mReceiverId,
                      mReceiverType: widget.mReceiverType,
                    ),
                  ),
                )
              : Expanded(
                  child: Container(
                    constraints: BoxConstraints(maxHeight: ChatInputModel.inputHeight.sp),
                    padding: EdgeInsets.only(top: 10.sp, bottom: 10.sp),
                    child: TextFieldRow(
                      mReceiverId: widget.mReceiverId,
                      mReceiverType: widget.mReceiverType,
                    ),
                  ),
                ),
          //表情
          Button(
            // margin: EdgeInsets.only(left: 15.sp),
            padding: EdgeInsets.all(10.sp),
            borderRadius: BorderRadius.circular(45.sp),
            child: Icon(
              model.getState() == ChatEditorState.EMOJI
                  ? Icons.keyboard_alt_outlined
                  : Icons.insert_emoticon_outlined,
              size: iconSize,
              color: blackColor,
            ),
            onPressed: () {
              if (model.getState() == ChatEditorState.EMOJI) {
                model.setState(ChatEditorState.KEYBOARD);
              } else {
                model.setState(ChatEditorState.EMOJI);
              }
            },
          ),
          Stack(
            alignment: Alignment.centerRight,
            clipBehavior: Clip.antiAlias,
            children: [
              Button(
                // margin: EdgeInsets.symmetric(horizontal: 15.sp),
                padding: EdgeInsets.all(10.sp),
                borderRadius: BorderRadius.circular(45.sp),
                child: Icon(Icons.add_circle_outline, size: iconSize, color: blackColor),
                onPressed: () {
                  if (_isSendButton) return;
                  if (model.getState() == ChatEditorState.ADDITION) {
                    model.setState(ChatEditorState.UNKNOWN);
                  } else {
                    model.setState(ChatEditorState.ADDITION);
                  }
                },
              ),
              AnimatedSize(
                duration: const Duration(milliseconds: 150),
                alignment: Alignment.centerLeft,
                child: SizedBox(
                  width: _isSendButton ? 100.sp : 0,
                  height: iconSize,
                  child: AnimatedOpacity(
                    opacity: _isSendButton ? 1 : 0,
                    duration: const Duration(milliseconds: 150),
                    child: Button(
                      padding: EdgeInsets.zero,
                      color: Colors.green,
                      borderRadius: BorderRadius.circular(10.sp),
                      child: Text(STR(10037), style: defTextStyle),
                      onPressed: () {
                        if (!_isSendButton) return;
                        _sendTextMessage(model.getText());
                      },
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(width: 15.sp),
          // !_isSendButton ? _buildSendBUtton() : _buildAddButton(),
        ],
      ),
    );
  }

  // bool _isSendButton = false;
  bool get _isSendButton {
    ChatInputModel model = ScopedModel.of<ChatInputModel>(context);
    bool isSendButton = false;
    if (model.getState() == ChatEditorState.KEYBOARD ||
        model.getState() == ChatEditorState.EMOJI ||
        model.getState() == ChatEditorState.UNKNOWN) {
      print('isSendButton text:${model.getText()}, insertText:${model.getInsertText()}');

      if (!model.isTextEmpty) {
        isSendButton = true;
      }
    }
    return isSendButton;
  }

  void _sendTextMessage(String value) {
    ChatInputModel model = ScopedModel.of<ChatInputModel>(context);
    // model.inputText = '';
    // model.notify();
    model.clearText();
    Uint8List messageContent = pb.Text(text: value).writeToBuffer();
    if (widget.mReceiverType == pb.ReceiverType.RT_ROOM) {
      LogicChat.PushRoom(
        roomId: widget.mReceiverId,
        context: OneContext().context,
        messageType: pb.MessageType.MT_TEXT,
        messageContent: messageContent,
        sendTime: LogicChat.getSendTime().toInt(),
      );
    } else {
      var list = TextSpanBuilder.decodeText(value);
      List<Int64> toUserIds = [];
      for (var item in list) {
        if (item.type == '@') {
          ATData data = item.data as ATData;
          toUserIds.add(data.userId);
        }
      }
      LogicChat.SendMessage(
        context: OneContext().context,
        receiverType: widget.mReceiverType,
        receiverId: widget.mReceiverId,
        messageType: pb.MessageType.MT_TEXT,
        messageContent: messageContent,
        toUserIds: toUserIds,
        sendTime: LogicChat.getSendTime().toInt(),
      );
    }
  }
}
