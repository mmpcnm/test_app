import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/custom/dialog/CustomDialog.dart';
import 'package:fim_app/custom/verification_box/src/verification_box.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/controller/logic_security.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:one_context/one_context.dart';

///设置6位密码
class SettingWalletPwdPage extends StatefulWidget {
  String title;
  String text;
  SettingWalletPwdPage({required this.title, this.text = ''});
  @override
  State<StatefulWidget> createState() => StateSettingWalletPwdPage();
}

class StateSettingWalletPwdPage extends State<SettingWalletPwdPage> {
  String _password1 = '';
  String _password2 = '';
  String _password3 = '';
  int _type = 0;
  String _hitText = '请输入原始密码';

  String get password {
    if (_type == 1) {
      return _password2;
    } else if (_type == 2) {
      return _password3;
    }
    return _password1;
  }

  @override
  void initState() {
    super.initState();
    if (!UserModel.isWalletPwdSet) {
      _type = 1;
      _password1 = '';
      _hitText = '请输入新密码';
    }
  }

  Widget _buildBox(bool isVisible, {ValueChanged? onSubmitted, ValueChanged? onChanged}) {
    return Visibility(
      visible: isVisible,
      child: SizedBox(
        width: 480.sp,
        height: 80.sp,
        child: VerificationBox(
          count: 6,
          unfocus: false,
          // borderWidth: 80.sp,
          showCursor: true,
          cursorColor: Colors.blue,
          textStyle: defTextStyle,
          onSubmitted: onSubmitted,
          onChanged: onChanged,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: defBackgroundColor,
      appBar: CustomAppbar(
        isBackIcon: true,
        isLineshade: true,
        title: Text(widget.title, style: defTitleTextStyle),
        actions: [
          Button(
            padding: EdgeInsets.symmetric(horizontal: 20.sp, vertical: 10.sp),
            child: Text('确认', style: TextStyle(fontSize: defFontSize, color: Colors.white)),
            isEnable: _password3.length == 6,
            color: Colors.green,
            disableColor: Colors.grey,
            onPressed: () {
              if (_password2 != _password3) {
                showMessageDialog(context, title: STR(10185), content: STR(10229));
                return;
              }
              LogicSecurity.SetWalletPwd(
                context: context,
                password: _password1,
                newPassword: _password3,
                logicCallBack: (code, resp) {
                  if (code == 0) {
                    EasyLoading.showToast('修改成功!');
                  }
                  OneContext().pop();
                },
              );
            },
          ),
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(defPaddingSize),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 32.sp),
            Center(child: Text(widget.text, style: defTextStyle, softWrap: true)),
            SizedBox(height: 32.sp),
            Center(child: Text(_hitText, style: defTextStyle, softWrap: true)),
            SizedBox(height: 10.sp),
            _buildBox(
              0 == _type,
              onChanged: (value) {
                String text = value;
                if (text.isEmpty || text.length < 6) return;
                setState(() {
                  _password1 = value;
                  _type = 1;
                  _hitText = '请输入新密码';
                });
              },
            ),
            _buildBox(
              1 == _type,
              onChanged: (value) {
                String text = value;
                if (text.isEmpty || text.length < 6) return;
                setState(() {
                  _password2 = value;
                  _type = 2;
                  _hitText = '请再次确认新密码';
                });
              },
            ),
            _buildBox(
              2 == _type,
              onChanged: (value) {
                setState(() {
                  _password3 = value;
                });
              },
              onSubmitted: (value) {
                _password3 = value;
                if (_password2 != _password3) {
                  showMessageDialog(context, title: STR(10185), content: STR(10229));
                  return;
                }
                LogicSecurity.SetWalletPwd(
                  context: context,
                  password: _password1,
                  newPassword: _password3,
                  logicCallBack: (code, resp) {
                    if (code == 0) {
                      EasyLoading.showToast('修改成功!');
                    }
                    OneContext().pop();
                  },
                );
              },
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: EdgeInsets.only(left: 25.w),
                child: TextButton(
                  child: Text(
                    '忘记密码？',
                    style: defTextStyle,
                  ),
                  onPressed: () {
                    EasyLoading.showToast('开发中。。。');
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
