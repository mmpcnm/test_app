import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/custom/wdiget/custom_list_table.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/pages/security/ModifyPasswordPage.dart';
import 'package:fim_app/pages/security/SettingWalletPwdPage.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:one_context/one_context.dart';
import 'package:scoped_model/scoped_model.dart';

class SecurityPage extends StatelessWidget {
  const SecurityPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: defBackgroundColor,
      appBar: CustomAppbar(
        isBackIcon: true,
        title: Text('安全中心', style: defTitleTextStyle),
      ),
      body: ListView(
        physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
        children: [
          _buildTable(
            leftText: '手机号码',
            rightText: UserModel.phoneNumber.isEmpty
                ? '未绑定'
                : UserModel.phoneNumber.replaceRange(3, 7, '*'),
            onPressed: () {
              EasyLoading.showToast('开发中。。。');
              // LogicSecurity.BindPhone(phone: phone)
            },
          ),
          _buildTable(
            leftText: '邮箱地址',
            rightText: '未设置',
            isLine: false,
            onPressed: () {
              EasyLoading.showToast('开发中。。。');
            },
          ),
          SizedBox(height: defTabelSplitHeight, child: Container(color: lineColor)),
          _buildTable(
            leftText: '登录密码',
            rightText: '******',
            onPressed: () {
              // EasyLoading.showToast('开发中。。。');
              OneContext().push(pageRoute(ModifyPasswordPage()));
            },
          ),
          ScopedModel<UserModel>(
            model: UserModel.instance,
            child: ScopedModelDescendant<UserModel>(
              builder: (BuildContext context, Widget? child, Model model) {
                return _buildTable(
                  leftText: '支付密码',
                  rightText: UserModel.isWalletPwdSet ? '修改' : '未设置',
                  isLine: false,
                  onPressed: () {
                    if (UserModel.isWalletPwdSet) {
                      OneContext().push(pageRoute(SettingWalletPwdPage(title: '修改支付密码')));
                    } else {
                      OneContext().push(pageRoute(SettingWalletPwdPage(title: '设置支付密码')));
                    }
                  },
                );
              },
            ),
          ),
          SizedBox(height: defTabelSplitHeight, child: Container(color: lineColor)),
          _buildTable(
            leftText: '聊天记录有效期',
            rightText: '永久',
            onPressed: () {
              EasyLoading.showToast('开发中。。。');
            },
          ),
          _buildTable(
            leftText: '自动销毁账号',
            rightText: '六个月',
            onPressed: () {
              EasyLoading.showToast('开发中。。。');
            },
          ),
        ],
      ),
    );
  }

  Widget _buildTable(
      {required String leftText,
      String? rightText,
      Color? rightTextColor,
      bool isLine = true,
      VoidCallback? onPressed}) {
    return CustomListTile(
      title: Text(leftText, style: defTextStyle),
      trailing: rightText != null
          ? Text(
              rightText,
              style: TextStyle(fontSize: defFontSize, color: rightTextColor ?? grayColor),
            )
          : null,
      bottomLine: isLine ? Divider(height: 1, color: lineColor) : null,
      padding: EdgeInsets.symmetric(horizontal: defPaddingSizeH, vertical: 25.sp),
      onPressed: onPressed,
      isArrowIcon: true,
    );
  }
}
