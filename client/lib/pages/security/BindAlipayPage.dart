import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/button/custom_button.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/custom/text/CusotmTextField.dart';
import 'package:fim_app/custom/text/CustomTextFieldFormatter.dart';
import 'package:fim_app/package/net/controller/logic_security.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:one_context/one_context.dart';

class BindAlipayPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => StateBindAlipayPage();
}

class StateBindAlipayPage extends State<BindAlipayPage> {
  String _name = '';
  String _account = '';

  CustomButtonConttroller _buttonConttroller = CustomButtonConttroller();

  bool get _isButtonEnable {
    if (_name.isEmpty || _account.isEmpty) {
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: defBackgroundColor,
      appBar: CustomAppbar(
        isBackIcon: true,
        isLineshade: true,
        title: Text('支付宝绑定', style: defTitleTextStyle),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 32.sp, vertical: defPaddingSize),
        child: ListView(
          physics: const NeverScrollableScrollPhysics(),
          children: [
            CustomTextField(
              hintText: '真实姓名',
              keyboardType: TextInputType.text,
              // maxLength: nickMaxLen,
              inputFormatters: [
                FilteringTextInputFormatter.allow(RegExp(r'[\u4E00-\u9FA5A-Za-z0-9_.]')),
                CustomTextFieldSpaceFormatter(),
              ],
              onChanged: (String value) {
                _name = value;
                _buttonConttroller.isEnable = _isButtonEnable;
              },
              onSaved: (String? value) {},
            ),
            SizedBox(height: defPaddingSize),
            CustomTextField(
              hintText: '支付宝账号',
              inputFormatters: [CustomTextFieldSpaceFormatter()],
              onChanged: (String value) {
                _account = value;
                _buttonConttroller.isEnable = _isButtonEnable;
              },
              onSaved: (String? value) {},
            ),
            SizedBox(height: defPaddingSize),
            CustomButton(
              child: Text('确定', style: defBtnTextStyleL),
              color: Colors.blue,
              disableColor: defBtnColorL,
              isEnable: _isButtonEnable,
              conttroller: _buttonConttroller,
              onPressed: () {
                LogicSecurity.BindAlipay(
                  context: context,
                  realname: _name,
                  alipayAccount: _account,
                  logicCallBack: (code, resp) {
                    if (code == 0) {
                      OneContext().pop();
                      EasyLoading.showSuccess('绑定完成');
                    }
                  },
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
