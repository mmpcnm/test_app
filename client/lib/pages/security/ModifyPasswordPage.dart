import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/button/custom_button.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/custom/text/CusotmTextField.dart';
import 'package:fim_app/custom/text/CustomTextFieldFormatter.dart';
import 'package:fim_app/custom/wdiget/custom_list_table.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/controller/logic_login.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:one_context/one_context.dart';

///修改登录密码
class ModifyPasswordPage extends StatelessWidget {
  String _localPassword = '';
  String _newPassword_1 = '';
  String _newPassword_2 = '';
  CustomButtonConttroller _buttonConttroller = CustomButtonConttroller();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  bool get _isButtonEnable {
    //return (_localPassword.isNotEmpty && _newPassword_1.isNotEmpty && _newPassword_2.isNotEmpty);
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: (TapDownDetails details) {
        SystemChannels.textInput.invokeMethod('TextInput.hide');
      },
      child: Scaffold(
        backgroundColor: defBackgroundColor,
        resizeToAvoidBottomInset: false,
        appBar: CustomAppbar(
          isBackIcon: true,
          isLineshade: true,
          title: Text('设置密码', style: defTitleTextStyle),
          actions: [
            CustomButton(
              child: Text('修改', style: defBtnTextStyleL),
              color: Colors.green,
              padding: EdgeInsets.symmetric(horizontal: 20.sp, vertical: 10.sp),
              disableColor: defBtnColorL,
              isEnable: _isButtonEnable,
              conttroller: _buttonConttroller,
              onPressed: () {
                var _state = _formKey.currentState;
                if (_state!.validate()) {
                  _state.save();
                  LogicLogin.ModifyPwd(
                    context: context,
                    pwd: _localPassword,
                    newPwd: _newPassword_2,
                    logicCallBack: (code, resp) {
                      if (code == 0) {
                        OneContext().pop();
                      }
                    },
                  );
                }
              },
            ),
          ],
        ),
        body: Form(
          key: _formKey,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 32.sp),
            child: Column(
              children: [
                CustomListTable.buildTable(
                  padding: EdgeInsets.symmetric(vertical: defPaddingSize),
                  trailing: Center(),
                  leading: Text('账号:  ', style: defTextStyle),
                  leftWidget: Text(UserModel.accountId,
                      style: TextStyle(fontSize: defFontSize, color: grayColor)),
                ),
                CustomTextField(
                  hintText: '请填写原密码',
                  icon: Text('原密码:', style: defTextStyle),
                  maxLength: passwordMaxLength,
                  isEyeIcon: true,
                  keyboardType: TextInputType.visiblePassword,
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(RegExp(r'[0-9a-zA-Z_]')),
                    CustomTextFieldSpaceFormatter(),
                  ],
                  onChanged: (String value) {
                    _localPassword = value;
                    _buttonConttroller.isEnable = _isButtonEnable;
                  },
                  onSaved: (String? value) {
                    _localPassword = value.toString();
                  },
                ),
                SizedBox(height: defPaddingSize),
                CustomTextField(
                  hintText: '请填写新密码',
                  icon: Text('新密码:', style: defTextStyle),
                  maxLength: passwordMaxLength,
                  isEyeIcon: true,
                  keyboardType: TextInputType.visiblePassword,
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(RegExp(r'[0-9a-zA-Z_]')),
                    CustomTextFieldSpaceFormatter(),
                  ],
                  onChanged: (String value) {
                    _newPassword_1 = value;
                    _buttonConttroller.isEnable = _isButtonEnable;
                  },
                  onSaved: (String? value) {
                    _newPassword_1 = value.toString();
                  },
                  validator: (String? value) {
                    if (_newPassword_1.length < passwordMinLength) {
                      return STR(10027, [passwordMinLength]);
                    }
                  },
                ),
                SizedBox(height: defPaddingSize),
                CustomTextField(
                  hintText: '再次填写确认',
                  icon: Text('${STR(10023)}:', style: defTextStyle),
                  maxLength: passwordMaxLength,
                  isEyeIcon: true,
                  keyboardType: TextInputType.visiblePassword,
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(RegExp(r'[0-9a-zA-Z_]')),
                    CustomTextFieldSpaceFormatter(),
                  ],
                  onChanged: (String value) {
                    _newPassword_2 = value;
                    _buttonConttroller.isEnable = _isButtonEnable;
                  },
                  onSaved: (String? value) {
                    _newPassword_2 = value.toString();
                  },
                  validator: (String? value) {
                    if (_newPassword_2.length < passwordMinLength) {
                      return STR(10027, [passwordMinLength]);
                    } else if (_newPassword_1 != _newPassword_2) {
                      return STR(10229);
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
