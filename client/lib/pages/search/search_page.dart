import 'dart:async';

import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/custom/wdiget/custom_list_table.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/controller/logic_friend.dart';
import 'package:fim_app/package/pb/logic.ext.pb.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/custom/wdiget/user_icon_widget.dart';
import 'package:fim_app/pages/friend/user_profile_page.dart';
import 'package:flutter/material.dart';
import 'package:one_context/one_context.dart';

class SearchPage extends StatefulWidget {
  static showSearchPage() {
    // showSearch(context: OneContext().context!, delegate: CustomSearchDelegate());
    OneContext().push(fadeRoute(SearchPage()));
  }

  @override
  State<StatefulWidget> createState() {
    return _SearchPage();
  }
}

class _SearchPage extends State<SearchPage> {
  List<User> _users = [];
  late Timer _delayTimer;
  String _findUserKey = '';
  String _findKey = '';

  @override
  void initState() {
    super.initState();
    _delayTimer = Timer.periodic(const Duration(milliseconds: 500), (timer) async {
      if (_findKey != _findUserKey) {
        _findKey = _findUserKey;
        if (_findKey.isEmpty) {
          _users.clear();
          setState(() {});
          return;
        }
        LogicFriend.SearchUser(
          key: _findKey,
          logicCallBack: (code, resp) {
            _users.clear();
            if (resp != null) {
              _users.addAll(resp.users);
            }
            setState(() {});
          },
        );
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _delayTimer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        backgroundColor: defBackgroundColor,
        appBar: CustomAppbar(
          isLineshade: false,
          title: SizedBox(
            height: 64.sp,
            child: TextField(
              decoration: InputDecoration(
                prefixIcon: Icon(
                  Icons.search,
                  color: Colors.black,
                  size: ScreenUtil.toWidth(32),
                ),
                hintText: STR(10030),
                hintStyle: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.grey,
                  fontSize: defFontSize,
                ), //'输入对方账号'
                border: UnderlineInputBorder(borderSide: BorderSide.none, borderRadius: BorderRadius.all(Radius.circular(15.sp))),
                filled: true,
                fillColor: Colors.white,
                isDense: true,
                contentPadding: EdgeInsets.symmetric(horizontal: 10.sp, vertical: 0),
              ),
              textAlignVertical: TextAlignVertical.center,
              maxLines: 1,
              onChanged: (value) {
                _findUserKey = value;
              },
            ),
          ),
          actions: [
            Button(
              margin: EdgeInsets.symmetric(horizontal: SUT_W(20)),
              padding: EdgeInsets.zero,
              child: Text(
                STR(10032),
                style: defTextStyle, // TextStyle(fontSize: defFontSize, color: Colors.black54, fontWeight: FontWeight.bold),
              ),
              onPressed: () => OneContext().pop(),
            ),
          ],
        ),
        body: Container(
          // padding: EdgeInsets.only(top: ScreenUtil.statusBarHeight),
          child: ListView.separated(
            itemBuilder: (context, index) {
              return _SearchUserItem(
                user: _users[index],
                onTap: (User user) {
                  FocusScope.of(context).requestFocus(FocusNode());
                  OneContext().push(fadeRoute(UserProfilePage(user)));
                },
              );
            },
            itemCount: _users.length,
            separatorBuilder: (BuildContext context, int index) {
              return Divider(height: 1, color: lineColor);
            },
          ),
        ),
      ),
    );
  }
}

///---------------------------------------------------------------------------------------------------------------------------
// class SearchPage {
//   static showSearchPage() {
//     showSearch(context: OneContext().context!, delegate: CustomSearchDelegate());
//   }
// }

// class CustomSearchDelegate extends SearchDelegate<String> {
//   List<User> _users = [];
//   CustomSearchDelegate({
//     TextInputType? keyboardType,
//     TextStyle? searchFieldStyle,
//   }) : super(
//           keyboardType: keyboardType,
//           searchFieldStyle: searchFieldStyle,
//         );

//   Future _reqFriend(BuildContext context, String textKey) async {
//     var resp = await LogicFriend.SearchUser(
//       context,
//       key: textKey,
//     );
//     _users.clear();
//     if (resp != null) {
//       _users.addAll(resp.users);
//     }
//   }

//   @override
//   List<Widget> buildActions(BuildContext context) {
//     return [
//       buttonEx(
//         padding: EdgeInsets.symmetric(horizontal: 20.sp, vertical: 10.sp),
//         child: Text(
//           '取消',
//           style: TextStyle(
//             fontSize: defFontSize,
//             color: Colors.black54,
//             fontWeight: FontWeight.bold,
//           ),
//         ),
//         onPressed: () {
//           close(context, '');
//         },
//       ),
//     ];
//   }

//   ///返回按钮
//   @override
//   Widget buildLeading(BuildContext context) {
//     return IconButton(
//       icon: Icon(
//         Icons.arrow_back_ios,
//         color: Colors.black,
//         size: defIconSize,
//       ),
//       onPressed: () {
//         close(context, '');
//       },
//     );
//   }

//   ///搜索结果控件，当用户点击软键盘上的“Search”时回调此方法
//   @override
//   Widget buildResults(BuildContext context) {
//     print(super.searchFieldLabel);
//     if (super.query.isNotEmpty)
//       return FutureBuilder(
//         future: _reqFriend(context, super.query),
//         builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
//           switch (snapshot.connectionState) {
//             case ConnectionState.none:
//               return Container();
//             case ConnectionState.waiting:
//             case ConnectionState.active:
//               return Container(
//                 child: Center(
//                   child: Text('加载中...', style: defTextStyle),
//                 ),
//               );
//             case ConnectionState.done:
//               return ListView.builder(
//                 physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
//                 itemBuilder: (context, index) {
//                   return _SearchUserItem(
//                     user: _users[index],
//                     onTap: (User user) {
//                       close(context, '');
//                       OneContext().push(fadeRoute(UserProfilePage(user)));
//                     },
//                   );
//                 },
//                 itemCount: _users.length,
//               );
//           }
//         },
//       );
//     else
//       return Center();
//   }

//   ///输入框发生变化时
//   @override
//   Widget buildSuggestions(BuildContext context) {
//     return buildResults(context);
//   }
// }

class _SearchUserItem extends StatelessWidget {
  User user;
  void Function(User user) onTap;
  _SearchUserItem({required this.user, required this.onTap});
  @override
  Widget build(BuildContext context) {
    return CustomListTile(
      color: Colors.white,
      leading: UserIconWidget(user.avatarUrl, circular: 5),
      title: Text(user.nickname, style: defTextStyle),
      onPressed: () => onTap(user),
    );

    // Column(
    //   children: [
    //     TextButton(
    //         onPressed: () {
    //           // FocusScope.of(context).requestFocus(FocusNode());
    //           // OneContext().push(fadeRoute(UserProfilePage(user)));
    //           onTap(user);
    //         },
    //         style: ButtonStyle(padding: MaterialStateProperty.all(EdgeInsets.zero)),
    //         child: ListTile(
    //           title: Text(user.nickname, style: defTextStyle),
    //           leading: UserIconWidget(user.avatarUrl, circular: 5),
    //         )),
    //     Divider(
    //       height: 1,
    //       color: Colors.black26,
    //     )
    //   ],
    // );
  }
}
