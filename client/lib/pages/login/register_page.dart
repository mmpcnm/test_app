import 'package:connectivity/connectivity.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/button/custom_button.dart';
import 'package:fim_app/custom/text/CusotmTextField.dart';
import 'package:fim_app/custom/text/CustomTextFieldFormatter.dart';
import 'package:fim_app/custom/app_bar/CustiomSliverPersistentHeaderDelegate.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/controller/logic_login.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/pages/loading/init_page.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/model/ConnectivityModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:one_context/one_context.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _RegisterPage();
  }
}

class _RegisterPage extends State<RegisterPage> {
  String _account = '';
  String _password = '';
  String _password2 = '';
  String _nickName = '';

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  CustomButtonConttroller _buttonConttroller = CustomButtonConttroller();

  bool _isFieldEmpty() {
    return _account.isEmpty || _password.isEmpty || _password2.isEmpty || _nickName.isEmpty;
  }

  //注册账号
  void _onRegisterClick() async {
    var state = await ConnectivityModel().checkConnectivity();
    if (state == ConnectivityResult.none) {
      EasyLoading.showError('当前无网络，请检查网络链接状况');
      return;
    }
    EasyLoading.show(maskType: EasyLoadingMaskType.clear);
    LogicLogin.Register(
      context,
      account: _account,
      password: _password,
      nickName: _nickName,
      logicCallBack: (code, presp) {
        EasyLoading.dismiss();
        if (code == 0) {
          _signIn();
        }
      },
    );
  }

  //登录账号
  void _signIn() {
    Future.delayed(Duration(milliseconds: 10), () {
      EasyLoading.show(status: '${STR(10230)}。。。', maskType: EasyLoadingMaskType.clear);
      LogicLogin.SignIn(
        context,
        account: _account,
        password: _password,
        logicCallBack: (code, presp) {
          EasyLoading.dismiss();
          if (code == 0) {
            OneContext().pushReplacement(fadeRoute(InitPage()));
          } else {
            OneContext().pop();
          }
        },
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[100],
      body: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTapDown: (TapDownDetails details) {
          // FocusScope.of(context).requestFocus(FocusNode());
          SystemChannels.textInput.invokeMethod('TextInput.hide');
        },
        child: Form(
          key: _formKey,
          child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(ResImgs('login_bg')),
                alignment: Alignment.topCenter,
                fit: BoxFit.fitWidth,
              ),
            ),
            alignment: Alignment.topCenter,
            padding: EdgeInsets.only(
              left: defPaddingSize,
              right: defPaddingSize,
              top: ScreenUtil.statusBarHeight,
            ),
            child: CustomScrollView(
              slivers: [
                SliverPersistentHeader(
                  pinned: false,
                  floating: true,
                  delegate: CustomSliverPersistentHeaderDelegate(
                    mMinExtent: 64.sp,
                    mMaxExtent: 225.sp,
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          left: 0,
                          top: 5.sp,
                          child: Button(
                            child:
                                Icon(Icons.arrow_back_ios, color: Colors.black, size: defIconSize),
                            padding: EdgeInsets.all(0),
                            onPressed: () {
                              OneContext().pop();
                            },
                          ),
                        ),
                        Center(
                          //注册账号
                          child: Text(STR(10020),
                              style: TextStyle(
                                  fontSize: 48.sp,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold)),
                        )
                      ],
                    ),
                  ),
                ),
                SliverList(
                  delegate: SliverChildListDelegate(
                    <Widget>[
                      // SizedBox(
                      //   height: 180.sp,
                      //   child: Center(
                      //     //注册账号
                      //     child: Text(STR(10020), style: TextStyle(fontSize: 48.sp, color: Colors.black, fontWeight: FontWeight.bold)),
                      //   ),
                      // ),
                      CustomTextField(
                        maxLength: accountMaxLength,
                        hintText: STR(10022), //'请输入用户名',
                        inputFormatters: [
                          FilteringTextInputFormatter.allow(RegExp(r'[0-9a-zA-Z_]')),
                          CustomTextFieldSpaceFormatter(),
                        ],
                        onChanged: (value) {
                          _account = value;
                          _buttonConttroller.isEnable = !_isFieldEmpty();
                        },
                        onSaved: (value) {
                          _account = value.toString();
                        },
                        validator: (value) {
                          if (value.toString().length < accountMinLength) {
                            return STR(10026, [accountMinLength]);
                          }
                        },
                      ),
                      SizedBox(height: defPaddingSize),
                      CustomTextField(
                        maxLength: passwordMaxLength,
                        isEyeIcon: true,
                        keyboardType: TextInputType.visiblePassword,
                        inputFormatters: [
                          FilteringTextInputFormatter.allow(RegExp(r'[0-9a-zA-Z_]')),
                          CustomTextFieldSpaceFormatter(),
                        ],
                        hintText: STR(10018), //'请输入密码',
                        onChanged: (value) {
                          _password = value;
                          _buttonConttroller.isEnable = !_isFieldEmpty();
                        },
                        onSaved: (value) {
                          _password = value.toString();
                        },
                        validator: (value) {
                          if (value.toString().length < passwordMinLength) {
                            return STR(10027, [passwordMinLength]);
                          }
                        },
                      ),
                      SizedBox(height: defPaddingSize),
                      CustomTextField(
                        maxLength: passwordMaxLength,
                        isEyeIcon: true,
                        keyboardType: TextInputType.visiblePassword,
                        inputFormatters: [
                          FilteringTextInputFormatter.allow(RegExp(r'[0-9a-zA-Z_]')),
                          CustomTextFieldSpaceFormatter(),
                        ],
                        hintText: STR(10023), //'确认密码',
                        onChanged: (value) {
                          _password2 = value;
                          _buttonConttroller.isEnable = !_isFieldEmpty();
                        },
                        onSaved: (value) {
                          _password2 = value.toString();
                        },
                        validator: (value) {
                          if (_password.compareTo(value.toString()) != 0) {
                            return STR(10229); //'两次密码不匹配';
                          }
                        },
                      ),
                      SizedBox(height: defPaddingSize),
                      CustomTextField(
                        maxLength: nickMaxLen,
                        hintText: '请输入昵称', //'请输入用户名',
                        inputFormatters: [
                          FilteringTextInputFormatter.allow(RegExp(r'[\u4E00-\u9FA5A-Za-z0-9_.]')),
                          CustomTextFieldSpaceFormatter(),
                        ],
                        onChanged: (value) {
                          _nickName = value;
                          _buttonConttroller.isEnable = !_isFieldEmpty();
                        },
                        onSaved: (value) {
                          _nickName = value.toString();
                        },
                        validator: (value) {
                          if (value.toString().length < nickMinLen) {
                            return '昵称长度最少3位';
                          }
                        },
                      ),
                      SizedBox(height: 35.sp),
                      CustomButton(
                        //注册
                        child: Text(STR(10024), style: defBtnTextStyleL),
                        disableColor: grayColor,
                        color: defBtnColor,
                        isEnable: !_isFieldEmpty(),
                        conttroller: _buttonConttroller,
                        onPressed: () {
                          var _state = _formKey.currentState;
                          if (_state!.validate()) {
                            _state.save();
                            _onRegisterClick();
                          }
                        },
                      ),
                      SizedBox(height: defPaddingSize),
                      Center(
                        child: Button(
                          padding: EdgeInsets.zero,
                          borderCircular: 0,
                          child: Text(STR(10025), style: defTextStyle),
                          onPressed: () => OneContext().pop(),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),

            // ListView(
            //   // physics: const NeverScrollableScrollPhysics(),
            //   children: <Widget>[
            //     // SizedBox(height: ScreenUtil.statusBarHeight),
            //     Row(
            //       children: [
            //         Button(
            //           child: Icon(Icons.arrow_back_ios, color: Colors.black, size: defIconSize),
            //           padding: EdgeInsets.all(0),
            //           onPressed: () {
            //             OneContext().pop();
            //           },
            //         )
            //       ],
            //     ),
            //     SizedBox(
            //       height: 180.sp,
            //       child: Center(
            //         //注册账号
            //         child: Text(STR(10020), style: TextStyle(fontSize: 48.sp, color: Colors.black, fontWeight: FontWeight.bold)),
            //       ),
            //     ),
            //     CustomTextField(
            //       maxLength: accountMaxLength,
            //       hintText: STR(10022), //'请输入用户名',
            //       inputFormatters: [
            //         FilteringTextInputFormatter.allow(RegExp(r'[0-9a-zA-Z_]')),
            //         CustomTextFieldSpaceFormatter(),
            //       ],
            //       onChanged: (value) {
            //         _account = value;
            //         _buttonConttroller.isEnable = !_isFieldEmpty();
            //       },
            //       onSaved: (value) {
            //         _account = value.toString();
            //       },
            //       validator: (value) {
            //         if (value.toString().length < accountMinLength) {
            //           return STR(10026, [accountMinLength]);
            //         }
            //       },
            //     ),
            //     SizedBox(height: paddingHorizontal),
            //     CustomTextField(
            //       maxLength: passwordMaxLength,
            //       isEyeIcon: true,
            //       keyboardType: TextInputType.visiblePassword,
            //       inputFormatters: [
            //         FilteringTextInputFormatter.allow(RegExp(r'[0-9a-zA-Z_]')),
            //         CustomTextFieldSpaceFormatter(),
            //       ],
            //       hintText: STR(10018), //'请输入密码',
            //       onChanged: (value) {
            //         _password = value;
            //         _buttonConttroller.isEnable = !_isFieldEmpty();
            //       },
            //       onSaved: (value) {
            //         _password = value.toString();
            //       },
            //       validator: (value) {
            //         if (value.toString().length < passwordMinLength) {
            //           return STR(10027, [passwordMinLength]);
            //         }
            //       },
            //     ),
            //     SizedBox(height: paddingHorizontal),
            //     CustomTextField(
            //       maxLength: passwordMaxLength,
            //       isEyeIcon: true,
            //       keyboardType: TextInputType.visiblePassword,
            //       inputFormatters: [
            //         FilteringTextInputFormatter.allow(RegExp(r'[0-9a-zA-Z_]')),
            //         CustomTextFieldSpaceFormatter(),
            //       ],
            //       hintText: STR(10023), //'确认密码',
            //       onChanged: (value) {
            //         _password2 = value;
            //         _buttonConttroller.isEnable = !_isFieldEmpty();
            //       },
            //       onSaved: (value) {
            //         _password2 = value.toString();
            //       },
            //       validator: (value) {
            //         if (_password.compareTo(value.toString()) != 0) {
            //           return STR(10229); //'两次密码不匹配';
            //         }
            //       },
            //     ),
            //     SizedBox(height: paddingHorizontal),
            //     CustomTextField(
            //       maxLength: nickMaxLen,
            //       hintText: '请输入昵称', //'请输入用户名',
            //       inputFormatters: [
            //         FilteringTextInputFormatter.allow(RegExp(r'[\u4E00-\u9FA5A-Za-z0-9_.]')),
            //         CustomTextFieldSpaceFormatter(),
            //       ],
            //       onChanged: (value) {
            //         _nickName = value;
            //         _buttonConttroller.isEnable = !_isFieldEmpty();
            //       },
            //       onSaved: (value) {
            //         _nickName = value.toString();
            //       },
            //       validator: (value) {
            //         if (value.toString().length < nickMinLen) {
            //           return '昵称长度最少3位';
            //         }
            //       },
            //     ),
            //     SizedBox(height: 35.sp),
            //     CustomButton(
            //       //注册
            //       child: Text(STR(10024), style: defBtnTextStyleL),
            //       disableColor: grayColor,
            //       color: defBtnColor,
            //       isEnable: !_isFieldEmpty(),
            //       conttroller: _buttonConttroller,
            //       onPressed: () {
            //         var _state = _formKey.currentState;
            //         if (_state!.validate()) {
            //           _state.save();
            //           _onRegisterClick();
            //         }
            //       },
            //     ),
            //     SizedBox(height: paddingHorizontal),
            //     Center(
            //       child: Button(
            //         padding: EdgeInsets.zero,
            //         borderCircular: 0,
            //         child: Text(STR(10025), style: defTextStyle),
            //         onPressed: () => OneContext().pop(),
            //       ),
            //     ),
            //   ],
            // ),
          ),
        ),
      ),
    );
  }
}
