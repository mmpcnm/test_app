import 'package:connectivity/connectivity.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/configs/api.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/button/custom_button.dart';
import 'package:fim_app/custom/text/CusotmTextField.dart';
import 'package:fim_app/custom/text/CustomTextFieldFormatter.dart';
import 'package:fim_app/custom/app_bar/CustiomSliverPersistentHeaderDelegate.dart';
import 'package:fim_app/custom/wdiget/custom_list_table.dart';
import 'package:fim_app/package/handler/Handler.dart';
import 'package:fim_app/package/handler/HandlerCode.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/controller/logic_login.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/pages/login/register_page.dart';
import 'package:fim_app/model/ConnectivityModel.dart';
import 'package:fim_app/model/user/user_model.dart' as model;
import 'package:fim_app/pages/loading/init_page.dart';
import 'package:fim_app/pages/test_setting_ip.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:one_context/one_context.dart';
import 'package:scoped_model/scoped_model.dart';

import 'package:flutter/material.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _SignInPage();
  }
}

class _SignInPage extends State<SignInPage> {
  String _account = model.UserModel.accountId;
  String _password = '';
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  CustomButtonConttroller _buttonConttroller = CustomButtonConttroller();

  @override
  void initState() {
    super.initState();
    GlobalHandler.instace.addListenable(HandlerCode.DEBUG_SET_IP, onUpdateIp);
  }

  @override
  void dispose() {
    GlobalHandler.instace.removeListenable(HandlerCode.DEBUG_SET_IP, onUpdateIp);
    super.dispose();
  }

  void onUpdateIp(HandlerCode key, dynamic data) {
    setState(() {});
  }

  bool _isFieldEmpty() {
    return _account.isEmpty || _password.isEmpty;
  }

  ///登录账号
  void _onAuthClick() async {
    FocusScope.of(context).requestFocus(FocusNode());
    var state = await ConnectivityModel().checkConnectivity();
    if (state == ConnectivityResult.none) {
      EasyLoading.showError('当前无网络，请检查网络链接状况');
      return;
    }
    EasyLoading.show(status: '${STR(10230)}。。。', maskType: EasyLoadingMaskType.clear);
    LogicLogin.SignIn(
      context,
      account: _account,
      password: _password,
      logicCallBack: (code, resp) {
        EasyLoading.dismiss();
        Future.delayed(const Duration(milliseconds: 100), () {
          if (code == 0) {
            CustomRoute().pushFade(InitPage());
          }
        });
      },
    );
  }

  ///注册按钮
  void _onRegisterClick() {
    // routeFadePush(RegisterPage());
    OneContext().push(pageRoute(const RegisterPage()));
  }

  static Iterable<Element> _reverseChildrenOf(Element element, bool skipOffstage) {
    assert(element != null);
    final List<Element> children = <Element>[];
    if (skipOffstage) {
      element.debugVisitOnstageChildren(children.add);
    } else {
      element.visitChildren(children.add);
    }
    return children;
  }

  ///找回密码
  void _onRecoverClick() {
    // routeFadePush(WebView());
    // EasyLoading.showToast('开发中...');
    var over = Overlay.of(context, rootOverlay: true);
    var list = _reverseChildrenOf(over?.context as Element, false).toList();
    for (var i = 0; i < list.length; i++) {
      var item = list[i];

      print('Element ${item.widget}');
    }
  }

  ///布局
  @override
  Widget build(BuildContext context) {
    // print('${OneContext().key.toString()}');
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: ScopedModel<STRManager>(
        model: STRManager.instance,
        child: ScopedModelDescendant<STRManager>(
          builder: (context, child, model) {
            return _buildWidget(context);
          },
        ),
      ),
    );
  }

  Widget _buildWidget(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[100],
      body: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTapDown: (TapDownDetails details) {
          // FocusScope.of(context).requestFocus(FocusNode());
          SystemChannels.textInput.invokeMethod('TextInput.hide');
        },
        child: Form(
          key: _formKey,
          child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(ResImgs('login_bg')),
                scale: 1.0.sp,
                alignment: Alignment.topCenter,
                fit: BoxFit.fitWidth,
              ),
            ),
            alignment: Alignment.topCenter,
            padding: EdgeInsets.only(
              left: defPaddingSize,
              right: defPaddingSize,
              top: ScreenUtil.statusBarHeight,
            ),
            child: CustomScrollView(
              slivers: [
                SliverPersistentHeader(
                  pinned: false,
                  floating: true,
                  delegate: CustomSliverPersistentHeaderDelegate(
                    mMinExtent: 64.sp,
                    mMaxExtent: 225.sp,
                    child: Center(
                      child: Text(STR(10016),
                          style: TextStyle(
                              fontSize: 48.sp, color: Colors.black, fontWeight: FontWeight.bold)),
                    ),
                  ),
                ),
                SliverList(
                  delegate: SliverChildListDelegate(
                    <Widget>[
                      CustomTextField(
                        maxLength: accountMaxLength,
                        hintText: STR(10022), //'请输入用户名'
                        initialValue: model.UserModel.accountId,
                        inputFormatters: [
                          FilteringTextInputFormatter.allow(RegExp(r'[0-9a-zA-Z_]')),
                          CustomTextFieldSpaceFormatter(),
                        ],
                        onChanged: (value) {
                          _account = value;
                          _buttonConttroller.isEnable = !_isFieldEmpty();
                        },
                        onSaved: (value) {
                          _account = value.toString();
                        },
                        validator: (value) {
                          if (value.toString().length < accountMinLength) {
                            return STR(10026, [accountMinLength]); //'账号至少6位';
                          }
                        },
                      ),
                      SizedBox(height: 10.sp),
                      CustomTextField(
                        maxLength: passwordMaxLength,
                        hintText: STR(10018), // '请输入密码',
                        isEyeIcon: true,
                        keyboardType: TextInputType.visiblePassword,
                        inputFormatters: [
                          FilteringTextInputFormatter.allow(RegExp(r'[0-9a-zA-Z_]')),
                          CustomTextFieldSpaceFormatter(),
                        ],
                        onChanged: (value) {
                          _password = value;
                          _buttonConttroller.isEnable = !_isFieldEmpty();
                        },
                        onSaved: (value) {
                          _password = value.toString();
                        },
                        validator: (value) {
                          if (value.toString().length < passwordMinLength) {
                            return STR(10027, [passwordMinLength]); // '密码至少6位';
                          }
                        },
                      ),
                      CustomButton(
                        margin: EdgeInsets.only(top: 35.sp, bottom: 25.sp),
                        child: Text(STR(10019), style: defBtnTextStyleL),
                        disableColor: grayColor,
                        borderCircular: 35.sp,
                        isEnable: !_isFieldEmpty(),
                        color: defBtnColor,
                        conttroller: _buttonConttroller,
                        onPressed: () {
                          var _state = _formKey.currentState;
                          if (_state!.validate()) {
                            _state.save();
                            _onAuthClick();
                          }
                        },
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Button(
                            margin: EdgeInsets.only(right: defPaddingSize),
                            padding: EdgeInsets.zero,
                            borderCircular: 0,
                            child: Text(STR(10020), style: defTextStyle),
                            onPressed: _onRegisterClick,
                          ),
                          // Image(image: AssetImage(ResImgs('abc_ab_share_pack_mtrl_alpha.9.png')),
                          SizedBox(
                              width: 1.sp,
                              height: defFontSize,
                              child: VerticalDivider(width: 1, color: lineColor)),
                          Button(
                            margin: EdgeInsets.only(left: defPaddingSize),
                            padding: EdgeInsets.zero,
                            borderCircular: 0,
                            child: Text(STR(10021), style: defTextStyle),
                            onPressed: _onRecoverClick,
                          ),
                        ],
                      ),
                      if (AppConfig.isDebug) ..._buildDebugWdiget(),
                    ],
                  ),
                ),
              ],
            ),

            // ListView(
            //   physics: const NeverScrollableScrollPhysics(),
            //   // mainAxisAlignment: MainAxisAlignment.center,
            //   children: children,
            // ),
          ),
        ),
      ),
    );
  }

  List<Widget> _buildDebugWdiget() {
    return <Widget>[
      SizedBox(height: 15.h),
      CustomListTile(
        padding: EdgeInsets.symmetric(horizontal: 15.sp, vertical: 5.sp),
        title: Text('Connect IP', style: defTextStyle),
        subtitle: Text('${AppConfig.connectIp}:${AppConfig.connectPort}', style: defTextStyle),
      ),
      Divider(height: 1, color: Colors.black),
      CustomListTile(
        padding: EdgeInsets.symmetric(horizontal: 15.sp, vertical: 5.sp),
        title: Text('Logic IP', style: defTextStyle),
        subtitle: Text('${AppConfig.logicIp}:${AppConfig.logicPort}', style: defTextStyle),
      ),
      Divider(height: 1, color: Colors.black),
      CustomListTile(
        padding: EdgeInsets.symmetric(horizontal: 15.sp, vertical: 5.sp),
        title: Text('Screen Size', style: defTextStyle),
        subtitle:
            Text('${ScreenUtil.screenWidth} x ${ScreenUtil.screenHeight}', style: defTextStyle),
      ),
      Divider(height: 1, color: Colors.black),
      CustomListTile(
        padding: EdgeInsets.symmetric(horizontal: 15.sp, vertical: 5.sp),
        title: Text('ScreenPx Size', style: defTextStyle),
        subtitle:
            Text('${ScreenUtil.screenWidthPx} x ${ScreenUtil.screenHeightPx}', style: defTextStyle),
      ),
      Divider(height: 1, color: Colors.black),
      CustomListTile(
        padding: EdgeInsets.symmetric(horizontal: 15.sp, vertical: 5.sp),
        title: Text('View Size', style: defTextStyle),
        subtitle: Text('${ScreenUtil.width} x ${ScreenUtil.height.toStringAsFixed(2)}',
            style: defTextStyle),
      ),
      SizedBox(height: 15.h),
      Button(
        onPressed: () {
          OneContext().push(pageRoute(TestSettingIp()));
        },
        color: Colors.blue,
        child: Text('测试 IP 设置', style: defBtnTextStyle),
      ),
    ];
  }
}
