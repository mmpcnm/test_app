import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:flutter/material.dart';
import 'package:one_context/one_context.dart';

class EditorFiledPage extends StatefulWidget {
  Widget? title;
  String? hitText;
  int? inputMaxLength;
  void Function(String)? onPassed;
  EditorFiledPage({this.title, this.hitText, this.inputMaxLength, this.onPassed});

  @override
  State<StatefulWidget> createState() {
    return _EditorFiledPage();
  }
}

class _EditorFiledPage extends State<EditorFiledPage> {
  FocusNode _editorNode = FocusNode();
  TextEditingController _controller = TextEditingController();

  void onPassed() {
    print('---------------------------------------------------');
    if (_controller.text.isEmpty) return;
    print('---------------------------------------------------');
    if (widget.onPassed != null) {
      print('---------------------------------------------------');
      widget.onPassed!.call(_controller.text);
      OneContext().pop();
    }
  }

  @override
  void dispose() {
    // FocusScope.of(OneContext().context!).requestFocus(FocusNode());
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Widget button;
    if (_controller.text.isEmpty) {
      button = Button(
        color: defBtnColorL,
        //完成
        child: Text(STR(10215), style: defBtnTextStyleL),
        onPressed: () {},
      );
    } else {
      button = Button(
        color: defBtnColor,
        //完成
        child: Text(STR(10215), style: defBtnTextStyle),
        onPressed: onPassed,
      );
    }

    return Scaffold(
      backgroundColor: defBackgroundColor,
      appBar: CustomAppbar(
        isBackIcon: true,
        isLineshade: true,
        title: widget.title,
        actions: [button],
      ),
      body: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          FocusScope.of(context).requestFocus(_editorNode);
        },
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: SUT_W(15)),
          child: ListView(
            physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
            children: [
              TextField(
                focusNode: _editorNode,
                maxLines: null,
                minLines: 30,
                maxLength: widget.inputMaxLength,
                controller: _controller,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: widget.hitText,
                    hintStyle: TextStyle(fontSize: SUT_SP(26), color: Colors.grey),
                    counterText: '',
                    contentPadding: EdgeInsets.all(0)),
                onChanged: (value) {
                  setState(() {});
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
