import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:one_context/one_context.dart';

class EditorInputPage extends StatefulWidget {
  String? title;
  String? content;
  String? hitText;
  int? inputMaxLength;
  bool inputIsLine;
  Widget? inputLeading;
  Widget? bottomWidget;
  List<Widget>? buttons;
  List<TextInputFormatter>? inputFormatters;
  void Function(String)? onPressed;
  EditorInputPage(
      {this.title, this.content, this.inputMaxLength, this.inputIsLine = false, this.hitText, this.inputLeading, this.bottomWidget, this.inputFormatters, this.onPressed});

  @override
  State<StatefulWidget> createState() {
    return _EditorInputPage();
  }
}

class _EditorInputPage extends State<EditorInputPage> {
  double _bottomHeight = 200;
  FocusNode _editorNode = FocusNode();
  TextEditingController _controller = TextEditingController();
  @override
  void dispose() {
    // FocusScope.of(OneContext().context!).requestFocus(FocusNode());
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: CustomAppbar(
        isBackIcon: true,
        isLineshade: false,
        backColor: Colors.white,
      ),
      body: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: LayoutBuilder(
          builder: (context, constraints) {
            return SingleChildScrollView(
              physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
              child: Container(
                // height: ScreenUtil.screenHeight,
                color: Colors.white,
                padding: EdgeInsets.symmetric(horizontal: SUT_W(50)),
                constraints: BoxConstraints(
                  minHeight: constraints.biggest.height,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: <Widget>[
                        SizedBox(height: SUT_H(50)),
                        Text(widget.title ?? ' ', style: TextStyle(fontWeight: FontWeight.bold, fontSize: SUT_SP(32), color: defFontColor)),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: SUT_H(25)),
                          child: Text(widget.content ?? '', style: TextStyle(fontSize: SUT_SP(28), color: defFontColor)),
                        ),
                        SizedBox(height: SUT_H(25)),
                        Divider(height: 1, color: grayColor),
                        TextField(
                          focusNode: _editorNode,
                          maxLines: widget.inputIsLine ? null : 1,
                          maxLength: widget.inputMaxLength ?? 30,
                          controller: _controller,
                          style: defTextStyle,
                          inputFormatters: widget.inputFormatters,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: widget.hitText,
                            hintStyle: TextStyle(fontSize: defMinFontSize, color: grayColor),
                            prefixIcon: Padding(padding: EdgeInsets.all(5.sp), child: widget.inputLeading),
                            counterText: '',
                          ),
                          textAlignVertical: TextAlignVertical.center,
                        ),
                        Divider(height: 1, color: grayColor),
                        Container(
                          alignment: Alignment.centerLeft,
                          child: widget.bottomWidget ?? SizedBox(),
                        )
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: _bottomHeight),
                      child: Button(
                        //完成
                        child: Text(STR(10215), style: defBtnTextStyle),
                        color: defBtnColor,
                        padding: EdgeInsets.symmetric(vertical: SUT_H(15), horizontal: SUT_W(150)),
                        onPressed: () {
                          if (widget.onPressed != null) {
                            widget.onPressed!.call(_controller.text);
                            OneContext().pop();
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
