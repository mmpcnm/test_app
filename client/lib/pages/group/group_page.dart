import 'package:fim_app/custom/azlistview/azlistview.dart';
import 'package:fim_app/custom/wdiget/CustomRefreshHandler.dart';
import 'package:fim_app/custom/wdiget/EmptyBackWidger.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/wdiget/custom_list_table.dart';
import 'package:fim_app/custom/wdiget/user_icon_widget.dart';
import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/controller/logic_group.dart';
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/model/group/group_scoped.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/pages/Group/new_group_page.dart';
import 'package:m_loading/m_loading.dart';
import 'package:one_context/one_context.dart';
import './widget/group_item.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class GroupItemBean extends ISuspensionBean {
  String tag = '群聊';
  int index = 0;
  GroupItemBean({required this.tag, required this.index}) {
    isShowSuspension = index == 0;
  }

  @override
  String getSuspensionTag() {
    return tag;
  }
}

class GroupPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _GroupPage();
  }
}

class _GroupPage extends State<GroupPage> {
  final List<GroupItemBean> _items = [];
  late CustomRefreshHanler mCustomRefreshHanler;
  @override
  void initState() {
    super.initState();
    mCustomRefreshHanler = CustomRefreshHanler(
        onRefreshHandlerStart: onRefreshHandlerStart, onRefreshHandlerEnd: onRefreshHandlerEnd);
    mCustomRefreshHanler.refreshHandler(isSetStatus: false);
  }

  @override
  void dispose() {
    mCustomRefreshHanler.dispose();
    super.dispose();
  }

  Future onRefreshHandlerStart() async {
    await LogicGroup.GetGroups();
    await LogicGroup.GetAllRoom();
  }

  Future onRefreshHandlerEnd() async {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: defBackgroundColor,
      appBar: CustomAppbar(
        isBackIcon: true,
        isLineshade: true,
        //群聊
        title: Text(STR(10029), style: defTitleTextStyle),
        actions: [
          InkWell(
            child: Icon(
              Icons.add_circle_outline,
              color: Colors.black,
            ),
            onTap: () {
              OneContext().push(pageRoute(NewGroupPage()));
            },
          )
        ],
      ),
      body: ScopedModel<GroupScoped>(
        model: GroupScoped.instance,
        child: ScopedModelDescendant<GroupScoped>(
          builder: (context, child, model) {
            return mCustomRefreshHanler.buildWidget(context, builder: () {
              _items.clear();
              for (int index = 0; index < GroupScoped.instance.roomCount; ++index) {
                Debug.d('index: $index, tag: 聊天室');
                _items.add(GroupItemBean(tag: '聊天室', index: index));
              }

              for (int index = 0; index < GroupScoped.instance.count; ++index) {
                _items.add(GroupItemBean(tag: '群聊', index: index));
              }

              int count = _items.length;
              if (count == 0) {
                return EmptyBackWidget(image: ResImgs('no_data_empty'), text: STR(10268));
              } else {
                return RefreshIndicator(
                  onRefresh: () async {
                    await mCustomRefreshHanler.refreshHandler(
                      isSetStatus: false,
                      duration: Duration(seconds: 1),
                    );
                  },
                  child: SuspensionView(
                    physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                    data: _items,
                    itemCount: _items.length,
                    itemBuilder: (context, index) {
                      var item = _items[index];

                      if (item.getSuspensionTag() == '聊天室') {
                        var room = GroupScoped.instance.atRoomData(item.index);
                        // return RoomItem(room);
                        return Column(
                          // mainAxisSize: MainAxisSize.min,
                          children: [
                            RoomItem(room),
                            Divider(height: 1, color: lineColor, indent: 108.sp),
                          ],
                        );
                      } else {
                        // return GroupItem(GroupScoped.instance.atGroup(item.index));
                        return Column(
                          // mainAxisSize: MainAxisSize.min,
                          children: [
                            GroupItem(GroupScoped.instance.atGroup(item.index)),
                            Divider(height: 1, color: lineColor, indent: 108.sp),
                          ],
                        );
                      }
                    },
                    susItemBuilder: (context, index) {
                      return Container(
                        color: defBackgroundColor,
                        width: SUT_S_WIDTH,
                        padding: EdgeInsets.all(defPaddingSize),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            _items[index].getSuspensionTag(),
                            style: defTextStyle,
                          ),
                        ),
                      );
                      // return CustomListTile(
                      //   padding: ,
                      //   middletitle: Text(_items[index].getSuspensionTag(), style: defTextStyle),
                      // );
                    },
                  ),
                );
              }
            });

            // if (mRefreshStatus == 1) {
            //   return Center(
            //     child: SizedBox(
            //       width: 100.sp,
            //       height: 100.sp,
            //       child: BallCircleOpacityLoading(
            //         // duration: Duration(minutes: 1),
            //         ballStyle: BallStyle(
            //           size: 10.sp,
            //           color: Colors.blue,
            //           ballType: BallType.solid,
            //         ),
            //       ),
            //     ),
            //   );
            // } else {

            //     // ListView.separated(
            //     //   itemCount: count == 0 ? 0 : GroupScoped.instance.count + 1 + GroupScoped.instance.roomCount,
            //     //   physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
            //     //   // separatorBuilder: (context, index) {
            //     //   //   // if (index == 0) {
            //     //   //   //   return Container(color: lineColor, height: defTabelSplitHeight);
            //     //   //   // } else {
            //     //   //   return SizedBox(height: paddingHorizontal);
            //     //   //   // }
            //     //   // },
            //     //   itemBuilder: (context, index) {
            //     //     if (index < GroupScoped.instance.roomCount) {
            //     //       if (index == 0) {
            //     //         return Column(
            //     //           mainAxisSize: MainAxisSize.min,
            //     //           children: [
            //     //             CustomListTile(
            //     //               middletitle: Text('聊天室', style: defTextStyle),
            //     //             ),
            //     //             Center(),
            //     //           ],
            //     //         );
            //     //       }
            //     //     } else if (index < count) {
            //     //       if (index == GroupScoped.instance.roomCount) {
            //     //       } else {
            //     //         return GroupItem(GroupScoped.instance.atGroup(index - GroupScoped.instance.roomCount));
            //     //       }
            //     //     } else {
            //     //       return Center(child: Text(STR(10223, [count]), style: defTextStyle));
            //     //     }
            //     //   },
            //     //   separatorBuilder: (BuildContext context, int index) {
            //     //     if (index == GroupScoped.instance.roomCount && GroupScoped.instance.roomCount != 0) {
            //     //       return CustomListTile(
            //     //         middletitle: Text('群聊', style: defTextStyle),
            //     //       );
            //     //     }
            //     //     return Divider(height: 1, color: lineColor, indent: 108.sp);
            //     //   },
            //     // );
            //   }
            // }
          },
        ),
      ),
    );
  }
}
