import 'package:fim_app/custom/wdiget/custom_list_table.dart';
import 'package:fim_app/package/net/controller/logic_group.dart';
import 'package:fim_app/pages/chat/chat_page.dart';
import 'package:fim_app/model/friend/friend_scoped.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/custom/wdiget/user_icon_widget.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/model/friend/friend_info.dart';
import 'package:fim_app/model/group/group_member_scoped.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:one_context/one_context.dart';

class NewGroupPage extends StatefulWidget {
  Int64 groupId;
  String tilteText;
  List<Int64>? select;
  NewGroupPage({this.groupId = Int64.ZERO, this.tilteText = '', this.select});
  @override
  State<StatefulWidget> createState() => _NewGroupPage();
}

class _NewGroupPage extends State<NewGroupPage> {
  List<FriendInfo> _friends = [];
  Set<Int64> _select = Set<Int64>();
  @override
  void initState() {
    super.initState();
    _friends = FriendScoped.instance.friends;
    List<Int64> userids = widget.select ?? [];
    for (var id in userids) {
      _select.add(id);
    }
  }

  List<Int64> getSelect() {
    List<Int64> ret = [];
    if (widget.groupId != 0) {
      for (var id in _select) {
        if (widget.select == null || !(widget.select!.contains(id))) ret.add(id);
      }
    } else {
      for (var id in _select) {
        ret.add(id);
      }
    }

    return ret;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppbar(
        isBackIcon: true,
        isLineshade: true,
        title: Text(
          widget.tilteText.isNotEmpty ? widget.tilteText : STR(10164), //'发起群聊',
          style: defTitleTextStyle,
        ),
      ),
      bottomSheet: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Button(
            margin: EdgeInsets.symmetric(vertical: SUT_H(15)),
            color: defBtnColor,
            child: Text(STR(10215), style: defBtnTextStyle),
            onPressed: () {
              List<Int64> ids = getSelect();
              if (ids.isEmpty) {
                OneContext().pop();
                return;
              }
              if (widget.groupId != 0) {
                // 添加群成员
                EasyLoading.show();
                LogicGroup.AddGroupMembers(context, groupId: widget.groupId, userIds: ids,
                    logicCallBack: (code, resp) {
                  EasyLoading.dismiss();
                  OneContext().pop();
                });
              } else {
                // 创建群
                EasyLoading.show();
                LogicGroup.CreateGroup(context, memberIds: ids, logicCallBack: (code, resp) {
                  EasyLoading.dismiss();
                  if (code == 0) {
                    gotpHome(OneContext().context!);
                    OneContext().push(pageRoute(ChatPage(
                        mReceiverId: resp!.groupId, mReceiverType: pb.ReceiverType.RT_GROUP)));
                  }
                });
              }
            },
          ),
          SizedBox(
            width: SUT_W(20),
          )
        ],
      ),
      body: ListView.builder(
        physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
        itemBuilder: (context, index) {
          var friend = _friends[index];
          bool isCheck = _select.contains(friend.userId);
          bool isStaticCheck = false;
          if (widget.select != null) {
            isStaticCheck = widget.select!.contains(friend.userId);
            if (isStaticCheck) isCheck = true;
          }

          if (widget.groupId > 0 && !isStaticCheck) {
            isStaticCheck =
                GroupMemberScoped.instance.isMember(groupId: widget.groupId, userId: friend.userId);
            if (isStaticCheck) isCheck = true;
          }

          return StateNewGroupItem(friend, isCheck: isCheck, isStaticCheck: isStaticCheck,
              onChanged: (userId, isCheck) {
            if (isCheck) {
              _select.add(userId);
            } else {
              _select.remove(userId);
            }
          });
        },
        itemCount: _friends.length,
      ),
    );
  }
}

class StateNewGroupItem extends StatefulWidget {
  FriendInfo friendModel;
  bool isCheck;
  bool isStaticCheck;
  void Function(Int64 userId, bool isCheck) onChanged;
  StateNewGroupItem(this.friendModel,
      {this.isCheck = false, this.isStaticCheck = false, required this.onChanged});
  @override
  State<StatefulWidget> createState() =>
      _NewGroupItem(isCheck: isCheck, isStaticCheck: isStaticCheck);
}

class _NewGroupItem extends State<StateNewGroupItem> {
  bool isCheck = false;
  bool isStaticCheck = false;
  _NewGroupItem({this.isStaticCheck = false, this.isCheck = false});

  void onCheckClick() {
    setState(() {
      isCheck = !isCheck;
      widget.onChanged(widget.friendModel.userId, isCheck);
    });
  }

  @override
  Widget build(BuildContext context) {
    Color? color = this.isStaticCheck ? grayColor : null;
    return CustomListTable.buildTable(
      leading: UserIconWidget(widget.friendModel.avatarUrl),
      leftText: widget.friendModel.userNickname,
      backColor: color,
      onPressed: !this.isStaticCheck ? onCheckClick : null,
      isLine: true,
      trailing: Center(),
      rightWidget: Icon(
        isCheck ? Icons.check_box_outlined : Icons.check_box_outline_blank_sharp,
        size: defIconSize,
        color: blackColor,
      ),
    );

    // if (isStaticCheck) {
    //   return CheckboxListTile(
    //     secondary: UserIconWidget(widget.friendModel.avatarUrl),
    //     checkColor: grayColor,
    //     title: Text(
    //       widget.friendModel.nickname,
    //       style: TextStyle(fontSize: defFontSize, color: defFontColor, fontWeight: FontWeight.bold),
    //     ),
    //     value: isCheck,
    //     selected: isCheck,
    //     onChanged: null,
    //     // onChanged: (value) {
    //     //   isCheck = !isCheck;
    //     //   onChanged(friendModel.userId, isCheck);
    //     // },
    //   );
    // } else {
    //   return CheckboxListTile(
    //     secondary: UserIconWidget(widget.friendModel.avatarUrl),
    //     title: Text(
    //       widget.friendModel.nickname,
    //       style: TextStyle(fontSize: defFontSize, color: defFontColor, fontWeight: FontWeight.bold),
    //     ),
    //     value: isCheck,
    //     onChanged: (value) {
    //       isCheck = !isCheck;
    //       setState(() {});
    //       widget.onChanged(widget.friendModel.userId, isCheck);
    //     },
    //   );
    // }
  }
}
