import 'package:fim_app/configs/im_config.dart';
import 'package:fim_app/custom/dialog/CustomDialog.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/wdiget/EmptyBackWidger.dart';
import 'package:fim_app/package/net/controller/logic_group.dart';
import 'package:fim_app/package/pb/logic.ext.pb.dart' as pb;
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/model/group/group_member_model.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/custom/wdiget/user_icon_widget.dart';
import 'package:fim_app/model/group/group_member_scoped.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/material.dart';
import 'package:one_context/one_context.dart';

//好友信息
class GroupUserProfilePage extends StatefulWidget {
  // GroupMemberModel user;
  Int64 memberId;
  Int64 groupId;
  GroupUserProfilePage({required this.groupId, required this.memberId});

  @override
  State<StatefulWidget> createState() {
    return _GroupUserProfilePage();
  }
}

class _GroupUserProfilePage extends State<GroupUserProfilePage> {
  GroupMemberModel? _mUser;
  @override
  void initState() {
    super.initState();
    _mUser =
        GroupMemberScoped.instance.getMember(groupId: widget.groupId, memberId: widget.memberId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: defBackgroundColor,
      appBar: CustomAppbar(isBackIcon: true, isLineshade: false),
      body: LayoutBuilder(
        builder: (context, constraints) {
          if (_mUser == null) {
            return EmptyBackWidget(image: ResImgs('no_data_empty'), text: '无群员信息');
          } else {
            return Stack(
              alignment: Alignment.topCenter,
              children: [
                _buildBack(),
                Padding(
                  padding: EdgeInsets.only(
                      top: 15.sp,
                      left: 25.sp,
                      right: 25.sp,
                      bottom: ScreenUtil.bottomBarHeight + 150.sp),
                  child: ListView(
                    physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                    children: [
                      SizedBox(height: 15.sp),
                      _buildUserIcon(),
                      SizedBox(height: 25.sp),
                      Divider(height: 1, color: lineColor),
                      _buildSign(),
                      Divider(height: 1, color: lineColor),
                    ],
                  ),
                ),
                Positioned(
                    bottom: ScreenUtil.bottomBarHeight + 50.sp,
                    child: _buildDelButton(constraints.maxWidth)),
              ],
            );
          }
        },
      ),
    );
  }

  //背景
  Widget _buildBack() {
    double circular = 25.sp;
    return Container(
      margin: EdgeInsets.only(bottom: ScreenUtil.bottomBarHeight),
      child: Card(
        child: Container(margin: EdgeInsets.all(0)),
        color: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(circular),
              topRight: Radius.circular(circular),
              bottomLeft: Radius.zero,
              bottomRight: Radius.zero),
        ),
      ),
    );
  }

  Widget _buildUserIcon() {
    List<InlineSpan> spans = [
      TextSpan(
          text: '${_mUser?.userNickname} ',
          style: TextStyle(fontSize: defFontSize, color: defFontColor))
    ];
    if (_mUser?.sex.sex != USER_SEX.UNKNOWN) {
      spans.add(
        WidgetSpan(
          alignment: PlaceholderAlignment.bottom,
          child: Image(width: 32.sp, height: 32.sp, image: AssetImage(_mUser!.sex.sex_image)),
        ),
      );
    }

    return Row(
      // width: ScreenUtil.toWidth(500),
      // height: ScreenUtil.toHeight(200),
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            UserIconWidget(_mUser!.avatarUrl, size: 120.sp),
            SizedBox(height: 15.sp),
            Text.rich(TextSpan(children: spans)),
            SizedBox(height: 15.sp),
            Text.rich(
              TextSpan(
                children: [
                  //备注
                  TextSpan(
                      text: '${STR(10216)}: ',
                      style: TextStyle(fontSize: defMinFontSize, color: defFontColor)),
                  TextSpan(
                      text: _mUser!.remarks,
                      style: TextStyle(fontSize: defMinFontSize, color: grayColor)),
                ],
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ],
    );
  }

  //个性签名
  Widget _buildSign() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 25.sp),
      child: Text.rich(
        TextSpan(
          children: [
            TextSpan(
                text: STR(10009), style: TextStyle(fontSize: defFontSize, color: defFontColor)),
            TextSpan(text: ' ~~~', style: TextStyle(fontSize: defMinFontSize, color: grayColor)),
          ],
        ),
      ),
    );
  }

  Widget _buildDelButton(double maxWidth) {
    var selfMemberType =
        GroupMemberScoped.instance.getMemberType(groupId: widget.groupId, userId: UserModel.userId);
    if (selfMemberType == pb.MemberType.GMT_ADMIN) {
      return Container(
        constraints: BoxConstraints(minWidth: maxWidth),
        padding: EdgeInsets.only(left: defPaddingSize, right: defPaddingSize),
        child: TextButton(
          onPressed: () {
            // 提示 是否将 ${xxx} 踢出该群？
            showMessageDialogEx(context,
                title: STR(10185),
                content: STR(10224, [_mUser!.userNickname]), confirmCallback: () {
              // gotoLogin(context);
              LogicGroup.DeleteGroupMember(groupId: _mUser!.groupId, userId: _mUser!.userId);
              OneContext().pop();
              // gotpHome(context);
            });
          },
          child: Text(STR(10225), style: TextStyle(fontSize: defFontSize, color: Colors.red[400])),
          style: ButtonStyle(
              shape: MaterialStateProperty.all(
                  RoundedRectangleBorder(borderRadius: BorderRadius.circular(45))),
              side: MaterialStateProperty.all(BorderSide(color: lineColor, width: 1))),
        ),
      );
    } else {
      return Container();
    }
  }
}
