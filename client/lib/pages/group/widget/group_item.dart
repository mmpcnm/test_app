import 'package:fim_app/configs/api.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/wdiget/custom_list_table.dart';
import 'package:fim_app/custom/wdiget/user_icon_widget.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/package/util/res_manager.dart';
import 'package:fim_app/model/group/group_model.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/pages/chat/chat_page.dart';
import 'package:fim_app/pages/group/widget/group_icon.dart';
import 'package:fim_app/model/group/group_scoped.dart';
import 'package:flutter/material.dart';
import 'package:one_context/one_context.dart';

class GroupItem extends StatelessWidget {
  GroupModel group;
  GroupItem(this.group);
  @override
  Widget build(BuildContext context) {
    String name = group.name;
    if (name.isEmpty) {
      name = STR(10222); //'未命名';
    }
    return CustomListTile(
      color: Colors.white,
      padding: EdgeInsets.symmetric(horizontal: defPaddingSize, vertical: 15.sp),
      leading: GroupIcon(group.groupId),
      title: Text(group.name, style: defTextStyle, overflow: TextOverflow.ellipsis),
      isArrowIcon: true,
      onPressed: () {
        OneContext().pop();
        OneContext().push(
          pageRoute(
            ChatPage(
              mReceiverId: group.groupId,
              mReceiverType: group.groupId == AppConfig.channelId
                  ? pb.ReceiverType.RT_ROOM
                  : pb.ReceiverType.RT_GROUP,
            ),
          ),
        );
      },
    );

    // return CustomListTable.buildTable(
    //   isLine: true,
    //   lineIndent: 108.sp,
    //   padding: EdgeInsets.symmetric(horizontal: defPaddingSize, vertical: 15.sp),
    //   leading: GroupIcon(group.groupId),
    //   leftWidget: Text(group.name, style: defTextStyle, overflow: TextOverflow.ellipsis),
    //   onPressed: () {
    //     OneContext().pop();
    //     OneContext().push(
    //       pageRoute(
    //         ChatPage(
    //           mReceiverId: group.groupId,
    //           mReceiverType: group.groupId == AppConfig.channelId ? pb.ReceiverType.RT_ROOM : pb.ReceiverType.RT_GROUP,
    //         ),
    //       ),
    //     );
    //   },
    // );
  }
}

class RoomItem extends StatelessWidget {
  RoomData roomData;
  RoomItem(this.roomData);
  @override
  Widget build(BuildContext context) {
    return CustomListTile(
      color: Colors.white,
      padding: EdgeInsets.symmetric(horizontal: defPaddingSize, vertical: 15.sp),
      leading: roomData.avatar_url.isEmpty
          ? UserIconWidget(ResImgs('app_icon'), isNetworkIamge: false)
          : UserIconWidget(roomData.avatar_url),
      title: Text(roomData.name, style: defTextStyle, overflow: TextOverflow.ellipsis),
      isArrowIcon: true,
      onPressed: () {
        OneContext().pop();
        OneContext().push(
          pageRoute(
            ChatPage(
              mReceiverId: roomData.roomId,
              mReceiverType: pb.ReceiverType.RT_ROOM,
            ),
          ),
        );
      },
    );
  }
}
