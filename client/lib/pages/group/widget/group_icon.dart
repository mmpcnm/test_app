import 'package:fim_app/custom/wdiget/user_icon_widget.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/model/group/group_member_model.dart';
import 'package:fim_app/model/group/group_member_scoped.dart';
import 'package:fim_app/model/group/group_scoped.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class GroupIcon extends StatefulWidget {
  Int64 groupId;
  GroupIcon(this.groupId, {Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _GroupIcon();
}

class _GroupIcon extends State<GroupIcon> {
  @override
  Widget build(BuildContext context) {
    return ScopedModel(
      model: GroupMemberScoped.instance,
      child: ScopedModelDescendant<GroupMemberScoped>(
        builder: (context, child, model) {
          return SizedBox(
            width: defAvatarSize,
            height: defAvatarSize,
            child: Material(
              color: whiteColor,
              type: MaterialType.button,
              borderRadius: BorderRadius.all(Radius.circular(defAvatarCircular)),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(defAvatarCircular)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: _buildIcons(),
                ),
              ),
            ),
          );

          // Container(
          //   width: defAvatarSize,
          //   height: defAvatarSize,
          //   decoration: BoxDecoration(
          //     color: whiteColor,
          //     borderRadius: BorderRadius.all(Radius.circular(5)),
          //   ),
          //   child: Column(
          //     mainAxisAlignment: MainAxisAlignment.center,
          //     crossAxisAlignment: CrossAxisAlignment.center,
          //     children: _buildIcons(),
          //   ),
          // );
        },
      ),
    );
  }

  String _avatar(GroupMemberModel model) {
    if (model.userId == UserModel.userId) {
      return UserModel.avatarUrl;
    } else {
      return model.avatarUrl;
    }
  }

  List<Widget> _buildIcons() {
    List<Widget> icons = [];
    // if (widget.groupId == AppConfig.channelId) {
    //   icons.add(UserIconWidget.createUserIconImage(ResImgs('app_icon'), size: defAvatarSize, isNetworkIamge: false));
    //   return icons;
    // }
    var group = GroupScoped.instance.getGroup(widget.groupId);
    if (group == null) {
      icons.add(UserIconWidget.createUserIconImage('', size: defAvatarSize, isNetworkIamge: false));
      return icons;
    } else if (group.avatarUrl.isNotEmpty) {
      icons.add(UserIconWidget.createUserIconImage(group.avatarUrl, size: defAvatarSize));
      return icons;
    }
    var members = GroupMemberScoped.instance.getMembers(groupId: widget.groupId);
    members.sort((a, b) => a.userId.compareTo(b.userId));
    int count = members.length;

    double iconSize =
        count > 1 ? (count > 6 ? defAvatarSize / 3 : defAvatarSize / 2) : defAvatarSize;
    if (count == 1) {
      icons.add(UserIconWidget.createUserIconImage(_avatar(members[0]), size: iconSize));
    } else if (count == 2) {
      icons.add(Row(
        children: [
          UserIconWidget.createUserIconImage(_avatar(members[0]), size: iconSize),
          UserIconWidget.createUserIconImage(_avatar(members[1]), size: iconSize),
        ],
      ));
    } else if (count == 3) {
      icons.addAll(
        [
          UserIconWidget.createUserIconImage(_avatar(members[0]), size: iconSize),
          Row(
            children: [
              UserIconWidget.createUserIconImage(_avatar(members[1]), size: iconSize),
              UserIconWidget.createUserIconImage(_avatar(members[2]), size: iconSize),
            ],
          ),
        ],
      );
    } else if (count >= 4 && count <= 6) {
      icons.addAll(
        [
          Row(
            children: [
              UserIconWidget.createUserIconImage(_avatar(members[0]), size: iconSize),
              UserIconWidget.createUserIconImage(_avatar(members[1]), size: iconSize),
            ],
          ),
          Row(
            children: [
              UserIconWidget.createUserIconImage(_avatar(members[2]), size: iconSize),
              UserIconWidget.createUserIconImage(_avatar(members[3]), size: iconSize),
            ],
          ),
        ],
      );
    } else if (count == 7) {
      icons.addAll(
        [
          UserIconWidget.createUserIconImage(_avatar(members[0]), size: iconSize),
          Row(
            children: [
              UserIconWidget.createUserIconImage(_avatar(members[1]), size: iconSize),
              UserIconWidget.createUserIconImage(_avatar(members[2]), size: iconSize),
              UserIconWidget.createUserIconImage(_avatar(members[3]), size: iconSize),
            ],
          ),
          Row(
            children: [
              UserIconWidget.createUserIconImage(_avatar(members[4]), size: iconSize),
              UserIconWidget.createUserIconImage(_avatar(members[5]), size: iconSize),
              UserIconWidget.createUserIconImage(_avatar(members[6]), size: iconSize),
            ],
          ),
        ],
      );
    } else if (count == 8) {
      icons.addAll(
        [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              UserIconWidget.createUserIconImage(_avatar(members[0]), size: iconSize),
              UserIconWidget.createUserIconImage(_avatar(members[1]), size: iconSize),
            ],
          ),
          Row(
            children: [
              UserIconWidget.createUserIconImage(_avatar(members[2]), size: iconSize),
              UserIconWidget.createUserIconImage(_avatar(members[3]), size: iconSize),
              UserIconWidget.createUserIconImage(_avatar(members[4]), size: iconSize),
            ],
          ),
          Row(
            children: [
              UserIconWidget.createUserIconImage(_avatar(members[5]), size: iconSize),
              UserIconWidget.createUserIconImage(_avatar(members[6]), size: iconSize),
              UserIconWidget.createUserIconImage(_avatar(members[7]), size: iconSize),
            ],
          ),
        ],
      );
    } else if (count > 8) {
      icons.addAll(
        [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              UserIconWidget.createUserIconImage(_avatar(members[0]), size: iconSize),
              UserIconWidget.createUserIconImage(_avatar(members[1]), size: iconSize),
              UserIconWidget.createUserIconImage(_avatar(members[2]), size: iconSize),
            ],
          ),
          Row(
            children: [
              UserIconWidget.createUserIconImage(_avatar(members[3]), size: iconSize),
              UserIconWidget.createUserIconImage(_avatar(members[4]), size: iconSize),
              UserIconWidget.createUserIconImage(_avatar(members[5]), size: iconSize),
            ],
          ),
          Row(
            children: [
              UserIconWidget.createUserIconImage(_avatar(members[6]), size: iconSize),
              UserIconWidget.createUserIconImage(_avatar(members[7]), size: iconSize),
              UserIconWidget.createUserIconImage(_avatar(members[8]), size: iconSize),
            ],
          ),
        ],
      );
    } else {
      icons.add(UserIconWidget.createUserIconImage(group.avatarUrl, size: defAvatarSize));
    }

    return icons;
  }
}
