import 'package:fim_app/custom/wdiget/user_icon_widget.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/package/pb/logic.ext.pb.dart' as pb;
import 'package:fim_app/model/group/group_member_model.dart';
import 'package:fim_app/model/group/group_member_scoped.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/material.dart';

class GroupDelMemberPage extends StatefulWidget {
  Int64 groupId;

  GroupDelMemberPage(this.groupId);

  @override
  State<StatefulWidget> createState() {
    return _GroupDelMemberPage();
  }
}

class _GroupDelMemberPage extends State<GroupDelMemberPage> {
  Set<Int64> _select = Set<Int64>();
  List<GroupMemberModel> _members = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _members = GroupMemberScoped.instance.getMembers(groupId: widget.groupId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: defBackgroundColor,
        appBar: CustomAppbar(
          isBackIcon: true,
          actions: [
            InkWell(
              //删除
              child: Text(STR(10193), style: defTextStyle),
              onTap: () {
                if (_select.isEmpty) return;
              },
            ),
          ],
        ),
        body: ListView.builder(
          padding: EdgeInsets.only(bottom: SUT_H(15)),
          physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
          itemBuilder: (context, index) {
            return _DelGroupMemberItem(
              _members[index],
              onChanged: (Int64 userId, bool isCheck) {
                if (isCheck) {
                  _select.add(userId);
                } else {
                  _select.remove(userId);
                }
              },
            );
          },
          itemCount: _members.length,
        ));
  }
}

class _DelGroupMemberItem extends StatelessWidget {
  GroupMemberModel member;
  bool isCheck = false;
  void Function(Int64 userId, bool isCheck) onChanged;

  _DelGroupMemberItem(this.member, {required this.onChanged});

  @override
  Widget build(BuildContext context) {
    return CheckboxListTile(
      secondary: UserIconWidget(member.avatarUrl),
      title: Text(
        member.userNickname,
        style: defTextStyle,
      ),
      value: isCheck,
      onChanged: (value) {
        isCheck = !isCheck;
        onChanged(member.userId, isCheck);
      },
    );
  }
}
