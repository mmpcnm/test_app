// ignore_for_file: file_names

import 'dart:convert';

import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/debug/custom_debug.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/controller/logic_user.dart';
import 'package:fim_app/pages/friend/user_profile_page.dart';
import 'package:fim_app/pages/webview/webview.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

import 'scan_widget/QrcodeReaderView.dart';

class ScanCodePage extends StatelessWidget {
  const ScanCodePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: QrcodeReaderView(
        onScan: onScan,
        headerWidget: null,
      ),
    );
  }

  Future onScan(String code) async {
    Debug.d('QrCode:$code');
    int index = code.indexOf(':');
    if (index != -1) {
      String codetype = code.substring(0, index);
      String body = code.substring(index + 3);
      if (codetype == 'http' || codetype == 'https') {
        List<String> arr = body.split('_');
        if (arr.length == 2 && arr[0] == 'userid' && int.tryParse(arr[1]) != null) {
          int userid = int.parse(arr[1]);
          LogicUser.GetUser(
            userId: Int64(userid),
            logicCallBack: (code, resp) {
              CustomRoute().pop();
              if (code == 0) {
                CustomRoute().pushPage(UserProfilePage(resp!.user));
              } else {
                EasyLoading.showError('获取用户信息失败！');
              }
            },
          );
        } else {
          CustomRoute().pop();
          CustomRoute().pushFade(WebView(url: code, isCloseIcon: true));
        }
        return;
      }
    }
    CustomRoute().pop();
    EasyLoading.showToast(STR(10231));
  }
}
