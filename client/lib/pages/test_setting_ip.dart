import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/configs/api.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/package/handler/Handler.dart';
import 'package:fim_app/package/handler/HandlerCode.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TestSettingIp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _TestSettingIp();
}

class _TestSettingIp extends State<TestSettingIp> {
  late String connectIp = '';
  late String connectPort = '';
  late String logicIp = '';
  late String logicPort = '';
  @override
  void initState() {
    super.initState();
    connectIp = AppConfig.connectIp;
    connectPort = AppConfig.connectPort.toString();
    logicIp = AppConfig.logicIp;
    logicPort = AppConfig.logicPort.toString();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        backgroundColor: defBackgroundColor,
        appBar: CustomAppbar(isBackIcon: true, title: Text('测试 IP 设置', style: defTitleTextStyle)),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: defPaddingSize),
          child: ListView(
            physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
            children: [
              SizedBox(height: 15.h),
              TextField(
                keyboardType: TextInputType.number,
                style: TextStyle(fontSize: 28.sp),
                // controller: TextEditingController(text: AppConfig.connectIp),
                decoration: InputDecoration(
                  labelText: 'Connect IP: ${AppConfig.connectIp}',
                  // hintText: '${AppConfig.connectIp}',
                ),
                maxLines: 1,
                onChanged: (value) {
                  connectIp = value;
                },
              ),
              SizedBox(height: 15.h),
              TextField(
                keyboardType: TextInputType.number,
                style: TextStyle(fontSize: 28.sp),
                // controller: TextEditingController(text: AppConfig.connectPort.toString()),
                decoration: InputDecoration(
                  labelText: 'Connect Port: ${AppConfig.connectPort}',
                  // hintText: '${AppConfig.connectPort}',
                ),
                maxLines: 1,
                onChanged: (value) {
                  connectPort = value;
                },
              ),
              SizedBox(height: 15.h),
              TextField(
                keyboardType: TextInputType.number,
                style: TextStyle(fontSize: 28.sp),
                // controller: TextEditingController(text: AppConfig.logicIp),
                decoration: InputDecoration(
                  labelText: 'Logic IP: ${AppConfig.logicIp}',
                  // hintText: '${AppConfig.logicIp}',
                ),
                maxLines: 1,
                onChanged: (value) {
                  logicIp = value;
                },
              ),
              SizedBox(height: 15.h),
              TextField(
                keyboardType: TextInputType.number,
                style: TextStyle(fontSize: 28.sp),
                // controller: TextEditingController(text: AppConfig.logicPort.toString()),
                decoration: InputDecoration(
                  labelText: 'Logic Port: ${AppConfig.logicPort}',
                  // hintText: '${AppConfig.logicPort}',
                ),
                maxLines: 1,
                onChanged: (value) {
                  logicPort = value;
                },
              ),
              SizedBox(height: 15.h),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Button(
                    onPressed: () {
                      AppConfig.connectIp = connectIp;
                      AppConfig.connectPort = int.parse(connectPort);
                      AppConfig.logicIp = logicIp;
                      AppConfig.logicPort = int.parse(logicPort);
                      setState(() {});
                      GlobalHandler().sendMessage(HandlerCode.DEBUG_SET_IP);
                    },
                    child: Text('确定', style: TextStyle(color: Colors.white, fontSize: defFontSize)),
                    color: Colors.blue,
                    padding: EdgeInsets.symmetric(horizontal: 100.w, vertical: 15.h),
                  ),
                  Button(
                    onPressed: () {
                      AppConfig.clear();
                      setState(() {});
                      GlobalHandler().sendMessage(HandlerCode.DEBUG_SET_IP);
                    },
                    child: Text('重置', style: TextStyle(color: Colors.white, fontSize: defFontSize)),
                    color: Colors.blue,
                    padding: EdgeInsets.symmetric(horizontal: 100.w, vertical: 15.h),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
