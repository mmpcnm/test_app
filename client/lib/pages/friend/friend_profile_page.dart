import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/custom/dialog/CustomDialog.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/package/net/controller/logic_friend.dart';
import 'package:fim_app/package/net/controller/logic_user.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/pages/chat/chat_page.dart';
import 'package:fim_app/model/chat/MessageModel.dart';
import 'package:fim_app/model/friend/friend_info.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/custom/wdiget/user_icon_widget.dart';
import 'package:fim_app/pages/editor/editor_input_page.dart';
import 'package:fim_app/model/friend/friend_scoped.dart';
import 'package:fixnum/fixnum.dart';
import 'package:flutter/material.dart';
import 'package:one_context/one_context.dart';
import 'package:scoped_model/scoped_model.dart';

//好友信息
class FriendProfilePage extends StatefulWidget {
  Int64 userId;
  FriendProfilePage(this.userId);

  @override
  State<StatefulWidget> createState() {
    return _FriendProfilePage();
  }
}

class _FriendProfilePage extends State<FriendProfilePage> {
  late FriendInfo _user;
  @override
  void initState() {
    super.initState();
    // _user = FriendScoped.instance.findFriend(widget.userId)!;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: defBackgroundColor,
      appBar: CustomAppbar(isBackIcon: true, isLineshade: false),
      body: ScopedModel<FriendScoped>(
        model: FriendScoped.instance,
        child: ScopedModelDescendant<FriendScoped>(
          builder: (context, child, model) {
            var user = FriendScoped.instance.findFriend(widget.userId);
            if (user == null) {
              return Center();
            }
            _user = user;
            print('_FriendProfilePage : $_user');
            return Stack(
              alignment: Alignment.topCenter,
              children: [
                _buildBack(),
                Positioned(
                    top: ScreenUtil.toHeight(180),
                    left: ScreenUtil.toWidth(35),
                    child: _buildUserIcon()),
                Positioned(top: SUT_H(240), right: SUT_W(35), child: _buileChatButton()),
                Positioned(
                  top: SUT_H(430),
                  child: Container(
                    width: SUT_S_WIDTH,
                    padding: EdgeInsets.only(left: SUT_W(35)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Divider(height: 1, color: lineColor),
                        _buildSign(),
                        Divider(height: 1, color: lineColor),
                        _buildRemarks(),
                        Divider(height: 1, color: lineColor),
                      ],
                    ),
                  ),
                ),
                Positioned(bottom: ScreenUtil.bottomBarHeight, child: _buildDelButton()),
              ],
            );
          },
        ),
      ),
    );
  }

  //背景
  Widget _buildBack() {
    double circular = ScreenUtil.toWidth(25);
    return Container(
      margin: EdgeInsets.only(top: ScreenUtil.toHeight(220)),
      child: Card(
        child: Container(
          margin: EdgeInsets.all(0),
        ),
        color: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(circular),
              topRight: Radius.circular(circular),
              bottomLeft: Radius.zero,
              bottomRight: Radius.zero),
        ),
      ),
    );
  }

  Widget _buildUserIcon() {
    return Center(
      // width: ScreenUtil.toWidth(500),
      // height: ScreenUtil.toHeight(200),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          UserIconWidget(_user.avatarUrl, size: SUT_W(120), circular: 45),
          SizedBox(height: ScreenUtil.toHeight(15)),
          Text.rich(
            TextSpan(
              children: [
                TextSpan(
                    text: _user.nickname,
                    style: TextStyle(
                        fontSize: ScreenUtil.toSP(28),
                        color: Colors.black87,
                        fontWeight: FontWeight.bold)),
                // WidgetSpan(
                //   child: Icon(
                //     Icons.lens,
                //     size: ScreenUtil.toWidth(18),
                //     color: Colors.greenAccent[400],
                //   ),
                //   alignment: PlaceholderAlignment.top,
                // ),
              ],
            ),
          ),
          SizedBox(
            height: ScreenUtil.toHeight(15),
          ),
          // Text.rich(
          //   TextSpan(
          //     children: [
          //       TextSpan(text: _user.nickname, style: TextStyle(fontSize: ScreenUtil.toSP(20), color: Colors.black45, fontWeight: FontWeight.bold)),
          //       // TextSpan(
          //       //   text: _user.account,
          //       //   style: TextStyle(
          //       //     fontSize: ScreenUtil.toSP(20),
          //       //     color: Colors.black45,
          //       //     fontWeight: FontWeight.bold,
          //       //   ),
          //       // ),
          //     ],
          //   ),
          //   textAlign: TextAlign.center,
          // ),
        ],
      ),
    );
  }

  //发送消息按钮
  Widget _buileChatButton() {
    return Button(
      child: Text(STR(10163), style: defBtnTextStyle),
      color: defBtnColor,
      borderCircular: 35.sp,
      onPressed: () {
        OneContext().pop();
        OneContext().push(
            pageRoute(ChatPage(mReceiverId: _user.userId, mReceiverType: pb.ReceiverType.RT_USER)));
      },
    );
  }

  //个性签名
  Widget _buildSign() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: SUT_H(25)),
      child: Text.rich(
        TextSpan(
          children: [
            TextSpan(
                text: STR(10009),
                style: TextStyle(
                    fontSize: SUT_SP(26), color: Colors.black87, fontWeight: FontWeight.bold)),
            TextSpan(
                text: _user.userExtra,
                style: TextStyle(
                    fontSize: SUT_SP(20), color: Colors.black54, fontWeight: FontWeight.bold)),
          ],
        ),
      ),
    );
  }

  Widget _buildRemarks() {
    return Button(
      padding: EdgeInsets.symmetric(vertical: SUT_H(25)),
      child: Row(
        children: [
          Text.rich(
            TextSpan(
              children: [
                //备注
                TextSpan(
                    text: '${STR(10216)} ',
                    style: TextStyle(
                        fontSize: SUT_SP(26), color: Colors.black87, fontWeight: FontWeight.bold)),
                TextSpan(
                    text: _user.remarks,
                    style: TextStyle(
                        fontSize: SUT_SP(20), color: Colors.black54, fontWeight: FontWeight.bold)),
              ],
            ),
          ),
        ],
      ),
      onPressed: () {
        OneContext().push(pageRoute(
          EditorInputPage(
            title: STR(10217), //'设置好友备注',
            content: STR(10010),
            hitText: STR(10218), //'请输入备注信息',
            inputMaxLength: constUserNameMaxLength,
            inputLeading: UserIconWidget(_user.avatarUrl, size: 64),
            bottomWidget: Text('${STR(10216)}: ${_user.remarks}', style: defTextStyle),
            onPressed: (value) {
              if (value.isEmpty) return;
              LogicFriend.SetFriend(
                  context: context, friendId: _user.userId, remarks: value, extra: _user.extra);
            },
          ),
        ));
      },
    );
  }

  Widget _buildDelButton() {
    return Container(
      // constraints: BoxConstraints(minWidth: maxWidth),
      width: SUT_S_WIDTH,
      padding: EdgeInsets.only(
          left: defPaddingSize, right: defPaddingSize, bottom: ScreenUtil.bottomBarHeight + 50.h),
      child: TextButton(
        onPressed: () {
          // 提示 是否删除好友
          showMessageDialogEx(context,
              title: STR(10185), content: '${STR(10219)} ${_user.nickname} ？', confirmCallback: () {
            LogicFriend.DelFriend(friendId: _user.userId);
            gotpHome(context, index: HomeViewIndex.FRIEND);
          });
        },
        //删除好友
        child: Text(STR(10220), style: TextStyle(fontSize: defFontSize, color: Colors.red[400])),
        style: ButtonStyle(
            padding: MaterialStateProperty.all(EdgeInsets.symmetric(vertical: 15.sp)),
            shape: MaterialStateProperty.all(
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(45))),
            side: MaterialStateProperty.all(BorderSide(color: lineColor, width: 1))),
      ),
    );
  }

  // Widget _buildTime() {
  //   int milliseconds = _user.updateTime.toInt() * 1000;
  //   DateTime updateTime = DateTime.fromMillisecondsSinceEpoch(milliseconds);
  //   print('spch:${milliseconds}, updateTime: ${updateTime.toString()}');
  //   String suptime = updateTime.toString().substring(5, 16);
  //   return Padding(
  //     padding: EdgeInsets.symmetric(
  //       vertical: SUT_H(25),
  //     ),
  //     child: Row(
  //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //       children: [
  //         Text(
  //           STR(10035),
  //           style: TextStyle(
  //             color: Colors.black87,
  //             fontSize: SUT_SP(26),
  //             fontWeight: FontWeight.bold,
  //           ),
  //         ),
  //         Padding(
  //           padding: EdgeInsets.only(
  //             right: SUT_W(35),
  //           ),
  //           child: Text(
  //             suptime,
  //             style: TextStyle(
  //               color: Colors.black54,
  //               fontSize: SUT_SP(24),
  //               fontWeight: FontWeight.bold,
  //             ),
  //           ),
  //         ),
  //       ],
  //     ),
  //   );
  // }
}
