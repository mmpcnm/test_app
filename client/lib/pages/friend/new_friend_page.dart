import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/pages/search/search_page.dart';
import 'package:fim_app/model/friend/new_friend_info.dart';
import 'package:fim_app/model/friend/new_friend_model.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/controller/logic_friend.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/custom/wdiget/user_icon_widget.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class NewFriendPage extends StatefulWidget {
  const NewFriendPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _NewFriendPage();
  }
}

class _NewFriendPage extends State<NewFriendPage> {
  List<NewFriendInfo> newFfiends = [];
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppbar(
        isLineshade: true,
        isBackIcon: true,
        title: Text(
          STR(10028),
          style: defTitleTextStyle,
        ),
        actions: [
          Button(
              child: Icon(Icons.search, color: blackColor, size: SUT_SP(46)),
              padding: EdgeInsets.all(SUT_W(25)),
              onPressed: () {
                SearchPage.showSearchPage();
                //OneContext().push(pageRoute(SearchPage()));
              }),
        ],
      ),
      body: Container(
        child: ScopedModel<NewFriendModel>(
          model: NewFriendModel.instance,
          child: ScopedModelDescendant<NewFriendModel>(
            builder: (context, child, model) {
              newFfiends = NewFriendModel.instance.newFriends;
              return ListView.builder(
                itemBuilder: (context, index) {
                  return _NewFrindItem(newFfiends[index]);
                },
                itemCount: newFfiends.length,
              );
            },
          ),
        ),
      ),
    );
  }
}

class _NewFrindItem extends StatelessWidget {
  final NewFriendInfo model;
  const _NewFrindItem(this.model);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: UserIconWidget(model.avatarUrl, circular: 5),
      title: Text(
        model.nickname,
        style: TextStyle(fontSize: defFontSize, color: Colors.black87),
      ),
      subtitle: Text(
        model.nickname,
        style: TextStyle(fontSize: defFontSize, color: Colors.black45),
      ),
      trailing: SizedBox(
        width: 90.w,
        height: 46.w,
        child: ElevatedButton(
          onPressed: () {
            LogicFriend.AgreeAddFriend(
                context: context,
                userId: model.friendId,
                remarks: model.nickname,
                logicCallBack: (code, resp) {});
          },
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(Colors.blue[200]),
            shape: MaterialStateProperty.all(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(45),
              ),
            ),
            padding: MaterialStateProperty.all(
              const EdgeInsets.symmetric(vertical: 3),
            ),
          ),
          child: Text(
            STR(10034),
            style: TextStyle(
                fontSize: ScreenUtil.toSP(18),
                color: Colors.blue[600],
                fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }
}
