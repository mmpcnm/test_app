import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/model/user/user_model.dart';
// import 'package:fim_app/package/Route/route.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/net/controller/logic_friend.dart';
import 'package:fim_app/package/pb/logic.ext.pb.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:one_context/one_context.dart';

//添加好友发送验证信息
class VerifyFriendPage extends StatelessWidget {
  User user;
  late TextEditingController controller;
  VerifyFriendPage(this.user) {
    controller = TextEditingController(text: '${STR(10038)}${UserModel.nickName}');
  }
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: CustomAppbar(
          isBackIcon: true,
          isLineshade: false,
          title: Text(
            STR(10036),
            style: defTitleTextStyle,
          ),
          actions: [
            TextButton(
              onPressed: () {
                print('VerifyFriendPage send: ${controller.text}');
                LogicFriend.AddFriend(
                  userId: user.userId,
                  description: controller.text,
                );
                OneContext().pop();
              },
              child: Text(
                STR(10037),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: SUT_SP(22),
                  color: Colors.black87,
                ),
              ),
            ),
          ],
        ),
        body: Container(
          padding: EdgeInsets.symmetric(
            horizontal: SUT_W(20),
            vertical: SUT_H(35),
          ),
          color: Colors.white,
          child: TextField(
            controller: controller,
            decoration: InputDecoration(
              // hintText: STR( 10038),
              hintStyle: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.white,
                fontSize: defFontSize,
              ),
              border: UnderlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.all(
                  Radius.circular(SUT_W(45)),
                ),
              ),
              filled: true,
              fillColor: Colors.grey[200],
              isDense: true,
            ),
            // onChanged: (value) {
            // },
          ),
        ),
      ),
    );
  }
}
