import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/button/button.dart';
import 'package:fim_app/package/localizations/str_manager.dart';
import 'package:fim_app/package/pb/connect.ext.pb.dart' as pb;
import 'package:fim_app/package/pb/logic.ext.pb.dart' as pb;
import 'package:fim_app/custom/route/CustomRoute.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/custom/wdiget/user_icon_widget.dart';
import 'package:fim_app/pages/chat/chat_page.dart';
import 'package:fim_app/pages/friend/verify_friend_page.dart';
import 'package:fim_app/model/friend/friend_scoped.dart';
import 'package:fim_app/model/user/user_model.dart';
import 'package:flutter/material.dart';
import 'package:one_context/one_context.dart';

//用户信息
class UserProfilePage extends StatefulWidget {
  final pb.User user;
  const UserProfilePage(this.user, {Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _UserProfilePage();
  }

  void onSendChat() {}
}

class _UserProfilePage extends State<UserProfilePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black54,
      appBar: CustomAppbar(
        isBackIcon: true,
        isLineshade: false,
        backIconColor: Colors.white,
      ),
      body: Stack(
        alignment: Alignment.topCenter,
        children: [
          _buildBack(),
          Positioned(
              top: ScreenUtil.toHeight(180), left: ScreenUtil.toWidth(35), child: _buildUserIcon()),
          Positioned(top: SUT_H(240), right: SUT_W(35), child: _buileAddButton()),
          Positioned(
            top: SUT_H(430),
            child: Container(
              width: SUT_S_WIDTH,
              padding: EdgeInsets.only(left: SUT_W(35)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Divider(height: 1, color: Colors.black87),
                  _buildSign(),
                  const Divider(height: 1, color: Colors.black87),
                  // _buildTime(),
                  // Divider(height: 1, color: Colors.black54),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  //背景
  Widget _buildBack() {
    double circular = ScreenUtil.toWidth(25);
    return Container(
      margin: EdgeInsets.only(top: ScreenUtil.toHeight(220)),
      child: Card(
        child: Container(
          margin: const EdgeInsets.all(0),
        ),
        color: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(circular),
              topRight: Radius.circular(circular),
              bottomLeft: Radius.zero,
              bottomRight: Radius.zero),
        ),
      ),
    );
  }

  /// 用户头像
  Widget _buildUserIcon() {
    return Center(
      // width: ScreenUtil.toWidth(500),
      // height: ScreenUtil.toHeight(200),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          UserIconWidget(widget.user.avatarUrl, size: SUT_W(120), circular: 50, frame: true),
          SizedBox(
            height: ScreenUtil.toHeight(15),
          ),
          Text.rich(
            TextSpan(
              children: [
                TextSpan(
                  text: widget.user.nickname,
                  style: TextStyle(
                      fontSize: ScreenUtil.toSP(28),
                      color: Colors.black87,
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
          SizedBox(
            height: ScreenUtil.toHeight(15),
          ),
          Text.rich(
            TextSpan(
              children: [
                TextSpan(
                  text: '${STR(10008)}:', //widget.user.nickname,
                  style: TextStyle(
                      fontSize: ScreenUtil.toSP(20), color: grayColor, fontWeight: FontWeight.bold),
                ),
                TextSpan(
                  text: widget.user.account,
                  style: TextStyle(
                      fontSize: ScreenUtil.toSP(20), color: grayColor, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }

  /// 头像右边按钮
  Widget _buileAddButton() {
    if (widget.user.userId == UserModel.userId) {
      //自己就不显示
      return const Center();
    } else if (FriendScoped.instance.isFriend(widget.user.userId)) {
      //好友就发送消息按钮
      return Button(
        color: defBtnColor,
        child: Text(STR(10163), style: defBtnTextStyle),
        onPressed: () {
          OneContext().pop();
          OneContext().push(
            pageRoute(
              ChatPage(mReceiverId: widget.user.userId, mReceiverType: pb.ReceiverType.RT_USER),
            ),
          );
        },
      );
    } else {
      //添加好友按钮
      return Button(
        color: defBtnColor,
        child: Text(STR(10033), style: defBtnTextStyle),
        onPressed: () {
          OneContext().pop();
          OneContext().push(fadeRoute(VerifyFriendPage(widget.user)));
        },
      );
    }
  }

  ///个性签名
  Widget _buildSign() {
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: SUT_H(25),
      ),
      child: Text.rich(
        TextSpan(
          children: [
            TextSpan(
              text: STR(10009),
              style: TextStyle(
                  fontSize: SUT_SP(26), color: Colors.black87, fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text: ' ~~~',
              style: TextStyle(
                  fontSize: SUT_SP(20), color: Colors.black54, fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }

  // ///上次更新时间
  // Widget _buildTime() {
  //   int milliseconds = widget.user.updateTime.toInt() * 1000;
  //   DateTime updateTime = DateTime.fromMillisecondsSinceEpoch(milliseconds);
  //   print('spch:${milliseconds}, updateTime: ${updateTime.toString()}');
  //   String suptime = updateTime.toString().substring(5, 16);
  //   return Padding(
  //     padding: EdgeInsets.symmetric(vertical: SUT_H(25)),
  //     child: Row(
  //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //       children: [
  //         Text(
  //           STR(10035),
  //           style: TextStyle(color: Colors.black87, fontSize: SUT_SP(26), fontWeight: FontWeight.bold),
  //         ),
  //         Padding(
  //           padding: EdgeInsets.only(right: SUT_W(35)),
  //           child: Text(
  //             suptime,
  //             style: TextStyle(color: Colors.black54, fontSize: SUT_SP(24), fontWeight: FontWeight.bold),
  //           ),
  //         ),
  //       ],
  //     ),
  //   );
  // }
}
