import 'package:fim_app/configs/api.dart';
import 'package:fim_app/configs/const_config.dart';
import 'package:fim_app/custom/app_bar/custom_app_bar.dart';
import 'package:fim_app/package/util/screen_util.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class Debug extends Model {
  static Debug instance = Debug();
  static final List<String> _listDebugs = [];

  static int get count => _listDebugs.length;
  static String at(int index) => _listDebugs[index];

  static final Color d_color = Colors.blue;
  static final Color e_color = Colors.red.shade900;
  static final Color w_color = Colors.yellow.shade700;

  static void d(Object? object) {
    if (object == null) return;
    instance._log(0, 'd:${object.toString()}');
  }

  static void w(Object? object) {
    if (object == null) return;
    instance._log(1, 'w:${object.toString()}');
  }

  static void e(Object? object) {
    if (object == null) return;
    instance._log(2, 'e: error: ${object.toString()}');
  }

  void _log(int type, String log) {
    if (AppConfig.isDebug) {
      if (count >= 100) {
        _listDebugs.removeAt(0);
      }
      _listDebugs.add(log);
      notifyListeners();
    }
    if (1 == type) {
      print('\x1B[33m:$log\x1B[0m');
    } else if (2 == type) {
      print('\x1B[31m:$log\x1B[0m');
    } else {
      print(log);
    }
  }
}

class DebugPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppbar(
        isBackIcon: true,
        title: Text('debug log', style: defTitleTextStyle),
      ),
      body: ScopedModel<Debug>(
        model: Debug.instance,
        child: ScopedModelDescendant<Debug>(
          builder: (context, child, model) {
            return Container(
              padding: EdgeInsets.symmetric(horizontal: SUT_SP(15)),
              // color: Colors.transparent,
              child: ListView.builder(
                reverse: true,
                physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                itemBuilder: (context, index) {
                  double bottom = index == 0 ? 15.sp : 0;
                  String log = Debug.at(Debug.count - index - 1);
                  String logType = log.substring(0, 1);
                  // log = log.substring(2);
                  Color logColor = Debug.d_color;
                  if (logType == 'e')
                    logColor = Debug.e_color;
                  else if (logType == 'w') logColor = Debug.w_color;

                  return Padding(
                    padding: EdgeInsets.only(top: 15.sp, bottom: bottom),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            log,
                            style: TextStyle(
                              fontSize: defFontSize,
                              color: logColor,
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                },
                itemCount: Debug.count,
              ),
            );
          },
        ),
      ),
    );
  }
}
