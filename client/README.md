# fim_app

A new Flutter project.
fvm flutter version 2.8.1
flutter配置官网有详细文档

网络接口
	grcp 具体配置文档参见官网

ExcelStringDart 项目
	多语言xls转换工具源码

项目结构
	android：安卓项目文件
	ios：项目文件
	assets： 资源文件位置
	lib： dart源代码
	{
		configs：
		{
			api.dart 网络ip端口 配置
			...
		},
		custom: 自定义通用Widget
		package：socket，多语言，grcpdart接口，本地存储工具等
		pages：界面源代码
		scoped：数据管理
	}
	proto：grcp 配置文件
	StringSLX：多语言xls转换工具
	build_apk.dat 打包安卓批处理文件
	
